#!/usr/bin/python


import shutil, os


class powrzInit:

	def __init__(self):

		try:
			print "Checking data folders exist"
			self.checkfolderexists("/root/data")
			self.checkfolderexists("/root/data/conf")
			self.checkfolderexists("/root/data/opportunity")
			self.checkfolderexists("/root/data/monthly-data")
		except Exception, e:
			print 'There was an error initializing data directories: ', e

		try:
			print "Check that the demand shedding file exists"
			self.copydefaultfiles("/root/data/conf/demand_response.conf","/root/powrz/scripts/demand_response_default.conf")
		except Exception, e:
			print 'There was an error initializing data directories: ', e

	# End DEF


	def checkfolderexists(self,dirtocheck):
		if not os.path.exists(dirtocheck):
			os.makedirs(dirtocheck)
			return False
		return True

	# End DEF


	def copydefaultfiles(self,filetocheck,source):
		if not os.path.exists(filetocheck):
			copyfile(source, filetocheck)

# End Class	


powrzInit = powrzInit()
