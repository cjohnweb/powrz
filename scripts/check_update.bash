#!/bin/bash

cd /root/powrz

python /root/heartbeat/heartbeat.py "Exec: /root/powrz/scripts/check_update.bash"

if [ ! -f /root/data/conf/measurz.gitport.conf ]; then
	git remote set-url origin https://powrz.update:Topsy23Kretts@heartbeat.measurz.net:5555/powrz/powrz.git
else
	PORT=`cat /root/data/conf/measurz.gitport.conf`

	git remote set-url origin https://powrz.update:Topsy23Kretts@heartbeat.measurz.net:$PORT/powrz/powrz.git
fi

git reset --hard origin/master
git pull

if [ ! -f /root/data/init.hash ]; then
        echo "" > /root/data/init.hash
fi

hash="$(sha1sum /root/powrz/init.bash)"

lasthash="$(cat /root/data/init.hash)"


if [ "${lasthash}" != "${hash}" ]
then

	python /root/heartbeat/heartbeat.py "Notice: Hashes different, running init.bash"
        # Installs programs, services, modifies files, init measurz, etc
        cd /root/powrz
        bash /root/powrz/init.bash 
        echo "$hash" > /root/data/init.hash

	python /root/heartbeat/heartbeat.py "Notice: init.bash Updates Installed"

else


	python /root/heartbeat/heartbeat.py "Notice: No init.bash updates needed"


fi

# Update heartbeat
#cd /root/heartbeat
#bash /root/heartbeat/update.bash


