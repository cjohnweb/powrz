#!/usr/bin/python

import Adafruit_BBIO.UART as UART
UART.setup("UART1")

import subprocess, serial, pickle
import random, redis, json, sys, os
from datetime import datetime
from time import sleep, time
from struct import *
import traceback

settings = dict()
settings['device_id'] = "0x01" # The RS485 ID of the Acuvim, usually 0x01
settings['read_delay'] = 0.08 #in seconds, 0.1 = 100 milliseconds
settings['com_device'] = '/dev/ttyO1'
settings['baud'] = 19200 #baud is set per device, default for power meter is 19200, for inverter it's 38400
settings['register_offset'] = 8

# * * * * 
def command(dembytes):
	if("split" not in dir(dembytes)):
		print 'DEMBYTTES: ',dembytes
		#return

	resp = dembytes.split()
	if(len(resp)<4):
		return "no valid command present"	
	series = []
	bbbb = 'BB' #2 for the checksum
	id = None
	for _byte in resp:
		if(id==None):
			id = _byte
		series.append(int(_byte, 16))
		bbbb = bbbb + 'B' #one B for every B
	com = serial.Serial('/dev/ttyO1','19200')
	checksum = computeCRC( series )
	series.append( checksum[0] )
	series.append( checksum[1] )
	packed = pack(bbbb, *series)
	com.write( packed )
	sleep(1) # If you remove this line, it will cause exception in get_meter_data <type 'exceptions.IndexError'> list index out of range
	data = " ".join(hex(ord(n)) for n in com.read(com.inWaiting()))
	
	# I should put my own exception here to test if data is coming back - to determine if the acuvim is even responding...
	
	# If you want to test serial data...
	print "sent:"
	print resp
	print ""
	
	print "received:"
	print data
	print ""
	
	return data		
# end def

# * * * * 
def computeCRC(data):
	crc = 0xffff
	for a in data:
		if isinstance(a , basestring):
			a = int(a, 16)
		crc ^= a
		for i in range(0, 8): 
			if crc & 0x0001:
				crc >>= 1
				crc ^= 0xA001
			else:
				crc >>= 1 
	return [(crc & 0x00FF),(crc>>8)]
# end def


print command("0x01 0x10 0x10 0x1d 0x00 0x01 0x02 0x00 0x01"):