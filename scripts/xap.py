import io 
import serial
import time
import struct
from struct import *

#************************
def computeCRC(data):
	crc = 0xffff
	for a in data:
		if isinstance(a , basestring):
			a = int(a, 16)
		crc ^= a
		for i in range(0, 8): 
			if crc & 0x0001:
				crc >>= 1
				crc ^= 0xA001
			else:
				crc >>= 1 
	return [(crc & 0x00FF),(crc>>8)]

# end def
#************************

#************************
def command(data):
	resp = data.split()
	series = []
	bbbb = 'BB' #2 for the checksum
	for _byte in resp:
		series.append(int(_byte, 16))
		bbbb = bbbb + 'B' #one B for every B
	
	checksum = computeCRC( series )
	#print checksum[0]
	#print checksum[1]
	
	series.append( checksum[0] )
	series.append( checksum[1] )
	
	packed = pack(bbbb, *series)
	print series
	for _point in series:
		print hex(_point)
	
	com.write( packed )
	time.sleep(.1)
	data = " ".join(hex(ord(n)) for n in com.read(com.inWaiting()))
	#print "com response:"
	#print data
	return data
# end def
#************************

'''

if  your baud is set to 9600 for some reason [the documentation indicates the default baud *should be* 19200]
then run through this to correct that. 9600 is too slow for current SGB3P code rev.

'''


#Read Timer
com = serial.Serial('/dev/ttyO1', 9600) 
inv_command = "0x01 0x03 0x10 0x40 0x00 0x06"
data = command(inv_command)
print "Ensure timer is being read correctly @ 9600"
print 'Data: ',data 

#basic paramater mode to 'pre' - this is the magical "make it report the actual value" button
com = serial.Serial('/dev/ttyO1', 9600)
inv_command = "0x01 0x10 0x10 0x1d 0x00 0x01 0x02 0x00 0x01"
data = command(inv_command)
print "Magic setting for accurate values"
print 'Data: ',data, "poof accurate data... perhaps"

#set baud to 19200
com = serial.Serial('/dev/ttyO1', 9600)
inv_command = "0x01 0x10 0x10 0x02 0x00 0x01 0x02 0x4b 0x00"
data = command(inv_command)
print "Set baud to 19200"
print 'Data: ',data

#Read Timer at new, more better baud rate
com = serial.Serial('/dev/ttyO1', 19200)
inv_command = "0x01 0x03 0x10 0x40 0x00 0x06"
print "Ensure timer is being read correctly @ 19200"
data = command(inv_command)
print 'Data: ',data 



###
#  additionally don't forget to adjust the basic parameter mode setting - or device will report "off" values
###
#basic paramater mode to 'pre' - this is the magical "make it report the actual value" button
com = serial.Serial('/dev/ttyO1', 19200)
inv_command = "0x01 0x10 0x10 0x1d 0x00 0x01 0x02 0x00 0x01"
data = command(inv_command)
print 'Data: ',data 

print "Magic setting for accurate values"
print 'Data: ',data
print "poof accurate data... perhaps"




