#!/bin/bash

cd /root/heartbeat/

# ----- /root/data folder
if [ -d /root/data ];
then
   echo -en 'Data Folder Exists (/root/data/)\n'
else
   echo -en 'Creating Data Folder (/root/data/)\n'
   mkdir /root/data
fi


# ----- /root/data/conf folder
if [ -d /root/data/conf ];
then
   echo -en 'Opportunity Exists (/root/data/conf)\n'
else
   echo -en 'Creating Opportunity Folder (/root/data/conf)\n'
   mkdir /root/data/conf
fi


# ----- /root/data/monthly-data/
if [ -d /root/data/monthly-data/ ];
then
   echo -en 'Monthly Data Folder Exists (/root/data/monthly-data/)\n'
else
   echo -en 'Creating Monthly Data Folder (/root/data/monthly-data/)\n'
   mkdir /root/data/monthly-data/
fi


# ----- /root/data/opportunity folder
if [ -d /root/data/opportunity ];
then
   echo -en 'Opportunity Exists (/root/data/opportunity)\n'
else
   echo -en 'Creating Opportunity Folder (/root/data/opportunity)\n'
   mkdir /root/data/opportunity
fi

date=$(date +"%Y%m%d%H%M")

# ----- Move Opportunity
if [ -a "/root/data/opportunity.txt" ]
then
	mv "/root/data/opportunity.txt" "/root/data/opportunity/opportunity ${date}.txt"
else
	echo -en "Opportunity does not exist\n"
fi


# Other things that should be done on boot
cd /root/powrz/scripts/
python /root/powrz/scripts/boot.py


# ----- Enable UART-1
cd /root/powrz/scripts/
python /root/powrz/scripts/enable-uart1.py


# ----- Start Node
echo -en "Starting Node\n"
nohup /usr/local/bin/forever start -c /usr/bin/node /root/powrz/interface/app.js >/dev/null 2>&1 &


# ----- Start Python
echo -en "Starting SGB3P\n"
cd /root/powrz/backend/
nohup python /root/powrz/backend/main.py >/dev/null 2>&1 &
