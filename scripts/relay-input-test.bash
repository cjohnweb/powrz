#!/bin/bash

GPIO_PATH=/sys/class/gpio
PIN_EXPORT_PATH=$GPIO_PATH/export
PIN_UNEXPORT_PATH=$GPIO_PATH/unexport

# Turn these pins off
echo 26 > $PIN_UNEXPORT_PATH
echo 23 > $PIN_UNEXPORT_PATH
echo 31 > $PIN_UNEXPORT_PATH
echo 50 > $PIN_UNEXPORT_PATH

# Turn these pins on
echo 26 > $PIN_EXPORT_PATH
echo 23 > $PIN_EXPORT_PATH
echo 31 > $PIN_EXPORT_PATH
echo 50 > $PIN_EXPORT_PATH

# Turn these pins to output pins
echo "out" > $GPIO_PATH/gpio26/direction
echo "out" > $GPIO_PATH/gpio23/direction
echo "out" > $GPIO_PATH/gpio31/direction
echo "out" > $GPIO_PATH/gpio50/direction

echo -en 'Turning on in 3...'
sleep 1
echo -en "2...."
sleep 1
echo -en "1...."
sleep 1
echo -en "\n\n#*************************************************************************************#\n\n"

echo -en '\n\nTurning Relays on now...\n\n'

echo 'Relay 1 on'
echo 1 > $GPIO_PATH/gpio26/value
sleep 1

echo 'Relay 2 on'
echo 1 > $GPIO_PATH/gpio23/value
sleep 1

echo 'Relay 3 on'
echo 1 > $GPIO_PATH/gpio31/value
sleep 1

echo 'Relay 4 on'
echo 1 > $GPIO_PATH/gpio50/value
sleep 1

echo -en "\n\nTurning Relays off now...\n\n"

echo 'Relay 1 off'
echo 0 > $GPIO_PATH/gpio26/value
sleep 1

echo 'Relay 2 off'
echo 0 > $GPIO_PATH/gpio23/value
sleep 1

echo 'Relay 3 off'
echo 0 > $GPIO_PATH/gpio31/value
sleep 1

echo 'Relay 4 off'
echo 0 > $GPIO_PATH/gpio50/value

echo 'All done testing relays!'


