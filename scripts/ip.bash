#!/bin/bash

ip=$(hostname -I)
# The script cant get the mac address when run from cron for some reason
#mac=$(ifconfig eth0 | grep -o -E '([[:xdigit:]]{1,2}:){5}[[:xdigit:]]{1,2}')
# Use this instead:

mac=$(cat /sys/class/net/eth0/address)
hostname=$(hostname)
DATE=$(date +%s)

#echo -en "http://192.168.1.90/actions.php?data=$ip;$mac;$hostname;$DATE"

content=$(wget "http://192.168.1.90/actions.php?data=$ip;$mac;$hostname;$DATE" -q -O -)

echo -en "\nResponse: $content\n\n"

cmd=${content:0:7}
value=${content:7}

if [ "$cmd" = "delete-" ]
then

	# Do something

fi

exit 1

