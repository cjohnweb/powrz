#!/usr/bin/python

import sys, redis


try:
	cmd = sys.argv[1]
except:
	print 'Requires a valid Parameter'
	print 'Usage: cmd.py CMD\n'
	print "Commands: \n\n\tGPRE\n\tSEC\n\tPRE\n\tCD\n\tCTDefaults\n\tCD1+\n\tCD2+\n\tCD3+\n\tCD1-\n\tCD2-\n\tCD3-\n\tCT2500\n\tCT2400\n\tCT1200\n\tCT1000\n\tCT800\n\tCT600\n\tCT300\n\tCT2100\n\tCT25\n\t3LN3CT3CT\n\t1LN\n\t1LL\n\t2LL\n\t3LL\n\t3LN\n\t3N\n\t1T\n\t2T\n\t3CT\n\tRDPWR\n\tRS\n\tON\n\tOFF\n\tBAUD\n\tEC\n\tDC\n\tRC\n\tIC\n"
	exit()


r = redis.Redis('localhost')
key='acuvim-cmds-filter'

p = r.rpush(key,cmd)

print 'PUSHING ',cmd,': Result', p

