#!/bin/bash

process_id=`ps aux | grep "main.py" | grep -v "grep"`;

if [ -z "$process_id" ]; then

    echo "process not running for certain."

    DATE=$(date)
    python /root/heartbeat/heartbeat.py "Notice: main.py process was down, starting again"

    cd /root/powrz/
    python /root/powrz/backend/main.py &

else 

    echo "main.py seems to be running";

fi

