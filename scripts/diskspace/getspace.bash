#!/bin/bash

# Run out of the correct directory
cd /root/powrz/scripts/diskspace

# Install PHP
apt-get install php5-common php5-cli

# Get all the file sizes
du -Ph / > /root/powrz/scripts/diskspace/dulog

# Now show me everything larger than 10MB
php processdulog.php
