#!/usr/bin/python

import Adafruit_BBIO.UART as UART
import Adafruit_BBIO.GPIO as GPIO
import sys, os, io
from datetime import datetime
from time import sleep, time
from struct import *
import traceback

# Nice ASCII Labels at http://patorjk.com/software/taag/#p=display&f=Graffiti&t=Local%20format

class control:

	# * * * * 
	def __init__(self):

		# Clear the screen off
		clear = lambda: os.system('clear')      # or os.system('clear') for Unix
		clear()

		a = os.system("kill `ps -eo pid,command | grep main.py | grep -v grep | awk '{print $1}'` &")	

		print "Powrz Relay Test Script By John Minton <john.minton@jlmei.com>\n\n"

		'''
__________       .__                             /\    __________________.___________   
\______   \ ____ |  | _____  ___.__. ______     / /   /  _____/\______   \   \_____  \  
 |       _// __ \|  | \__  \<   |  |/  ___/    / /   /   \  ___ |     ___/   |/   |   \ 
 |    |   \  ___/|  |__/ __ \\___  |\___ \    / /    \    \_\  \|    |   |   /    |    \
 |____|_  /\___  >____(____  / ____/____  >  / /      \______  /|____|   |___\_______  /
        \/     \/          \/\/         \/   \/              \/                      \/ 
		'''

		try:
			# Relays for Demand Shedding, key'd for relay id...relay_1 = K1
			self.relaygpio = dict()
			self.relaygpio['1'] = "P8_14"    #Relay 1 is p8/14, k1
			self.relaygpio['2'] = "P8_13"    #Relay 2 is p8/13, k2 
			self.relaygpio['3'] = "P9_13"    #Relay 3 is p9/13, k3
			self.relaygpio['4'] = "P9_14"    #Relay 4 is p9/14, k4

			# Set relay pins as outputs
			for pin in self.relaygpio:
				print 'Setting up GPIO Pin ', self.relaygpio[pin], ' as output/off'
				GPIO.setup(self.relaygpio[pin], GPIO.OUT)
				GPIO.output(self.relaygpio[pin], GPIO.LOW)
			print '\n\n'
		except Exception, e:
			print 'Error initilizing Demand Shaving / GPIO pins: ', e

		'''
               .__                 _________ __          __          
_______   ____ |  | _____  ___.__./   _____//  |______ _/  |_  ____  
\_  __ \_/ __ \|  | \__  \<   |  |\_____  \\   __\__  \\   __\/ __ \ 
 |  | \/\  ___/|  |__/ __ \\___  |/        \|  |  / __ \|  | \  ___/ 
 |__|    \___  >____(____  / ____/_______  /|__| (____  /__|  \___  >
             \/          \/\/            \/           \/          \/ 
		'''

		# This is the default dict for demand shedding logic / tracking - Key'd for priority
		self.relayState = {
			'1':{"state":"off"},
			'2':{"state":"off"},
			'3':{"state":"off"},
			'4':{"state":"off"}
		}

		# Main Program Loop
		while True:

			# Set Relays output to default on state
			for rid in range(1,5):
				rid = str(rid)
				print 'Relay ', rid, ' - ',self.relayState[rid]['state']

			print '\n\n'
						
			self.ask()
			
			print '\n\n'
				
		# End While True
			
	# End Def		

	def ask(self):
		
		var = raw_input("Enter a Relay ID (1-4) and press Enter (CTRL+c to quit): ")
		
		try:
			var = int(var)
		except Exception, e:
			print "Please enter a valid Interger, 1-4. \n"
			return

		if var > 0 and var <= 4:
			var = str(var)
			
			if self.relayState[var]['state'] == "off":
				self.relayState[var]['state'] = "on"
				GPIO.output(self.relaygpio[var], GPIO.HIGH)

			else:
				self.relayState[var]['state'] = "off"
				GPIO.output(self.relaygpio[var], GPIO.LOW)
		else:
			print "Invalid Entry, only enter 1 - 4.\n\n"

# End Class


'''
___________                            __             __  .__             _________ .__                        
\_   _____/__  ___ ____   ____  __ ___/  |_  ____   _/  |_|  |__   ____   \_   ___ \|  | _____    ______ ______
 |    __)_\  \/  // __ \_/ ___\|  |  \   __\/ __ \  \   __\  |  \_/ __ \  /    \  \/|  | \__  \  /  ___//  ___/
 |        \>    <\  ___/\  \___|  |  /|  | \  ___/   |  | |   Y  \  ___/  \     \___|  |__/ __ \_\___ \ \___ \ 
/_______  /__/\_ \\___  >\___  >____/ |__|  \___  >  |__| |___|  /\___  >  \______  /____(____  /____  >____  >
        \/      \/    \/     \/                 \/             \/     \/          \/          \/     \/     \/ 
'''


# * * * * * * * * * * *
control = control() # *
# * * * * * * * * * * *
