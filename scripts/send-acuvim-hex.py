#!/usr/bin/python
#!/usr/bin/python

import Adafruit_BBIO.UART as UART
import Adafruit_BBIO.GPIO as GPIO
import subprocess, serial, pickle
import redis, json, sys, os, io
from datetime import datetime
from time import sleep, time
from pprint import pprint
from struct import *
import traceback
#import wind

'''
# Not pushing to Redis
r = redis.Redis('localhost')
key='acuvim-cmds'
p = r.rpush(key,cmd)
print 'PUSHING ',cmd,': Result', p
'''



class acuvimcmd:

	# * * * * 
	def __init__(self):
		
		self.data = dict()
		
		try:
			cmds = sys.argv[1]
		except:
			print '\n\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\n'
			print 'Requires a valid Parameter'
			print 'Usage: cmd.py CMD\n'
			print 'Commands: [ 0x01 0x-- 0x-- ] (Send hex cmd to Acuvim. Do not send CRC at end of string)'
			print '\n\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\n'
			exit()

		self.settings = {}		
		self.settings['device_id'] = "0x01" # The RS485 ID of the Acuvim, usually 0x01
		self.settings['read_delay'] = 0.08 #in seconds, 0.1 = 100 milliseconds
		self.settings['com_device'] = '/dev/ttyO1'
		self.settings['baud'] = 19200 #baud is set per device, default for power meter is 19200, for inverter it's 38400
		self.settings['register_offset'] = 8
	
		self.power_meter_commands = {
			'pfactor' : ' 0x03 0x40 0x34 0x00 0x08', # power_factor
			'phase' : ' 0x03 0x40 0x00 0x00 0x30', # phase and system power
			'realtime': ' 0x03 0x40 0x48 0x00 0x12', # energy summaries from meter
			'parameter_phases': ' 0x03 0x10 0x03 0x00 0x12' # CT values, etc
		}

		try:
			self.settings['com'] = serial.Serial(self.settings['com_device'], self.settings['baud'])
		except Exception, e:
			print 'Could not establish Serial Connection: ', e

		cmds = cmds.split(';')
		for cmd in cmds:
			cmd = cmd.strip()
			print '\nProcessing Acuvim Command: {' + cmd + "} | ", self.command(cmd), '\n' 

		
		got_meter_data = self.get_meter_data()	#
		
		print '\n'
		print 'phase_a_cd \t', self.data['phase_a_cd'], '\n',
		print 'phase_b_cd \t', self.data['phase_b_cd'], '\n',
		print 'phase_c_cd \t', self.data['phase_c_cd'], '\n',
		
		print 'ct1 \t\t', self.data['ct1'], '\n',
		print 'ct2 \t\t', self.data['ct2'], '\n',

		#print ' \t', self.data[''], '\n',
		
		print '\n\n'
		
		
	def command(self, dembytes):
		#if("split" not in dir(dembytes)):
		#	print 'DEMBYTTES: ',dembytes
		#	return
		
		resp = dembytes.split()
		if(len(resp)<4):
			return "no valid command present"	
		series = []
		bbbb = 'BB' #2 for the checksum
		id = None
		for _byte in resp:
			if(id==None):
				id = _byte
			series.append(int(_byte, 16))
			bbbb = bbbb + 'B' #one B for every B
		self.settings['com'] = serial.Serial(self.settings['com_device'], self.settings['baud'])
		checksum = self.computeCRC( series )
		series.append( checksum[0] )
		series.append( checksum[1] )
		packed = pack(bbbb, *series)
		self.settings['com'].write( packed )
		sleep(self.settings['read_delay']) # If you remove this line, it will cause exception in get_meter_data <type 'exceptions.IndexError'> list index out of range
		data = " ".join(hex(ord(n)) for n in self.settings['com'].read(self.settings['com'].inWaiting()))
		
		# If you want to test serial data...
		#print '\n'
		#print "Sent:\t", resp, '\n\n'
		#print "Received:\t", data, '\n\n'
		
		return data		
		# end def
	
	
	# * * * *
	def computeCRC(self,data):
		crc = 0xffff
		for a in data:
			if isinstance(a , basestring):
				a = int(a, 16)
			crc ^= a
			for i in range(0, 8): 
				if crc & 0x0001:
					crc >>= 1
					crc ^= 0xA001
				else:
					crc >>= 1 
		return [(crc & 0x00FF),(crc>>8)]
	# end def
	
	
	# * * * * 
	def ieee_convert(self,_hex):
		_places = 4
		_val = 0.0
		_converted = bin(int(_hex, 16))[2:].zfill(16)
		_binary = str(_converted).rjust(32, "0")
		_sign = 1 if _binary[0] == '0' else -1
		_exp = _binary[1:9]
		_exp = int( _exp, 2)-127
		_exp = 2 ** _exp
		_mantissa = _binary[9:];
		_mantissa = int( _mantissa, 2 );
		_mantissa = 1.0 + (int(_mantissa) / 8388608.0)
		_val = round( (_sign * _exp * _mantissa), _places);
		return _val
	
	# end def


	# * * * * 
	def inc(self): #workaround for python's stupid insistence on not implementing an proper incrementation operator [ no ++i ]  
		self.i += 1
		return self.i
	# end def

	def get_meter_data(self):
		#del self.data
		#self.data = {}
		id = self.settings['device_id']
		self.data['unixtime'] = int(time()) # Get a fresh time stamp :)
		
		try:
			self.data['pfactor'] = self.command( id + ' ' + self.power_meter_commands['pfactor'] ) 	# power_factor
			self.data['phase'] = self.command(id + ' ' + self.power_meter_commands['phase'] ) 		# phase and system power
			self.data['realtime'] = self.command(id + ' ' + self.power_meter_commands['realtime'] ) # energy summaries from meter
			self.data['parameter_phases'] = self.command(id + ' ' + self.power_meter_commands['parameter_phases'] ) # CT values, etc
		except Exception, e:
			print '\tThere was an error getting serial data in get_meter_data: ', e
		
		# Power Factor					
		self.i = self.settings['register_offset']+2 # 1 under to allow the inc routine to place the id where it needs to be
		pfactor = self.data['pfactor'].split(' 0x')
		self.data['phase_a_power_factor'] = pfactor[(self.inc())].zfill(2).replace('0x','').zfill(2) + '' + pfactor[(self.inc())].zfill(2) + '' + pfactor[(self.inc())].zfill(2) + '' + pfactor[(self.inc())].zfill(2)
		self.data['phase_b_power_factor'] = pfactor[(self.inc())].zfill(2) + '' + pfactor[(self.inc())].zfill(2) + '' + pfactor[(self.inc())].zfill(2) + '' + pfactor[(self.inc())].zfill(2)
		self.data['phase_c_power_factor'] = pfactor[(self.inc())].zfill(2) + '' + pfactor[(self.inc())].zfill(2) + '' + pfactor[(self.inc())].zfill(2) + '' + pfactor[(self.inc())].zfill(2)
		self.data['power_factor'] = pfactor[(self.inc())].zfill(2) + '' + pfactor[(self.inc())].zfill(2) + '' + pfactor[(self.inc())].zfill(2) + '' + pfactor[(self.inc())].zfill(2)
	
		self.data['phase_a_power_factor'] = self.ieee_convert(self.data['phase_a_power_factor']);
		self.data['phase_b_power_factor'] = self.ieee_convert(self.data['phase_b_power_factor']);
		self.data['phase_c_power_factor'] = self.ieee_convert(self.data['phase_c_power_factor']);
		self.data['power_factor'] = self.ieee_convert(self.data['power_factor']);
	
		# Phase Power
		self.i = self.settings['register_offset']+2 # 1 under to allow the inc routine to place the id where it needs to be
		phase = self.data['phase'].split(' 0x')
	
		self.data['frequency'] = phase[(self.inc())].zfill(2).replace('0x','').zfill(2) + '' + phase[(self.inc())].zfill(2) + '' + phase[(self.inc())].zfill(2) + '' + phase[(self.inc())].zfill(2) #4000H~4001H Frequency F1 F'] = Rx R
	
		self.data['phase_a_voltage'] = phase[(self.inc())].zfill(2) + '' + phase[(self.inc())].zfill(2) + '' + phase[(self.inc())].zfill(2) + '' + phase[(self.inc())].zfill(2) #4002H~4003H Phase voltage V1 F1 U=Rx(PT1/PT2) R
		self.data['phase_b_voltage'] = phase[(self.inc())].zfill(2) + '' + phase[(self.inc())].zfill(2) + '' + phase[(self.inc())].zfill(2) + '' + phase[(self.inc())].zfill(2) #4004H~4005H Phase voltage V2 F1 U=Rx(PT1/PT2) R
		self.data['phase_c_voltage'] = phase[(self.inc())].zfill(2) + '' + phase[(self.inc())].zfill(2) + '' + phase[(self.inc())].zfill(2) + '' + phase[(self.inc())].zfill(2) #4006H~4007H Phase voltage V3 F1 U=Rx(PT1/PT2) R
		self.data['average_voltage'] = self.ieee_convert(phase[(self.inc())].zfill(2) + '' + phase[(self.inc())].zfill(2) + '' + phase[(self.inc())].zfill(2) + '' + phase[(self.inc())].zfill(2)) #4008H~4009H Average voltage Vavg F1 U=Rx(PT1/PT2) R
	
		self.data['line_a_voltage'] = self.ieee_convert(phase[(self.inc())].zfill(2) + '' + phase[(self.inc())].zfill(2) + '' + phase[(self.inc())].zfill(2) + '' + phase[(self.inc())].zfill(2) )#400aH~400bH Line voltage V12 F1 U=Rx(PT1/PT2) R
		self.data['line_b_voltage'] = self.ieee_convert(phase[(self.inc())].zfill(2) + '' + phase[(self.inc())].zfill(2) + '' + phase[(self.inc())].zfill(2) + '' + phase[(self.inc())].zfill(2) )#400cH~400dH Line voltage V23 F1 U=Rx(PT1/PT2) R
		self.data['line_c_voltage'] = self.ieee_convert(phase[(self.inc())].zfill(2) + '' + phase[(self.inc())].zfill(2) + '' + phase[(self.inc())].zfill(2) + '' + phase[(self.inc())].zfill(2) )#400eH~400fH Line voltage V31 F1 U=Rx(PT1/PT2) R
		self.data['average_line_voltage'] = self.ieee_convert(phase[(self.inc())].zfill(2) + '' + phase[(self.inc())].zfill(2) + '' + phase[(self.inc())].zfill(2) + '' + phase[(self.inc())].zfill(2) )#4010H~4011H Average line voltage Vlavg F1 U=Rx(PT1/PT2) R
	
		self.data['phase_a_current'] = phase[(self.inc())].zfill(2) + '' + phase[(self.inc())].zfill(2) + '' + phase[(self.inc())].zfill(2) + '' + phase[(self.inc())].zfill(2) #4012H~4013H Phase(line)current I1 F1 I=Rx?~W(CT1/CT2) R
		self.data['phase_b_current'] = phase[(self.inc())].zfill(2) + '' + phase[(self.inc())].zfill(2) + '' + phase[(self.inc())].zfill(2) + '' + phase[(self.inc())].zfill(2) #4014H~4015H Phase(line)current I2 F1 I=Rx?~W(CT1/CT2) R
		self.data['phase_c_current'] = phase[(self.inc())].zfill(2) + '' + phase[(self.inc())].zfill(2) + '' + phase[(self.inc())].zfill(2) + '' + phase[(self.inc())].zfill(2) #4016H~4017H Phase(line)current I3 F1 I=Rx?~W(CT1/CT2) R
		self.data['average_current'] = self.ieee_convert(phase[(self.inc())].zfill(2) + '' + phase[(self.inc())].zfill(2) + '' + phase[(self.inc())].zfill(2) + '' + phase[(self.inc())].zfill(2) )#4018H~4019H Average current Iavg F1 I=Rx?~W(CT1/CT2) R
		self.data['neutral_current_in'] = self.ieee_convert(phase[(self.inc())].zfill(2) + '' + phase[(self.inc())].zfill(2) + '' + phase[(self.inc())].zfill(2) + '' + phase[(self.inc())].zfill(2) )#401aH~401bH Neutral current In F1 I=Rx?~W(CT1/CT2) R
	
		self.data['phase_a_power'] = phase[(self.inc())].zfill(2) + '' + phase[(self.inc())].zfill(2) + '' + phase[(self.inc())].zfill(2) + '' + phase[(self.inc())].zfill(2) #401cH~401dH Phase A power Pa F1 P=Rx(PT1/PT2)(CT1/CT2) R
		self.data['phase_b_power'] = phase[(self.inc())].zfill(2) + '' + phase[(self.inc())].zfill(2) + '' + phase[(self.inc())].zfill(2) + '' + phase[(self.inc())].zfill(2) #401eH-401fH Phase B power Pb F1 P=Rx(PT1/PT2)(CT1/CT2) R
		self.data['phase_c_power'] = phase[(self.inc())].zfill(2) + '' + phase[(self.inc())].zfill(2) + '' + phase[(self.inc())].zfill(2) + '' + phase[(self.inc())].zfill(2) #4020H-4021H Phase C power Pc F1 P=Rx(PT1/PT2)(CT1/CT2) R
		self.data['system_power'] = phase[(self.inc())].zfill(2) + '' + phase[(self.inc())].zfill(2) + '' + phase[(self.inc())].zfill(2) + '' + phase[(self.inc())].zfill(2) #4022H-4023H System power Psum F1 P=Rx(PT1/PT2)(CT1/CT2) R       
		
		self.data['frequency'] = self.ieee_convert(self.data['frequency'])
	
		self.data['phase_a_voltage'] = self.ieee_convert(self.data['phase_a_voltage'])
		self.data['phase_b_voltage'] = self.ieee_convert(self.data['phase_b_voltage'])
		self.data['phase_c_voltage'] = self.ieee_convert(self.data['phase_c_voltage'])
	
		self.data['phase_a_current'] = self.ieee_convert(self.data['phase_a_current'])
		self.data['phase_b_current'] = self.ieee_convert(self.data['phase_b_current'])
		self.data['phase_c_current'] = self.ieee_convert(self.data['phase_c_current'])
	
	
		self.data['phase_a_power'] = self.ieee_convert(self.data['phase_a_power'])/1000
		self.data['phase_b_power'] = self.ieee_convert(self.data['phase_b_power'])/1000
		self.data['phase_c_power'] = self.ieee_convert(self.data['phase_c_power'])/1000
		self.data['system_power'] = self.ieee_convert(self.data['system_power'])/1000
	
		calculated_a_power = self.data['phase_a_current']*self.data['phase_a_voltage']
		calculated_b_power = self.data['phase_b_current']*self.data['phase_b_voltage'] 
		calculated_c_power = self.data['phase_c_current']*self.data['phase_c_voltage']
	
		if(self.data['phase_a_power']<0):
			calculated_a_power = calculated_a_power*-1
	
		if(self.data['phase_b_power']<0):
			calculated_b_power = calculated_b_power*-1
	
		if(self.data['phase_c_power']<0):
			calculated_c_power = calculated_c_power*-1
	
		self.data['calculated_a_power'] = calculated_a_power
		self.data['calculated_b_power'] = calculated_b_power
		self.data['calculated_c_power'] = calculated_c_power
		calculated_power = (calculated_a_power+calculated_b_power+calculated_c_power)*.001
		self.data['calculated_power'] = calculated_power
		
		self.data['power'] = self.data['system_power'] 
		
		parameters = self.data['parameter_phases'].split(' 0x')
		self.i = self.settings['register_offset']+2 # 1 under to allow the inc routine to place the id where it needs to be
	
		self.data['voltage_wiring_type'] = int(parameters[(self.inc())].zfill(2) + '' + parameters[(self.inc())].zfill(2), 16)
		self.data['current_wiring_type'] = int(parameters[(self.inc())].zfill(2) + '' + parameters[(self.inc())].zfill(2), 16)
		self.data['pt1_low'] = int(parameters[(self.inc())].zfill(2) + '' + parameters[(self.inc())].zfill(2), 16)
		self.data['pt1_high'] = int(parameters[(self.inc())].zfill(2) + '' + parameters[(self.inc())].zfill(2), 16)
		self.data['pt2'] = int(parameters[(self.inc())].zfill(2) + '' + parameters[(self.inc())].zfill(2), 16)
		self.data['ct1'] = int(parameters[(self.inc())].zfill(2) + '' + parameters[(self.inc())].zfill(2), 16) 
		self.data['ct2'] = int(parameters[(self.inc())].zfill(2) + '' + parameters[(self.inc())].zfill(2), 16)
		self.data['phase_a_cd'] = parameters[41].zfill(2) + '' + parameters[42].zfill(2)
		self.data['phase_b_cd'] = parameters[43].zfill(2) + '' + parameters[44].zfill(2)
		self.data['phase_c_cd'] = parameters[45].zfill(2) + '' + parameters[46].zfill(2)
	
		realtime = self.data['realtime'].split(' 0x')
		self.i = self.settings['register_offset']+2 # 1 under to allow the inc routine to place the id where it needs to be
		energy_imp = realtime[(self.inc())].zfill(2) + '' + realtime[(self.inc())].zfill(2) + '' + realtime[(self.inc())].zfill(2) + '' + realtime[(self.inc())].zfill(2); #4048H-4049H Energy IMP F4/F7 0-999999999 R/W [v/10]
		energy_exp = realtime[(self.inc())].zfill(2) + '' + realtime[(self.inc())].zfill(2) + '' + realtime[(self.inc())].zfill(2) + '' + realtime[(self.inc())].zfill(2); #404aH-404bH Energy EXP F4/F7 0-999999999 R/W [v/10]
		reactive_energy_imp = realtime[(self.inc())].zfill(2) + '' + realtime[(self.inc())].zfill(2) + '' + realtime[(self.inc())].zfill(2) + '' + realtime[(self.inc())].zfill(2); #404cH-404dH Reactive energy IMP F5/F8 0-999999999 R/W [v/1000]
		reactive_energy_exp = realtime[(self.inc())].zfill(2) + '' + realtime[(self.inc())].zfill(2) + '' + realtime[(self.inc())].zfill(2) + '' + realtime[(self.inc())].zfill(2); #404eH-404fH Reactive energy EXP F5/F8 0-999999999 R/W [v/1000]
		energy_total = realtime[(self.inc())].zfill(2) + '' + realtime[(self.inc())].zfill(2) + '' + realtime[(self.inc())].zfill(2) + '' + realtime[(self.inc())].zfill(2); #4050H-4051H Energy TOTAL F4/F7 0-999999999 R/W [v/10]
		energy_net = realtime[(self.inc())].zfill(2) + '' + realtime[(self.inc())].zfill(2) + '' + realtime[(self.inc())].zfill(2) + '' + realtime[(self.inc())].zfill(2); #4052H-4053H Energy NET F4/F7 0-999999999 R/W [v/10]
		reactive_energy_total = realtime[(self.inc())].zfill(2) + '' + realtime[(self.inc())].zfill(2) + '' + realtime[(self.inc())].zfill(2) + '' + realtime[(self.inc())].zfill(2); #4054H-4055H Reactive energy TOTAL F5/F8 0-999999999 R/W
		reactive_energy_net = realtime[(self.inc())].zfill(2) + '' + realtime[(self.inc())].zfill(2) + '' + realtime[(self.inc())].zfill(2) + '' + realtime[(self.inc())].zfill(2); #4056H-4057H Reactive energy NET F5/F8 0-999999999 R/W
		apparent_energy = realtime[(self.inc())].zfill(2) + '' + realtime[(self.inc())].zfill(2) + '' + realtime[(self.inc())].zfill(2) + '' + realtime[(self.inc())].zfill(2); #4058H-4059H Apparent energy F6/F9 0-999999999 R/W
	
		self.data['energy_imp'] = int( energy_imp, 16)
		self.data['energy_exp'] = int( energy_exp, 16)
		self.data['reactive_energy_imp'] = int( reactive_energy_imp, 16)
		self.data['reactive_energy_exp'] = int( reactive_energy_exp, 16)
		self.data['energy_total'] = int( energy_total, 16)
		self.data['energy_net'] = int( energy_net, 16)
		self.data['reactive_energy_total'] = int( reactive_energy_total, 16)
		self.data['reactive_energy_net'] = int( reactive_energy_net, 16)
		self.data['apparent_energy'] = int( apparent_energy, 16)
	
		return True
	# end def	


# * * * * * * * * * * *
acuvimcmd = acuvimcmd() # *
# * * * * * * * * * * *
