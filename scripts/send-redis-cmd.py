#!/usr/bin/python

import sys, redis


try:
	cmd = sys.argv[1]
except:
	print 'Requires a valid Parameter'
	print 'Usage: cmd.py CMD\n'
	print 'Commands:\nGPRE\nSEC\nPRE\nCD\nCD1+ Change Phase A Current Directon +\nCD2+ Change Phase B Current Directon +\nCD3+ Change Phase C Current Directon +\nCD1- Change Phase A Current Directon -\nCD2- Change Phase B Current Directon -\nCD3- Change Phase C Current Directon -\nCT300\nCT600\nCT800\nCT1200\nCT2400\nCT Defaults\n3LN3CT3CT\n2LN3CT3CT\nenergy_clear\ndemand_clear\nruntime_clear\n' 	
	exit()


r = redis.Redis('localhost')
key='acuvim-cmds-filter'

p = r.rpush(key,cmd)

print 'PUSHING ',cmd,': Result', p

