#!/usr/bin/python

import subprocess


print "\n"



try:	
	lip = subprocess.check_output("hostname -I", shell=True) 
	lip = lip.strip()
        print "LIP was detected as: ", lip

except Exception, e:
	lip = ""
        print 'Could not get Local IP: ', e





print "\n"


try:
	p = subprocess.Popen(['dig','+short','myip.opendns.com','@resolver1.opendns.com', ''], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	wan, err = p.communicate()
	wan = wan.strip()
	print "WAN IP: ", wan


except Exception, e:
	wan = ""
        print 'Could not get WAN IP Address: ', e






print "\n"


try:

	f = open("/root/data/lip",'w')
	f.write(lip + " " + wan)
	f.close();
	
	print "Saved LIP to /root/data/lip"
	
except Exception, e:
	print 'Could not write data to file: ', e

print "\n"


