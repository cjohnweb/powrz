#!/usr/bin/python


print "importing redis module"
import redis


print "connecting to redis"
r = redis.Redis('localhost')

print "Saving redis RAM Cache to disk"
r.save()

print "Done...exiting\n\n"
exit()

