#!/bin/bash

# ----- Report that we are booting up
sleep 10
python /root/heartbeat/heartbeat.py "Powered On"
