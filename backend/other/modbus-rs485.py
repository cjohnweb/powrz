#!/usr/bin/python
# encoding=utf8


# Modbus Serial Controller by John Minton (John@PythonJohn.Com) is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.
# View this license specification here: http://creativecommons.org/licenses/by-sa/4.0/
# Permissions beyond the scope of this license may be available at http://PythonJohn.Com/

import subprocess, serial
import sys, os, io, inspect

class modbus_serial_controller:

	# * * * * 
	def __init__(self):

		# Serial Settings
		self.settings['device_id'] = "0x01" # The RS485 ID of the Acuvim, usually 0x01
		self.settings['read_delay'] = 0.1 #in seconds, 0.1 = 100 milliseconds
		self.settings['com_device'] = '/dev/ttyO1'
		self.settings['baud'] = 19200 #baud is set per device, default for power meter is 19200, for inverter it's 38400
		self.settings['register_offset'] = 8

		# Pass your modbus commands here
		# CRC is generated automatically
		# Modbus command to get energy summaries
		modbus_command = " 0x03 0x40 0x48 0x00 0x12"
		
		data = self.command(rid+modbus_command, "true") # Set last arguement to true to see debug data

		print data
	# END def


	# * * * * 
	def command(self, dembytes, debug = None):
		try:
			dataBytes = dembytes.split()
			if(len(dataBytes)<4):
				raise ValueError('String too short to be valid Modbus command')
			series = []
			bbbb = 'BB' # Start with 2 for the checksum. Each "B" in the variable represents a byte of data
			id = None
			for _byte in dataBytes:
				if(id==None):
					id = _byte
				series.append(int(_byte, 16))
				bbbb = bbbb + 'B' # one B for every Byte
			self.settings['com'] = serial.Serial(self.settings['com_device'], self.settings['baud'])
			checksum = self.computeCRC( series )
			#print "Checksum : " + str(checksum)
			series.append( checksum[0] )
			series.append( checksum[1] )
			packed = pack(bbbb, *series)
			#print "Packed: " + str(packed)
			self.settings['com'].write( packed )
			sleep(self.settings['read_delay']) # If you remove this line, it will cause exception in get_meter_data <type 'exceptions.IndexError'> list index out of range
			data = " ".join(hex(ord(n)) for n in self.settings['com'].read(self.settings['com'].inWaiting()))
			
			# If you want to test serial data...
			if(debug == "true"):
				print "Sent:", dataBytes, "\n"
				print "Received:", data, "\n"

			# Standard CRC 16 validation - make sure the data from the serial device is valid.
			checkCRC = self.validateCRC(dataBytes,data)

			if(checkCRC == "true"):
				return data	
			else:
				return "false" # Pass a false or raise an exception...whatever you prefer
				#raise ValueError("Bad CRC from Serial Device")

		except Exception, e:
			print "There was an error in self.command: ", self.lineno(), e
	# END def


	# This is where we want to verify data coming back from the serial device is valid
	# * * * * 
	def validateCRC(self,original,data):
		try:

			# Split string into list
			dataPieces = data.split();      #print "The Data: ", dataPieces

			# Original Modbus command       #print "Original: ", original
			lenOriginal = len(original)+2

			dataCRC = dataPieces[-2:]       #print "Data CRC: ", dataCRC
		
			# Get the data, No CRC, No Original
			dataNoCRC = dataPieces[:-2]
			lenDataNoCRC = len(dataNoCRC)
			lenOriginalSlice = slice(lenOriginal,lenDataNoCRC);
			dataNoCRC = dataNoCRC[lenOriginalSlice]    #print "JUST THE DATA: " + str(dataNoCRC)

			#print "Computing CRC..."

			# Calculate the CRC of the data
			calculatedCRC = self.computeCRC(dataNoCRC)

			calculatedCRCHex = []

			c=0
			for b in calculatedCRC:
				c = c+1
				#print "B"+str(c)+": "+ str(hex(b))
				calculatedCRCHex.append(hex(b))
			#print "Calculate CRC: ", calculatedCRCHex

			dataNoCRCDec = []

			for b in dataNoCRC: 
				b = int(b, 16)
				dataNoCRCDec.append(b)

			#print "dataNoCRCDec: " + str(dataNoCRCDec)

			newDataCRC = []
			c=0
			for b in dataCRC:
				c = c+1
				#print "B"+str(c)+": "+ str(int(b,16))
				newDataCRC.append(int(b,16))

			#print "Calc CRC Dec: " + str(calculatedCRC)
			#print "Data CRC Dec: " + str(newDataCRC)

			#print "Calc CRC Hex: " + str(newCalculatedCRC)
			#print "Data CRC Hex: " + str(dataCRC)

			# Compair the Calculated CRC with the Reported Data CRC
			# Return true or false
			if(str(calculatedCRC) == str(newDataCRC)):
				return "true"
			else:
				print "false"

		except Exception, e:
			print "Exception in ValidateCRC function: ", Exception, e
	# END def




	# This is the CRC function that generates a CRC from the data we pass to it
	# * * * * 
	def computeCRC(self,data):
		#print "Type: " + str(type(data))
		crc = 0xffff
		
		for a in data:
			if isinstance(a , basestring):
				a = int(a, 16)
			crc ^= a
			for i in range(0, 8): 
				if crc & 0x0001:
					crc >>= 1
					crc ^= 0xA001
				else:
					crc >>= 1 
		return [(crc & 0x00FF),(crc>>8)]
	# END def



	# * * * * 
	def ieee_convert(self,_hex):
		_places = 4
		_val = 0.0
		_converted = bin(int(_hex, 16))[2:].zfill(16)
		_binary = str(_converted).rjust(32, "0")
		_sign = 1 if _binary[0] == '0' else -1
		_exp = _binary[1:9]
		_exp = int( _exp, 2)-127
		_exp = 2 ** _exp
		_mantissa = _binary[9:];
		_mantissa = int( _mantissa, 2 );
		_mantissa = 1.0 + (int(_mantissa) / 8388608.0)
		_val = round( (_sign * _exp * _mantissa), _places);
		return _val

	# END def

	def lineno(self):
	    """Returns the current line number in our program."""
	    return inspect.currentframe().f_back.f_lineno
	# END def


# Instantiate the class:
modbus_serial_controller = modbus_serial_controller()



