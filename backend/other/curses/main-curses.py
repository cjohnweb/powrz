#!/usr/bin/python
# encoding=utf8


import Adafruit_BBIO.UART as UART
import Adafruit_BBIO.GPIO as GPIO
import subprocess, serial, pickle
import redis, json, sys, os, io
from datetime import datetime
from time import sleep, time
from pprint import pprint
from struct import *
import wind
import inspect
import shutil
#import traceback # We don't need this atm, we use the self.lineno() def

# For Thread support, supported in Python 2.x but Depricated in Python 3.x, Python 3.x requires threading module or _thread for backwards compatability
import thread

# For web support
import httplib
import urllib
import zlib

# For CLI bsaed GUI
import curses 



class powrz_controller:

	# * * * * 
	def __init__(self):

		self.out = ""
		screen = curses.initscr() 
		curses.noecho() 
		screen.keypad(1)

		self.th = '''
********************************************************************************************************************
            __ __    _____    _____                          _____                                      _       
         __|  |  |  |     |  |   __|___ ___ ___ ___ _ _     |     |___ ___      ___ ___ ___ ___ ___ ___| |_ ___ 
        |  |  |  |__| | | |  |   __|   | -_|  _| . | | |_   |-   -|   |  _|    | . |  _| -_|_ -| -_|   |  _|_ -|
        |_____|_____|_|_|_|  |_____|_|_|___|_| |_  |_  | |  |_____|_|_|___|    |  _|_| |___|___|___|_|_|_| |___|
                                               |___|___|_|                     |_|              
                             _______  _______  _     _  ______    _______        __   __  ____  
                            |       ||       || | _ | ||    _ |  |       |      |  | |  ||    | 
                            |    _  ||   _   || || || ||   | ||  |____   |      |  |_|  | |   | 
                            |   |_| ||  | |  ||       ||   |_||_  ____|  |      |       | |   | 
                            |    ___||  |_|  ||       ||    __  || ______|      |       | |   | 
                            |   |    |       ||   _   ||   |  | || |_____        |     |  |   | 
                            |___|    |_______||__| |__||___|  |_||_______|        |___|   |___| 
					                                                                            
					| JLM Energy Inc, Powrz revision 0.0.0.1 |
					|                                        |
					|   John Minton: john.minton@jlmei.com   |
					|   JR Chew: jr.chew@jlmei.com           |
					|   March 2015                           |

	System Init... Please wait.

		'''

		screen.addstr(0,0, self.th)
		screen.refresh()

		sleep(1);

		self.connect_redis()
		self.init_variables()
		self.init_relays_gpio()
		self.init_serial_conn()

		'''
		______________ ______________ .____    ________   ________ __________ 
		\__    ___/   |   \_   _____/ |    |   \_____  \  \_____  \\______   \
		  |    | /    ~    \    __)_  |    |    /   |   \  /   |   \|     ___/
		  |    | \    Y    /        \ |    |___/    |    \/    |    \    |    
		  |____|  \___|_  /_______  / |_______ \_______  /\_______  /____|    
		                \/        \/          \/       \/         \/          
		'''

		# Main Program Loop
		while True:

			try: #(Last Resort exception)
			
				self.out = ""
				self.pad = "This is the pad line\n#2"
				self.startTime = float(time())
				self.data = dict()
				self.data['device'] = self.settings['hostname']
				self.data['type'] = 'secondinterval'

				# Local IP - read from file
				self.read_lip_info()
				
				# Process all the redis acuvim notations, convert to acuvim hex codes and stuff back into redis 'acuvim cmds' key to be processed by "process_acuvim_cmds"
				self.notation_to_hex()
				
				# Take hex from redis and send to acuvim via serial connection
				self.process_acuvim_cmds()

				try:
					self.get_meter_data()
					if self.acuvim ==False:
						self.out += "Could not reach the Acuvim. Is it connected correctly?\n"
 				except Exception, e:
					self.out += "There was an error communicating with the Acuvim: \n"#, self.lineno(), e

				try:
					self.out += "\n\t\t\t\t\tMeter Data\n"
					self.out += "~" * 100
					self.out += "\n"

					for key in self.data:
						self.out += "\t" + str(key) + ": " + str(self.data[key]) + "\n"

				except Exception, e:
					self.out += "There was an error printing the measurz data to the screen: "#, self.lineno(), e

				self.out += "\n"

				# Get the windspeeds
				#self.get_windspeeds()

				# Get Demand Data and perform the relay control
				self.demand_shedding()

				# Send Data to Measurz as JSON string
				self.web_comms(json.dumps(self.data))

				# Copy data, one for local one for measurz
				measurzdata = json.dumps(self.data)

				# Saving revolving data to Redis for local interface
				self.SaveRevolvingSecondDataRedis(measurzdata)

				# Save Opportunity Data in the event of power outage
				self.opportunity(measurzdata)
			
				# Loop Timing
				self.loop_timing()

			except Exception, e:
				self.out += 'Last Resort: There was en error somewhere in the script that was not caught by an exception: '#, self.lineno(), e

			screen.addstr(0,0, self.out)

			screen.clrtobot()

			screen.refresh()

		# End While True
		
		curses.endwin()

	# End Def		


	def connect_redis(self): 

		try:
			self.r = redis.Redis('localhost')
			ping = self.r.ping()
			if ping == True:
				self.out += "Connected to Redis Successfull!\n"
		except Exception, e:
				self.out += 'Could not connect to Redis: '#, self.lineno(), e
				sleep(1)
				self.connect_redis()
	# End DEF


	def init_variables(self):

		try:
			# Setup the settings array
			self.settings = {}
			self.targetTime = 1

			# - - -                          Time and Date Settings                       - - - - #
			self.settings['month'] = datetime.now().strftime("%m")
			self.settings['year'] = datetime.now().year

			# - - -                               Files and Paths                         - - - - #
			self.settings['dir'] = "/root/powrz/backend/"
			self.settings['datadir'] = '/root/data/'
			self.settings['opportunity'] = self.settings['datadir']+"opportunity.txt"
			self.settings['opportunity_folder'] = self.settings['datadir']+"opportunity/"
			self.settings['demand_shedding_conf'] = self.settings['datadir']+"conf/demand_response.conf"
			self.settings['monthly_data_dir'] = self.settings['datadir'] + 'monthly-data/'
			self.settings['opportunity_limit'] = 30

			# - - -                          Measurz Server Address                       - - - - #
			self.measurz_address = 'm2.measurz.net'				# Measurz domainname 
			self.measurz_url = '/sgb3p.io4/'                 	# Measurz URL path with prefixed and suffixed slashes 
			self.measurz_port = 8080                    		# Measurz data collection port
			self.connection_timeout = 2
			
			# - - -                               Serial Settings                         - - - - #
			self.settings['device_id'] = "0x01" # The RS485 ID of the Acuvim, usually 0x01
			self.settings['read_delay'] = 0.08 #in seconds, 0.1 = 100 milliseconds
			self.settings['com_device'] = '/dev/ttyO1'
			self.settings['baud'] = 19200 #baud is set per device, default for power meter is 19200, for inverter it's 38400
			self.settings['register_offset'] = 8

			# - - -                                   Redis Keys                          - - - - #
			self.settings['redis-log'] = 'log'
			self.settings['redis-failed-data'] = 'failed-data'
			self.settings['redis-revolving-data'] = 'revolving-data'
			self.settings['redis-acuvim-cmds-node'] = 'acuvim-cmds-filter' #list type
			self.settings['redis-acuvim-cmds'] = 'acuvim-cmds' #list type
			self.settings['redis-15-min-cron-last-run'] = '15-min-cron-last-run'
			self.settings['redis-disk-space'] = "disk-space"
			self.settings['redis-disk-space'] = "disk-space"
			self.settings['size_of_revolving_data'] = 1800
			self.settings['redis-failed-limit'] = 100
		
			# - - -                               Acuvim Commands                         - - - - #
			self.settings['acuvim-cmds'] = {
				'GPRE':'0x01 0x03 0x10 0x1d 0x00 0x01',
				'SEC':'0x01 0x10 0x10 0x1d 0x00 0x01 0x02 0x00 0x01',
				'PRE':'0x01 0x10 0x10 0x1d 0x00 0x01 0x02 0x00 0x01',
				'CD':'0x01 0x03 0x10 0x12 0x00 0x03',
				
				'CD1+':'0x01 0x10 0x10 0x12 0x00 0x01 0x02 0x00 0x00',
				'CD2+':'0x01 0x10 0x10 0x13 0x00 0x01 0x02 0x00 0x00',
				'CD3+':'0x01 0x10 0x10 0x14 0x00 0x01 0x02 0x00 0x00',
				
				'CD1-':'0x01 0x10 0x10 0x12 0x00 0x01 0x02 0x00 0x01',
				'CD2-':'0x01 0x10 0x10 0x13 0x00 0x01 0x02 0x00 0x01',
				'CD3-':'0x01 0x10 0x10 0x14 0x00 0x01 0x02 0x00 0x01',
				
				'CT300':'0x01 0x10 0x10 0x08 0x00 0x02 0x04 0x01 0x2c 0x00 0x05',
				'CT600':'0x01 0x10 0x10 0x08 0x00 0x02 0x04 0x02 0x58 0x00 0x05',
				'CT800':'0x01 0x10 0x10 0x08 0x00 0x02 0x04 0x03 0x20 0x00 0x05',
				'CT1200':'0x01 0x10 0x10 0x08 0x00 0x02 0x04 0x04 0xb0 0x00 0x05',
				'CT2400':'0x01 0x10 0x10 0x08 0x00 0x02 0x04 0x09 0x60 0x00 0x05',
				'CT Defaults':'0x01 0x10 0x10 0x08 0x00 0x02 0x04 0x04 0xb0 0x00 0x05',
				'3LN3CT3CT':'0x01 0x10 0x10 0x08 0x00 0x02 0x04 0x04 0xb0 0x00 0x05',
				#'1LL2CT':   '0x01 0x10 0x-- 0x-- 0x00 ',
				#'1LL2CT':'0x01 0x10 0x10 0x08 0x00 0x02 0x04 0x04 0xb0 0x00 0x05',
				'energy_clear':'0x01 0x10 0x10 0x16 0x00 0x02 0x01 0x00 0x01',
				'demand_clear':'0x01 0x10 0x10 0x0f 0x00 0x02 0x01 0x00 0x01',
				'runtime_clear':'0x01 0x10 0x10 0x11 0x00 0x02 0x01 0x00 0x01',
				'imp_clear':'0x01 0x10 0x40 0x48 0x00 0x02 0x04 0x00 0x00 0x00 0x00'
			}

			self.power_meter_commands = {
				'pfactor' : ' 0x03 0x40 0x34 0x00 0x08', # power_factor
				'phase' : ' 0x03 0x40 0x00 0x00 0x30', # phase and system power
				'realtime': ' 0x03 0x40 0x48 0x00 0x12', # energy summaries from meter
				'parameter_phases': ' 0x03 0x10 0x03 0x00 0x12' # CT values, etc
			}
			

			# - - -               Do not touch this! I kill you!               - - - - #
			self.settings['hostname'] = subprocess.check_output("hostname", shell=True).strip()

			# Hostname & Device are both set to the device hostname.
			self.settings['device'] = self.settings['hostname']
			
			# Type
			self.settings['type'] = 'power_meter'
			
			# Mac Address
			self.read_mac_info()
			
			# Local IP - read from file
			self.read_lip_info()

		except Exception, e:
			self.out += "There was an error setting initial variables: "#, self.lineno(), e
		

		try:
			self.i = 0
			self.difference = 0
			self.lastTime = 0
			self.noacuvim = False		
			self.skipwind = False
		except Exception, e:
			self.out += 'There was an error setting misc variables: '#, self.lineno(), e
		

		try:

			# This is the default dict for demand shedding logic / tracking - Key'd for priority
			self.relayState = {
				'0':{"currentState":"off", "desiredState":"off", "counter":0},  # MUST be 'off, off, 0'
				'1':{"currentState":"off", "desiredState":"off", "counter":0},
				'2':{"currentState":"off", "desiredState":"off", "counter":0},
				'3':{"currentState":"off", "desiredState":"off", "counter":0},
				'4':{"currentState":"off", "desiredState":"off", "counter":0},
				'5':{"currentState":"on" , "desiredState":"on" , "counter":0}   # MUST be 'on, on, 0'
			}

		except Exception, e:
			self.out += 'There was an error initializing relayStates: '#, self.lineno(), e

	# END DEF



	def init_relays_gpio(self):

		try:

			# Relays for Demand Shedding, key'd for relay id...relay_1 = K1
			self.relaygpio = dict()
			self.relaygpio['1'] = "P8_14"    #Relay 1 is p8/14, k1
			self.relaygpio['2'] = "P8_13"    #Relay 2 is p8/13, k2 
			self.relaygpio['3'] = "P9_13"    #Relay 3 is p9/13, k3
			self.relaygpio['4'] = "P9_14"    #Relay 4 is p9/14, k4

			# For each actual relay...set to output and leave in the off position
			for pin in self.relaygpio:
				GPIO.setup(self.relaygpio[pin], GPIO.OUT) # First set the pin to an output
				GPIO.output(self.relaygpio[pin], GPIO.LOW) # Then also set the pin/relay to "off" position
				
				self.out += 'GPIO Pin ' + str(self.relaygpio[pin]) + ' set to output and turned off\n'

		except Exception, e:
			self.out += 'Error initializing GPIO and setting defaul "off" state: '#, self.lineno(), e

		self.out += "\n"

	# END DEF


	def init_serial_conn(self):

		try:
			# UART1 for Rs485
			UART.setup("UART1")
			self.settings['com'] = serial.Serial(self.settings['com_device'], self.settings['baud'])
		except Exception, e:
			self.out += 'Error setting UART1, could not establish Serial Connection: '#, self.lineno(), e

	# END DEF


	def read_lip_info(self):

		try:
			# Get the Local IP from file
			file = open("/root/data/lip","r")
			self.settings['lip'] = file.read()
			file.close
			del file
		except Exception, e:
			self.out += "Could not read the LIP file."#, self.lineno(), e

	# END DEF


	def notation_to_hex(self):

		try:

			if self.noacuvim == False:

				# Count Commands from Node
				count_node_cmds = self.r.llen(self.settings['redis-acuvim-cmds-node'])
									
				# If Node Commands...
				if count_node_cmds > 0:

					self.out += 'Count Node Commands: ', count_node_cmds
					
					# Get the oldest command
					nodecmd = str(self.r.lrange(self.settings['redis-acuvim-cmds-node'],0,0)[0])
					
					self.out += 'Node CMD: ', nodecmd
					
					if nodecmd in self.settings['acuvim-cmds']:

						# If the specified command is valid...
						if self.settings['acuvim-cmds'][nodecmd] != '':
							
							self.out += 'Running Node Command: ',nodecmd
							
							# Get the hex code we need to send to Acuvim
							nodecmd = self.settings['acuvim-cmds'][nodecmd]
							
							self.out += 'Node Command is ',nodecmd
							
							# Add the hex code to the Acuvim Commands que list from Redis
							self.r.rpush(self.settings['redis-acuvim-cmds'],nodecmd)

					else:
						self.out += 'Bad command, removing...'
						
					# Remove the CMD from node acuvim commands list
					self.r.ltrim(self.settings['redis-acuvim-cmds-node'],1,count_node_cmds)

			# self.noacuvim

		except Exception, e:
			self.out += 'There was an error processing the CMD from Node/Redis: '#, self.lineno(), e

	# END DEF

	def process_acuvim_cmds(self):
		
		try:	

			if self.noacuvim == False:

				# Check for commands from web interface
				count_acuvim_cmds = self.r.llen(self.settings['redis-acuvim-cmds'])
				
				# If there are cmds in que
				if count_acuvim_cmds > 0:

					self.out += '\n # == -Received Acuvim Command- == #\n'

					# Get the command
					self.out += 'Getting CMD From Redis'
					cmd = str(self.r.lrange(self.settings['redis-acuvim-cmds'],0,0)[0])

					# Remove the CMD from the que
					self.out += 'Removing CMD From Redis Que\n'
					self.r.ltrim(self.settings['redis-acuvim-cmds'],1,count_acuvim_cmds)
					
					if cmd[:4] == "0x01":
						self.out += 'Processing Command: ',cmd
						self.out += self.command(cmd)
					else:
						self.out += "Invalid cmd was discarded:\n\t", cmd
			# self.noacuvim
		except Exception, e:
			self.out += 'Error: Processing Acuvim cmds: '#, self.lineno(), e

	# END DEF


	def get_meter_data(self):

		try:

			self.acuvim = True

			id = self.settings['device_id']
			self.data['unixtime'] = int(time()) # Get a fresh time stamp :)

			try:
				
				self.data['pfactor'] = self.command( id + ' ' + self.power_meter_commands['pfactor'] ) 	# power_factor
				self.data['phase'] = self.command(id + ' ' + self.power_meter_commands['phase'] ) 		# phase and system power
				self.data['realtime'] = self.command(id + ' ' + self.power_meter_commands['realtime'] ) # energy summaries from meter
				self.data['parameter_phases'] = self.command(id + ' ' + self.power_meter_commands['parameter_phases'] ) # CT values, etc
				
			except Exception, e:
				self.acuvim = False

			try:			
				# Power Factor					
				self.i = self.settings['register_offset']+2 # 1 under to allow the inc routine to place the id where it needs to be
				pfactor = self.data['pfactor'].split(' 0x')
				self.data['phase_a_power_factor'] = pfactor[(self.inc())].replace('0x','') + '' + pfactor[(self.inc())] + '' + pfactor[(self.inc())] + '' + pfactor[(self.inc())]
				self.data['phase_b_power_factor'] = pfactor[(self.inc())] + '' + pfactor[(self.inc())] + '' + pfactor[(self.inc())] + '' + pfactor[(self.inc())]
				self.data['phase_c_power_factor'] = pfactor[(self.inc())] + '' + pfactor[(self.inc())] + '' + pfactor[(self.inc())] + '' + pfactor[(self.inc())]
				self.data['power_factor'] = pfactor[(self.inc())] + '' + pfactor[(self.inc())] + '' + pfactor[(self.inc())] + '' + pfactor[(self.inc())]

				self.data['phase_a_power_factor'] = self.ieee_convert(self.data['phase_a_power_factor']);
				self.data['phase_b_power_factor'] = self.ieee_convert(self.data['phase_b_power_factor']);
				self.data['phase_c_power_factor'] = self.ieee_convert(self.data['phase_c_power_factor']);
				self.data['power_factor'] = self.ieee_convert(self.data['power_factor']);

				# Phase Power
				self.i = self.settings['register_offset']+2 # 1 under to allow the inc routine to place the id where it needs to be
				phase = self.data['phase'].split(' 0x')

				self.data['frequency'] = phase[(self.inc())].replace('0x','') + '' + phase[(self.inc())] + '' + phase[(self.inc())] + '' + phase[(self.inc())] #4000H~4001H Frequency F1 F'] = Rx R

				self.data['phase_a_voltage'] = phase[(self.inc())] + '' + phase[(self.inc())] + '' + phase[(self.inc())] + '' + phase[(self.inc())] #4002H~4003H Phase voltage V1 F1 U=Rx(PT1/PT2) R
				self.data['phase_b_voltage'] = phase[(self.inc())] + '' + phase[(self.inc())] + '' + phase[(self.inc())] + '' + phase[(self.inc())] #4004H~4005H Phase voltage V2 F1 U=Rx(PT1/PT2) R
				self.data['phase_c_voltage'] = phase[(self.inc())] + '' + phase[(self.inc())] + '' + phase[(self.inc())] + '' + phase[(self.inc())] #4006H~4007H Phase voltage V3 F1 U=Rx(PT1/PT2) R
				self.data['average_voltage'] = self.ieee_convert(phase[(self.inc())] + '' + phase[(self.inc())] + '' + phase[(self.inc())] + '' + phase[(self.inc())]) #4008H~4009H Average voltage Vavg F1 U=Rx(PT1/PT2) R

				self.data['line_a_voltage'] = self.ieee_convert(phase[(self.inc())] + '' + phase[(self.inc())] + '' + phase[(self.inc())] + '' + phase[(self.inc())] )#400aH~400bH Line voltage V12 F1 U=Rx(PT1/PT2) R
				self.data['line_b_voltage'] = self.ieee_convert(phase[(self.inc())] + '' + phase[(self.inc())] + '' + phase[(self.inc())] + '' + phase[(self.inc())] )#400cH~400dH Line voltage V23 F1 U=Rx(PT1/PT2) R
				self.data['line_c_voltage'] = self.ieee_convert(phase[(self.inc())] + '' + phase[(self.inc())] + '' + phase[(self.inc())] + '' + phase[(self.inc())] )#400eH~400fH Line voltage V31 F1 U=Rx(PT1/PT2) R
				self.data['average_line_voltage'] = self.ieee_convert(phase[(self.inc())] + '' + phase[(self.inc())] + '' + phase[(self.inc())] + '' + phase[(self.inc())] )#4010H~4011H Average line voltage Vlavg F1 U=Rx(PT1/PT2) R

				self.data['phase_a_current'] = phase[(self.inc())] + '' + phase[(self.inc())] + '' + phase[(self.inc())] + '' + phase[(self.inc())] #4012H~4013H Phase(line)current I1 F1 I=Rx?~W(CT1/CT2) R
				self.data['phase_b_current'] = phase[(self.inc())] + '' + phase[(self.inc())] + '' + phase[(self.inc())] + '' + phase[(self.inc())] #4014H~4015H Phase(line)current I2 F1 I=Rx?~W(CT1/CT2) R
				self.data['phase_c_current'] = phase[(self.inc())] + '' + phase[(self.inc())] + '' + phase[(self.inc())] + '' + phase[(self.inc())] #4016H~4017H Phase(line)current I3 F1 I=Rx?~W(CT1/CT2) R
				self.data['average_current'] = self.ieee_convert(phase[(self.inc())] + '' + phase[(self.inc())] + '' + phase[(self.inc())] + '' + phase[(self.inc())] )#4018H~4019H Average current Iavg F1 I=Rx?~W(CT1/CT2) R
				self.data['neutral_current_in'] = self.ieee_convert(phase[(self.inc())] + '' + phase[(self.inc())] + '' + phase[(self.inc())] + '' + phase[(self.inc())] )#401aH~401bH Neutral current In F1 I=Rx?~W(CT1/CT2) R

				self.data['phase_a_power'] = phase[(self.inc())] + '' + phase[(self.inc())] + '' + phase[(self.inc())] + '' + phase[(self.inc())] #401cH~401dH Phase A power Pa F1 P=Rx(PT1/PT2)(CT1/CT2) R
				self.data['phase_b_power'] = phase[(self.inc())] + '' + phase[(self.inc())] + '' + phase[(self.inc())] + '' + phase[(self.inc())] #401eH-401fH Phase B power Pb F1 P=Rx(PT1/PT2)(CT1/CT2) R
				self.data['phase_c_power'] = phase[(self.inc())] + '' + phase[(self.inc())] + '' + phase[(self.inc())] + '' + phase[(self.inc())] #4020H-4021H Phase C power Pc F1 P=Rx(PT1/PT2)(CT1/CT2) R
				self.data['system_power'] = phase[(self.inc())] + '' + phase[(self.inc())] + '' + phase[(self.inc())] + '' + phase[(self.inc())] #4022H-4023H System power Psum F1 P=Rx(PT1/PT2)(CT1/CT2) R       
				
				self.data['frequency'] = self.ieee_convert(self.data['frequency'])

				self.data['phase_a_voltage'] = self.ieee_convert(self.data['phase_a_voltage'])
				self.data['phase_b_voltage'] = self.ieee_convert(self.data['phase_b_voltage'])
				self.data['phase_c_voltage'] = self.ieee_convert(self.data['phase_c_voltage'])

				self.data['phase_a_current'] = self.ieee_convert(self.data['phase_a_current'])
				self.data['phase_b_current'] = self.ieee_convert(self.data['phase_b_current'])
				self.data['phase_c_current'] = self.ieee_convert(self.data['phase_c_current'])


				self.data['phase_a_power'] = self.ieee_convert(self.data['phase_a_power'])/1000
				self.data['phase_b_power'] = self.ieee_convert(self.data['phase_b_power'])/1000
				self.data['phase_c_power'] = self.ieee_convert(self.data['phase_c_power'])/1000
				self.data['system_power'] = self.ieee_convert(self.data['system_power'])/1000

				calculated_a_power = self.data['phase_a_current']*self.data['phase_a_voltage']
				calculated_b_power = self.data['phase_b_current']*self.data['phase_b_voltage'] 
				calculated_c_power = self.data['phase_c_current']*self.data['phase_c_voltage']

				if(self.data['phase_a_power']<0):
					calculated_a_power = calculated_a_power*-1

				if(self.data['phase_b_power']<0):
					calculated_b_power = calculated_b_power*-1

				if(self.data['phase_c_power']<0):
					calculated_c_power = calculated_c_power*-1
			
				self.data['calculated_a_power'] = calculated_a_power
				self.data['calculated_b_power'] = calculated_b_power
				self.data['calculated_c_power'] = calculated_c_power
				calculated_power = (calculated_a_power+calculated_b_power+calculated_c_power)*.001
				self.data['calculated_power'] = calculated_power
				
				self.data['power'] = self.data['system_power'] 
				
				parameters = self.data['parameter_phases'].split(' 0x')
				self.i = self.settings['register_offset']+2 # 1 under to allow the inc routine to place the id where it needs to be

				self.data['voltage_wiring_type'] = int(parameters[(self.inc())] + '' + parameters[(self.inc())], 16)
				self.data['current_wiring_type'] = int(parameters[(self.inc())] + '' + parameters[(self.inc())], 16)
				self.data['pt1_low'] = int(parameters[(self.inc())] + '' + parameters[(self.inc())], 16)
				self.data['pt1_high'] = int(parameters[(self.inc())] + '' + parameters[(self.inc())], 16)
				self.data['pt2'] = int(parameters[(self.inc())] + '' + parameters[(self.inc())], 16)
				self.data['ct1'] = int(parameters[(self.inc())] + '' + parameters[(self.inc())], 16) 
				self.data['ct2'] = int(parameters[(self.inc())] + '' + parameters[(self.inc())], 16)
				self.data['phase_a_cd'] = parameters[41] + '' + parameters[42]
				self.data['phase_b_cd'] = parameters[43] + '' + parameters[44]
				self.data['phase_c_cd'] = parameters[45] + '' + parameters[46]

				realtime = self.data['realtime'].split(' 0x')
				
				self.i = self.settings['register_offset']+2 # 1 under to allow the inc routine to place the id where it needs to be
				energy_imp = realtime[(self.inc())] + '' + realtime[(self.inc())] + '' + realtime[(self.inc())] + '' + realtime[(self.inc())]; #4048H-4049H Energy IMP F4/F7 0-999999999 R/W [v/10]
				energy_exp = realtime[(self.inc())] + '' + realtime[(self.inc())] + '' + realtime[(self.inc())] + '' + realtime[(self.inc())]; #404aH-404bH Energy EXP F4/F7 0-999999999 R/W [v/10]
				reactive_energy_imp = realtime[(self.inc())] + '' + realtime[(self.inc())] + '' + realtime[(self.inc())] + '' + realtime[(self.inc())]; #404cH-404dH Reactive energy IMP F5/F8 0-999999999 R/W [v/1000]
				reactive_energy_exp = realtime[(self.inc())] + '' + realtime[(self.inc())] + '' + realtime[(self.inc())] + '' + realtime[(self.inc())]; #404eH-404fH Reactive energy EXP F5/F8 0-999999999 R/W [v/1000]
				energy_total = realtime[(self.inc())] + '' + realtime[(self.inc())] + '' + realtime[(self.inc())] + '' + realtime[(self.inc())]; #4050H-4051H Energy TOTAL F4/F7 0-999999999 R/W [v/10]
				energy_net = realtime[(self.inc())] + '' + realtime[(self.inc())] + '' + realtime[(self.inc())] + '' + realtime[(self.inc())]; #4052H-4053H Energy NET F4/F7 0-999999999 R/W [v/10]
				reactive_energy_total = realtime[(self.inc())] + '' + realtime[(self.inc())] + '' + realtime[(self.inc())] + '' + realtime[(self.inc())]; #4054H-4055H Reactive energy TOTAL F5/F8 0-999999999 R/W
				reactive_energy_net = realtime[(self.inc())] + '' + realtime[(self.inc())] + '' + realtime[(self.inc())] + '' + realtime[(self.inc())]; #4056H-4057H Reactive energy NET F5/F8 0-999999999 R/W
				apparent_energy = realtime[(self.inc())] + '' + realtime[(self.inc())] + '' + realtime[(self.inc())] + '' + realtime[(self.inc())]; #4058H-4059H Apparent energy F6/F9 0-999999999 R/W


				self.data['energy_imp'] = int( energy_imp, 16)
				self.data['energy_exp'] = int( energy_exp, 16)
				self.data['reactive_energy_imp'] = int( reactive_energy_imp, 16)
				self.data['reactive_energy_exp'] = int( reactive_energy_exp, 16)
				self.data['energy_total'] = int( energy_total, 16)
				self.data['energy_net'] = int( energy_net, 16)
				self.data['reactive_energy_total'] = int( reactive_energy_total, 16)
				self.data['reactive_energy_net'] = int( reactive_energy_net, 16)
				self.data['apparent_energy'] = int( apparent_energy, 16)

			except Exception, e:
				self.noacuvim = True
				

			del self.data['pfactor']
			del self.data['phase']
			del self.data['realtime']
			del self.data['parameter_phases']

		except Exception, e:
			self.noacuvim = True
		#

	# end def	


	def get_windspeeds(self):

		try:
			
			self.out += '\t\t\t\t\tWindspeeds\n'
			self.out += '~' * 100
			self.out += "\n"

			windspeeds = []
			
			windspeeds = wind.speed()
			self.data['anemometer'] = float(windspeeds[0])
			
			# Correct the default number issue
			if self.data['anemometer'] < 0.5:
				self.data['anemometer'] = 0.0
			if self.data['anemometer'] > 1000.0:
				self.data['anemometer'] = 0.0
			
			self.out += "Anemometer: " + str(self.data['anemometer'])
			self.out += "\n\n"
		except Exception, e:
			self.out += 'Error getting windspeed data: ' + str(self.lineno()) + " " + str(e) + "\n\n"
	# END DEF


	def demand_shedding(self):

		try:
			self.out += '\t\t\t\t\tDemand Shedding\n'
			self.out += '~' * 100
			self.out += "\n"
			try:

				if not os.path.isfile(self.settings['demand_shedding_conf']):
					shutil.copy2('/root/powrz/scripts/demand_response_default.conf', self.settings['demand_shedding_conf'])

				# Read in Node / User generated settings file as dict relays
				fh = open(self.settings['demand_shedding_conf'],"r")
				demand_settings_json = fh.read()
				fh.close()

				conf = json.loads(demand_settings_json)
				conf['system_peak_power'] = float(conf['system_peak_power'])

			except Exception, e:
				self.out += '\tDemand shaving Error: JSON: '#, self.lineno(), e
				
				# Set these variables to prevent errors
				conf = {}

				conf['system_peak_power'] = float(0.0)
				
				conf['relay_1'] = {}
				conf['relay_2'] = {}
				conf['relay_3'] = {}
				conf['relay_4'] = {}

				tp = 0
				for k in conf: # Load Defaults
					tp = tp + 1
					
					#self.out += 'Type: ',type(conf[k]['delay'])
					#self.out += 'Delay: ',conf[k]['delay']

					conf[k]['delay'] = int(0)
					conf[k]['power'] = float(0)
					conf[k]['enabled'] = False
					conf[k]['priority'] = str(tp)
					conf[k]['name'] = "Invalid conf file, please reconfigure your Demand Settings."
					conf[k]['desc'] = ""

			self.data['system_peak_power'] = float(conf['system_peak_power'])
			
			priorities = {
				'1':{},
				'2':{},
				'3':{},
				'4':{}
			}


			self.out += "\n\tSystem Peak Power: " + str(conf['system_peak_power']) + "\n"
			
			if self.noacuvim ==True:
				power=self.data['power']=0
			else:
				power = self.data['power']

			self.out += "\tPower: " + str(power) + "\n"
			self.out += "\n"

			# This loops through each relay, and inserts the data into the priorities dict ordered by priority.
			#priority = str(relays[relay_id]['priority']) #cast to string just in case
			for n in range(1,5): 
				relay_id = 'relay_' + str(n)
				priorities[str(conf[relay_id]['priority'])][relay_id] = conf[relay_id]
				#priorities['1']['relay_1'] = name, desc, power, enabled, delay, etc

			# print our headers to the screen
			self.out += "Priority  Relay  Stat\tCState\tDState\tPlate/plate+pow\tCounter/Delay\n"

			# Now that we have our data in the priorities dict
			# We can loop through it and decide what relays to turn on or off, etc
			for priority in range(1,5): 
			
				# Here we have the priority
				priority = str(priority)
			
				# Next, we get the relay id, which looks like "relay_#"
				# Since it's also an array / dict key, we need it to access the data in the relay_# dict.
				for relay_id in priorities[priority]: 
		
					# If we are over the power limit, we are gonna set the desired relay state to off, else on:	
					# Current priority ID - 1
					priLess = str(int(priority)-1)
					# Current priority + 1
					priMore = str(int(priority)+1)
					

					# Determine True / False Booleen
					priorities[priority][relay_id]['enabled'] = True if priorities[priority][relay_id]['enabled'] == "true" else False # convert 'enabled' from text true/false to python boolean True or False
			
					# - - -                       Now we get to the logic                   - - - - #
					# - - -                     Determine the Desired State                 - - - - #
					# - - -           Determine all conditions and states in this area      - - - - #
			
					# If the relay is enabled
					if priorities[priority][relay_id]['enabled'] == True:
			
						# Determine the proper desired state



						if power >= conf['system_peak_power']  and self.relayState[priLess]['currentState'] == "off" and self.relayState[priLess]['desiredState'] == "off":												
							#and self.relayState[priority]['currentState'] == "on"						
							self.relayState[priority]['desiredState'] = "off"

						elif power+float(priorities[priority][relay_id]['power']) < conf['system_peak_power'] and self.relayState[priMore]['currentState'] == "on" and self.relayState[priMore]['desiredState'] == "on":
							#and self.relayState[priority]['currentState'] == "off" 
							self.relayState[priority]['desiredState'] = "on"

			
						# - - -                      Delay Tracking & Execution                 - - - - #
						
						if self.noacuvim == True:
							self.relayState[priority]['desiredState'] = "off"



						# Check if Desired State if different from current state
						if self.relayState[priority]['desiredState'] != self.relayState[priority]['currentState']:
							#self.out += "PRI",priority,"desired state != current state: ", relay['desiredState']," - - ",self.laststate[priority]['currentState']
							# Increment Counter..this should only happen when a state change happens and counter <= delay
							self.relayState[priority]['counter'] = self.relayState[priority]['counter'] + 1
			
							# If the delay limit has been reached or exceeded
							if int(self.relayState[priority]['counter']) >= int(priorities[priority][relay_id]['delay']):
								
								# Toggle the relay state 
								if self.relayState[priority]['desiredState'] == "off":
									#self.out += "Turning On"
									self.relayState[priority]['currentState'] = "off"
									GPIO.output(self.relaygpio[relay_id[-1]], GPIO.LOW)
								else:
									#self.out += "Turning Off"
									self.relayState[priority]['currentState'] = "on"
									GPIO.output(self.relaygpio[relay_id[-1]], GPIO.HIGH)
			
								#self.out += "Resetting Counter"
			
								# Reset the counter so we don't trigger this block repeatedly
								self.relayState[priority]['counter'] = 0
						else:
							# We don't count unless relay['desiredState'] != self.laststate[priority]['currentState']
							self.relayState[priority]['counter'] = 0
							#self.out += "PRI",priority,"desired state == current state: ", relay['desiredState']," == ",self.laststate[priority]['currentState']
			
					else:
						# Not Enabled, leave off
						self.relayState[priority]['desiredState'] = "off"
						self.relayState[priority]['currentState'] = "off"
						GPIO.output(self.relaygpio[relay_id[-1]], GPIO.LOW)


					# - - -                      Delay Tracking & Execution                 - - - - #
					# - - -                         self.out += Results To Screen                 - - - - #

					if priorities[priority][relay_id]['enabled'] == True:
						self.out += "" + str(priority) + "\t " + str(relay_id[-1]) + "\t True\t" + str(self.relayState[priority]['currentState']) + "\t" + str(self.relayState[priority]['desiredState']) + "\t" + str(priorities[priority][relay_id]['power']) + " " + str(float(priorities[priority][relay_id]['power']) + power) + "\t" + str(self.relayState[priority]['counter']) + "sec /" + str(priorities[priority][relay_id]['delay']) + "sec" + "\n"
					else:
						self.out += "" + str(priority) + "\t " + str(relay_id[-1]) + "\t False\t" + str(self.relayState[priority]['currentState']) + "\t-------------------------------------" + "\n"
					
					# We want to report this in our data
					self.relayState[priority]['currentState'] = self.relayState[priority]['currentState']
					
					# Place data into live data stream
					conf[relay_id]['currentState'] = self.relayState[priority]['currentState']
					conf[relay_id]['desiredState'] = self.relayState[priority]['desiredState']
					#conf[relay_id]['currentState'] = self.relayState[priority]['currentState']
					#conf[relay_id]['currentState'] = self.relayState[priority]['currentState']


					self.data[relay_id] = conf[relay_id]


				# And this is all the data we report
				#self.data['relay_1'] = self.data['demand_settings']['relay_1']
				#self.data['relay_2'] = self.data['demand_settings']['relay_2']
				#self.data['relay_3'] = self.data['demand_settings']['relay_3']
				#self.data['relay_4'] = self.data['demand_settings']['relay_4']
				#del self.data['demand_settings']

			self.out += '\n\n'
			
			# Move all the relay info to demand_settings
			#self.data['demand_settings'] = relays

			#self.out += "NewPower: ", newpower
			#self.out += self.data
		except Exception, e:
			self.out += "There was an error Shedding Demand: "#, self.lineno(), e

	# end Def


	def web_comms(self, thedata):

		try:
			# Send job to new thread with data
			thread.start_new_thread(self.web, (thedata,))
		except Exception, e:
			self.out += "Could not call new web thread:"#, self.lineno(), e

	#END DEF

	
	# This is for saving the second interval data via Redis
	def SaveRevolvingSecondDataRedis(self,data): 

		try:
			# Append the data to the bottom of the list
			self.r.rpush(self.settings['redis-revolving-data'],data) # Appends to end of list - use the
			
			# Check the size of the list
			datasize = self.r.llen(self.settings['redis-revolving-data'])-1 # -1 to account for 0 being the key for the first entry

			# Max size of the data (number of entries)
			maxdatasize = self.settings['size_of_revolving_data']
			
			# If the length of the data-store is greater than max
			if datasize >= maxdatasize:
				# Determine how much bigger datasize is than maxdatasize
				del_first_entries = datasize - maxdatasize + 60
						
				# Trim the oldest entry by the amount we figured in del_first_entries 
				self.r.ltrim(self.settings['redis-revolving-data'],del_first_entries,datasize)

		except Exception, e:
			self.out += "There was an error Saving Revolving Second Data: "#, self.lineno(), e

	# END DEF


	def opportunity(self,csv):

		try:
			'''
			This function will append data to opportunity file and then limit
			the file to only so many entries.
			'''
			# How big should the file be?
			limit = int(self.settings['opportunity_limit'])

			# If the file does not exist, create it
			if not os.path.exists(self.settings['opportunity']):
				open(self.settings['opportunity'], 'w').close() 

			# Add line of data to file
			with open(self.settings['opportunity'], "a") as ofile:
				ofile.write(csv + '\n')  
			
			# Count total lines
			c = int(self.file_len(self.settings['opportunity']))

			# Trim to limit
			if c > limit:
				# Calculate numbers
				trunc = c - limit

				# Truncate file to 'limit' lines
				with open(self.settings['opportunity'], 'r') as fin:
				    data = fin.read().splitlines(True)
				with open(self.settings['opportunity'], 'w') as fout:
				    fout.writelines(data[trunc:])
		except Exception, e:
			self.out += "There was an error saving to Opportunity!? "#, self.lineno(), e

	# * * * *

	def loop_timing(self):

		try:

			endTime = float(time())
			self.difference = float(endTime-self.startTime)
			self.difference = float("%.2f" % self.difference)
			ssleep = float("%.2f" % (self.targetTime-self.difference))
			if ssleep > 0:
				sleep(ssleep)
			
			checkTime = float(time()) - self.startTime
			growthTime = checkTime-self.lastTime
			self.lastTime = checkTime

			self.out += "\n\tLoop time:\t" + str(checkTime) + "\n"

		except Exception, e:
			self.out += 'Loop Timing & Sleep Error: '+ str(self.lineno()) + str(e)
	# END DEF

	
	def file_len(self,fname):
	    with open(fname) as f:
	        for i, l in enumerate(f):
	            pass
	    return i + 1
    # END DEF


	def command(self, dembytes):
		try:
			#if("split" not in dir(dembytes)):
			#	self.out += 'DEMBYTTES: ',dembytes
			#	return

			resp = dembytes.split()
			if(len(resp)<4):
				return "no valid command present"	
			series = []
			bbbb = 'BB' #2 for the checksum
			id = None
			for _byte in resp:
				if(id==None):
					id = _byte
				series.append(int(_byte, 16))
				bbbb = bbbb + 'B' #one B for every B
			self.settings['com'] = serial.Serial(self.settings['com_device'], self.settings['baud'])
			checksum = self.computeCRC( series )
			series.append( checksum[0] )
			series.append( checksum[1] )
			packed = pack(bbbb, *series)
			self.settings['com'].write( packed )
			sleep(self.settings['read_delay']) # If you remove this line, it will cause exception in get_meter_data <type 'exceptions.IndexError'> list index out of range
			data = " ".join(hex(ord(n)) for n in self.settings['com'].read(self.settings['com'].inWaiting()))
			
			# I should put my own exception here to test if data is coming back - to determine if the acuvim is even responding...
			
			'''
			# If you want to test serial data...
			self.out += "sent:"
			self.out += resp
			self.out += ""
			
			self.out += "received:"
			self.out += data
			self.out += ""
			'''
			
			return data		
		except Exception, e:
			self.out += "There was an error in self.command: "#, self.lineno(), e
	# end def
	
	
	# * * * * 
	def computeCRC(self,data):
		crc = 0xffff
		for a in data:
			if isinstance(a , basestring):
				a = int(a, 16)
			crc ^= a
			for i in range(0, 8): 
				if crc & 0x0001:
					crc >>= 1
					crc ^= 0xA001
				else:
					crc >>= 1 
		return [(crc & 0x00FF),(crc>>8)]
	# end def
	
	
	# * * * * 
	def ieee_convert(self,_hex):
		_places = 4
		_val = 0.0
		_converted = bin(int(_hex, 16))[2:].zfill(16)
		_binary = str(_converted).rjust(32, "0")
		_sign = 1 if _binary[0] == '0' else -1
		_exp = _binary[1:9]
		_exp = int( _exp, 2)-127
		_exp = 2 ** _exp
		_mantissa = _binary[9:];
		_mantissa = int( _mantissa, 2 );
		_mantissa = 1.0 + (int(_mantissa) / 8388608.0)
		_val = round( (_sign * _exp * _mantissa), _places);
		return _val
	
	# end def
	
	# * * * * 
	def inc(self): #workaround for python's stupid insistence on not implementing an proper incrementation operator [ no ++i ]  
		self.i += 1
		return self.i
	# end def	
	

	def lineno(self):
	    """Returns the current line number in our program."""
	    return inspect.currentframe().f_back.f_lineno

	def update_demand_shedding(self,keys,value):

		confFile = self.settings['demand_shedding_conf'] # only used here in this function...
		
		# Open settings file
		try:
			# Read in Node / User generated settings file as dict relays
			fh = open(confFile,"r")
			demand_json = fh.read()
			fh.close()
		except Exception, e:
			self.out += '\tResponse.Py: Could not read ', conffile, ' - '#, self.lineno(), e
			return false
		
		try:
			# Convert JSON to dict object
			demand_dict = json.loads(demand_json)

			relay = keys[:7] # relay_1
			index = keys[8:] # name
			
			if relay[:6] == "relay_":
				
				if index == "name" or index == "desc" or index == "enabled" or index == "power" or index == "priority" or index == "delay":
					self.out += 'Saving demand_dict[',relay ,'][',index,'] = "',value,'"'
					demand_dict[relay][index] = value # Replace Key with new Value
			
			elif keys == "system_peak_power":
				demand_dict["system_peak_power"] = value

			else:
				self.out += 'Unrecognized Command, was not processed: '
				self.out += '\trelay: \t', relay
				self.out += '\tindex: \t', index
				self.out += '\tvalue: \t', value

		except Exception, e:
			self.out += '\t There was an error converting demand shedding key values to python dict : '#, self.lineno(), e
			return false
			
		# Convert back to JSON
		newjson = json.dumps(demand_dict)
		
		# Save File 
		w = open(confFile,"w") # write the new file
		w.write(newjson)
		w.close()
	# End Def

	def web(self,data):

		post_data = [] # This will hold all the data we need

		try:
			# Count failed lines of data
			failed_lines_count = self.r.llen(self.settings['redis-failed-data'])
			self.out += 'Failed Data Lines: ' + str(failed_lines_count) + "\n"

			# If we have failed data, let's pull it into a variable
			if failed_lines_count > 0:
				
				# Set fdata to list type
				fdata = [] # The data list from Redis will get into this
				failed_data = [] # Get ready to load this with failed data as we loop through fdata
				
				# Get all the failed data
				fdata = self.r.lrange(self.settings['redis-failed-data'],0,failed_lines_count)
				
				# For each line of data, append a \newline
				for line in fdata: # fdata holds all the failed data
					failed_data.append(line) # append each line to this failed data var
			
				# Append our data to the failed data
				failed_data.append(data)
				
				# Now out post data
				post_data = failed_data

				### Remove proper lines from failed data
				if failed_lines_count >= self.settings['redis-failed-limit']:
					# Now we need to remove the first/top failed_lines_count lines from the failed_data file
					self.r.ltrim(self.settings['redis-failed-data'], 30, self.r.llen(self.settings['redis-failed-data'])) # remove thirty seconds of data in order to lessen the frequency of REDIS requests....
			else:
				post_data.append(data)

		except Exception, e:
			self.out += '"Failed Data" Error: ' + str(self.lineno()) + str(e) + "\n"


			# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - #
			# - - -                    Send Data to Measurz                         - - - - #
			# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - #

		try:

			encoded_device_data = json.dumps(post_data)	
			compressed_encoded_device_data = zlib.compress(encoded_device_data, 8)
			post_data = {"data":compressed_encoded_device_data}
			params = urllib.urlencode(post_data)
			headers = {"Content-type": "application/x-www-form-urlencoded", "Accept": "text/plain" } #, "Accept-encoding": "gzip"}
			http_request = httplib.HTTPConnection( self.measurz_address, self.measurz_port, timeout=self.connection_timeout )
			http_request.request( "POST", self.measurz_url, params, headers )
			response = http_request.getresponse()
			response_data = response.read()
			http_request.close()

			# If we get a response from measurz, we need to process it. This also happens with 15-min-cron.py.
			if response_data:

				# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - #
				# - - -                   Get Data from Pickle File                     - - - - #
				# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - #
				
				self.out += "Response: ", response_data," |-----"
				
				datapieces = response_data.split(';')
				
				for piece in datapieces:
					piece = piece.strip()
					if(piece==''):
						continue # If empty piece, skip to the end of this itteration through the loop
				
					# Some keys we use to identify the data...
					rkey7 = piece[:7] # First 7 digits of piece
					rkey4 = piece[:4] # First 4 digits of piece
					#self.out += "RKey4:\t",rkey4,"\nRKey7:\t",rkey7,"\n\n"
					                                                         
					if rkey7 == "force--": # Tell the device to update now
						self.out += 'Force: ', piece[7:]
						a = os.system('python /root/updates/check_update.py '+piece[7:])	
				
					elif rkey4 == "cmd_": # Process a command
				
						self.out += 'CMD:\t',
						if piece[4:8] == "set_":
							self.out += "Set:\t",piece[8:] 
							kvpair = piece[8:] # This is something like "relay_1_name: name"
							kv = kvpair.split(':') # break apart at the colon "relay_1_name" and " name"
							keys = kv[0].strip() # First index is key   -------------^              ^
							value = kv[1].strip() # Second index is value  -------------------------^
						
							# We need to send the relay and the key / value to this function. Figure shit out from there and you should be good.
							update_demand_shedding(keys,value) # Do it 
				
						elif piece[4:8] == "get_":
							pass # We don't have any get functions yet
							
					elif rkey4 == "0x01": # Process an Acuvim command - hex only
						self.out += 'Processing Acuvim Command: {' + piece + "}"
						self.r.rpush(self.settings['redis-acuvim-cmds'],piece)
				
					else:
						self.out += "Response.Py: Unknown Command Received from Measurz: ", piece

			#else:  #self.out += "No Response"
					
			### Remove proper lines from failed data
			if failed_lines_count > 0:
				# Now we need to remove the first/top failed_lines_count lines from the failed_data file
				self.r.ltrim(self.settings['redis-failed-data'],failed_lines_count,self.r.llen(self.settings['redis-failed-data']))

		except Exception, e:
			self.out += 'Send Data to Measurz Error: '#, self.lineno(), e
			
			# Write 1 line data to redis failed-data list
			self.r.rpush(self.settings['redis-failed-data'],data)

		#del post_data # Can I do that here?


	def read_mac_info(self):
		try:
			# Get the MAC address of the system
			file = open('/sys/class/net/eth0/address', 'r')
			self.settings['mac'] = file.read().strip()
			file.close()
		except Exception, e:
			self.out += "Could not read the MAC Address."#, self.lineno(), e
	# END DEF


# End Class







'''
___________                            __             __  .__             _________ .__                        
\_   _____/__  ___ ____   ____  __ ___/  |_  ____   _/  |_|  |__   ____   \_   ___ \|  | _____    ______ ______
 |    __)_\  \/  // __ \_/ ___\|  |  \   __\/ __ \  \   __\  |  \_/ __ \  /    \  \/|  | \__  \  /  ___//  ___/
 |        \>    <\  ___/\  \___|  |  /|  | \  ___/   |  | |   Y  \  ___/  \     \___|  |__/ __ \_\___ \ \___ \ 
/_______  /__/\_ \\___  >\___  >____/ |__|  \___  >  |__| |___|  /\___  >  \______  /____(____  /____  >____  >
        \/      \/    \/     \/                 \/             \/     \/          \/          \/     \/     \/ 
'''



# * * * * * * * * * * *
powrz_controller = powrz_controller() # *
# * * * * * * * * * * *

