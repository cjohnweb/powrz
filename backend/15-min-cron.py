#!/usr/bin/python

import glob, operator
import subprocess
import traceback
import datetime
import httplib
import urllib
import pickle
import redis
import time
import zlib
import json
import os


# Connect to redis, 10 attempts and then quit or continue
def connect_redis(c): 
	try:
		print 'Trying to connect to Redis Server...'
		if c >= 10:
			exit(1)
		r = redis.Redis('localhost')
		ping = r.ping()
		if ping == True:
			print "Connected to Redis Successfull!\n"
			return r
	except Exception, e:
			c += 1
			print 'Could not connect to Redis (Failed ',c,' times): ', e
			time.sleep(1)
			connect_redis(c)
# end Def

print '15 Minute Cron Script for SGB3p / Powrz\n'
print 'Creates 15 minute interval data for power, windpseed and demand shedding data. Saves the data to a monthly record file and sends to measurz.\n\n'

# Connect to Redis
r = connect_redis(0)

print 'Initializing Stuff'
settings = {}

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - #
# - - -                       System Settings                           - - - - #
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - #
settings['hostname'] = subprocess.check_output("hostname", shell=True)
settings['hostname'] = settings['hostname'].strip()
print 'Hostname: ',settings['hostname']

settings['lip'] = subprocess.check_output("hostname -I", shell=True) 
settings['lip'] = settings['lip'].strip()
print 'Local IP: ',settings['lip']

file = open('/sys/class/net/eth0/address', 'r')
settings['mac'] = file.read()
settings['mac'] = settings['mac'].strip()
file.close()

measurz_addressm2 = 'm2.measurz.net'											# Measurz domainname 
measurz_urlm2 = '/sgb3p.io4/'                 									# Measurz URL path with prefixed and suffixed slashes 
measurz_portm2 = 8080                    										# Measurz data collection port

measurz_addressm3 = 'm3.measurz.net'											# Measurz domainname 
measurz_urlm3 = '/'																# Measurz URL path with prefixed and suffixed slashes 
measurz_portm3 = 1125                    										# Measurz data collection port

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - #
# - - -                        Other Settings                           - - - - #
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - #
settings['month'] = datetime.datetime.now().strftime("%m")
settings['year'] = datetime.datetime.now().year
settings['dir'] = "/root/powrz/backend/"
settings['datadir'] = '/root/data/'
settings['monthly_data_dir'] = settings['datadir'] + 'monthly-data/'
settings['fifteenmin_data_filename_no_path'] = str(settings['year']) + '-' + str(settings['month']) + '_data.txt'
settings['fifteenmin_data_filename'] = settings['monthly_data_dir'] + settings['fifteenmin_data_filename_no_path']

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - #
# - - -                       Redis Keys & Data                         - - - - #
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - #
settings['redis-failed-data'] = 'failed-data'
settings['redis-revolving-data'] = 'revolving-data'
settings['redis-acuvim-cmds-node'] = 'acuvim-cmds-filter' #list type
settings['redis-acuvim-cmds'] = 'acuvim-cmds' #list type
settings['redis-log'] = 'log'
settings['redis-15-min-cron-last-run'] = '15-min-cron-last-run'
settings['redis-disk-space'] = "disk-space"

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - #
# - - -                           Functions                             - - - - #
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - #
def get_oldest_file(files, _invert=False):
    """ Find and return the oldest file of input file names.
    Only one wins tie. Values based on time distance from present.
    Use of `_invert` inverts logic to make this a youngest routine,
    to be used more clearly via `get_youngest_file`.
    """
    gt = operator.lt if _invert else operator.gt
    # Check for empty list.
    if not files:
        return None
    # Raw epoch distance.
    now = time.time()
    # Select first as arbitrary sentinel file, storing name and age.
    oldest = files[0], now - os.path.getctime(files[0])
    # Iterate over all remaining files.
    for f in files[1:]:
        age = now - os.path.getctime(f)
        if gt(age, oldest[1]):
            # Set new oldest.
            oldest = f, age
    # Return just the name of oldest file.
    return oldest[0]

def get_youngest_file(files):
    return get_oldest_file(files, _invert=True)

def file_len(fname):
    with open(fname) as f:
        for i, l in enumerate(f):
            pass
    return i + 1

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - #
# - - -                       Setting Dir's & Var's                     - - - - #
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - #

try:
	
	# From now to 15 mins / 900 seconds ago - the time frame we want to capture 
	now = int(time.time()) # Now
	
	# Check failed data folder & files exists
	if not os.path.isfile(settings['fifteenmin_data_filename']):
		if not os.path.isdir(settings['monthly_data_dir']):
			os.makedirs(os.path.dirname(settings['monthly_data_dir']))
		open(settings['fifteenmin_data_filename'], 'a', os.O_NONBLOCK).close()
	
	# second, mintute, hour, day and month all need to be 0 padded. ie 01 02 03 04, etc - ("%02d")
	second = "%02d" % (datetime.datetime.now().second)
	minute = "%02d" % (datetime.datetime.now().minute)
	minute = int(minute)

	print "\tActual Minute: ", minute
	
	# Ensure the minute is 00, 15, 30 or 45.
	if( minute < 15 ):
		minute = "00"
	elif( minute >= 15 and minute < 30 ):
		minute = "15"
	elif( minute >= 30 and minute < 45 ):
		minute = "30"
	else:
		minute = "45"
	
	print "\tMod Minute: ", minute
	
	hour = "%02d" % (datetime.datetime.now().hour)
	day = "%02d" % (datetime.datetime.now().day)
	month = "%02d" % (datetime.datetime.now().month)
	year = datetime.datetime.now().year
	timemark = int(now) - 900 # Filter data based on unix timestamp
	
	print 'Initialzing Variables'
	
	powerlist = [] 					# Initialize list
	d = {} 							# Initialize Dict
	count = int(0) 					# Initialize Int

	avg_power = float(0) 			# Initialize float
	avg_power_a = float(0) 			# Initialize float
	avg_power_b = float(0)			# Initialize float
	avg_power_c = float(0) 			# Initialize float
	avg_windspeed = float(0) 		# Initialize float

	sum_power = float(0) 			# Initialize float
	sum_power_a = float(0) 			# Initialize float
	sum_power_b = float(0)			# Initialize float
	sum_power_c = float(0) 			# Initialize float
	sum_windspeed = float(0) 		# Initialize float

	interval_power = ""				# Initialize string
	interval_power_a = ""			# Initialize string 
	interval_power_b = ""			# Initialize string
	interval_power_c = ""			# Initialize string
	interval_windspeed = ""			# Initialize string

	
except Exception, e:
	print 'Error Setting variables & checking folders: ', e

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - #
# - - -                          Getting Data                           - - - - #
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - #

try:
	
	print 'Getting size of Redis data'
	
	len = r.llen(settings['redis-revolving-data']) 	# Get size of revolving data cache from redis
	
	startlimit = len - 1000 		# We want to get the last 1000 data points
	if startlimit < 0: 				# If it's a negative number, we don't have 1000 lines of data, so just set it to 0
		startlimit = 0

except Exception, e:
	print '', e

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - #
# - - -                          The Loop                               - - - - #
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - #

try:

	print 'Pulling the data'

	# Get the data
	jsons = r.lrange(settings['redis-revolving-data'],startlimit,len)
	
	for jsondata in jsons:
	
		jsondata = jsondata.strip() 
	
		d = {}
		d = json.loads(jsondata)
	
		# Make sure each data point is in the right time frame
		if (d['unixtime'] >= timemark) and (d['unixtime'] <= now):

			# Count + 1 - keep track of how many data points we are actually pulling out
			count += float(1)
	
			# Add up the values for average later
			sum_power = sum_power + d['power']
			sum_power_a = sum_power_a + d['phase_a_power']
			sum_power_b = sum_power_b + d['phase_b_power']
			sum_power_c = sum_power_c + d['phase_c_power']

			try:
				sum_windspeed = sum_windspeed + d['anemometer']
			except Exception, e:
				print "No windspeed data for sum"

			# Save the points into CSV format
			interval_power = interval_power + str(d['power']) + ","
			interval_power_a = interval_power_a + str(d['phase_a_power']) + ","
			interval_power_b = interval_power_b + str(d['phase_b_power']) + ","
			interval_power_c = interval_power_c + str(d['phase_c_power']) + ","

			try:
				interval_windspeed = interval_windspeed + str(d['anemometer']) + ","
			except Exception, e:
				print "No windspeed data for interval"


except Exception, e:
	print 'There was an error in the Loop: ', e, traceback.print_exc() 


try:
	interval_power = interval_power[0:-1] # remove trailing slash
	interval_power_a = interval_power_a[0:-1] # remove trailing slash
	interval_power_b = interval_power_b[0:-1] # remove trailing slash
	interval_power_c = interval_power_c[0:-1] # remove trailing slash

	try:
		interval_windspeed = interval_windspeed[0:-1] # remove trailing slash
	except Exception, e:
		pass

	
except Exception, e:
	print 'There was an error removing the trailing slash: ', e, traceback.print_exc() 



# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - #
# - - -                          Creating Averages                      - - - - #
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - #

try:
	
	# Divide power total by number of entries
	# Can't divide by 0, so if any 0's, result is 0
	
	print 'Creating Averages...'
	
	if count != 0:
		
		if sum_power != 0:
			avg_power = (sum_power/count)
		else:
			avg_power = float(0)
		
		if sum_power_a != 0:
			avg_power_a = (sum_power_a/count)
		else:
			avg_power_a = float(0)
		
		if sum_power_b != 0:
			avg_power_b = (sum_power_b/count)
		else:
			avg_power_b = float(0)
		
		if sum_power_c != 0:
			avg_power_c = (sum_power_c/count)
		else:
			avg_power_c = float(0)
	
		try:
			if sum_windspeed != 0:
				avg_windspeed = (sum_windspeed/count)
			else:
				avg_windspeed = float(0)
		except Exception, e:
			print "No windspeed data"


	else:
		avg_power = float(0)
		avg_power_a = float(0)
		avg_power_c = float(0)
		avg_power_b = float(0)
		avg_windspeed = float(0)
			
except Exception, e:
	print 'There was an error creating Averages: ', e, traceback.print_exc() 


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - #
# - - -                    Data to SGIP / CSV                           - - - - #
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - #

try:

	print 'Creating CSV of data for monthly file'
	
	# Assemble 15 minute interval entry:
	data_str = str(now) +","+ str(year)+"-"+str(month)+"-"+str(day)+" "+str(hour)+":"+str(minute)+":"+str(second) +","+ str(avg_power) +","+ str(avg_power_a) +","+ str(avg_power_b) +","+ str(avg_power_c)

	try:
		data_str += ","+ str(avg_windspeed) 
	except Exception, e:
		pass

except Exception, e:
	print 'There was an error assembling the CSV: ', e

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - #
# - - -                  Write data to the month-year file              - - - - #
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - #

try:
	
	# Make sure the month-year_data.txt file exists and then write the line of jsondata to it
	if not os.path.isfile(settings['fifteenmin_data_filename']):
		if not os.path.isdir(settings['monthly_data_dir']):
			os.makedirs(os.path.dirname(settings['monthly_data_dir']))
		open(settings['fifteenmin_data_filename'], 'a', os.O_NONBLOCK).close()
	
	# Save results to settings['monthly_data_dir'] file
	print 'Writting to file: ', settings['fifteenmin_data_filename']
	f = open(settings['fifteenmin_data_filename'], 'a', os.O_NONBLOCK)
	f.write(data_str + '\r\n')
	f.flush()
	
except Exception, e:
	print 'There was an error writting data to month-year file: ', e


try:

	print '\n\n\n'

	data_dict = {
			"monitoring-parameter":"commands=no",
			"type":"15minuteinterval",
			"device":settings['hostname'],
			"timestamp":str(now),
			"date":str(year)+"-"+str(month)+"-"+str(day)+" "+str(hour)+":"+str(minute)+":"+str(second),
			"power":str(avg_power),
			"interval_power":interval_power,
			"anemometer":str(avg_windspeed),
			"rs485_id":"0x01",
			"node_id":settings['hostname'],
			"mac":settings['mac'],
			"lip":settings['lip'],
			"system_time":str(now),
			"interval_hour":hour,
			"interval_minute":minute,
			"interval_series":interval_power,
			"interval_data":avg_power,
			"interval_cumulative":sum_power,

			# , 
			#"phase_a_power":str(avga),
			#"phase_b_power":str(avgb),
			#"phase_c_power":str(avgc),
			#"interval_powera":interval_powa,
			#"interval_powerb":interval_powb,
			#"interval_powerc":interval_powc
	}

	printthis = {
	"device":settings['hostname'],
	"timestamp":str(now),
	"date":str(year)+"-"+str(month)+"-"+str(day)+" "+str(hour)+":"+str(minute)+":"+str(second),
	"power":str(avg_power),
	"anemometer":str(avg_windspeed),
	"rs485_id":"0x01",
	"node_id":settings['hostname'],
	"mac":settings['mac'],
	"lip":settings['lip'],
	"system_time":str(now),
	"interval_hour":hour,
	"interval_minute":minute,
	"interval_data":avg_power,
	}

	for key in printthis:
		print key, ' = ', printthis[key]


	m3data = {"0": data_dict}

except Exception, e:
	print 'There was an error preping data to Measurz: ', e


# Post to M2 Measurz

try:
	post_data = { "0" : data_dict }
	encoded_device_data = json.dumps(post_data)
	compressed_encoded_device_data = zlib.compress(encoded_device_data, 8)
	post_data = {"data":compressed_encoded_device_data}
	params = urllib.urlencode(post_data)
	headers = {"Content-type": "application/x-www-form-urlencoded", "Accept": "text/plain" } #, "Accept-encoding": "gzip"}
	http_request = httplib.HTTPConnection( measurz_addressm2, measurz_portm2, timeout=5 )
	http_request.request( "POST", measurz_urlm2, params, headers )
	response = http_request.getresponse()
	response_data = response.read()
	http_request.close()
	print "M2 Response: ", response_data	
	
except Exception, e:
	print 'There was an error seinding data to m2.measurz.net: ', e



# Post to M3 Measurz

try:

	post_data = {"data":json.dumps(m3data)}
	print post_data
	headers = {"Content-type": "application/json", "Accept": "text/plain" }
	http_request = httplib.HTTPConnection( measurz_addressm3, measurz_portm3, timeout=5 )
	http_request.request( "POST", measurz_urlm3, json.dumps(post_data), headers )
	response = http_request.getresponse()
	response_data = response.read()         
	http_request.close()

	print "M3 Response: ", response_data

except Exception, e:
	print 'There was an error seinding data to m3.measurz.net: ', e



# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - #
# - - -                     Redis force memory to disk                  - - - - #
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - #

try:
	r.save()
except Exception, e:
	print 'There was an error saving Redis Cache to disk: ', e

