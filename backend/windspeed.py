#!/bin/python

import socket
import sys
import struct

#How much do I hate python?
#			I hate python THIS much
#	|________________(*-*)_________________|

class windspeed:

	def __init__(self):

		self.windspeed = {
			'hertz' : 0,
			'rotationsPerMinute' : 0,
			'milesPerHour' : 0,
			'metersPerSecond' : 0
		}

		self.windBuffer = []

	#enddef __init__()


	def caca_socket(self): 

		sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
			
		sockPath = '/tmp/windspeed.sock'
		try:
			sock.connect(sockPath)
		except socket.error, msg:
			print >>sys.stderr, msg
			sys.exit(1)
		
		outBuf = ">"
		try:
			sock.sendall(outBuf)
		
			bytesGot = 0
			bytesNeed = 16
			inBuf = ''
			
			while bytesGot < bytesNeed:
				inBuf += sock.recv(16)
				bytesGot = len(inBuf)
		
		finally:
			return struct.unpack('4f', inBuf)	

	#enddef caca_socket()

	
	def get_wind(self):
		
		self.windBuffer = self.caca_socket()
	
		self.windspeed['hertz'] = self.windBuffer[0]
		self.windspeed['rotationsPerMinute'] = self.windBuffer[1]
		self.windspeed['milesPerHour'] = self.windBuffer[2]
		self.windspeed['metersPerSecond'] = self.windBuffer[3]

		return self.windspeed

	#enddef get_wind()		
		
#endclass windspeed


if __name__ == '__main__':

	wind = windspeed()
	print wind.get_wind()
