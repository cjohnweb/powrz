#!/bin/python

import windspeed
import time

wind = windspeed.windspeed()
ws = {}

while True:
	
	ws = wind.get_wind()

	print '\n'
	print '\033[2K\033[34mFrequency: \033[0m' + str(ws['hertz'])
	print '\033[2K\033[34mRPM: \033[0m' + str(ws['rotationsPerMinute'])
	print '\033[2K\033[34mMPH: \033[0m' + str(ws['milesPerHour'])
	print '\033[2K\033[34mm/s: \033[0m' + str(ws['metersPerSecond'])
	print '\033[8A\n'

	time.sleep(100.0 / 1000.0)

