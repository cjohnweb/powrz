/**************************************************************
*			b_bone.c
**************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

int init_dt_overlay(char *overlay)
{
	int i;
	FILE *slotDes;
	char filename[36];
	char *command;
	size_t size;

	//Load the device tree overlay
	//To begin, find the cape manager. It changes sometimes.
	for(i = 0; i < 255; i++)
	{
		sprintf(filename, "/sys/devices/bone_capemgr.%u/slots", i);
		slotDes = fopen(filename, "w");
		if(slotDes != NULL)
		{
			break;
		}
	}
	if(i == 256)
	{
		//Couldn't find capemgr directory
		return -1;
	}		
	
	//Load the overlay
//	fprintf(slotDes, "%s", overlay);
	
	command = malloc(1024 * sizeof(char));
	
	sprintf(command, "echo %s > %s", overlay, filename);
	fprintf(slotDes, "%s", overlay);
	fclose(slotDes);

//	if(system(command) == -1)
//	{
//		free(command);
//		return -1;
//	}

	memset(command, 0, 1024);

	//Wait until it loads
	fprintf(stderr, "Loading %s...\n", overlay);
	i = -1;
	do
	{
		slotDes = fopen(filename, "r");
		getdelim(&command, &size, EOF, slotDes);
		
		if(strstr(command, filename) == NULL)
		{ 
			if(strstr(command, overlay) != NULL)
			{
				i = 0;
			}
		}

		fclose(slotDes);
	}
	while(i == -1);
		
	free(command);

	return 0;
}
