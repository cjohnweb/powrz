#!/bin/bash

cp wind.service /lib/systemd/system
systemctl disable wind.service
systemctl enable wind.service
systemctl start wind.service
