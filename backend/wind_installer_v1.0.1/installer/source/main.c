/**************************************************************
*					main.c
*	Measure anemometer frequency from the PRU
*************************************************************/

#include "b_bone.h"
#include "socket_stuff.h"
#include "manager.h"
#include <pruss/prussdrv.h>
#include <pruss/pruss_intc_mapping.h>
#include <stdio.h>
#include <stdint.h>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <sched.h>

#define DDR_BASEADDR 0x80000000

struct ws_value
{
	float hertz;
	float rotationsPerMinute;
	float milesPerHour;
	float metersPerSecond;
};

union windspeed
{
	float ws_array[4];
	char ws_bytes[16];
	struct ws_value value;
};

union windspeed windspeed;

static int memoryHandle;
static void *sharedPRUMemory;
static void *ddrMemory;

tpruss_intc_initdata intc = PRUSS_INTC_INITDATA;


unsigned int go_PRU_go(void)
{
	unsigned int ok;

	if(init_dt_overlay("SGB-PRU-INOUT") == -1)
	{
		fprintf(stderr, "Overlay failed to install.");
		return -1;
	}
	usleep(1000000);

	//I assume this initializes the PRU or something
//	ok = prussdrv_init();
//	if(ok != 0)
//	{
//		fprintf(stderr, "PRU Init failed\n");
//		return -1;
//	}

	return 0;
}


unsigned int start_PRU0(char *codeFile, char *dataFile)
{
	unsigned int ok;

	//Something about interrupts
	ok = prussdrv_open(PRU_EVTOUT_0);
	if(ok != 0)
	{
		fprintf(stderr, "PRU0 Interrupt failed to open\n");
		return -1;
	}
	
	//Initialize the interrupt
	ok = prussdrv_pruintc_init(&intc);
	if(ok != 0)
	{
		fprintf(stderr, "PRU0 Interrupt failed to initialize\n");
		return -1;
	}
	
	//Load the data (which is probably all zeros, but do it anyway)
	if(dataFile != NULL)
	{
		ok = prussdrv_load_datafile(0, dataFile);
	}

	//Load and run the program
	//TODO: If c-compiled, it's possible the code doesn't start from address 0
	ok = prussdrv_exec_program_at(0, codeFile, 0);
	if(ok != 0)
	{
		fprintf(stderr, "PRU0 failed to load or run\n");
		return -1;
	}
	
	//Wait for the PRU to start
	fprintf(stderr, "Waiting...\n");
	prussdrv_pru_wait_event(PRU_EVTOUT_0);
	prussdrv_pru_clear_event(PRU_EVTOUT_0, PRU0_ARM_INTERRUPT);
	fprintf(stderr, "Got event\n");
	
	return 0;
}	


unsigned int start_PRU1(char *codeFile, char *dataFile)
{
	unsigned int ok;

	//Something about interrupts
	ok = prussdrv_open(PRU_EVTOUT_1);
	if(ok != 0)
	{
		fprintf(stderr, "PRU Interrupt failed to open\n");
		return -1;
	}
	
	//Initialize the interrupt
	ok = prussdrv_pruintc_init(&intc);
	if(ok != 0)
	{
		fprintf(stderr, "PRU Interrupt failed to initialize\n");
		return -1;
	}
	
	//Load the data (which is probably all zeros, but do it anyway)
	if(dataFile != NULL)
	{	 
		ok = prussdrv_load_datafile(1, dataFile);
	}

	//Load and run the program
	ok = prussdrv_exec_program_at(1, codeFile, 0);
	if(ok != 0)
	{
		fprintf(stderr, "PRU1 failed to load or run\n");
		return -1;
	}

	//Wait for the PRU to start
	fprintf(stderr, "Waiting...\n");
	prussdrv_pru_wait_event(PRU_EVTOUT_1);
	prussdrv_pru_clear_event(PRU_EVTOUT_1, PRU1_ARM_INTERRUPT);
	fprintf(stderr, "Got event\n");

	return 0;
}


unsigned int stop_PRU0(void)
{
	unsigned int ok;

	//Clear the event
	ok = prussdrv_pru_clear_event(PRU_EVTOUT_0, PRU0_ARM_INTERRUPT);
	if(ok)
	{
		fprintf(stderr, "PRU Event failed to clear\n");
	}

	//Stop the PRU
	ok = prussdrv_pru_disable(0);
	if(ok != 0)
	{
		fprintf(stderr, "PRU just won't stop\n");
	}

	return 0;
}

	
unsigned int stop_PRU1(void)
{
	unsigned int ok;

	//Clear the event
	ok = prussdrv_pru_clear_event(PRU_EVTOUT_1, PRU0_ARM_INTERRUPT);
	if(ok)
	{
		fprintf(stderr, "PRU Event failed to clear\n");
	}

	//Stop the PRU
	ok = prussdrv_pru_disable(1);
	if(ok != 0)
	{
		fprintf(stderr, "PRU just won't stop\n");
	}

	return 0;
}

unsigned int quit_PRU(void)
{
	unsigned int ok;

	//De-initialize the PRU
	ok = prussdrv_exit();
	if(ok != 0)
	{
		fprintf(stderr, "PRU can't exit\n");
	}
}


int get_windspeed(unsigned int ticks)
{
	if(ticks == 0)
	{
		return -1;
	}

	//Convert anemometer pulse periods (measured in 5nS ticks) to meaningful values
	//The formulae are provided on p.6 of 
	//	"MODEL 010C WIND SPEED SENSOR OPERATION MANUAL" 
	//		by Met One Instruments

	windspeed.value.hertz = 200000000.0 / ticks;
	windspeed.value.rotationsPerMinute = windspeed.value.hertz * 3 / 2;
	windspeed.value.milesPerHour = (windspeed.value.rotationsPerMinute / 16.676) + 0.6;
	windspeed.value.metersPerSecond = windspeed.value.milesPerHour * 2.2369;

//	printf("\n\033[2KFrequency: 0x%08X\n",  *(int*)&windspeed.value.hertz);
//	printf("\033[2KRPM: 0x%08X\n", *(int*)&windspeed.value.rotationsPerMinute);
//	printf("\033[2KMPH: 0x%08X\n", *(int*)&windspeed.value.milesPerHour);
//	printf("\033[2Km/s: 0x%08X\n\n\033[6A", *(int*)&windspeed.value.metersPerSecond);
	
	return 0;
}
	
int main(void)
{
	unsigned int ok;
	unsigned int *sharedMem32;
	char command[32];
	pid_t processID = getpid();

	//Tell the program manager your PID
	talk_through_socket(WIND_MANAGER_SOCK, (char*)&processID, sizeof(pid_t));
		
	if(open_async_socket("/tmp/windspeed.sock") < 0)
	{
		fprintf(stderr, "Bad socket");
		exit(EXIT_FAILURE);
	}

	ok = go_PRU_go();
	if(ok != 0)
	{
		fprintf(stderr, "I don't know why I bother\n");
		quit_PRU();
		exit(EXIT_FAILURE);
	}

	ok = start_PRU1("/bin/wind/c_timer.bin", NULL);
	if(ok == -1)
	{
		exit(EXIT_FAILURE);
	}

	ok = start_PRU0("/bin/wind/c_windspeed.bin", NULL);
	if(ok == -1)
	{
		exit(EXIT_FAILURE);
	}

	//Map the share memory space
	ok = prussdrv_map_prumem(PRUSS0_SHARED_DATARAM, (void**)&sharedMem32);
	if(ok == -1)
	{
		exit(EXIT_FAILURE);
	}

	while(1)
	{
		while(get_from_async_socket(command) != 0)
		{		
			get_windspeed(*sharedMem32);
			send_to_async_socket(&windspeed.ws_bytes[0]);
		}
		
		keep_async_socket_alive();
	
	}

	return 0;
}
	
		
	
