/**************************************************************
*				socket_stuff.h
**************************************************************/

#ifndef SOCKET_STUFF_H
#define SOCKET_STUFF_H


int talk_through_socket(const char*, char*, int);
int listen_through_socket(const char*, char*, int);
int open_async_socket(const char*);
int get_from_async_socket(char*);
int send_to_async_socket(char*);


#endif //SOCKET_STUFF_H
