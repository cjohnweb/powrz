/**************************************************************
*				socket_stuff.c
*************************************************************/

#include <sys/un.h>
#include <sys/socket.h>
#include <string.h>
#include <sys/mount.h>
#include <stdio.h>
#include <errno.h>
#include <sys/mman.h>
#include <signal.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/time.h>

static int bore_socket(const char*, struct sockaddr_un*, unsigned int*);
static int create_socket(const char*);
static int plug_socket(const char*);

union floatUnion
{
	float fword32;
	char byte8[4];
};


static char *globalSockMem;


volatile sig_atomic_t childPID = 0;
volatile sig_atomic_t parentPID = 0;
volatile sig_atomic_t killFlag = 0;
volatile sig_atomic_t socketReadFlag = 0;
volatile sig_atomic_t socketWriteFlag = 0;

void socket_read_handler(int sig)
{
	socketReadFlag = 1;
	signal(sig, socket_read_handler);
}

void socket_write_handler(int sig)
{
	socketWriteFlag = 1;
	signal(sig, socket_write_handler);
}

void kill_handler(int sig)
{
	//Kinda recursive handler that terminates all the processes
	if(killFlag)
	{
		raise(sig);
	}

	killFlag = 1;

	//Kill the child
	kill(childPID, SIGKILL);
	
	signal(sig, kill_handler);
	raise(sig);
	
	exit(EXIT_FAILURE);
}




int is_parent_alive(void)
{
	if(kill(parentPID, 0) == -1)
	{
		return -1;
	}
}

static int bore_socket(const char *filename, struct sockaddr_un *name, unsigned int *size)
{
	int sockDes;

	//Create the socket
	sockDes = socket(PF_LOCAL, SOCK_STREAM, 0);
	perror("Create socket ");
	if(sockDes < 0)
	{
		return -1;
	}

	//Prepare to bind the socket to a name
	name->sun_family = AF_LOCAL;
	strcpy(name->sun_path, filename);

	*size = SUN_LEN(name);


	return sockDes;
}

	
static int create_socket(const char *filename)
{
	struct sockaddr_un name;
	int sockDes;
	unsigned int size;

	sockDes = bore_socket(filename, (struct sockaddr_un*)&name, &size);
	if(sockDes == -1)
	{
		return -1;
	}

	//Connect to the socket
	if(connect(sockDes, (struct sockaddr*)&name, size) < 0)
	{
//		perror("Connect: ");
		close(sockDes);
		return -1;
	}
//	perror("Connect: ");
	
	return sockDes;
}
	

static int plug_socket(const char *filename)
{
	struct sockaddr_un name;
	int dirtySock;
	int freshSock;
	unsigned int size;
	fd_set socketSet;
	struct timeval spinTime;

	dirtySock = bore_socket(filename, (struct sockaddr_un*)&name, &size);
	if(dirtySock == -1)
	{
		return -1;
	}
	
	//Bind will throw errors if the socket already exists.
	unlink(name.sun_path);
	
	//Bind the name to the socket
	if(bind(dirtySock, (struct sockaddr*)&name, size) < 0)
	{
//		perror("Bind: ");
		close(dirtySock);
		return -1;
	}
//	perror("Bind: ");

	//This socket accepts connections. It does not make connections. 
	if(listen(dirtySock, 1) < 0)
	{	
		close(dirtySock);
		return -1;
	}
	
	FD_ZERO(&socketSet);
	FD_SET(dirtySock, &socketSet);
	spinTime.tv_sec = 0;
	spinTime.tv_usec = 100000;

	//Freewheel until the connection is open at the other end
	select(dirtySock + 1, &socketSet, NULL, NULL, &spinTime);
	freshSock = accept(dirtySock, (struct sockaddr*)&name, &size);
	if(freshSock < 0)
	{
		close(dirtySock);
		return -1;
	}

	close(dirtySock);
	return freshSock;
}


int talk_through_socket(const char *socket, char *writeData, int writeLength)
{
	//Single-shot write-to-socket function (not quite a client)

	int sockDes;
	
	struct timeval busyTime;
	fd_set pollSet;	

	sockDes = create_socket(socket);
	if(sockDes == -1)
	{
		return -1;
	}

	FD_ZERO(&pollSet);
	FD_SET(sockDes, &pollSet);

	busyTime.tv_sec = 0;
	busyTime.tv_usec = 100000;
	
	select(sockDes + 1, NULL, &pollSet, NULL, &busyTime); 	
//	perror("Select: ");

	send(sockDes, (const void*) writeData, writeLength, 0);
//	perror("Send: ");

	close(sockDes);

	return 0;
}	

int listen_for_socket(const char *socket, char *readData, int readLength)
{
	//Single-shot read-from-socket function (not quite a server)

	int sockDes;
	struct timeval busyTime;
	fd_set pollSet;

	sockDes = plug_socket(socket);

	recv(sockDes, readData, readLength, 0);

//	close(sockDes);

	return sockDes;		
}

int respond_to_socket(int sockDes, char *response, int respLength)
{
	//Respond to an open socket after receiving
	send(sockDes, (const void*)response, respLength, 0);
	close(sockDes);
	return 0;
}

int listen_loop(const char *socketName)
{
	int stinkySock;
	int freshSock;
	fd_set activeSet, readSet;
	int ok, i;
	struct sockaddr_un name;
	size_t size;
	struct timeval timeout = {.tv_usec = 0, .tv_sec = 60};		
	int fdMax;

	//Bind will throw errors if the socket already exists.
	unlink(socketName);
	
	//Create the socket and set up as a server
	stinkySock = socket(PF_LOCAL, SOCK_STREAM, 0);
	if(stinkySock < 0)
	{
		perror("Socket problems");
		return -1;
	}

	//Prepare to bind the socket to a name
	name.sun_family = AF_LOCAL;
	strcpy(name.sun_path, socketName);

	size = SUN_LEN(&name);


	//Bind the name to the socket
	if(bind(stinkySock, (struct sockaddr*)&name, size) < 0)
	{
		perror("Bind problems");
		close(stinkySock);
		return -1;
	}
	
	//Hey!
	if(listen(stinkySock, 1) < 0)
	{
		close(stinkySock);
		return -1;
	}
	
	FD_ZERO(&activeSet);
	FD_SET(stinkySock, &activeSet);
	
	fdMax = stinkySock + 1;
	
	while(1)
	{

				
//		ok = select(FD_SETSIZE, &activeSet, NULL, NULL, NULL);
//		if(ok < 0)
//		{
//			perror("Poll Socket: ");
//			close(stinkySock);	
//			return -1;
//		}

		memcpy(&readSet, &activeSet, sizeof(readSet));
			
		timeout.tv_sec = 5;	
		timeout.tv_usec = 0;
			
		ok = select(fdMax, &readSet, NULL, NULL, &timeout);
		if(ok < 0 && errno != EINTR)
		{
			perror("Poll Socket: ");
			close(stinkySock);	
			return -1;
		}
		else if(ok == 0)
		{
			//Don't let the child become a zombie
			if(is_parent_alive() == -1)
			{
  				perror("The parent process is dead; thus the socket must die.");
  				return -1;
			}
		}
		else if(ok > 0)
		{
			if(FD_ISSET(stinkySock, &readSet))
			{
				//Connection requenst on original socket
				size = sizeof(name);
				freshSock = accept(stinkySock, (struct sockaddr*)&name, &size);
				if(freshSock < 0)
				{
					perror("Accept: ");
					close(freshSock);
					close(stinkySock);
					return -1;
				}
			
				FD_SET(freshSock, &readSet);
				fdMax = fdMax < freshSock ? freshSock : fdMax;
				FD_CLR(stinkySock, &readSet);
			}	
				
			for(i = 0; i < fdMax + 1; i++)
			{
				if(FD_ISSET(i, &readSet))
				{
					recv(freshSock, globalSockMem, 32, 0);
//					fprintf(stdout, "GOT: %s\n", globalSockMem);
			
					//Signal the parent that the message has been received
					if(kill(parentPID, SIGUSR1) == -1)
					{
						close(freshSock);
						close(stinkySock);
						return -1;
					}
			
					//Wait for a response from the parent
					while(socketWriteFlag == 0);
					socketWriteFlag = 0;
			
//					fprintf(stdout, "SENDING: %s\n", globalSockMem + 32);		
					//Write back to the client socket
					send(freshSock, (const void*)(globalSockMem + 32), 32, 0); 
				
					close(freshSock);
					FD_CLR(i, &activeSet);
				}
			}
		}
	}		
	close(stinkySock);
}


int open_async_socket(const char *socket)
{
	pid_t pid;
	
	int sockDes;

	globalSockMem = mmap(NULL, sizeof(char) * 64, 
			PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);	

	//It feels so very inefficient to replicate this entire program 
	//	just to do this one tiny thing
	pid = fork();
	if(pid < 0)
	{
		perror("Bad fork");
		return -1;
	}

	if(pid == 0)
	{
		//This is the child process. It's going to wait for the socket in the background
		//	while the main process get's on with it's business. The child and parent communicate
		//	using globalSockMem, SIGUSR1, and SIGUSR2
		signal(SIGUSR2, socket_write_handler);
		fprintf(stderr, "Child process started\n");
		parentPID = getppid();
		listen_loop(socket);				
		
		_exit(EXIT_FAILURE);
	}
	else
	{
		//This is the parent that has just given birth to a child process.
		//	Get back to work parent!
		signal(SIGUSR1, socket_read_handler);
		childPID = pid;
		fprintf(stderr, "Child process PID: %u\n", childPID);
		return 0;
	}	
}


int get_from_async_socket(char *payload)
{	
	if(socketReadFlag == 0)	
	{
		return -1;
	}
	
	socketReadFlag = 0;
	strcpy(payload, globalSockMem);
	return 0;
}


int send_to_async_socket(char *payload)
{
	memset(globalSockMem, 0, 32);
	memcpy(globalSockMem + 32, payload, 16);
	kill(childPID, SIGUSR2);
	
	return 0;
}

int keep_async_socket_alive(void)
{
	//Check if the socket process is still running
	if(kill(childPID, 0) < 0)
	{
		//It died, try to get it going again
		open_async_socket("/tmp/gridzConfig.sock");
	}

	return 0;
}
	
