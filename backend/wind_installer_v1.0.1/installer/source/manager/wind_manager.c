/**************************************************************
*				wind_manager.c
*		Launch processes and restart
*			them if they become unruly.
**************************************************************/


#include "manager.h"
#include "socket_stuff.h"
#include <stdlib.h>
#include <stdio.h>
#include <signal.h>

volatile sig_atomic_t killSig = 0;
volatile int windPid;

void killSig_handler(int sig)
{
	if(killSig)
	{
		raise(sig);
	}

	killSig = 1;	
	
	//Kill all the process
	kill(windPid, SIGKILL);
	
	signal(sig, killSig_handler);
	raise(sig);

	exit(-1);
}


int main(void)
{
	//Map everything that can halt execution to killSig
	signal(SIGTERM, killSig_handler); 
	signal(SIGINT, killSig_handler); 
	signal(SIGQUIT, killSig_handler);  
	signal(SIGKILL, killSig_handler);  
	signal(SIGHUP, killSig_handler); 
	
	//Start the controller
	system(WINDSPEED_EXEC);
	listen_through_socket(WIND_MANAGER_SOCK, (char*)&windPid, 4);
	fprintf(stderr, "Windspeed Meter is alive. PID: %u\n", windPid);
	
	while(killSig == 0)
	{
		//Check if the Gridz Controller PID exists
		if(kill(windPid, 0) != 0) 
		{
			//The thing crashed. Try to restart.
			system(WINDSPEED_EXEC);
			listen_through_socket(WIND_MANAGER_SOCK, (char*)&windPid, 4);
			fprintf(stderr, "Crashed... Restarting... New PID: %u\n", windPid);	
			continue;
		}

		sleep(1);
	}

	return -1;	
}			
			
			
	

