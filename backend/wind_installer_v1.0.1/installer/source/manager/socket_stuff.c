/**************************************************************
*				socket_stuff.c
*************************************************************/

#include <sys/un.h>
#include <sys/socket.h>
#include <string.h>
#include <sys/mount.h>
#include <stdio.h>
#include <errno.h>

static int bore_socket(const char*, struct sockaddr_un*, unsigned int*);
static int create_socket(const char*);
static int plug_socket(const char*);


union floatUnion
{
	float fword32;
	char byte8[4];
};



static int bore_socket(const char *filename, struct sockaddr_un *name, unsigned int *size)
{
	int sockDes;

	//Create the socket
	sockDes = socket(PF_LOCAL, SOCK_STREAM, 0);
//	perror("Create socket: ");
	if(sockDes < 0)
	{
		return -1;
	}

	//Prepare to bind the socket to a name
	name->sun_family = AF_LOCAL;
	strcpy(name->sun_path, filename);

	*size = SUN_LEN(name);


	return sockDes;
}

	
static int create_socket(const char *filename)
{
	struct sockaddr_un name;
	int sockDes;
	unsigned int size;

	sockDes = bore_socket(filename, (struct sockaddr_un*)&name, &size);
	if(sockDes == -1)
	{
		return -1;
	}

	//Connect to the socket
	if(connect(sockDes, (struct sockaddr*)&name, size) < 0)
	{
//		perror("Connect: ");
		close(sockDes);
		return -1;
	}
//	perror("Connect: ");
	
	return sockDes;
}
	

static int plug_socket(const char *filename)
{
	struct sockaddr_un name;
	int dirtySock;
	int freshSock;
	unsigned int size;
	fd_set socketSet;
	struct timeval spinTime;

	dirtySock = bore_socket(filename, (struct sockaddr_un*)&name, &size);
	if(dirtySock == -1)
	{
		return -1;
	}
	
	//Bind will through errors if the socket already exists.
	unlink(name.sun_path);
	
	//Bind the name to the socket
	if(bind(dirtySock, (struct sockaddr*)&name, size) < 0)
	{
//		perror("Bind: ");
		close(dirtySock);
		return -1;
	}
//	perror("Bind: ");

	//This socket accepts connections. It does not make connections. 
	if(listen(dirtySock, 1) < 0)
	{	
		close(dirtySock);
		return -1;
	}
	
	FD_ZERO(&socketSet);
	FD_SET(dirtySock, &socketSet);
	spinTime.tv_sec = 1;
	spinTime.tv_usec = 0;

	//Freewheel until the connection is open at the other end
	select(dirtySock + 1, &socketSet, NULL, NULL, /*&spinTime*/NULL);
	freshSock = accept(dirtySock, (struct sockaddr*)&name, &size);
	if(freshSock < 0)
	{
		close(dirtySock);
		return -1;
	}

	close(dirtySock);
	return freshSock;
}


int talk_through_socket(const char *socket, char *writeData, int writeLength)
{
	//Single-shot write-to-socket function (not quite a client)

	int sockDes;
	
	struct timeval busyTime;
	fd_set pollSet;	

	sockDes = create_socket(socket);
	if(sockDes == -1)
	{
		return -1;
	}

	FD_ZERO(&pollSet);
	FD_SET(sockDes, &pollSet);

	busyTime.tv_sec = 0;
	busyTime.tv_usec = 100000;
	
	select(sockDes + 1, NULL, &pollSet, NULL, &busyTime); 	
//	perror("Select: ");

	send(sockDes, (const void*) writeData, writeLength, 0);
//	perror("Send: ");

	close(sockDes);

	return 0;
}	

int listen_through_socket(const char *socket, char *readData, int readLength)
{
	//Single-shot read-from-socket function (not quite a server)

	int sockDes;
	struct timeval busyTime;
	fd_set pollSet;

	sockDes = plug_socket(socket);

	recv(sockDes, readData, readLength, 0);

	close(sockDes);

	return 0;		
}

	
//int main(void)
//{
//	union floatUnion x = {.fword32 = -10};
//	float sign[4] = {1.0, 1.0, -1.0, -1.0};
//	union floatUnion y;
//	int quadrant = 0;
//
//	while(1)
//	{
//		//The sine of a triagle wave
//		y.fword32 = sin(x.fword32);
//		x.fword32 += sign[quadrant] * 0.31831;
//		if(x.fword32 >= 10 || x.fword32 <= -10)
//		{
//			quadrant++;
//			if(quadrant == 4)
//			{
//				quadrant = 0;
//			}
//		}
//		printf("Out: %f\n", y.fword32);
//
//		talk_through_socket("/tmp/smellySocks", &y.byte8[0], 4);
//	
//		sleep(1);
//	}
//	
//
//	
////	//Mount a tmpfs
////	mkdir("/tmp/gridz/", S_IRWXU | S_IRGRP | S_IXGRP | S_IROTH | S_IXOTH);
////	printf("%d\n", mount("none", "/tmp/gridz/", "tmpfs", 0, "mode=0700,uid=65534"));
////	perror("");
//
//}

