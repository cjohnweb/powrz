#!/bin/bash

cp SGB-PRU-INOUT-00A0.dtbo /lib/firmware

mkdir /bin/wind

cp c_windspeed.bin /bin/wind/
cp c_timer.bin /bin/wind/
cp windspeed /bin/wind/

cp lib/* /usr/lib

cd source
cp manager/wind_manager /bin/wind/manager
cp daemon/wind_daemon /bin/wind/daemon
cd daemon
bash service_en.sh
