var settings = {}
settings.environment = 'local';  // local / remote

// position of realtime data in redis "revolving-data" list

settings.data_map = {
    'device'                    : '',
    'energyimp'                 : '',
    'energyexp'                 : '',
    'reactive_energyimp'        : '',
    'reactive_energyexp'        : '',
    'energy_net'                : '',
    'energy_total'              : '',
    'reactive_energy_net'       : '',
    'reactive_energy_total'     : '',
    'apparent_energy'           : '',
    'frequency'                 : '',
    'average_voltage'           : '',
    'line_a_voltage'            : '',
    'line_b_voltage'            : '',
    'line_c_voltage'            : '',
    'average_line_voltage'      : '',
    'phase_a_current'           : 7,
    'phase_b_current'           : 8,
    'phase_c_current'           : 9,
    'average_current'           : '',
    'neutral_current_in'        : '',
    'phase_a_voltage'           : 11,
    'phase_b_voltage'           : 12,
    'phase_c_voltage'           : 13,
    'phase_a_power_factor'      : 15,
    'phase_b_power_factor'      : 16,
    'phase_c_power_factor'      : 17,
    'power_factor'              : 14,
    'phase_a_power'             : 3,
    'phase_b_power'             : 4,
    'phase_c_power'             : 5,
    'system_power'              : '',
    'alt_system_power'          : '',
    'power'                     : '',
    'phase_a_apparent_power'    : '',
    'phase_b_apparent_power'    : '',
    'phase_c_apparent_power'    : '',
     'system_apparent_power'    : '',
    'ct1'                       : '',
    'ct2'                       : '',
    'utc_created'               : '',
    'created'                   : ''
}