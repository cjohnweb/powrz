// dependencies
var config              = require('./config');
var express             = require('express');
var app                 = require('express')();
var server              = require('http').createServer(app);
var io                  = require('socket.io')(server); // attach socket.io instance to http server
var summon              = require('express-summon-route');
var path                = require('path');
var logger              = require('morgan');
var session             = require('express-session');
var crypto              = require('crypto'); 
var bodyParser          = require('body-parser');    
var redis               = require('redis');
var merge               = require('merge');
var fs                  = require('fs');

// init redis client
var client = redis.createClient();


// initial session
app.use(session({
  secret: 'Alpine 1',
  resave: false,
  saveUninitialized: true
}));

// summon routing    
summon.use(app, express);

// for post processing
app.use(bodyParser.json());

app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
	
	extended: true

})); 

// http server
var port = process.env.PORT || 80;

server.listen(port, function(){
	
	console.log('Powrz 0.0.5 running on ' + port);

});


// views
app.set('views', path.join(__dirname, 'views'));            // path name routing
app.set('view engine', 'jade');

// static asset routing
app.use(express.static(path.join(__dirname, 'public')));

// define routes & requests
var index       = require('./routes/index');        app.use('/', index);






/// catch 404 and forward to error handler
app.use(function(req, res, next){
    
	var err = new Error('Not Found');
	
	err.status = 404;
	
	next(err);
	
});

/// error handlers

// development error handler // will print stacktrace
if (app.get('env') === 'development'){
	
	app.use(function(err, req, res, next){
		
		res.status(err.status || 500);
		
		res.render('error', {
		    message: err.message,
		    error: err
		});

	});
}

// production error handler // no stacktraces leaked to user
app.use(function(err, req, res, next){
	
	res.status(err.status || 500);
	
	res.render('error',{
		
		message: err.message,
		
		error: {}
		
	});
	
});

module.exports = app;








