// core requirments
var express             = require('express');
var router              = express.Router();
var jsdom               = require('jsdom'); 
var $                   = require('jquery')(require('jsdom').jsdom().parentWindow);
var redis               = require("redis");

var client = redis.createClient();

router.get('/', function(req, res){

    // error handler
    client.on("error", function (err){
        console.log("Error " + err);
    });

    client.lrange('revolving-data', 0, 300, graphData);

    function graphData(err, data){

        var length = data.length; 

        var pointList = [];

        for(var i = 0; i < length; i++){
            var point = JSON.parse(data[i]);
            var singleObj = {}
            singleObj['point'] = point.power;
            pointList.push(singleObj);
        }

        var viewData = {
            pointList: pointList
        }

        res.json(pointList);

        //res.render('graph', viewData);

        res.end();

    }

});

module.exports = router;