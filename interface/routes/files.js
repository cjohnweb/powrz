// core requirments
var express             = require('express');
var router              = express.Router();
var fs                  = require('fs');
var jsdom               = require('jsdom'); 
var $                   = require('jquery')(require('jsdom').jsdom().parentWindow);
var session             = require('express-session');
var bodyParser          = require('body-parser');



var filePath = '/root/data/monthly-data/';

router.get('/', function(req, res){
    
    // read point data for this very second
    //var now = fs.readFileSync('/home/sgb-3p/data/now.data', 'utf8');

    // get file listing
    var files = fs.readdirSync(filePath);

    // format for output
    var fileList = []; 

    // loop through and create fileList for view
    $.each(files, function(i, val){

        var singleObj = {}

        singleObj['file'] = val;

        var year = val.substr(0,4);

        var month = val.substr(5,2);

        var from = new Date(year,month,01).getTime();
        
        //console.log(from);
        
        var to = new Date(year,month,01).setMonth(new Date(year,month,01).getMonth() + 1);
        
        singleObj['from'] = from / 1000;

        singleObj['to'] = to / 1000;

        fileList.push(singleObj);

    });
    
    var files = '';

    var viewData = {
        fileList: fileList
    }

    // render our response
    res.render('files', viewData);

    res.end();

});


module.exports = router;
