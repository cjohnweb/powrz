// core requirments
var express             = require('express');
var router              = express.Router();
var jsdom               = require('jsdom'); 
var $                   = require('jquery')(require('jsdom').jsdom().parentWindow);
var redis               = require("redis");
var client              = redis.createClient();
var csv                 = require('csv-parse');
var Q                   = require('q');
var session             = require('express-session');
var bodyParser          = require('body-parser');


function c2a(strData, strDelimiter){

    strDelimiter = (strDelimiter || ",");

    var objPattern = new RegExp(
        (
            // Delimiters.
            "(\\" + strDelimiter + "|\\r?\\n|\\r|^)" +

            // Quoted fields.
            "(?:\"([^\"]*(?:\"\"[^\"]*)*)\"|" +

            // Standard fields.
            "([^\"\\" + strDelimiter + "\\r\\n]*))"
        ),
        "gi"
        );

    var arrData = [[]];

    var arrMatches = null;

    while (arrMatches = objPattern.exec( strData )){

        var strMatchedDelimiter = arrMatches[ 1 ];

        if (
            strMatchedDelimiter.length &&
            strMatchedDelimiter !== strDelimiter
            ){

            arrData.push( [] );

        }

        var strMatchedValue;

        if (arrMatches[ 2 ]){

            strMatchedValue = arrMatches[ 2 ].replace(
                new RegExp( "\"\"", "g" ),
                "\""
                );

        } else {

            strMatchedValue = arrMatches[ 3 ];

        }

        arrData[ arrData.length - 1 ].push( strMatchedValue );
    }

    // Return the parsed data.
    return( arrData );
}


router.get('/:format/:from?/:to?/:realtime?', function(req, res){
    
    var session = req.session;
    // authentication check
    if(session.authenticated != true){
        res.writeHead(302, {
          'Location': '/'
        });
        res.end();
    }
    
    
    
    var length = 900;
    var points = [];
    var date_filtered = [];


    client.llen('revolving-data', revolving_data);
            
    function revolving_data(err, records){

        var start = records - (length+1);

        console.log(start);

        console.log(records);

        client.lrange('revolving-data', start, records, function(err, data){ 

            // return csv results
            if(req.params.format == 'csv'){

                for(var i = 0; i < length; i++){

                    var point = c2a(data[i],',');

                    // #### DATE FILTERING

                    // if EITHER param is set

                    //console.log(point[0][0]);

                    if(req.params.from || req.params.to){

                        // if BOTH params set *****
                        if(req.params.from 
                                && req.params.to
                                && req.params.from != 0
                                && req.params.to != 0){

                                //console.log(point[0][0]); console.log(req.params.to);

                                if(point[0][0] >= req.params.from && point[0][0] <= req.params.to){

                                    points.push(point[0][7]);

                                }


                        }else{

                            // if FROM set *****
                            if(req.params.from && req.params.from != 0){

                                if(point[0][0] >= req.params.from){

                                    points.push(point[0][7]);

                                }

                            }

                            // if TO set *****
                            if(req.params.to && req.params.to != 0){

                                if(point[0][0] <= req.params.to){

                                    points.push(point[0][7]);

                                }
                            }
                        }

                    }else{

                        points.push(point[0][7]);

                    }

                }

                var viewData = {
                    pointlist: points
                }

                res.render('current', viewData);

                res.end();

            }

        });

    }
    
    // error handler
    client.on("error", function (err){
        console.log("Error " + err);
    });
    
});


module.exports = router;








