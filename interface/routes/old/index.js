// core requirments
var config        = require('../config');
var express       = require('express');
var router        = express.Router();
var fs            = require('fs');
var jsdom         = require('jsdom'); 
var $             = require('jquery')(require('jsdom').jsdom().parentWindow);
var session       = require('express-session');
var crypto        = require('crypto'); 
var bodyParser    = require('body-parser');  


/* get home page. */
router.get('/', function(req, res){

    require('dns').resolve('www.google.com', function(err){

        if(err){

            var session = req.session;

            res.render('auth', { 
                title: 'SGB-3P'
            });

        }else{

            
            res.render('auth', { 
                title: 'SGB-3P'
            });
            

            /*
            res.writeHead(302, {
              'Location': 'http://measurz.net/'
            });
            res.end();
            */

        }
    });

});





/* get home page. */
router.get('/range/:from?/:to?/:root?/', function(req, res){

    var session = req.session;

    // authentication check
    if(session.authenticated != true){
        res.writeHead(302, {
          'Location': '/'
        });
        res.end(); 
        next();
    }

    res.render('index', { 
        title: 'SGB-3P', 
        from : req.params.from,
        to: req.params.to
    });

});


/* res.end() allows sgb-3p hardware layer data to process in app.js catchall */
router.post('/', function(req, res){

    res.json(res);

    res.end();

});


module.exports = router;
