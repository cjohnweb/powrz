/* 
 * JR Chew - jr.chew@jlmei.com
 * Demand Data Controller
 * Returns data settings to demand tab interface.
 */

// core requirments
var config              = require('../config');
var express             = require('express');
var router              = express.Router();
var csv                 = require('csv-parse'); // needed?
var fs                  = require('fs');

var path = '/root/settings/demand_response.conf';

router.get('/', function(req, res){
   
    var file_content = fs.readFileSync(path).toString();
    
    res.json(JSON.parse(file_content));
    
});

module.exports = router;