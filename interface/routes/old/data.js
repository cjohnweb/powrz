/* 
 * JR Chew - jr.chew@jlmei.com
 * Core Data Controller
 * Pull data from local or remote data source as defined in config.js.
 * 
 */

// core requirments
var config              = require('../config');
var express             = require('express');
var router              = express.Router();
var redis               = require("redis");
var client              = redis.createClient();
var csv                 = require('csv-parse');
var fs                  = require('fs');
var merge               = require('merge');

// dependent functions
function days_in_month(y, m){
   return /8|3|5|10/.test(--m)?30:m==1?(!(y%4)&&y%100)||!(y%400)?29:28:31;
}

function pad(n, width, z){
    z = z || '0';
    n = n + '';
    return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
}

function c2a(strData, strDelimiter){

    strDelimiter = (strDelimiter || ",");

    var objPattern = new RegExp(
        (
            // Delimiters.
            "(\\" + strDelimiter + "|\\r?\\n|\\r|^)" +

            // Quoted fields.
            "(?:\"([^\"]*(?:\"\"[^\"]*)*)\"|" +

            // Standard fields.
            "([^\"\\" + strDelimiter + "\\r\\n]*))"
        ),
        "gi"
        );

    var arrData = [[]];

    var arrMatches = null;

    while (arrMatches = objPattern.exec( strData )){

        var strMatchedDelimiter = arrMatches[ 1 ];

        if (
            strMatchedDelimiter.length &&
            strMatchedDelimiter !== strDelimiter
            ){

            arrData.push( [] );

        }

        var strMatchedValue;

        if (arrMatches[ 2 ]){

            strMatchedValue = arrMatches[ 2 ].replace(
                new RegExp( "\"\"", "g" ),
                "\""
                );

        } else {

            strMatchedValue = arrMatches[ 3 ];

        }

        arrData[ arrData.length - 1 ].push( strMatchedValue );
    }

    // Return the parsed data.
    return( arrData );
}


function getDistinctArray(arr){
    var dups = {};
    return arr.filter(function(el) {
        var hash = el.valueOf();
        var isDup = dups[hash];
        dups[hash] = true;
        return !isDup;
    });
}


Date.prototype.add_days = function(days){
    
    var date = new Date(this.valueOf());
    
    date.setDate(date.getDate() + days);
    
    return date;
    
}


// dumb little function to take Javascript dates, parse the month and return numeric.
function numeric_month(date){
    
    var month = new Array();

    month['Jan'] = 01;
    month['Feb'] = 02;
    month['Mar'] = 03;
    month['Apr'] = 04;
    month['May'] = 05;
    month['Jun'] = 06;
    month['Jul'] = 07;
    month['Aug'] = 08;
    month['Sep'] = 09;
    month['Oct'] = 10;
    month['Nov'] = 11;
    month['Dec'] = 12;

    return month[date.toString().substr(4,3)]; 

}

function add_tz_offset(date){
    
   var offset = date.getTimezoneOffset()*60000; // local time isn't neccessary here.  

   var adjusted = new Date(date.getTime()+offset);
   
   return adjusted;
   
}


// return an array of days in between from / to
function get_dates(from, to){
      
    var date_arr = new Array();

    var today = from;
    
    while (today <= to){
        date_arr.push(today)
        today = today.add_days(1);
    }

    return date_arr;
    
}


function get_map_key(key){
        
    var keys = {
    
        0  : 'unixtime',
        1  : 'frequency',
        2  : 'power',
        3  : 'phase_a_power',
        4  : 'phase_b_power',
        5  : 'phase_c_power',
        6  : 'average_current',
        7  : 'phase_a_current',
        8  : 'phase_b_current',
        9  : 'phase_c_current',
        10 : 'average_voltage',
        11 : 'phase_a_voltage',
        12 : 'phase_b_voltage',
        13 : 'phase_c_voltage',
        14 : 'power_factor',
        15 : 'phase_a_power_factor',
        16 : 'phase_b_power_factor',
        17 : 'phase_c_power_factor',
        18 : 'phase_a_cd',
        19 : 'phase_b_cd',
        20 : 'phase_c_cd',
        21 : 'anemometer',
        22 : 'priority_1',
        23 : 'priority_2',
        24 : 'priority_3',
        25 : 'priority_4',
        26 : 'unknown'       
    }
    
    return keys[key];
    
}



// split our redis list into usable objects
// stupid redis list is causing this sloppiness. 
function redis_split(realtime, length, divisor){    
    
    //console.log(realtime);

    var rt_length = length * divisor; //

    var spliterate = 0;

    var split = {};

    var temp_obj = {}

    var temp_count = 0;

    for(var i = 0; i < rt_length+1; i++){

        temp_obj[get_map_key(temp_count)] = realtime[0][i -1];

        temp_count++;

        if((i % divisor) == 0){
            
            split[spliterate] = temp_obj;

            spliterate++;

            temp_obj = {}

            temp_count = 0;

        }

    }

    return split;

}


////////////////////////////////////////////////////////////////////////////////
// 1. SEGMENT DATA /////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////


router.get('/segment/:from?/:to?/:download?', function(req, res){
    
    // add our offsets, assumes end user is in the same timezone as this box
    var from = add_tz_offset(new Date(req.params.from*1000));
    from.setHours(0);
    from.setMinutes(0); 

    var to = add_tz_offset(new Date(req.params.to*1000));
    to.setHours(23);
    to.setMinutes(45); 
    
    // make an array of dates we need to look query
    var date_arr = get_dates(from, to);

    
    // loop though dates and make an array of files we need to query
    var year_month = [];
    
    for(i=0; i < date_arr.length; i++){
        
        var full_date = JSON.stringify(date_arr[i]);
    
        year_month[i] = full_date.substr(1,7);
    
    }
    
    
    // distinct array of files we'll loop through
    var files = getDistinctArray(year_month);

    var points = [];
    var shedding = [];
    
    var times = [00, 15, 30, 45]; // we're only interested in times with these minutes in them

    for(i=0; i < files.length; i++){ // for each file in array

        var path = '/root/data/monthly-data/'+files[i]+'_data.txt'; // path should be declared in a config file somewhere

        var days = days_in_month(files[i].substr(0,4), files[i].substr(5,2)); // year/month
        
        var segments = days * 96; // establishes how many 15 minute segments in this date range

        if(fs.existsSync(path)){ // file exists

            // read file contents and extract power data
            var file_content = fs.readFileSync(path).toString();

            // run through all 15 minute segments within this file
            for(u=0; u<= segments; u++){

                // increment by 15 minutes
                var increment = u * 900000;
                
                var now = new Date(from.getTime() + increment).toString();
                //var year = now.substr(11,4);
                //var month = pad(numeric_month(now), 2);
                
                var year = files[i].substr(0,4);
                var month = files[i].substr(5,7);
                var day = pad(now.substr(8,2), 2);
                var hour = now.substr(16,2);
                var minute = now.substr(19,2);

                var criteria  = year+'-'+month+'-'+day+' '+hour+':'+minute; // criteria we are searching for

                // search the file for the criteria
                if(file_content.indexOf(criteria) > 0){ // criteria found
                    
                    // if between var from / var to, put it in our points array
                    if(new Date(criteria).getTime() >= from.getTime() && new Date(criteria).getTime() <= to.getTime()){

                        var point_data = c2a(file_content.substr(file_content.indexOf(criteria), 100));

                        points.push(point_data[0][1]);
   
                        if(point_data[0][6]){
                            shedding.push(point_data[0][6]);
                        }else{
                            shedding.push(0);
                        }
                        
                    }

                }else{ // criteria not found, pad zero

                    // if between var from / var to, put it in our points array
                    if(new Date(criteria).getTime() >= from.getTime() && new Date(criteria).getTime() <= to.getTime()){

                        var point_data = c2a(file_content.substr(file_content.indexOf(criteria), 100));

                        points.push(0);
                        shedding.push(0);

                    }

                }
                
            }
            
        }else{ // file doesn't exist. pad.

            // find out how many days are in the month and pad with zero's for each segment
            
            for(u=0; u<= segments; u++){
                
                var increment = u * 900000;
                
                var now = new Date(from.getTime() + increment).toString();
                
                // use the iterated file loop to figure out which month we're generating 0's for.
                var year = files[i].substr(0,4);
                var month = files[i].substr(5,7);
                var day = pad(now.substr(8,2), 2);
                var hour = now.substr(16,2);
                var minute = now.substr(19,2);
                
                var criteria  = year+'-'+month+'-'+day+' '+hour+':'+minute; // criteria we are searching for

                // if between var from / var to, put it in our points array
                if(new Date(criteria).getTime() >= from.getTime() && new Date(criteria).getTime() <= to.getTime()){
                    points.push(0);
                    shedding.push(0);
                }
                
     
            }
            
        }
        
    } // end for each file loop
    
    if(req.params.download){

        var filename = 'data.csv';
        res.attachment(filename);

        res.end(points.toString(), 'UTF-8');
        
    }else{
        
        var viewData = {
            pointlist: points
        }

        //res.render('data', viewData);
        
        res.json({ 0: points, 1: shedding });
        //res.end();
    
    }

});



////////////////////////////////////////////////////////////////////////////////
// 2. REALTIME DATA ////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
  
router.get('/realtime', function(req, res){  
    
    var length = 60;
    
    var points = [];
    
    var date_filtered = [];
    
    client.llen('revolving-data', revolving_data);

    function revolving_data(err, records){

        var start = records - (length+1); // why +1?

        client.lrange('revolving-data', start, records, function(err, data){ 

            // reverse the data and push it into an array
            //var realtime = c2a(data.reverse());
            
            //var realtime = c2a(data.reverse());
            
            // 60 records = 1 per second / 22 is the number of elements that we're pulling from the redis list
            res.json(redis_split(c2a(data), 60, 28));
            
            res.end();
            
        });

    }
    
    // error handler
    client.on("error", function (err){
        console.log("Error " + err);
    });
    
});

module.exports = router;