    // core requirments
    var express             = require('express');
    var router              = express.Router();
    var fs                  = require('fs');
    var jsdom               = require('jsdom'); 
    var $                   = require('jquery')(require('jsdom').jsdom().parentWindow);
    var redis               = require("redis");
    var request             = require("request");
    var csv                 = require('csv-parse');
    var session             = require('express-session');
    var bodyParser          = require('body-parser');


    var filePath = '/home/sgb-3p/data/';
    var client = redis.createClient();

    function graphData(){
        return 1;
    }
    
    router.get('/', function(req, res){

        // error handler
        client.on("error", function (err){
            console.log("Error " + err);
        });


        // pull revolving data and return a json object
        client.llen('revolving-data', revolvingData);
            
        
            
        function revolvingData(err, records){
            
            records = records - 1;
            
            client.lrange('revolving-data', records-1, records, function(err, data){

                csv(data, {comment: '#'}, function(err, output){
                    
                    if(typeof(output[0]) != 'undefined'){
                    
                        var out = output[0];

                        // loop & replace undefined values with 0 to prevent node crash
                        for(var i = 0; i < out.length; i++){
                            if(typeof(out[i]) == 'undefined'){
                                out[i] = 0;
                            }
                        }

                        var viewData = {
                            'rt-power-a'        : out[3],
                            'rt-power-b'        : out[4],
                            'rt-power-c'        : out[5],
                            'rt-power-pwr'      : out[2],
                            'rt-power-frq'      : out[1],
                            'rt-power-pf'       : '',

                            'rt-current-a'      : out[7],
                            'rt-current-b'      : out[8],
                            'rt-current-c'      : out[9],
                            'rt-current-pfa'    : out[15],
                            'rt-current-pfb'    : out[16],
                            'rt-current-pfc'    : out[17],

                            'rt-voltage-a'      : out[11],
                            'rt-voltage-b'      : out[12],
                            'rt-voltage-c'      : out[13],
                            'rt-voltage-lva'    : '',
                            'rt-voltage-lvb'    : '',
                            'rt-voltage-lvc'    : '',

                            'rt-system-enet'    : '',
                            'rt-system-knet'    : '',
                            'rt-system-aeng'    : '',
                            'rt-system-ct1'     : '',
                            'rt-system-ct2'     : ''
                        }
                       
                        
                        res.json(viewData);

                    }
                    
                    res.end();
                    
                });

            });
            
            
            
        } // func
      
    });

    module.exports = router;
