// core requirments
var express             = require('express');
var router              = express.Router();
var jsdom               = require('jsdom'); 
var $                   = require('jquery')(require('jsdom').jsdom().parentWindow);
var redis               = require('redis');
var client              = redis.createClient();
var csv                 = require('csv-parse');
var Q                   = require('q');
var session             = require('express-session');
var bodyParser          = require('body-parser');

function c2a(strData, strDelimiter){

    strDelimiter = (strDelimiter || ",");

    var objPattern = new RegExp(
        (
            // Delimiters.
            "(\\" + strDelimiter + "|\\r?\\n|\\r|^)" +

            // Quoted fields.
            "(?:\"([^\"]*(?:\"\"[^\"]*)*)\"|" +

            // Standard fields.
            "([^\"\\" + strDelimiter + "\\r\\n]*))"
        ),
        "gi"
        );

    var arrData = [[]];

    var arrMatches = null;

    while (arrMatches = objPattern.exec( strData )){

        var strMatchedDelimiter = arrMatches[ 1 ];

        if (
            strMatchedDelimiter.length &&
            strMatchedDelimiter !== strDelimiter
            ){

            arrData.push( [] );

        }

        var strMatchedValue;

        if (arrMatches[ 2 ]){

            strMatchedValue = arrMatches[ 2 ].replace(
                new RegExp( "\"\"", "g" ),
                "\""
                );

        } else {

            strMatchedValue = arrMatches[ 3 ];

        }

        arrData[ arrData.length - 1 ].push( strMatchedValue );
    }

    // Return the parsed data.
    return( arrData );
}


router.get('/power/:phase?/:from?/:to?', function(req, res){
    
    var length = 60;
    var points = [];
    var date_filtered = [];


    client.llen('revolving-data', revolving_data);

    function revolving_data(err, records){

        var start = records - (length+1);

        //console.log(start);
        //console.log(records);



        client.lrange('revolving-data', start, records, function(err, data){ 

            for(var i = 0; i < length; i++){

                var point = c2a(data[i],',');

                var point = point.reverse();

                var map = 2;
                
                if(req.params.phase == 'a'){
                    var map = 3;
                }
                
                if(req.params.phase == 'b'){
                    var map = 4;
                }
                
                if(req.params.phase == 'c'){
                    var map = 5;
                }
                
                if(req.params.phase == 'total'){
                    var map = 2;
                }
                
                

                // #### DATE FILTERING

                // if EITHER param is set

                if(req.params.from || req.params.to){

                    // if BOTH params set *****
                    if(req.params.from 
                            && req.params.to
                            && req.params.from != 0
                            && req.params.to != 0){

                            //console.log(point[0][0]); console.log(req.params.to);

                            if(point[0][0] >= req.params.from && point[0][0] <= req.params.to){

                                points.push(point[0][map]);

                            }


                    }else{

                        // if FROM set *****
                        if(req.params.from && req.params.from != 0){

                            if(point[0][0] >= req.params.from){

                                points.push(point[0][map]);

                            }

                        }

                        // if TO set *****
                        if(req.params.to && req.params.to != 0){

                            if(point[0][0] <= req.params.to){

                                points.push(point[0][map]);

                            }
                        }
                    }

                }else{

                    points.push(point[0][map]);

                }

            }



            var viewData = {
                pointlist: points
            }



            res.render('power', viewData);

            res.end();



        });

    }

    
    // error handler
    client.on("error", function (err){
        console.log("Error " + err);
    });
    
});


router.get('/current/:phase?/:from?/:to?', function(req, res){
    
    var length = 60;
    var points = [];
    var date_filtered = [];


    client.llen('revolving-data', revolving_data);
            
    function revolving_data(err, records){

        var start = records - (length+1);

        //console.log(start);
        //console.log(records);



        client.lrange('revolving-data', start, records, function(err, data){ 

            // return csv results

            for(var i = 0; i < length; i++){

                var point = c2a(data[i],',').reverse();

                var point = point.reverse();

                var map = 7;
                
                if(req.params.phase == 'a'){
                    var map = 7;
                }
                
                if(req.params.phase == 'b'){
                    var map = 8;
                }
                
                if(req.params.phase == 'c'){
                    var map = 9;
                }

                // #### DATE FILTERING

                // if EITHER param is set

                //console.log(point[0][0]);

                if(req.params.from || req.params.to){

                    // if BOTH params set *****
                    if(req.params.from 
                            && req.params.to
                            && req.params.from != 0
                            && req.params.to != 0){

                            //console.log(point[0][0]); console.log(req.params.to);

                            if(point[0][0] >= req.params.from && point[0][0] <= req.params.to){

                                points.push(point[0][map]);

                            }


                    }else{

                        // if FROM set *****
                        if(req.params.from && req.params.from != 0){

                            if(point[0][0] >= req.params.from){

                                points.push(point[0][map]);

                            }

                        }

                        // if TO set *****
                        if(req.params.to && req.params.to != 0){

                            if(point[0][0] <= req.params.to){

                                points.push(point[0][map]);

                            }
                        }
                    }

                }else{

                    points.push(point[0][map]);

                }

            }

            var viewData = {
                pointlist: points
            }

            res.render('power', viewData);

            res.end();


        });

    }
    
    // error handler
    client.on("error", function (err){
        console.log("Error " + err);
    });
    
});


router.get('/voltage/:phase?/:from?/:to?', function(req, res){
    
    var length = 60;
    var points = [];
    var date_filtered = [];


    client.llen('revolving-data', revolving_data);
            
    function revolving_data(err, records){

        var start = records - (length+1);
        
        client.lrange('revolving-data', start, records, function(err, data){ 

            // return csv results

            for(var i = 0; i < length; i++){

                var point = c2a(data[i],',').reverse();

                var point = point.reverse();

                var map = 11;
                
                if(req.params.phase == 'a'){
                    var map = 11;
                }
                
                if(req.params.phase == 'b'){
                    var map = 12;
                }
                
                if(req.params.phase == 'c'){
                    var map = 13;
                }

                // #### DATE FILTERING

                // if EITHER param is set

                //console.log(point[0][0]);

                if(req.params.from || req.params.to){

                    // if BOTH params set *****
                    if(req.params.from 
                            && req.params.to
                            && req.params.from != 0
                            && req.params.to != 0){

                            //console.log(point[0][0]); console.log(req.params.to);

                            if(point[0][0] >= req.params.from && point[0][0] <= req.params.to){

                                points.push(point[0][map]);

                            }


                    }else{

                        // if FROM set *****
                        if(req.params.from && req.params.from != 0){

                            if(point[0][0] >= req.params.from){

                                points.push(point[0][map]);

                            }

                        }

                        // if TO set *****
                        if(req.params.to && req.params.to != 0){

                            if(point[0][0] <= req.params.to){

                                points.push(point[0][map]);

                            }
                        }
                    }

                }else{

                    points.push(point[0][map]);

                }

            }

            var viewData = {
                pointlist: points
            }

            res.render('power', viewData);

            res.end();

        });

    }
    
    // error handler
    client.on("error", function (err){
        console.log("Error " + err);
    });
    
});




router.get('/pf/:phase?/:from?/:to?', function(req, res){
    
    
    var length = 60;
    var points = [];
    var date_filtered = [];


    client.llen('revolving-data', revolving_data);
            
    function revolving_data(err, records){

        var start = records - (length+1);
        
        client.lrange('revolving-data', start, records, function(err, data){ 

            // return csv results

            for(var i = 0; i < length; i++){

                var point = c2a(data[i],',').reverse();

                var point = point.reverse();

                var map = 15;
                
                if(req.params.phase == 'a'){
                    var map = 15;
                }
                
                if(req.params.phase == 'b'){
                    var map = 16;
                }
                
                if(req.params.phase == 'c'){
                    var map = 17;
                }

                // #### DATE FILTERING

                // if EITHER param is set

                //console.log(point[0][0]);

                if(req.params.from || req.params.to){

                    // if BOTH params set *****
                    if(req.params.from 
                            && req.params.to
                            && req.params.from != 0
                            && req.params.to != 0){

                            //console.log(point[0][0]); console.log(req.params.to);

                            if(point[0][0] >= req.params.from && point[0][0] <= req.params.to){

                                points.push(point[0][map]);

                            }


                    }else{

                        // if FROM set *****
                        if(req.params.from && req.params.from != 0){

                            if(point[0][0] >= req.params.from){

                                points.push(point[0][map]);

                            }

                        }

                        // if TO set *****
                        if(req.params.to && req.params.to != 0){

                            if(point[0][0] <= req.params.to){

                                points.push(point[0][map]);

                            }
                        }
                    }

                }else{

                    points.push(point[0][map]);

                }

            }

            var viewData = {
                pointlist: points
            }

            res.render('power', viewData);

            res.end();

        });

    }
    
    // error handler
    client.on("error", function (err){
        console.log("Error " + err);
    });
    
});






module.exports = router;








