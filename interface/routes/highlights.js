// core requirments
var express             = require('express');
var router              = express.Router();
var jsdom               = require('jsdom'); 
var $                   = require('jquery')(require('jsdom').jsdom().parentWindow);


router.get('/:from/:to', function(req, res){
    
    console.log('highlights');
    
    var viewData = {
        high: 0,
        avg: 0,
        low: 0
    }

    
    res.render('highlights', viewData);

    res.end();
    
});

module.exports = router;