/* 
 * JR Chew - jr.chew@jlmei.com
 * Stats Interface Functionality
 * 
 * All functionality for on page stats.
 * 
 */

function stats(data){

    // power
    $('.power-label-a').html('<div class="label">'+data[59].phase_a_power+' <sup>kW</sup><span class="phase">Phase A</span></div>');
    $('.power-label-b').html('<div class="label">'+data[59].phase_b_power+' <sup>kW</sup><span class="phase">Phase B</span></div>');
    $('.power-label-c').html('<div class="label">'+data[59].phase_c_power+' <sup>kW</sup><span class="phase">Phase C</span></div>');
    $('.power-label-d').html('<div class="label">'+data[59].power+' <sup>kW</sup><span class="phase">All Phases</span></div>');

    $('.rt-power-a').html('<sup><a data-graph="power-phase-a" href="#">'+data[59].phase_a_power+'kW</a></sup>');
    $('.rt-power-b').html('<sup><a data-graph="power-phase-b" href="#">'+data[59].phase_b_power+'kW</a></sup>');
    $('.rt-power-c').html('<sup><a data-graph="power-phase-c" href="#">'+data[59].phase_c_power+'kW</a></sup>');
    $('.rt-power-d').html('<sup><a data-graph="power-phase-d" href="#">'+data[59].power+'kW</a></sup>');

    // current
    $('.current-label-a').html('<div class="label">'+data[59].phase_a_current+' <sup>A</sup><span class="phase">Phase A</span></div>');
    $('.current-label-b').html('<div class="label">'+data[59].phase_b_current+' <sup>A</sup><span class="phase">Phase B</span></div>');
    $('.current-label-c').html('<div class="label">'+data[59].phase_c_current+' <sup>A</sup><span class="phase">Phase C</span></div>');

    $('.rt-current-a').html('<sup><a data-graph="current-phase-a" href="#">'+data[59].phase_a_current+'A</a></sup>');
    $('.rt-current-b').html('<sup><a data-graph="current-phase-b" href="#">'+data[59].phase_b_current+'A</a></sup>');
    $('.rt-current-c').html('<sup><a data-graph="current-phase-c" href="#">'+data[59].phase_c_current+'A</a></sup>');

    // voltage
    $('.voltage-label-a').html('<div class="label">'+data[59].phase_a_voltage+' <sup>V</sup><span class="phase">Phase A</span></div>');
    $('.voltage-label-b').html('<div class="label">'+data[59].phase_b_voltage+' <sup>V</sup><span class="phase">Phase B</span></div>');
    $('.voltage-label-c').html('<div class="label">'+data[59].phase_c_voltage+' <sup>V</sup><span class="phase">Phase C</span></div>');

    $('.rt-voltage-a').html('<sup><a data-graph="voltage-phase-a" href="#">'+data[59].phase_a_voltage+'V</a></sup>');
    $('.rt-voltage-b').html('<sup><a data-graph="voltage-phase-b" href="#">'+data[59].phase_b_voltage+'V</a></sup>');
    $('.rt-voltage-c').html('<sup><a data-graph="voltage-phase-c" href="#">'+data[59].phase_c_voltage+'V</a></sup>');

    // power factor
    $('.pf-label-a').html('<div class="label">'+data[59].phase_a_power_factor+' <sup></sup><span class="phase">Phase A</span><span class="leading-lagging">Leading</span></div></div>');
    $('.pf-label-b').html('<div class="label">'+data[59].phase_b_power_factor+' <sup></sup><span class="phase">Phase A</span><span class="leading-lagging">Leading</span></div></div>');
    $('.pf-label-c').html('<div class="label">'+data[59].phase_c_power_factor+' <sup></sup><span class="phase">Phase A</span><span class="leading-lagging">Leading</span></div></div>');

    $('.rt-pf-a').html('<sup><a data-graph="power-factor-phase-a" href="#">'+data[59].phase_a_power_factor+'</a></sup>');
    $('.rt-pf-b').html('<sup><a data-graph="power-factor-phase-b" href="#">'+data[59].phase_b_power_factor+'</a></sup>');
    $('.rt-pf-c').html('<sup><a data-graph="power-factor-phase-c" href="#">'+data[59].phase_c_power_factor+'</a></sup>');

    // windspeed
    $('.wind-label-a').html('<div class="label">'+data[59].anemometer+' <sup>MPH</sup></div>');
    $('.rt-wind-a').html('<sup><a data-graph="power-phase-c" href="#">'+data[59].anemometer+'MPH</a></sup>'); 

}