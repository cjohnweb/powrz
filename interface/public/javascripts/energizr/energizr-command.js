
	var energizr_live_commands = new energizr_commands();
	
	function refresh_data( ) {
		energizr_live_commands.current_config_id = 0;
		clearTimeout(energizr_live_commands.timer_id);
		control_units_select_list_();
	}

	window.onload = function() {
		  live_data();
	};
	
	function check_status( board, id ) {
		energizr_live_commands.command(
			"energizr_cfg_response", 
			{
				"_sgb_serial_number":board,
				"id":id
			}, 
			function(data) {
				if( data ) {
					try{
						data = JSON.parse(data);
					} catch(ex) {
						console.log('invalid json string:'+data);
					}
				}
				if(data!=null){
	        	    var status_id = data[0]['status_id'];
	        	    var sent_counter = data[0]['sent_counter'];
	        	    var sgb_command = data[0]['sgb_command'];
					if(sgb_command!=null && sent_counter>=1){
		        	    if(sgb_command.substring(0, 'engconfig{0'.length) === 'engconfig{0') {// status_id != 0 && //startWith
		            	    	//alert('started withhhh 0');
		        				energizr_live_commands.engconfig0(response);
		        				energizr_live_commands.current_config_id = 1;
		        				energizr_live_commands.attempts = 0;
		        				control_units_select_list_();
		    				}
		    				if(sgb_command=='engconfig{1}') {
		        				energizr_live_commands.engconfig1(response);
		        				energizr_live_commands.current_config_id = 2;
		        				energizr_live_commands.attempts = 0;
		        				control_units_select_list_();
		    				}
		    				if(sgb_command=='engconfig{2}') {
		        				energizr_live_commands.engconfig2(response);
		        				energizr_live_commands.current_config_id = 3;
		        				energizr_live_commands.attempts = 0;
		        				control_units_select_list_();
		    				}
		    				if(sgb_command=='engconfig{3}') {
		        				energizr_live_commands.engconfig3(response);
		        				//energizr_live_commands.current_config_id = 4;
		        				//energizr_live_commands.attempts = 0;
		        				//control_units_select_list_();
		    				}
							utils.stop_spinner("energizr_spinner");
		        	    } else {
		    				if(energizr_live_commands.attempts == energizr_live_commands.max_attempts) {
								utils.stop_spinner("energizr_spinner");
		    					console.log('NO!');
		    				} else {
		        				energizr_live_commands.attempts ++ ;
		        				energizr_live_commands.timer_id = setTimeout( 'check_response("' + board + '", ' + id + ' );', 1000);
		    				}
						}
						$('.debug_output').html(formatted);
					}
				}
		);	
	}
	
	function clear_text_box(){
		var objInput = document.getElementsByTagName("INPUT");
		console.log(objInput);
		for (var iCount = 0; iCount < objInput.length; iCount++) {
			if (objInput[iCount].type == "text" && objInput[iCount].Name != "title" ){
				objInput[iCount].value = "";
			}
		}
	}
	
	// Get a list of existing control units by title
	//* * * * * * * 
	$.get('/measurz/system/lookups/select/?_lookup=control_units&_title=Energizr&_type=energizr&_lookup_id=&_lookup_key=box_serial_number  ', function(data) {
	    ////console.log(data);
	    $("#controlUnit-placeholder").html("<label>Select a device</label>" + data);
	    $("#control_units_select_list").attr({
	        name: "control_unit_id",
	        validate: "required, non_zero_number"
	    });
	});

	// Get a list of installation types
	//* * * * * * * 
	$.get('/measurz/system/lookups/select/?_lookup=control_unit_types&_title=installation_types&_lookup_id=&_lookup_key=slug', function(data) {
	    $("#control_unit_types-placeholder").html(data);
	    $("#control_unit_types_select_list option").each(
    		function(item) {
    			var desu = this.innerHTML;
    			desu = desu.trim();
    			var _type = $("#installation_type").val( );
    			if(desu == _type) {
    				$("#installation_type_id").val( this.value );
    			}
    		}
	    ); 
	});
	
	var temp_data_timer = false;
	var temp_data_timeout = 1000;
	function live_data() {
		
		utils.spin("energizr_spinner2");
		
		clearTimeout(temp_data_timer);
		//element = document.getElementById('control_units_select_list');
	    //$("#control_unit_id").val(element.options[element.selectedIndex].value);
	    var board = '00000384';
	    board = board.trim();
    	// Get a list of existing control units by title
    	//* * * * * * *
		energizr_live_commands.command(
			"energizr_data", 
			{
				"_sgb_serial_number":board
			}, 
			function(data) {
				if( data ) {
					try{
						data = JSON.parse(data);
					} catch(ex) {
						console.log('invalid json string:' + data);
					}
				}
				if(data!=null){
					eng_data = data['results']['rows'];
					
					$("#battery_voltage").val(eng_data[0]['battery_voltage']);	
					$("#battery_current").val(eng_data[0]['battery_current']);	
					$("#battery_power").val(Math.round(eng_data[0]['battery_current']*eng_data[0]['battery_voltage']* 100) / 100);	
					$("#battery_state_of_charge").val(eng_data[0]['battery_state_of_charge']);	
					$("#battery_charging_stage").val(eng_data[0]['charging_stage']);
					//grid_relay	load_relay	renewable_relay	generator_relay
					$("#created").val(eng_data[0]['created']);	
					
					if(eng_data[0]['ac_input_voltage']!=null){	
						$("#grid_voltage").val(eng_data[0]['ac_input_voltage']);		
						$("#grid_current").val(eng_data[0]['ac_input_current']);		
						$("#grid_power").val(eng_data[0]['ac_input_power']);		
						$("#grid_frequency").val(eng_data[0]['ac_input_frequency']);		
						$("#grid_power_factor").val(eng_data[0]['ac_input_power_factor']);	
					}
					if(null!=eng_data[0]['generator_voltage']){	
						$("#generator_voltage").val(eng_data[0]['generator_voltage']);		
						$("#generator_current").val(eng_data[0]['generator_current']);		
						$("#generator_power").val(eng_data[0]['generator_power']);		
						$("#generator_frequency").val(eng_data[0]['generator_frequency']);		
						$("#generator_power_factor").val(eng_data[0]['generator_power_factor']);	
					}
					if(null!=eng_data[0]['ac_output_voltage']){
						$("#ac_load_voltage").val(eng_data[0]['ac_output_voltage']);		
						$("#ac_load_current").val(eng_data[0]['ac_output_current']);		
						$("#ac_load_power").val(eng_data[0]['ac_output_power']);		
						$("#ac_load_frequency").val(eng_data[0]['ac_output_frequency']);		
						$("#ac_load_power_factor").val(eng_data[0]['ac_output_power_factor']);	
					}
					if(null!=eng_data[0]['renewables_voltage']){
						$("#renewable_voltage").val(eng_data[0]['renewables_voltage']);		
						$("#renewable_current").val(eng_data[0]['renewables_current']);		
						$("#renewable_power").val(eng_data[0]['renewables_power']);		
						$("#renewable_frequency").val(eng_data[0]['renewables_frequency']);		
						$("#renewable_power_factor").val(eng_data[0]['renewables_power_factor']);						
					}
		        }
				var formatted = "DATA:";
				for(item in data['results']['rows']) {
    				for(key in data['results']['rows'][item]) {
    					formatted += '<b>' + key + ':</b> <span style="color:blue;">' + data['results']['rows'][item][key] + "</span>\r\n";
    				}				
				}
				$('.data_output').html(formatted);
			}
		);
    	temp_data_timer = setTimeout('live_data();', temp_data_timeout);
		utils.stop_spinner("energizr_spinner2");
	}
	
	function SubmitChange() {
		var update = false;
		element = document.getElementById('control_units_select_list');
	    $("#control_unit_id").val(element.options[element.selectedIndex].value);
	    var board = element.options[element.selectedIndex].innerHTML;
	    board = board.trim();

		//1
		
		if(energizr_live_commands.config_0 != null) {
			console.log(energizr_live_commands.config_0);
			if(!(document.getElementById('gridOn').checked == energizr_live_commands.config_0.value_00_gridOn &&
				document.getElementById('maxGridVoltage').value == energizr_live_commands.config_0.value_1_maxGridVoltage &&
				document.getElementById('minGridVoltage').value == energizr_live_commands.config_0.value_2_minGridVoltage &&
				document.getElementById('maxGridFreq').value == energizr_live_commands.config_0.value_3_maxGridFreq && 
				document.getElementById('minGridFreq').value == energizr_live_commands.config_0.value_4_minGridFreq &&
				document.getElementById('demand').value == energizr_live_commands.config_0.value_5_demand )) {				
				
					console.log('send grid Command');
					var value0 = 1;
					if(document.getElementById('gridOn').checked){ 
						value0 = 1;
					}else{
						value0 = 0;
					}
					var value1 = document.getElementById('maxGridVoltage').value;
					var value2 = document.getElementById('minGridVoltage').value;
					var value3 = document.getElementById('maxGridFreq').value;
					var value4 = document.getElementById('minGridFreq').value;
					var value5 = document.getElementById('demand').value;
					
					energizr_live_commands.command(
						'energizr_cfg_set',
						{
							"_sgb_serial_number":board,
							"cfg":0,
							"params": value0 + "," + value1 +","+ value2 +","+ value3 + "," + value4 + "," + value5
						},
				    	function(data) {
							data = data.trim();
							if( data ) {
								try{
									data = JSON.parse(data);
									update = true;
								} catch(ex) {
									console.log('invalid json string: ' + data +' ');
									console.log(ex);
								}
							}
							console.log(data);
	
			    		}
					);
					/*energizr_live_commands.current_config_id = 0;
					clearTimeout(energizr_live_commands.timer_id);
					control_units_select_list_();*/
        	    //var status_id = data[0]['status_id'];
        	    //var sent_counter = data[0]['sent_counter'];

        	    
				//if( sent_counter>=1 ) {
    			//	if(sgb_command=='engconfig{0}') {
    							
			} else {
				console.log('not send grid command');
			};
		}
		if(energizr_live_commands.config_1 != null) {
			if(!(document.getElementById('genOn').checked == energizr_live_commands.config_1.value_02_genOn &&
					document.getElementById('genStartAuto').checked == energizr_live_commands.config_1.value_01_genStartAuto &&
					document.getElementById('3wire').checked == energizr_live_commands.config_1.value_00_3wire &&
					document.getElementById('maxGenVoltage').value == energizr_live_commands.config_1.value_1_maxGenVoltage && 
					document.getElementById('minGenVoltage').value == energizr_live_commands.config_1.value_2_minGenVoltage &&
					document.getElementById('maxGenFreq').value == energizr_live_commands.config_1.value_3_maxGenFreq &&
					document.getElementById('minGenFreq').value == energizr_live_commands.config_1.value_4_minGenFreq)) {				

				console.log('send gen Command');
				var value_02_genOn = 1;
				var value_01_genStartAuto = 1;
				var value_00_3wire = 1;
				if(document.getElementById('genOn').checked){ 
					value_02_genOn = 1;
				}else{
					value_02_genOn = 0;
				}
				if(document.getElementById('genStartAuto').checked){ 
					value_01_genStartAuto = 1;
				}else{
					value_01_genStartAuto = 0;
				}
				if(document.getElementById('3wire').checked){ 
					value_00_3wire = 1;
				}else{
					value_00_3wire = 0;
				}

				var value0 = parseInt(value_00_3wire +''+ value_01_genStartAuto + value_02_genOn, 2);
				var value1 = document.getElementById('maxGenVoltage').value;
				var value2 = document.getElementById('minGenVoltage').value;
				var value3 = document.getElementById('maxGenFreq').value;
				var value4 = document.getElementById('minGenFreq').value;
				
				energizr_live_commands.command(
					'energizr_cfg_set',
					{
						"_sgb_serial_number":board,//"00000382",//
						"cfg":1,
						"params": value0 + "," + value1 +","+ value2 +","+ value3 + "," + value4
					},
			    	function(data) {
						data = data.trim();
						if( data ) {
							try{
								data = JSON.parse(data);
								update = true;
							} catch(ex) {
								console.log('invalid json string: ' + data +' ');
								console.log(ex);
							}
						}
						console.log(data);
		    		}
		    		
				);
					
			} else {
				console.log('not send gen command');
			}
		}
		if(energizr_live_commands.config_2 != null) {
			
			if(!(document.getElementById('renewOn').checked == energizr_live_commands.config_2.value_03_renewOn &&
					document.getElementById('energizrZ').checked == energizr_live_commands.config_2.value_02_energizrZ &&
					/////document.getElementById('dhcp').value == energizr_live_commands.config_2.value_01_dhcp &&
					true == energizr_live_commands.config_2.value_01_dhcp &&
					document.getElementById('enerlinkSlave').checked == energizr_live_commands.config_2.value_00_enerlinkSlave && 
					document.getElementById('nominal_rating').value == energizr_live_commands.config_2.value_1_nominal_rating)) {				

				console.log('send renew Command');

				var value_03_renewOn = 1;
				var value_02_energizrZ = 1;
				value_01_dhcp = 1;
				var value_00_enerlinkSlave = 1;
				if(document.getElementById('renewOn').checked){ 
					value_03_renewOn = 1;
				}else{
					value_03_renewOn = 0;
				}
				if(document.getElementById('energizrZ').checked){ 
					value_02_energizrZ = 1;
				}else{
					value_02_energizrZ = 0;
				}
				if(document.getElementById('enerlinkSlave').checked){ 
					value_00_enerlinkSlave = 1;
				}else{
					value_00_enerlinkSlave = 0;
				}

				var value0 = parseInt( value_00_enerlinkSlave+''+ value_01_dhcp + value_02_energizrZ + value_03_renewOn, 2);
				var value1 = document.getElementById('nominal_rating').value;
				
				energizr_live_commands.command(
					'energizr_cfg_set',
					{
						"_sgb_serial_number":board,
						"cfg":2,
						"params": value0 + "," + value1 
					},
			    	function(data) {
						data = data.trim();
						if( data ) {
							try{
								data = JSON.parse(data);
								update = true;
							} catch(ex) {
								console.log('invalid json string: ' + data +' ');
								console.log(ex);
							}
						}
						console.log(data);
		    		}
				);
			
			} else {
				console.log('not send renew command');
			}
		}
		if(energizr_live_commands.config_3 != null) {
			if(!(document.getElementById('24v').checked == energizr_live_commands.config_3.value_01_24v &&
					document.getElementById('li-ion').checked == energizr_live_commands.config_3.value_00_liion &&
					document.getElementById('minBatteryStateOfCharge').value == energizr_live_commands.config_3.value_1_minBatteryStateOfCharge &&
					document.getElementById('maxBatteryVoltage').value == energizr_live_commands.config_3.value_2_maxBatteryVoltage && 
					document.getElementById('minBatteryVoltage').value == energizr_live_commands.config_3.value_3_minBatteryVoltage &&
					document.getElementById('maxBatteryCapacity').value == energizr_live_commands.config_3.value_4_maxBatteryCapacity &&
					document.getElementById('maxBatteryCurrent').value == energizr_live_commands.config_3.value_5_maxBatteryCurrent)) {				
				console.log('send battery Command');
				var value_01_24v = 1;
				var value_00_liion = 1;
				if(document.getElementById('24v').checked){ 
					value_01_24v = 1;
				}else{
					value_01_24v = 0;
				}
				if(document.getElementById('li-ion').checked){ 
					value_00_liion = 1;
				}else{
					value_00_liion = 0;
				}

				var value0 = parseInt(value_00_liion +''+ value_01_24v, 2);
				var value1 = document.getElementById('minBatteryStateOfCharge').value;
				var value2 = document.getElementById('maxBatteryVoltage').value;
				var value3 = document.getElementById('minBatteryVoltage').value * 10;
				var value4 = document.getElementById('maxBatteryCapacity').value;
				var value5 = document.getElementById('maxBatteryCurrent').value;
				
				energizr_live_commands.command(
					'energizr_cfg_set',
					{
						"_sgb_serial_number":board,
						"cfg":3,
						"params": value0 + "," + value1 +","+ value2 +","+ value3 + "," + value4 + "," + value5
					},
			    	function(data) {
						data = data.trim();
						if( data ) {
							try{
								data = JSON.parse(data);
								update = true;
							} catch(ex) {
								console.log('invalid json string: ' + data +' ');
								console.log(ex);
							}
						}
						console.log(data);

		    		}
				);
			} else {
				console.log('not send battery command');
			};
		}
		else{
			
		}
		if(update){
			clear_text_box();
			energizr_live_commands.current_config_id = 0;
			clearTimeout(energizr_live_commands.timer_id);
			control_units_select_list_();
		}
	}
	
	//to send any command to energizr_live_commands object
	//energizr_live_commands.command('energizr_cfg_set', params={"params":"trippin"}, function(data) {console.log(data)});


