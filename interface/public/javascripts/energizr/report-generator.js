$(function() {

	var opts = {
		lines: 13, // The number of lines to draw
		length: 20, // The length of each line
		width: 10, // The line thickness
		radius: 30, // The radius of the inner circle
		corners: 1, // Corner roundness (0..1)
		rotate: 0, // The rotation offset
		direction: 1, // 1: clockwise, -1: counterclockwise
		color: '#0096FF', // #rgb or #rrggbb or array of colors
		speed: 1, // Rounds per second
		trail: 60, // Afterglow percentage
		shadow: false, // Whether to render a shadow
		hwaccel: false, // Whether to use hardware acceleration
		className: 'spinner', // The CSS class to assign to the spinner
		zIndex: 2e9, // The z-index (defaults to 2000000000)
		top: '50%', // Top position relative to parent
		left: '25%' // Left position relative to parent
	};
	var spinner5 = new Spinner(opts).spin();
	
	//datepicker function for the calendar,
    $("#datepicker1").datepicker();
    $("#datepicker2").datepicker();
  	$("#datepicker1").on('change', function() {
  		if (($("#datepicker1").val().charAt(2) === '/') && ($("#datepicker2").val().charAt(2) === '/')) {
	    	//spinner goes here
	    	$('#spinner5').append(spinner5.el);
	    	$('#btnBox').slideDown(function() {
    			$('#btnBox').css({"display":"block"});
    		});

    		$.get('/energizr/' + boardNumber + '/data/?start=' + $('#datepicker1').val() + "&end=" + $('#datepicker2').val() + "&format=csv", function(data) {
    			generateData(data);
    			$('#spinner5').find(('.spinner')).stop().hide();
    		});
  		}
    });

  	$("#datepicker2").on('change', function() {
  		if (($("#datepicker2").val().charAt(2) === '/') && ($("#datepicker1").val().charAt(2) === '/')) {
	    	$('#spinner5').append(spinner5.el);
	    	$('#btnBox').slideDown(function() {
    			$('#btnBox').css({"display":"block"});
    		});

    		$.get('/energizr/' + boardNumber + '/data/?start=' + $('#datepicker1').val() + "&end=" + $('#datepicker2').val() + "&format=csv", function(data) {
    			generateData(data);
    			$('#spinner5').find(('.spinner')).stop().hide();
    		});
  		}
    }); 


    // looks weird in safari
    function generateData(data) {
		
		$('#export').on('click',function(){

		    var blobby = new Blob([data], {type: 'text/plain'});
			var filename = $('#datepicker1').val() + "_" + $('#datepicker2').val() + "_" + boardNumber + ".csv";
			$("#export").attr({
			    'download' : filename,
			    'href': window.URL.createObjectURL(blobby),
			    'target': '_blank'
		    });
		
		});
	}
});