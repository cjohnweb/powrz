$(function(){
	var stop = false;

	var colorArr = [
		"#969696", //grey
		"rgba(0,150,255,.8)", //blue
		"rgba(249, 161, 0, .9)",  //orange
		"rgba(17, 144, 32, 0.9)",  // dark green
		'#969696',  // lt grey
		'rgba(0,212,255,.9)',  // lt blue
		'rgba(255,191,12,0.9)',  // orange
		'rgba(37,194,37,.9)',  // lt green
	];

	function powerGraph(type, colorNum, dataArr, highNum, lowNum) {
		var chartMax = 1000;
		if ((highNum >=1000) && (highNum < 2000)) {
			chartMax = 2000;
		} else if ((highNum >= 2000) && (highNum < 3000)) {
			chartMax = 3000;
		} else if ((highNum >= 3000) && (highNum < 4000)) {
			chartMax = 4000;
		} else if ((highNum >= 4000) && (highNum < 5000)) {
			chartMax = 5000;
		} else if ((highNum >= 5000) && (highNum < 6000)) {
			chartMax = 6000;
		} else if ((highNum >= 6000) && (highNum < 7000)) {
			chartMax = 7000;
		}
		$('#' + type + '-graph').sparkline(dataArr, {
			type: 'bar',
			height: 91,
			chartRangeMin: 0, 
			chartRangeMax: chartMax, 
			fillColor: true,
			barWidth: $('#battery-graph').width() * (1/59), 
			barSpacing: 1,
			disableHighlight: true,
			barColor: colorArr[colorNum],
			numberDigitGroupSep: ''
		});
	}

	//rgba(0,150,255,0.9)
	var opts = {
		lines: 13, // The number of lines to draw
		length: 20, // The length of each line
		width: 10, // The line thickness
		radius: 30, // The radius of the inner circle
		corners: 1, // Corner roundness (0..1)
		rotate: 0, // The rotation offset
		direction: 1, // 1: clockwise, -1: counterclockwise
		color: '#0096FF', // #rgb or #rrggbb or array of colors
		speed: 1, // Rounds per second
		trail: 60, // Afterglow percentage
		shadow: false, // Whether to render a shadow
		hwaccel: false, // Whether to use hardware acceleration
		className: 'spinner', // The CSS class to assign to the spinner
		zIndex: 2e9, // The z-index (defaults to 2000000000)
		top: '30%', // Top position relative to parent
		left: '50%' // Left position relative to parent
	};

	// initializes all the spinners for the settings section 
	// commented out until we can add real data
	/*var spinners = [spinner0, spinner1, spinner2, spinner3];
	for (var i = 0; i < spinners.length; i++) {
		spinners[i] = new Spinner(opts).spin();
		$('#spinner' + i).append(spinners[i].el);
	}*/


	var sectionWidth;
	function chartHeightAdjuster() {
		// if window is a certain height, the main chart adjusts
		if ($(window).height() > 895) {
			$('.chart-wrap').height('100rem');
			$('.chart-headline').css({'line-height':'70rem','top':'20rem'});
		}
		if (($(window).height() <= 895) && (($(window).height() > 800))) {
			$('.chart-wrap').height('90rem');
			$('.chart-headline').css({'line-height':'100rem','top':'0rem'});
		} 
		if ($(window).height() <= 800) {
			$('.chart-wrap').height('65rem');
			$('.chart-headline').css({'line-height':'30rem'});
		}
	}
	function chartWidthAdjuster(argument) {
		if ($(window).width() < 500) {
			if ($('#i').find('.mobile-adjust').hasClass('col-1-2')) {
				$('#i').find('.mobile-adjust').removeClass('col-1-2').addClass('col-1-1');
			} else if ($('#i').find('.mobile-adjust2').hasClass('col-1-2-lap')) {

				$('#i').find('.mobile-adjust2').removeClass('col-1-2-lap').removeClass('col-1-4').addClass('col-1-1');
			}
		} else {
			if ($('#i').find('.mobile-adjust').hasClass('col-1-1')) {
				$('#i').find('.mobile-adjust').removeClass('col-1-1').addClass('col-1-2');
			} else if ($('#i').find('.mobile-adjust2').hasClass('col-1-1')) {
				$('#i').find('.mobile-adjust2').removeClass('col-1-1').addClass('col-1-2-lap').addClass('col-1-4');
			}
		}
		if ($(window).width() < 960) {
			if ($('#io').hasClass('flex')) {
				$('#io').removeClass('flex');
			}
		} else {
			if (!$('#io').hasClass('flex')) {
				$('#io').addClass('flex');
			}
		}
	}
	chartHeightAdjuster();
	chartWidthAdjuster();
	// when window adjusts to a certain height or width
	function windowResize() {
			sectionWidth = $('.section').width() * 0.63;
			$('.section').find('canvas').css({"width":sectionWidth + "px"});
			chartHeightAdjuster();
			chartWidthAdjuster();
	}
	$(window).resize(windowResize());

	var lastBATT = 0;
	var lastGRID = 0;
	var lastAC = 0;
	var lastREN = 0;
	//var lastGEN = 0;

	var batteryHigh = 0;
	var batteryLow = 0;
	//var generatorHigh = 0; 
	//var generatorLow = 0;
	var gridHigh = 0;
	var gridLow = 0;
	var renewablesHigh = 0;
	var renewablesLow = 0;
	var acLoadHigh = 0;
	var acLoadLow = 0;
	var totalHigh = 0;

	var generatorPlaceHolder = 0;

	function fillMonitorArray(data) {
		
		function fillSectionWithData(reading, theHigh, theLow, theLast, graphVals) {
			
			if (reading !== null) {
				if (graphVals === renewablesGraphVals) {
					reading = Math.abs(parseFloat(reading));
 				}
				if (parseFloat(reading) > theHigh) {
					theHigh = parseFloat(reading);
					if (theHigh > totalHigh) {
						totalHigh = theHigh;
					}
				} else if (parseFloat(reading) < theLow) {
					theLow = parseFloat(reading);
				}
				if (graphVals === renewablesGraphVals) {
					graphVals.push(Math.abs(parseFloat(reading)));
					theLast = Math.abs(parseFloat(reading));
				} else {
					graphVals.push(parseFloat(reading));
					theLast = parseFloat(reading);
				}
			} else {
				graphVals.push(theLast);
			}
			return theLast;
			
		}
		function fillSectionWithCalculatedData(voltageReading, currentReading, theHigh, theLow, theLast, graphVals) {
			if ((voltageReading !== null) && (currentReading !== null)) {
				var power = voltageReading * currentReading;
				if (power > theHigh) {
					theHigh = parseInt(power);
					if (theHigh > totalHigh) {
						totalHigh = theHigh;
					}
				} else if (power < theLow) {
					theLow = parseInt(power);
				}		
				graphVals.push((parseInt(power)));
				theLast = parseInt(power);
			} else {
				graphVals.push(parseInt(theLast));
			}
			return theLast;
		}
		// this populates the data for battery power. 
		for (var i = data.length-1; i >= 0; i--) {
			//run fillSectionWithData for all the other power readings 
			lastBATT = fillSectionWithCalculatedData(data[i].battery_voltage, data[i].battery_current, batteryHigh, batteryLow, lastBATT, batteryGraphVals);
			lastAC = fillSectionWithCalculatedData(data[i].ac_output_voltage, data[i].ac_output_current, acLoadHigh, acLoadLow, lastAC, acLoadGraphVals);
			lastGRID = fillSectionWithData(data[i].ac_input_power, gridHigh, gridLow, lastGRID, gridGraphVals);
			lastREN = fillSectionWithData(data[i].renewables_power, Math.abs(renewablesHigh), Math.abs(renewablesLow), lastREN, renewablesGraphVals);
			//lastGEN = fillSectionWithData(data[i].generator_power, generatorHigh, generatorLow, lastGEN, generatorGraphVals);
		}

		renewablesGraphVals.splice(0,2);
		//generatorGraphVals.splice(0,2);
		acLoadGraphVals.splice(0,2);
		gridGraphVals.splice(0,2);
		batteryGraphVals.splice(0,2);

		powerGraph("battery", 3, batteryGraphVals, totalHigh, batteryLow);
		powerGraph("grid", 1, gridGraphVals, totalHigh, gridLow);
		powerGraph('ac-load', 0, acLoadGraphVals, totalHigh,acLoadLow);
		powerGraph('renewables', 2, renewablesGraphVals, totalHigh, renewablesLow);
		//powerGraph('generator', 1, generatorGraphVals, generatorHigh, generatorLow);

		return [batteryGraphVals,gridGraphVals,acLoadGraphVals,renewablesGraphVals,/*generatorGraphVals,*/batteryHigh,gridHigh,acLoadHigh,renewablesHigh/*,generatorHigh*/,batteryLow,gridLow,acLoadLow,renewablesLow/*,generatorLow*/];
	} // end of fillMonitorArray()

	// 60 values fills the chart area
	var gridGraphVals = [];
	var acLoadGraphVals = [];
	var renewablesGraphVals = [];
	/*var generatorGraphVals = [];*/
	var batteryGraphVals = [];


	if( boardNumber ) { //sanity check - this will poll and gleetch out even when no boardnumber is present
		setTimeout(function() {
			$.get('/energizr/' + boardNumber + '/data/?limit=' + 60, function(data) {
				var a = fillMonitorArray(data);
				sectionWidth = $('.section').width() * 0.63;
				$('.section').find('canvas').css({"width":sectionWidth + "px"});
			});
		}, 2000);
	}

	var barValues = [1,1,1,1];
	function initBarCharts(value, high) {
		var highestNum = 40;
		if (high > 40) {
			highestNum = high;
		} 
		$('.dynamicbar').sparkline(value, {
				type: 'bar', 
				height: 250, 
				chartRangeMin: 0, 
				chartRangeMax: highestNum, 
				fillColor: true,
				barWidth: 62, 
				barSpacing: 1,
				disableHighlight: true,
				colorMap: colorArr,
		});
		$('.dynamicbar').find('canvas').addClass('canvas-bar-chart');
	}

	var high = 0;
	for (var i = 0; i < barValues.length; i++) {
		if (barValues[i] > high) {
			high = barValues[i];
		}
	}

	initBarCharts(barValues, high);

	//data for the donut chart
	var mydonutvalues = [[10],[10],[10]];
	var donutData = [
	    {
	        value: mydonutvalues[2][0],
	        color:colorArr[2],
	        highlight: colorArr[6],
	        label: "Renewable Power",
	    },
	    {
	        value: mydonutvalues[0][0],
	        color: colorArr[3],
	        highlight: colorArr[7],
	        label: "Battery Power",
	    },
	    {
	        value: mydonutvalues[1][0],
	        color: "rgba(0,150,255,.9)",
	        highlight: "rgba(0,150,255,.7)",
	        label: "Grid Power",
	    }
	];
	//initializes the donut chart
	var options = {animationEasing : "easeOutSine"};
	var ctx = document.getElementById('donutChart').getContext('2d');
    var donutChart = new Chart(ctx).Doughnut(donutData, options);

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////
	var batteryPower, renewablesPower, gridPower, acLoadPower;
	var liveCounter = 0;
	var abcd;
	var errorCounter = 0;
	function liveData(prev) {
		if( !boardNumber ) { //sanity check - this will poll and gleetch out even when no boardnumber is present
			console.log('no board serial number present');
			return false;
		}

		if (stop === true) {return;}

		function delayedPortionOfLiveData() {
			$.get('/energizr/' + boardNumber + '/data/?limit=2', function(data) {
				abcd = data;

				if (prev) {
					if (prev[0].created !== data[0].created) {
						fillMonitorArray(data);
						sectionWidth = $('.section').width() * 0.63;
						$('.section').find('canvas').css({"width":sectionWidth + "px"});
					}
				}
				
				var update = (new Date(data[0].created)).toString().slice(0,24);
				$('#lastUpdated').text(update);
				// an array of all the inputs that need data
				var monitorArr = ['battery_state_of_charge', "battery_voltage", "battery_current", "battery_power", "charging_stage",
								"ac_input_voltage","ac_input_current","ac_input_power","ac_input_frequency","ac_input_power_factor",
								"ac_output_voltage","ac_output_current","ac_output_frequency","ac_output_power_factor",
								"renewables_voltage","renewables_current","renewables_power","renewables_frequency","renewables_power_factor",
								"generator_voltage","generator_frequency", "grid_relay", "load_relay", "renewable_relay", "generator_relay"
								];
				// only run this first part once
				if (liveCounter === 0) {
					renewablesPower = 0; gridPower = 0; acLoadPower = 0;
					liveCounter++;
				}
				if ((data[data.length-1].renewables_power !== null)) {
					renewablesPower = Math.abs(parseInt(data[data.length-1].renewables_power));
				} else if (data[data.length-2].renewables_power !== null) {
					renewablesPower = Math.abs(parseInt(data[data.length-2].renewables_power));
				}
				if ((data[data.length-1].ac_input_power !== null)) {
					gridPower = parseInt(data[data.length-1].ac_input_power);
				} else if ((data[data.length-2].ac_input_power !== null)) {
					gridPower = parseInt(data[data.length-2].ac_input_power);
				}
				if ((data[data.length-1].ac_output_voltage !== null)) {
					acLoadPower = data[data.length-1].ac_output_voltage * data[data.length-1].ac_output_current;
					acLoadPower = acLoadPower.toFixed(2);
				} else if ((data[data.length-2].ac_output_voltage !== null)) {
					acLoadPower = data[data.length-2].ac_output_voltage * data[data.length-2].ac_output_current;
					acLoadPower = acLoadPower.toFixed(2);
				}
				if ((data[data.length-1].battery_voltage !== null) && (data[data.length-1].battery_current !== null)) {
					batteryPower = data[data.length-1].battery_voltage * data[data.length-1].battery_current;
					batteryPower = batteryPower.toFixed(2);
				}

				// inserts battery power into input
				$('.battery_power').html(batteryPower);
				function domPowerFiller(power, selector) {
					if (Math.abs(power) >= 1000) {
						$(selector).html((power/1000).toFixed(2));
						$(selector).closest('.section').find('.unit-label').html('kW');
						$(selector).closest('label').find('sup').html('kW');
						$(selector).closest('.ber').find('.unit-label').html('kW');
					} else {
						$(selector).html(parseInt(power));
						$(selector).closest('.section').find('.unit-label').html('W');
						$(selector).closest('label').find('sup').html('W');
						$(selector).closest('.ber').find('.unit-label').html('W');
					}
				}
				domPowerFiller(batteryPower, '.battery_power');
				domPowerFiller(acLoadPower, '.ac_output_power');

				for (var key in data[data.length-1]) {
					for (var i = 0,monitorLen=monitorArr.length; i < monitorLen; i++) {
						if (monitorArr[i] === key) {
							var indexAlt;
							if (data[data.length-1][key] === null) {
								indexAlt = data.length-2;
							} else {
								indexAlt = data.length-1;
							}
							if (data[indexAlt][key] != null) {
								if ((key.indexOf('power') >= 0) && (key.indexOf('factor') < 0)) {
									if (Math.abs(parseInt(data[indexAlt][key])) > 1000) {
										if ((key.indexOf('output') >= 0) || (key.indexOf('renewable') >= 0)) {
											$('.' + monitorArr[i]).html((Math.abs(parseFloat(data[indexAlt][key]))/1000).toFixed(2));
										} else if (key.indexOf('renewable') >= 0) {
											$('.' + monitorArr[i]).html((Math.abs(parseFloat(data[indexAlt][key]))/1000).toFixed(2));
										} else {
											$('.' + monitorArr[i]).html((parseFloat(data[indexAlt][key])/1000).toFixed(2));
										}
										$('.' + monitorArr[i]).closest('.section').find('.unit-label').html('kW');
										$('.' + monitorArr[i]).closest('label').find('sup').html('kW');
										$('.' + monitorArr[i]).closest('.ber').find('.unit-label').html('kW');
										$('.' + monitorArr[i]).closest('.section').find('.unit-label').text('kW');
										$('.' + monitorArr[i]).closest('label').find('sup').text('kW');
										$('.' + monitorArr[i]).closest('.ber').find('.unit-label').text('kW');
									} else {
										if ((key.indexOf('output') >= 0) || (key.indexOf('renewable')) >= 0) {
											$('.' + monitorArr[i]).html(Math.abs(parseFloat(data[indexAlt][key]).toFixed(1)));
										} else {
											$('.' + monitorArr[i]).html(parseFloat(data[indexAlt][key]).toFixed(1));
										}

										$('.' + monitorArr[i]).closest('.section').find('.unit-label').html('W');
										$('.' + monitorArr[i]).closest('label').find('sup').html('W');
										$('.' + monitorArr[i]).closest('.ber').find('.unit-label').html('W');
										$('.' + monitorArr[i]).closest('.section').find('.unit-label').text('W');
										$('.' + monitorArr[i]).closest('label').find('sup').text('W');
										$('.' + monitorArr[i]).closest('.ber').find('.unit-label').text('W');
									}
								} else if (key.indexOf('relay') >= 0) {
									if (key.indexOf('grid') >= 0) {
										(data[indexAlt][key] === true) ? $('.grid_relay').find('span').text('Yes') : $('.grid_relay').find('span').text('No');
									} else if (key.indexOf('renewable') >= 0) {
										(data[indexAlt][key] === true) ? $('.renewable_relay').find('span').text('Yes') : $('.renewable_relay').find('span').text('No');
									} else if (key.indexOf('load') >= 0) {
										(data[indexAlt][key] === true) ? $('.load_relay').find('span').text('Yes') : $('.load_relay').find('span').text('No');
									} else if (key.indexOf('generator') >= 0) {
										(data[indexAlt][key] === true) ? $('.generator_relay').find('span').text('Yes') : $('.generator_relay').find('span').text('No');
									}
								} else {
									var unit = '';
									if (key.indexOf('voltage') >= 0) {unit = 'V';}
									if (key.indexOf('current') >= 0) {unit = 'A';}
									if (key.indexOf('frequency') >= 0) {unit = 'Hz';}
									if ((key.indexOf('battery_state_of_charge') >= 0) && (data[indexAlt][key] === "-999")) {data[indexAlt][key] = "0";}
									if (key.indexOf('battery_state_of_charge') >= 0) {unit = '%';}
									if (unit !== '') {
										$('.' + monitorArr[i]).find('span').text(data[indexAlt][key] + " " + unit);
									} else {
										$('.' + monitorArr[i]).find('span').text(data[indexAlt][key]);
									}
								}
							} 
						}
					}
				}
	
				//this will load generator values instead of grid values if the grid isn't connected and the generator is
				if ((data[0].generator_relay === true) && (data[0].grid_relay === false)) {
					$('.ac_input_rotation').text('GENERATOR');
					
					if ((data[0].generator_power !== null)) {
						gridPower = parseInt(data[0].generator_power);
						generatorPlaceHolder = gridPower;
					} else {
						gridPower = generatorPlaceHolder;
					}
					$('.chart_ac_input').html(gridPower);
				} else {
					$('.ac_input_rotation').text('GRID');
				}

				var barValues = [Math.abs(acLoadPower), Math.abs(gridPower),Math.abs(renewablesPower),Math.abs(batteryPower)];
				if (batteryPower <= 0) {batteryPower = 0;}
				
				//if (renewablesPower <= 0) {renewablesPower = Math.abs(renewablesPower);}
				var high = 0;
				for (var i = 0, barValuesLen=barValues.length; i < barValuesLen; i++) {
					if (barValues[i] > high) {
						high = barValues[i];
					}
				}
				initBarCharts(barValues, high);
				var donutGridPower;
				if (gridPower <= 0) {donutGridPower = 0;} else {donutGridPower = gridPower;}
				if (renewablesPower <= 0) {renewablesPower = 0.1;}
				var mydonutvalues = [[donutGridPower],[renewablesPower],[batteryPower]];
				if (high < 40) {high = 40;}
				// updates the donut chart
				donutChart.segments[0].value = parseFloat(mydonutvalues[1][0]);
				donutChart.segments[1].value = parseFloat(mydonutvalues[2][0]);
				donutChart.segments[2].value = parseFloat(mydonutvalues[0][0]);
				// Would update the first dataset's value of 'Green' to be 10
				donutChart.update();
				if (errorCounter > 0) {
					errorCounter--;
				}
				return abcd;
			})
			.fail(function(){
				errorCounter++;
				if (errorCounter >= 5){
					// maybe change this to an alert so the custormer knows
					console.log("ERROR!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
					$('.icon-container2').find('i').removeClass('icon-pause-3').addClass('icon-play-2');
					stop = true;
					return;
				}
			});
			liveData(abcd);
		} // end of delayedPortionOfLiveData

		globalTimeout = setTimeout(delayedPortionOfLiveData,2000);	
	}
	liveData();

	$('.getData').on('click', function() {
		if ($(this).find('i').hasClass('icon-play-2')) {
			$(this).find('i').removeClass('icon-play-2').addClass('icon-pause-3');
			stop = false;
			errorCounter = 0;
			liveData();
		} else {
			$(this).find('i').removeClass('icon-pause-3').addClass('icon-play-2');
			stop = true;
		}
	});
});