var SuperInput = function(options) {
	var _this = this;
	var inputSelector;
	var inputSlider;
	
	function E(a){return document.createElement(a);}
	
	//sets neccessary inputs for slider to run.  Others can be ignored.  They will be overwritten if included in options object
	this.rangemin = 0;
	this.rangemax = 100;
	this.units = '';

	// loops through options object and applies included options to this
	for (var option in options) {
		this[option] = options[option];
	}
	inputSelector = $('#' + this.selectorName);

	// creates a wrapper for the input
	this.wrapper = E('div');
	this.wrapper.className = 'input-wrapper';
	this.wrapper.id = this.selectorName + '-wrapper';
	this.wrapper.style.position = "relative";
	inputSelector.wrap(this.wrapper); 

	// creates an error popup
	this.errorNotice = E('div');
	this.errorNotice.innerHTML = "<p>Value must be <br>below " + this.rangemax + this.units + "<br>and above " + this.rangemin + this.units + " </p>";
	this.errorNotice.className = 'error-popup';
	this.errorNotice.id = this.selectorName + '-error-popup';
	this.errorNotice.style.position = "absolute";
	this.errorNotice.style.top = "-62px";
	this.errorNotice.style.left = "calc(50% - 63px)";
	this.errorNotice.style.border = "1.2rem solid rgba(74,74,74,1)";
	this.errorNotice.style.padding = "4px";
	this.errorNotice.style.background = "rgba(74,74,74,1)";
	this.errorNotice.style.width = "150px";
	this.errorNotice.style.height = "20px";
	this.errorNotice.style.opacity = "1";
	this.errorNotice.style.lineHeight = "16px";
	this.errorNotice.style.color = '#fff';
	this.errorNotice.style.borderRadius = "1rem";
	this.errorNotice.style.webkitBoxShadow = "0 5px 25px -5px rgba(0,0,0,.5)";
	this.errorNotice.style.boxShadow = "0 5px 25px -5px rgba(0,0,0,.5)";
	this.errorNotice.style.display = "none";


	$('body').find('#' + this.selectorName + '-wrapper').append(this.errorNotice);

	//creates and styles the left ticker
	this.leftTick = E('i');
	this.leftTick.className = "icon-left-open-4";
	this.leftTick.id = this.selectorName + '-lt-ticker';
	$('#' + this.selectorName + '-wrapper').prepend(this.leftTick); //no prepend in javascript
	this.leftTick.style.position = "absolute";
	this.leftTick.style.top = "5rem";
	this.leftTick.style.left = "2rem";
	this.leftTick.style.fontSize = "2rem";
	this.leftTick.style.color = "rgba(0,0,0,.5)";

	//creates and styles the right ticker
	this.rightTick = E('i');
	this.rightTick.className = "icon-right-open-4";
	this.rightTick.id = this.selectorName + '-rt-ticker';
	document.getElementById(this.selectorName + '-wrapper').appendChild(this.rightTick);
	this.rightTick.style.position = "absolute";
	this.rightTick.style.top = "5rem";
	this.rightTick.style.right = "2rem";
	this.rightTick.style.fontSize = "2rem";
	this.rightTick.style.color = "rgba(0,0,0,.5)";

	// centers the text in the inputs
	$('#' + this.selectorName).css({"text-align":"center"});
	
	//creates and inserts slider after input
	this.slider = E('div');
	this.slider.id = this.selectorName + '-slider';
	this.slider.style.margin = "0 0 20px 0";
	this.slider.style.position = "relative";
	document.getElementById(this.selectorName + '-wrapper').appendChild(this.slider);
	inputSlider = $('#' + this.selectorName + '-slider');

	// this initializes the slider
	inputSlider.noUiSlider({
		start: [ this.startpoint ],
		step: 1,
		connect: this.connected,
		range: {
			'min': this.rangemin,
			'max': this.rangemax
		},
	});
		
	// sets the initial values for the inputs
	$('#' + this.selectorName).val(this.startpoint + " " + this.units);
	// when you change slider value the input value changes
	inputSlider.on({
		slide:
		function() {
			inputSelector.css({"color":"rgba(120,120,120,0.9)"});
			inputSelector.val(parseInt(inputSlider.val()) + " " + _this.units);
			$(this).closest('.input-wrapper').find('input').focus();
			_this.errorNotice.style.display = "none";
			$('input').removeClass('error-input-border');
			if (_this.hasOwnProperty('minmax')) {
				if (((_this.minmax === "min") && (parseInt($("#" + _this.linkedTo).val()) > parseInt(inputSelector.val()))) || ((_this.minmax === "max") && (parseInt($("#" + _this.linkedTo).val()) < parseInt(inputSelector.val())))) {
					$("#" + _this.linkedTo).closest('.input-wrapper').find(".error-popup").css({"display":"none"});
				} 
			}
		}
	});

	inputSlider.on('mousedown', function() {
		$(this).closest('.input-wrapper').find('input').focus();
	});

	//when you manually change the input, the slider will move to correct spot
	inputSelector.change(function() {
		if ((parseInt(inputSelector.val()) >= _this.rangemin) && (parseInt(inputSelector.val()) <= _this.rangemax)) {
			inputSelector.css({"color":"rgba(120,120,120,0.9)"});
			_this.errorNotice.style.display = "none";
			inputSelector.val(parseInt(inputSelector.val()) + " " + _this.units);
			inputSlider.val(parseInt(inputSelector.val()), {"animate": true});
			$('input').removeClass('error-input-border');

			if (_this.hasOwnProperty('minmax')) {
				if (((_this.minmax === "min") && (parseInt($("#" + _this.linkedTo).val()) > parseInt(inputSelector.val()))) || ((_this.minmax === "max") && (parseInt($("#" + _this.linkedTo).val()) < parseInt(inputSelector.val())))) {
					$("#" + _this.linkedTo).closest('.input-wrapper').find(".error-popup").css({"display":"none"});
				} 
			}
		} else {
			_this.errorNotice.style.display = 'initial';
			_this.errorNotice.innerHTML = "<p>Value must be <br>below " + _this.rangemax + _this.units + "<br>and above " + _this.rangemin + _this.units + " </p>";
			$(inputSelector).addClass('error-input-border');
			TweenLite.to(_this.errorNotice, 1, {
				width:120, 
				opacity: 1,
				height: "auto",
				ease:Elastic.easeOut
			});
		}
	});
	
	// when you click on the arrow it increments and decrements value
	$('#' + this.selectorName).closest('.input-wrapper').on('mousedown', 'i', function(e) {
		e.preventDefault();
	});
	$('#' + this.selectorName).closest('.input-wrapper').on('click', 'i', function() {
		var selector = $(this).closest('.input-wrapper').find(inputSelector);
		if ($(this).hasClass('icon-right-open-4')) {
			if ((parseInt(inputSelector.val() + 1) >= _this.rangemin) && (parseInt(inputSelector.val()) <= _this.rangemax - 1)) {
				$(this).closest('.input-wrapper').find('input').focus();
				selector.val((parseInt(selector.val()) + 1 + " " + _this.units));
				inputSlider.val((parseInt(inputSelector.val()) + " " + _this.units), {"animate": true});
			} 
		} else {
			if (((parseInt(inputSelector.val()) - 1) >= _this.rangemin) && (parseInt(inputSelector.val()) <= _this.rangemax + 1)) {
				$(this).closest('.input-wrapper').find('input').focus();
				selector.val((parseInt(selector.val()) - 1 + " " + _this.units));
				inputSlider.val((parseInt(inputSelector.val()) + " " + _this.units), {"animate": true});
			}
		}
	});
	
}; // end of SuperInput constructor