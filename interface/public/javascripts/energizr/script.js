$(function() {
	
	// define random jquery functions throughout the code
	var jqueryObj = {
		
		showSectionData: function() {
			if ($('.section').hasClass('tall')) {
				$('.section').animate({
					height: '144px'
				}, {
					complete: function(){
						$('.section-dropdown').css({display:'none'});
					}
				});
				$('.section').removeClass('tall');		
			} else {
				$('.section').animate({
					height: '297px'
				});
				$('.section').addClass('tall');
				$('.section-dropdown').css({display:'block'});
			}
		},
		changeTabs: function() {
			$('.sidetab2').addClass('active');
			$('.sidetab1').removeClass('active');
			$('.tab-wrap').removeClass('active');
			$('#tab2').addClass('active');
		},
		fixTheTemplate: function() {
			$('#o').removeClass('sort-1-hand').addClass('sort-2-hand');
			$('#i').removeClass('sort-2-hand').addClass('sort-1-hand');
		},
		//chart spacing isn't right in Firefox, this fixes it
		checkIfFireFox: function() {
			var isFirefox = typeof InstallTrigger !== 'undefined';
			if (isFirefox) {
				$('.bar-chart').css({"margin-bottom":"10px"});
			}
		},
		datepicker3OnChange: function(){
			start = $('#datepicker3').val();
			return start;
		},
		addHiddenToGridSect: function() {
			$('#grid-section').addClass('hidden');
		},
		removeHiddenToGridSect: function() {
			$('#grid-section').removeClass('hidden');
		},
		addHiddenToGenSect: function() {
			$('#generator-section').addClass('hidden');
		},
		removeHiddenToGenSect: function() {
			$('#grid-section').removeClass('hidden');
		},
		addHiddenToRenSect: function() {
			$('#renewables-section').addClass('hidden');
		},
		removeHiddenToRenSect: function() {
			$('#renewables-section').removeClass('hidden');
		},
		getEnergizrs: function(data) {
			if (data.length > 0) {
				for (var i = 0; i < data.length; i++) {
					var link;
					if (boardNumber) {
						link = './'+data[i].board_serial_number;
					} else {
						link = './'+data[i].board_serial_number;
					}
					console.log("link",link);
					var date = new Date(data[i].created);
					var month = (date.getMonth() + 1);
					var day = (date.getDate() < 10)? "0" + date.getDate():date.getDate();
					var strToAppend = '<div style="display: flex;" class="table-row"><div style="flex: 1 1 0%"><a href="'+ link.toString() +'">' + data[i].title + '</a></div><div style="flex: 1 1 0%;"><label class="serialNum">' + data[i].board_serial_number + '</label></div><div style="flex: 1 1 0%;"><label>' + month + '/' + day + '/' + date.getFullYear() + '</label></div>';
					$('.report-chart').find('.data-table').append(strToAppend);
				}
			}
		},

		// functions below are for the SGIP incentive page which isn't going to be in version 1.0
/*		viewScreenScheduleSlideUp: function() {
			$('.view-screen-schedule').css({'display':"none"});
			$('.edit-screen-schedule').slideDown();
		},
		viewScreenScheduleOnClick: function(){
			$('.view-screen-schedule').slideUp(viewScreenScheduleSlideUp());
		},
		editScreenScheduleSlideDown: function(){
			$('.edit-screen-schedule').css({'display':"none"});
			$('.view-screen-schedule').slideDown();
		},
		editScreenScheduleOnClick: function(){
			if ($('#timepicker1').val()) {
				$('.edit-screen-schedule').slideUp(editScreenScheduleSlideDown());
				var string = $('.edit-screen-schedule').find('select').val() + " " + $('#timepicker1').val();
				$('.discharge-time').text(string);
			}
		},*/
		// used to pick an end date for the chart, not going to work until the 15 minute data is working
		/*datepicker4OnChange: function(){
			end = $('#datepicker4').val();
			return end;
		}*/
	};

	// start on the monitor tab if a board number exists
	if (boardNumber) {
		setTimeout(jqueryObj.changeTabs ,5);
	}

	$.get('/control_units', jqueryObj.getEnergizrs);

	jqueryObj.fixTheTemplate();
	jqueryObj.checkIfFireFox();

	//when you click save button
	$('#grid-save-btn').on('click', function() {var _this = this;saveSection(_this, "grid-error");});
	$('#battery-save-btn').on('click', function(){var _this = this;  saveSection(_this, "battery-error");});
	$('#generator-save-btn').on('click', function(){var _this = this;  saveSection( _this, "generator-error");});
	$('#general-save-btn').on('click', function(){var _this = this;  saveSection(_this, "general-error");});

	// change tabs in the settings page
	$('.config-tabs').on('click','li', function() { // need to transfer this;
		// change the tab
		var _this = this;
		$(this).closest('ul').find('.active-config-tab').removeClass('active-config-tab');
		$(this).addClass('active-config-tab');
		//change the body
		var panel = $("#" + $(this).text().toLowerCase() + "-config-tab");
		panel.closest('#tab-container').find('.active-tab-body').removeClass('active-tab-body');
		panel.addClass('active-tab-body');
	});

	//colors used in this array
	var colorArr = [
		'rgba(150, 150, 150, 1)', //grey
		'rgba(0, 150, 255, 0.8)', //blue
		'rgba(249, 161, 0, 0.8)', //orange
		'#35BA35', //dark green
		'#dadada', //lt grey
		'#008bff', //orange
		'rgba(255,191,12,0.9)', //blue
		'#3FCF3F' //lt green
	];

	$('.icon-container3').on('click', function() {
		if ($('.icon-container3').hasClass('addHelp')) {
			$('.icon-container3').removeClass('addHelp');
			if (!$('.help').hasClass('hidden')) {
				$('.help').addClass('hidden');
			}
		} else {
			$('.icon-container3').addClass('addHelp');
		}
	});


	//initialize 
	$('#datepicker3').datepicker();
	$('#datepicker3').on('change', jqueryObj.datepicker3OnChange);
	var start = $('#datepicker3').val();
	$('.fullView').closest('.icon-container').on('click', jqueryObj.showSectionData);

	// this is disabled until the second data is condensed to 15 minute intervals
	//$('#datepicker4').datepicker();
	//var end = $('#datepicker4').val();
	//section below is disabled until data is condensed to 15 minute stuff
	/*$('#datepicker4').on('change', jqueryObj.datepicker4OnChange());*/


	// below is for the SGIP page.  Not going to be in version 1.0
	/*$('.view-screen-schedule-btn').on('click', jqueryObj.viewScreenScheduleOnClick());
	$('.edit-screen-schedule').find('button').on('click', jqueryObj.editScreenScheduleOnClick());*/
	

	/////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////
	// DO NOT MESS with sliderArr! //////////////////////////////////////////////////////
	var sliderArr = [
		{
			index: 0,
			selectorName: 'demand',
			startpoint: 0,
			connected: 'lower',
			rangemin: 0, 
			rangemax: 9999,
			presName: "Max Demand",
			tab: "Grid",
			configValue: 5
		}, 
		{
			index: 1,
			selectorName: 'maxGridVoltage',
			startpoint: 201,
			connected: 'lower',
			rangemin: 200, 
			rangemax: 300,
			units: "V",
			minmax: "max",
			linkedTo: "minGridVoltage",
			presName: "Voltage Maximum",
			tab: "Grid",
			configValue: 1
		}, 
		{
			index: 2,
			selectorName: 'minGridVoltage',
			startpoint: 200,
			connected: 'lower',
			rangemin: 200, 
			rangemax: 300,
			units: "V",
			minmax: "min",
			linkedTo: "maxGridVoltage",
			presName: "Voltage Minimum",
			tab: "Grid",
			configValue: 2
		}, 
		{
			index: 3,
			selectorName: 'maxGridFreq',
			startpoint: 40,
			rangemin: 40,
			rangemax: 70,
			connected: 'lower',
			units: "Hz",
			minmax: "max",
			linkedTo: "minGridFreq",
			presName: "Frequency Maximum",
			tab: "Grid",
			configValue: 3
		},
		{
			index: 4,
			selectorName: 'minGridFreq',
			startpoint: 40,
			connected: 'lower',
			rangemin: 40,
			rangemax: 70,
			units: "Hz",
			minmax: "min",
			linkedTo: "maxGridFreq",
			presName: "Frequency Minimum",
			tab: "Grid",
			configValue: 4,
		}, 
		{
			index: 5,
			selectorName: 'maxBatteryCurrent',
			startpoint: 1,
			rangemin: 1,
			rangemax: 124,
			connected: "lower",
			units: "A",
			presName: "Max Current",
			tab: "Battery",
			configValue: 4,
		}, 
		{
			index: 6,
			selectorName: 'minBatteryStateOfCharge',
			startpoint: 0,
			rangemin: 0,
			rangemax: 100,
			connected: "lower",
			units: "%",
			presName: "Min State of Charge",
			tab: "Battery",
			configValue: 1,
		}, 
		{
			index: 7,
			selectorName: 'maxBatteryVoltage',
			startpoint: 35,
			rangemin: 34,
			rangemax: 67,
			connected: "lower",
			units: "V",
			minmax: "max",
			linkedTo: "minBatteryVoltage",
			presName: "Max Voltage",
			tab: "Battery",
			configValue: 2,
		}, 
		{
			index: 8,
			selectorName: 'minBatteryVoltage',
			startpoint: 34,
			rangemin: 34,
			rangemax: 67,
			connected: "lower",
			units: "V",
			minmax: "min",
			linkedTo: "maxBatteryVoltage",
			presName: "Min Voltage",
			tab: "Battery",
			configValue: 3,
		},
		{
			index: 9,
			selectorName: 'maxGenVoltage',
			startpoint: 201,
			rangemin: 200,
			rangemax: 300,
			connected: 'lower',
			units: "V",
			minmax: "max",
			linkedTo: "minGenVoltage",
			presName: "Max Voltage",
			tab: "Generator",
			configValue: 1,
		}, 
		{
			index: 10,
			selectorName: 'minGenVoltage',
			startpoint: 200,
			rangemin: 200,
			rangemax: 300,
			connected: 'lower',
			units: "V",
			minmax: "min",
			linkedTo: "maxGenVoltage",
			presName: "Min Voltage",
			tab: "Generator",
			configValue: 2,
		},
		{
			index: 11,
			selectorName: 'maxGenFreq',
			startpoint: 41,
			rangemin: 40,
			rangemax: 70,
			connected: 'lower',
			units: "Hz",
			minmax: "max",
			linkedTo: "minGenFreq",
			presName: "Max Voltage",
			tab: "Generator",
			configValue: 3,
		}, 
		{
			index: 12,
			selectorName: 'minGenFreq',
			startpoint: 40,
			rangemin: 40,
			rangemax: 70,
			connected: 'lower',
			units: "Hz",
			minmax: "min",
			linkedTo: "maxGenFreq",
			presName: "Min Frequency",
			tab: "Generator",
			configValue: 4,
		},
		{
			index: 13, selectorName: 'gridOn', startpoint: true, tab: "Grid", configValue: 0, bit:3, option:1
		},
		{
			index: 14, selectorName: 'gridOff',startpoint: false,tab: "Grid",configValue: 0, bit:3, option:0
		},
		{
			index: 15, selectorName: '48v',startpoint: true,tab: "Battery",configValue: 0, bit:2, option:1
		},
		{
			index: 16, selectorName: 'lead-acid',startpoint: false,tab: "Battery",configValue: 0, bit: 3, option:0
		},
		{
			index: 17, selectorName: 'liFePO4',startpoint: true,tab: "Battery",configValue: 0, bit:3, option:1 
		},
		{
			index: 18, selectorName: 'genOn',startpoint: false,tab: "Generator",configValue: 0, bit: 0, option:1
		},
		{
			index: 19, selectorName: 'genOff',startpoint: true,tab: "Generator",configValue: 0, bit: 0, option:0
		},
		{
			index: 20, selectorName: 'genType100',startpoint: true,tab: "Generator",configValue: 0, bit: 3, option:1
		},
		{
			index: 21, selectorName: 'genType200',startpoint: false,tab: "Generator",configValue: 0,  bit: 3, option:0
		},
		{
			index: 22, selectorName: 'genStartAuto',startpoint: true,tab: "Generator",configValue: 0, bit: 1, option:1
		},
		{
			index: 23, selectorName: 'genStartManu',startpoint: false,tab: "Generator",configValue: 0, bit: 1, option:0
		},
		{
			index: 24, selectorName: '2wire',startpoint: true,tab: "Generator",configValue: 0, bit: 2, option:0
		},
		{
			index: 25, selectorName: '3wire',startpoint: false,tab: "Generator",configValue: 0, bit: 2, option:1
		},
		{
			index: 26, selectorName: 'renewOn',startpoint: true,tab: "General",configValue: 0, bit: 0, option: 1
		},
		{
			index: 27, selectorName: 'renewOff',startpoint: false,tab: "General",configValue: 0, bit: 0, option: 0
		},
		{
			index: 28, selectorName: 'energizrZ',startpoint: true,tab: "General",configValue: 0, bit: 1, option: 1
		},
		{
			index: 29, selectorName: 'enerlinkSlave',startpoint: true,tab: "General",configValue: 0, bit: 3, option: 1
		},
	];

	//////////////// Do NOT MESS with sliderArr above! //////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////

	// loops through the sliderarr and creates new SuperInputs
	var inst = [];
	for (var i = 0; i < sliderArr.length; i++) {
		if (sliderArr[i].configValue !== 0) {
			inst.push(new SuperInput(sliderArr[i]));
		}
	}

	function hideMonitoringSection(num, currentTab) {
		if (num % 2 === 0) {
			if (currentTab === 0) {
				$('#grid-section').fadeOut(jqueryObj.addHiddenToGridSect());
			}
			if (currentTab === 1) {
				$('#generator-section').fadeOut(jqueryObj.addHiddenToGenSect());
			}
			if (currentTab === 2) {
				$('#renewables-section').fadeOut(jqueryObj.addHiddenToRenSect());
			}
		} else {
			if (currentTab === 0) {
				$('#grid-section').fadeIn(jqueryObj.removeHiddenToGridSect());
			}
			if (currentTab === 1) {
				$('#generator-section').fadeIn(jqueryObj.removeHiddenToGenSect());
			}
			if (currentTab === 2) {
				$('#renewables-section').fadeIn(jqueryObj.removeHiddenToRenSect());
			}
		}
	}

	function binaryShrinker(data, currentTab) {
		var keyValArray = data.split(';');
		var binaryVal = keyValArray[0].split('=');
		binaryVal = binaryVal[1];  // this is the digit you want

		var tab;
		function Grid() {
			tab = 0;
			if (binaryVal == 1) {
				sliderArr[13].startpoint = true; // gridOn
				sliderArr[14].startpoint = false;
				$('#gridOn').prop('checked', true);
				$('#gridOff').prop('checked', false);
			} else {
				sliderArr[13].startpoint = false;
				sliderArr[14].startpoint = true; // gridOff
				$('#gridOff').prop('checked', true);
				$('#gridOn').prop('checked', false);
			}
		} 

		function Generator() {
			tab = 1;
			var strBinaryGenInfo = Number(binaryVal).toString(2);
			if(strBinaryGenInfo.length < 3){
				pad = "0000";
				strBinaryGenInfo = (pad + strBinaryGenInfo).slice(-pad.length);	
			}
			if (strBinaryGenInfo[3]== 1){
				sliderArr[18].startpoint = true;
				sliderArr[19].startpoint = false;
				$('#genOn').prop('checked', true);
				$('#genOff').prop('checked', false);
			} else {
				sliderArr[18].startpoint = false;
				sliderArr[19].startpoint = true;
				$('#genOn').prop('checked', false);
				$('#genOff').prop('checked', true);
			}
			if (strBinaryGenInfo[2]== 1) {
				sliderArr[22].startpoint = true;
				sliderArr[23].startpoint = false;
				$('#genStartAuto').prop('checked', true);
				$('#genStartManu').prop('checked', false);
			} else {
				sliderArr[22].startpoint = false;
				sliderArr[23].startpoint = true;
				$('#genOn').prop('checked', false);
				$('#genOff').prop('checked', true);
			}
			if (strBinaryGenInfo[1]== 1) {
				sliderArr[25].startpoint = true; 
				sliderArr[24].startpoint = false; 
				$('#3wire').prop('checked', true);
				$('#2wire').prop('checked', false);
			} else {
				sliderArr[25].startpoint = false; 
				sliderArr[24].startpoint = true; 
				$('#3wire').prop('checked', false);
				$('#2wire').prop('checked', true);
			}
			if (strBinaryGenInfo[0]== 1) {
				sliderArr[20].startpoint = false;  
				sliderArr[21].startpoint = true;
				$('#genType100').prop('checked', false);
				$('#genType200').prop('checked', true);
			} else {
				sliderArr[20].startpoint = true;  
				sliderArr[21].startpoint = false; 
				$('#genType100').prop('checked', true);
				$('#genType200').prop('checked', false);
			}
		} 

		function Renewables() {
			tab = 2;
			var strBinaryRenInfo = Number(binaryVal).toString(2);
			if(strBinaryRenInfo.length < 4){
				pad = "0000";
				strBinaryRenInfo = (pad + strBinaryRenInfo).slice(-pad.length);
			}
			if(strBinaryRenInfo[3]== '1'){
				sliderArr[26].startpoint = true;
				sliderArr[27].startpoint = false;
				$('#renewOn').prop('checked', true);
				$('#renewOff').prop('checked', false);
			} else {
				sliderArr[27].startpoint = true;
				sliderArr[26].startpoint = false;
				$('#renewOff').prop('checked', true);
				$('#renewOn').prop('checked', false);
			}
			if(strBinaryRenInfo[2]== '1'){
				sliderArr[28].startpoint = true;
				$('#energizrZ').prop('checked', true);
			} else {
				sliderArr[28].startpoint = false;
				$('#energizrZ').prop('checked', false);
			}
			if (strBinaryRenInfo[0]== '1') {
				sliderArr[29].startpoint = true;
				$('#enerlinkSlave').prop('checked', true);
			} else {
				sliderArr[29].startpoint = false;
				$('#enerlinkSlave').prop('checked', false);
			}
		} 

		function Battery() {
			var strBinaryBatteryInfo = Number(binaryVal).toString(2);
			if (strBinaryBatteryInfo.length < 2){
				pad = "00";
				strBinaryBatteryInfo = (pad + strBinaryBatteryInfo).slice(-pad.length);	
			}
			if (strBinaryBatteryInfo[1]== 1){
				sliderArr[15].startpoint = false;
				$('#24v').prop('checked', true);
				$('#48v').prop('checked', false);
			} else {
				sliderArr[15].startpoint = true;
				$('#24v').prop('checked', false);
				$('#48v').prop('checked', true);
			}
			if (strBinaryBatteryInfo[0]== 1){
				sliderArr[17].startpoint = true;
				sliderArr[16].startpoint = false;
				$('#liFePO4').prop('checked', true);
				$('#lead-acid').prop('checked', false);
			} else {
				sliderArr[16].startpoint = true;
				sliderArr[17].startpoint = false;
				$('#liFePO4').prop('checked', false);
				$('#lead-acid').prop('checked', true);
			}
		}

		//no more neverending if else statements, declare function for logic and call it by name - so much easier to work with really 
		var valid_functions = {
			"Grid":Grid,
			"Generator":Generator,
			"Renewables":Renewables,
			"Battery":Battery
		};
		valid_functions[currentTab](); 

		hideMonitoringSection(binaryVal, tab);
	}


	// function to shrink processes in runit()
	function shrinkNonBinaryLogic(data, currentTab) { 
		// don't repeat yourself just decreases the amount of code written in  shrinkNOnBinaryLogic()
		function dry() {
			sliderArr[j].startpoint = dataArr[i];
			if (sliderArr[j].units) {
				if ((sliderArr[j].selectorName === "minBatteryVoltage") || (sliderArr[j].selectorName === "maxBatteryVoltage")) {
					$('#' + sliderArr[j].selectorName).val(parseInt(sliderArr[j].startpoint)/10 + " " + sliderArr[j].units);
					$('#' + sliderArr[j].selectorName + '-slider').val((parseInt(sliderArr[j].startpoint)/10 + " " + sliderArr[j].units), {"animate": true});
				} else {
					$('#' + sliderArr[j].selectorName).val(parseInt(sliderArr[j].startpoint) + " " + sliderArr[j].units);
					$('#' + sliderArr[j].selectorName + '-slider').val((parseInt(sliderArr[j].startpoint) + " " + sliderArr[j].units), {"animate": true});
				}
			} else {
				$('#' + sliderArr[j].selectorName).val(parseInt(sliderArr[j].startpoint));
				$('#' + sliderArr[j].selectorName + '-slider').val((parseInt(sliderArr[j].startpoint)), {"animate": true});
			}
		} //end of dry()
		var dataArr = data.split(';');
		for (var i = 1; i < dataArr.length-1; i++) {
			var equalsSign = dataArr[i].indexOf('=');
			dataArr[i] = dataArr[i].slice((equalsSign+1));
			for (var j = 0; j < sliderArr.length; j++) {
				if (sliderArr[j].tab === currentTab) {
					if ((i === 1) && (sliderArr[j].configValue === 1)) {
						dry();
					} else if ((i === 2) && (sliderArr[j].configValue === 2)) {
						dry();
					} else if ((i === 3) && (sliderArr[j].configValue === 3)) {
						dry();
					} else if ((i === 4) && (sliderArr[j].configValue === 4)) {
						dry();
					} else if ((i === 5) && (sliderArr[j].configValue === 5)) {
						dry();
					}
				}
			}
		}
	} // end of shrinkNonBinaryLogic

	function runit(data, currentTab) {	
		binaryShrinker(data, currentTab);
		shrinkNonBinaryLogic(data, currentTab);
	}
 
 	// this asdfsdfadf
	function get_config(config, id, func) {
		if(!boardNumber) {
			return false;
		}
		$.get('/energizr/' + boardNumber + '/config/'+id, function(data) {
			if (data !== null) {
				runit(data, config);
				// tell the spinner to stop here
				$('#spinner'+id).find(('.spinner')).stop().hide();
			} else {
				/*setTimeout( 
					function() { 
						get_config(config, id); 
					}, 
					2000
				); //just keep trying forever i guess?*/
			}
		})
		.always(function() {
		    if (func) {
		    	func();
		    }
		});
		$( "input[type='checkbox']" ).prop({
			disabled: true
		});
		$( "input[type='radio']" ).prop({
			disabled: true
		});

	}

	var helpSelectorArr = [
		{selector:'.report-chart', on: 1},
		{selector: '.icon-container', on: 0}, 
		{selector: '.icon-container2', on: 0},
		{selector: '.icon-container3', on: 0},
		{selector: '.help-container', on: 0},
		{selector: '.config-tab-container', on: 0}, 
		{selector: '.donut-chart-container', on: 0}, 
		{selector: '.bar-chart-container', on: 0}, 
		{selector: '.monitor-headline2', on: 0}, 
		{selector: '.report-help', on: 0}
	 ];

	 function removeHiddenClass(nameSel, index) {
	 	$(nameSel).on('mouseover', function() {
	 		var _this = this;
	 		if ($('.icon-container3').hasClass('addHelp')) {
		 		var thisHelp = $(this).find('.help');
		 		if ($(this).find('.help').hasClass('hidden')) {
					thisHelp.fadeIn(function() {
						thisHelp.removeClass('hidden');
					});
				}
		 		for (var j = 0; j < helpSelectorArr.length; j++) {
		 			if (j !== index) {
		 				if (!$(helpSelectorArr[j].selector).find('.help').hasClass('hidden')) {
							$(helpSelectorArr[j].selector).find('.help').addClass('hidden');
							$(helpSelectorArr[j].selector).find('.help').fadeOut();
						}
		 			}
		 		}
	 		}
	 	}).on('mouseleave', function() {
			var thisHelp = $(this).find('.help');
			if (!$(this).find('.help').hasClass('hidden')) {
				thisHelp.addClass('hidden');
				thisHelp.fadeOut();
			}
		});
	 }

	 for (var i = 0; i < helpSelectorArr.length; i++) {
	 	removeHiddenClass(helpSelectorArr[i].selector, i);
	 }

	// This section is temporarily cancelled because these config commands are all screwed up
/*	get_config('Grid', 0, function(){
		get_config('Generator', 1, function() {
			get_config('Renewables', 2, function() {
				get_config('Battery', 3); 
			});
		});
	});*/


//////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////

	function notify(inputVal, index, arr) {
		function displayError(minmaxSymbol) {
			inst[index].errorNotice.style.display = 'initial';
			$('#' + inst[index].selectorName).addClass('error-input-border');
			inst[index].errorNotice.innerHTML = sliderArr[index].presName + " cant save because max "+ minmaxSymbol +" min";
			TweenLite.to(inst[index].errorNotice, 1, {
				width:120, 
				opacity: 1,
				height: "auto",
				ease:Elastic.easeOut
			});
			return sliderArr[index].presName + ' cant save because max '+ minmaxSymbol +' min<br>';
		}
		function createVal() {
			sliderArr[index].startpoint = parseInt(inputVal);
			arr.push({
				configValue:"val"+sliderArr[index].configValue,
				value:sliderArr[index].startpoint
			});
		}
		if ((parseInt(inputVal) != sliderArr[index].startpoint)) { // if value on tab is different on page, it triggers section below
			if ((parseInt(inputVal) < sliderArr[index].rangemin) || (parseInt(inputVal) > sliderArr[index].rangemax)) { //checks if value is above the min allowed and below the max
				//trigger error 1
				return sliderArr[index].presName + ' cant save because its out of range<br>';
			} else {
				if (sliderArr[index].minmax === "min") {
					if ((parseInt(inputVal) > parseInt($("#" + sliderArr[index].linkedTo).val()))) { //makes sure the value is below the corresponding max value 
						var a = displayError('>');
						return a;
					} else {
						createVal();
					}
				} else if (sliderArr[index].minmax === "max") {
					if ((parseInt(inputVal) < parseInt($("#" + sliderArr[index].linkedTo).val()))) {  // makes sure the max is above the min value
						//trigger error 3
						var a = displayError('<');
						//return a;
						return sliderArr[index].presName + ' cant save because max < min<br>';
					} else { 
						createVal();
					}
				} else {
					createVal();
				}
			}
		} else { // this section is triggered if value wasn't changed or the same as when it started
			createVal();
		} 
	}

	function saveSection(elem, errorBoxName) {
		var tab;
		var inputArray = $("#" + elem.id).closest('.config-tab-body').find('input[type="text"]'); //array that pulls the ids on each individual tab
		var valueArr = [];
		// this section below tells postConfig how many values to expect
		if ($("#" + elem.id).closest('.config-tab-body')[0].id === 'grid-config-tab') {
			tab = 0;
		} else if ($("#" + elem.id).closest('.config-tab-body')[0].id === 'battery-config-tab') {
			tab = 3;
		} else if ($("#" + elem.id).closest('.config-tab-body')[0].id === 'generator-config-tab') {
			tab = 1;
		} else if ($("#" + elem.id).closest('.config-tab-body')[0].id === 'general-config-tab') {
			tab = 2; 

		}
		var errorArray = [];
		var counter = 0;
		for (var i = 0; i < sliderArr.length; i++) { 			// loops through sliderArr 
			for (var j = 0; j < inputArray.length; j++) {      			// loops through tab
				if (inputArray[j].id === sliderArr[i].selectorName) {	// if they are referencing the same thing 
					var inputVal = inputArray[j].value;					// create an inputVal variable and set it to the value of the tab
					var temporaryErrorHolder = notify(inputVal, i, valueArr); 			
					if (temporaryErrorHolder) {
						errorArray[counter] = temporaryErrorHolder;
						counter++;
					}
				}
			}
		}
		// var showing = 0;
		var radioValues = ['#gridOn','#genOn','#genStartAuto',
							'#3wire','#genType100','#renewOn',
							'#energizrZ','#enerlinkSlave',
							'#24v','#liFePO4'
						];
		
		if (errorArray.length === 0) {
			var binaryVal = 0;
			if (tab == 0) {
				if ($('#gridOn').is(':checked')) {
					binaryVal++;
				} 
			} else if (tab == 1) {
				if ($("#genOn").is(":checked")) {
					binaryVal++;
				} 
				if ($('#genStartAuto').is(":checked")) {
					binaryVal += 10;
				} 
				if ($('#3wire').is(":checked")) {
					binaryVal += 100;
				}
				if ($('#genType100').is(":checked")) {
					binaryVal += 1000;
				}
			} else if ( tab == 2) {
				if ($("#renewOn").is(":checked")) {
					binaryVal++;
				} 
				if ($('#energizrZ').is(":checked")) {
					binaryVal += 10;
				} 
				if ($('#enerlinkSlave').is(":checked")) {
					binaryVal += 1000;
				}
			} else if ( tab == 3 ) {
				if ($("#24v").is(":checked")) {
					binaryVal++;
				} 
				if ($('#liFePO4').is(":checked")) {
					binaryVal += 10;
				} 
			}
			
			binaryVal = binaryVal.toString();

			var val0 = parseInt(binaryVal, 2).toString();
			
			// this hides the monitoring for specific sections if they aren't connected
			hideMonitoringSection(val0, tab);

			for (var i = 0; i < valueArr.length; i++) {
				if (valueArr[i].configValue === "val1") { //you need the value and the config value
					var val1 = valueArr[i].value;
				} else if (valueArr[i].configValue === "val2") { //you need the value and the config value
					var val2 = valueArr[i].value;
				} else if (valueArr[i].configValue === "val3") { //you need the value and the config value
					var val3 = valueArr[i].value;
				} else if (valueArr[i].configValue === "val4") { //you need the value and the config value
					var val4 = valueArr[i].value;
				} else if (valueArr[i].configValue === "val5") { //you need the value and the config value
					var val5 = valueArr[i].value;
				}
			}

			//console.log([tab, val0,val1.toString(),val2.toString(),val3.toString(),val4.toString(),val5.toString()]);
			if (val5) {
				postConfig(tab,val0.toString(),val1.toString(),val2.toString(),val3.toString(),val4.toString(),val5.toString());
			} else if (val4) {
				postConfig(tab,val0.toString(),val1.toString(),val2.toString(),val3.toString(),val4.toString());
			} else if (val3) {
				postConfig(tab,val0.toString(),val1.toString(),val2.toString(),val3.toString());
			} else if (val2) {
				postConfig(tab,val0.toString(),val1.toString(),val2.toString());
			} else if (val1) {
				postConfig(tab,val0.toString(),val1.toString());
			} else if (val0) {
				postConfig(tab, val0.toString());
			}
			

			// posts the data to the database... with a get request
			// we need to figure out what to do with the "binary" values
			function postConfig(configSection, val0, val1, val2, val3, val4, val5) {
				
				var opts = {
					lines: 13, // The number of lines to draw
					length: 20, // The length of each line
					width: 10, // The line thickness
					radius: 30, // The radius of the inner circle
					corners: 1, // Corner roundness (0..1)
					rotate: 0, // The rotation offset
					direction: 1, // 1: clockwise, -1: counterclockwise
					color: '#0096FF', // #rgb or #rrggbb or array of colors
					speed: 1, // Rounds per second
					trail: 60, // Afterglow percentage
					shadow: false, // Whether to render a shadow
					hwaccel: false, // Whether to use hardware acceleration
					className: 'spinner', // The CSS class to assign to the spinner
					zIndex: 2e9, // The z-index (defaults to 2000000000)
					top: '30%', // Top position relative to parent
					left: '50%' // Left position relative to parent
				};
				
				//initialize spinners
				/*var spinnerPost0 = new Spinner(opts).spin();
				var spinnerPost1 = new Spinner(opts).spin();
				var spinnerPost2 = new Spinner(opts).spin();
				var spinnerPost3 = new Spinner(opts).spin();*/
				if (configSection === 0) {
					$('#spinner0').append(spinnerPost0.el);
				} else if (configSection === 1) {
					$('#spinner1').append(spinnerPost1.el);
				} else if (configSection === 2) {
					$('#spinner2').append(spinnerPost2.el);
				} else if (configSection === 3) {
					$('#spinner3').append(spinnerPost3.el);
				}

				var valString = ';0=' + val0;
				if ((configSection === 2)) {
					valString += ';1=1400'; // the 1400 is for the nominal rating which we are no longer using.  It still needs to be included though.
				}
				if ((configSection === 0)){
					valString += ';1=' + val1 + ';2=' + val2 + ';3=' + val3 + ';4=' + val4 + ';5=' + val5;
				} else if ((configSection === 1) || (configSection === 3)) {
					valString += ';1=' + val1 + ';2=' + val2 + ';3=' + val3 + ';4=' + val4;
				}
				valString += ";";
				var posted = $.post('/energizr/' + boardNumber + '/config/' + configSection + valString, function(data) {
					alert('saved');
				}).fail(function() {
					alert('error: not saved');
				});
				posted.always(function() {
					$('#spinner' + configSection).find(('.spinner')).stop().hide();
				});
			}
		}
	}

	

	$('.calendar-container').find('.generate-chart').on('click',function(){
		$('.chart-wrap').find('i').remove();
		
		// numdays is equal to 1 until the data gets condensed to 15 minute data from second data
		//var numdays = (((new Date(end).getTime()) - (new Date(start).getTime())) + 86400000)/86400000;
		var numdays = 1;

		var opts = {
			lines: 13, // The number of lines to draw
			length: 20, // The length of each line
			width: 10, // The line thickness
			radius: 30, // The radius of the inner circle
			corners: 1, // Corner roundness (0..1)
			rotate: 0, // The rotation offset
			direction: 1, // 1: clockwise, -1: counterclockwise
			color: '#000', // #rgb or #rrggbb or array of colors
			speed: 1, // Rounds per second
			trail: 60, // Afterglow percentage
			shadow: false, // Whether to render a shadow
			hwaccel: false, // Whether to use hardware acceleration
			className: 'spinner', // The CSS class to assign to the spinner
			zIndex: 2e9, // The z-index (defaults to 2000000000)
			top: '50%', // Top position relative to parent
			left: '50%' // Left position relative to parent
		};

		// initializes the spinner
		var spinner4 = new Spinner(opts).spin();
		$('.chart-wrap').append(spinner4.el);

		// pulls the date for the triceraplots
		$.get('/energizr/' + boardNumber + '/data/?start=' + start + "&end=" + start + "&numdays=" + numdays, function(data) {
			

			/*function reducePeaks(index) {
				for (var i = 0; i < data[index].length; i++) {
					if ((data[index][i] > ((data[index][i-12] * 4))) && (data[index][i] > ((data[index][i+12] * 4)))) {
						data[index][i] = data[index][i+12];
						console.log('i',i);
					}
				}	
			}
			reducePeaks(0);
			reducePeaks(1);
			reducePeaks(2);*/

		
			/*for (var i = 0; i < (data[3].length); i++) {
				if (data[3][i] > 100) {data[3][i] = 100;}
				data[3][i] = (data[3][i]/100) * data[5];
			}*/
			



			var lastDate;
			var newDate;
			var counter = 0;
			var blankSpaceArr = [];
			for (var i = 0; i < data[4].length; i++) {
				if (counter !== 0) {
					newDate = data[4][i]/1000;
					// do your work here and then change last date to this date 
					dateDiff = newDate - lastDate;
					
					if (dateDiff >= 60) {
						blankSpaceArr.push(data[4][i]);
						for (var j = 0; j < Math.floor(dateDiff); j++) {
							data[0].splice(i,0,0);
							data[1].splice(i,0,0);
							data[2].splice(i,0,0);
							data[3].splice(i,0,0);
							data[4].splice(i,0,data[4][i]);
						}
					}
					lastDate = newDate;
				} else {
					lastDate = data[4][i].created/1000; //this equals seconds
				}
				counter++;
			}

			var chartyChart = new Triceraplots({
				crunch_file : '/javascripts/triceraplots/triceraplots-worker.js', // worker thread
				container: document.getElementById('triceraChart'), // html element 
				scrubber : true, // draw scrubber at bottom?
				data : [data[0], data[1], data[2], data[3]], 
				dates : data[4], // UTC dates in MS integers
				autosort : false, // Stack charts based on avg value
				y_axis_align : "right",
				label_format : function(val){
					if(val > 1000){
						return (val / 1000).toFixed(3) + " kW";
					}else{
						return (val).toFixed(0) + " W";
					}
				},
			});

			function triceraplotsColors() {
				$('.chart-wrap').css({"background-color":"rgba(235, 235, 235, 0.8)"});
				var c = document.getElementById('data_index_3');
				var ctx = c.getContext('2d');
				
				var gradient = ctx.createLinearGradient(0, 0, 0, ($('#data_index_3').height() - 64));
				gradient.addColorStop(0, "rgba(0,212,255,.75)");
	            gradient.addColorStop(1, "rgba(0,151,255,.7)");
				chartyChart.colors[1] = gradient;
				chartyChart.plot_styles[1] = "fill";

				chartyChart.colors[0] =  colorArr[0];//blue
				chartyChart.plot_styles[0] = "fill";
				chartyChart.colors[2] = colorArr[2];  //orange
				chartyChart.plot_styles[2] = "line";
				chartyChart.colors[3] = colorArr[7];  // lt green
				chartyChart.plot_styles[3] = "line";	
				chartyChart.colors[4] = colorArr[4];  // lt grey
				chartyChart.colors[5] = colorArr[5];  // lt blue
				chartyChart.colors[6] = colorArr[6];  // orange
				chartyChart.colors[7] = colorArr[3];  // dk green
			}
			triceraplotsColors();

			$('.chart-wrap').find(('.spinner')).stop().hide();
		});
	});

});