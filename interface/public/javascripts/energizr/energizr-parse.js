		engconfig0: function( data ) {
			console.log("engconfig0 data: ");
			var keyValArray = data.split(';');
			for(var i = 0 ; i < keyValArray.length ; i++) {
				var keyVal = keyValArray[i].split('=');
				
				if(i == 0) {
					if(keyVal[1] = 1){
						energizr_commands.config_0.value_00_gridOn = true;
						document.getElementById("gridOn").checked = true;
						document.getElementById("gridOff").checked = false;
					} else {
						energizr_commands.config_0.value_00_gridOn = false;
						document.getElementById("gridOff").checked = true;
						document.getElementById("gridOn").checked = false;
					}
				}
				if(i == 1) {
					$("#maxGridVoltage").val( keyVal[1] );
					energizr_commands.config_0.value_1_maxGridVoltage = keyVal[1]; 
				}
				else if(i == 2) {
					$("#minGridVoltage").val( keyVal[1] );
					energizr_commands.config_0.value_2_minGridVoltage = keyVal[1]; 
				}
				else if(i == 3) {
					$("#maxGridFreq").val( keyVal[1] );
					energizr_commands.config_0.value_3_maxGridFreq = keyVal[1];
				}
				else if(i == 4) {
					$("#minGridFreq").val( keyVal[1] );
					energizr_commands.config_0.value_4_minGridFreq = keyVal[1]; 
				}
				else if(i == 5) {
					$("#demand").val( keyVal[1] );
					energizr_commands.config_0.value_5_demand = keyVal[1]; 
				}else{
					//console.log(energizr_commands.config_0);
					//energizr_commands.config_0. = keyVal[1];
				}
			}
						
			/*console.clear();*/ //commented out so I can see what's happening
			console.log(energizr_commands.config_0);
			console.info('the response object is now the current config state - saved in memory');
     		
			return;
		},
		engconfig1: function( data ) {
			var keyValArray = data.split(';');
			for(var i = 0 ; i < keyValArray.length ; i++) {
				var keyVal = keyValArray[i].split('=');
				
				if(i == 0) {
					
					var strBinaryGenInfo = Number(keyVal[1]).toString(2);//binary

					if(strBinaryGenInfo.length < 3){
						pad = "0000";
						strBinaryGenInfo = (pad + strBinaryGenInfo).slice(-pad.length);
						
					}
					strBinaryGenInfo = strBinaryGenInfo.split('');
					for (var j = 0; j < strBinaryGenInfo.length; j++) {
						
						if(strBinaryGenInfo[3]== 1){
							energizr_commands.config_1.value_03_genOn = true;
							document.getElementById("genOn").checked = true;
							document.getElementById("genOff").checked = false;
						}
						else{
							energizr_commands.config_1.value_03_genOn = false;
							document.getElementById("genOn").checked = false;
							document.getElementById("genOff").checked = true;
						}
						if(strBinaryGenInfo[2]== 1){
							energizr_commands.config_1.value_02_genStartAuto = true;
							document.getElementById("genStartAuto").checked = true;
							document.getElementById("genStartManu").checked = false;
						}
						else{
							energizr_commands.config_1.value_02_genStartAuto = false;
							document.getElementById("genStartAuto").checked = false;
							document.getElementById("genStartManu").checked = true;
						}
						if(strBinaryGenInfo[1]== 1){
							energizr_commands.config_1.value_01_3wire = true;
							document.getElementById("3wire").checked = true;
							document.getElementById("2wire").checked = false;
						}
						else{
							energizr_commands.config_1.value_01_3wire = false;
							document.getElementById("3wire").checked = false;
							document.getElementById("2wire").checked = true;
						}
						if(strBinaryGenInfo[0]== 1){
							energizr_commands.config_1.value_00_genType100 = false;
							document.getElementById("genType100").checked = false;
							document.getElementById("genType200").checked = true;
						}
						else{
							energizr_commands.config_1.value_00_genType100 = true;
							document.getElementById("genType100").checked = true;
							document.getElementById("genType200").checked = false;
						}
					}
				}
				if(i == 1) {
					$("#maxGenVoltage").val( keyVal[1] );
					energizr_commands.config_1.value_1_maxGenVoltage = keyVal[1];
				}
				else if(i == 2) {
					$("#minGenVoltage").val( keyVal[1] );
					energizr_commands.config_1.value_2_minGenVoltage = keyVal[1];
				}
				else if(i == 3) {
					$("#maxGenFreq").val( keyVal[1] );
					energizr_commands.config_1.d = keyVal[1];
					energizr_commands.config_1.value_3_maxGenFreq = keyVal[1];
				}
				else if(i == 4) {
					$("#minGenFreq").val( keyVal[1] );
					energizr_commands.config_1.value_4_minGenFreq = keyVal[1];
				}
				else{
					
				}
			}
		},
		engconfig2: function( data ) {
			var keyValArray = data.split(';');
			for(var i = 0 ; i < keyValArray.length ; i++) {
				
				var keyVal = keyValArray[i].split('=');
				
				if(i == 0) {
					
					var strBinaryRenInfo = Number(keyVal[1]).toString(2);
					if(strBinaryRenInfo.length < 4){
						pad = "0000";
						strBinaryRenInfo = (pad + strBinaryRenInfo).slice(-pad.length);
					}
					strBinaryRenInfo = strBinaryRenInfo.split('');
					
					for (var j = 0; j < strBinaryRenInfo.length; j++) {
						if(strBinaryRenInfo[3]== 1){
							energizr_commands.config_2.value_03_renewOn = true;
							document.getElementById("renewOn").checked = true;
							document.getElementById("renewOff").checked = false;
						}
						else{
							energizr_commands.config_2.value_03_renewOn = false;
							document.getElementById("renewOn").checked = false;
							document.getElementById("renewOff").checked = true;
						}
						if(strBinaryRenInfo[2]== 1){
							energizr_commands.config_2.value_02_energizrZ = true;
							document.getElementById("energizrZ").checked = true;
						}
						else{
							energizr_commands.config_2.value_02_energizrZ = false;
							document.getElementById("energizrZ").checked = false;
						}
						if(strBinaryRenInfo[1] == 1){
							energizr_commands.config_2.value_01_dhcp = true;
						}
						else{
							energizr_commands.config_2.value_01_dhcp = true;//???
						}
						if(strBinaryRenInfo[0]== 1){
							energizr_commands.config_2.value_00_enerlinkSlave = true;
							document.getElementById("enerlinkSlave").checked = true;
						}
						else{
							energizr_commands.config_2.value_00_enerlinkSlave = false;
							document.getElementById("enerlinkSlave").checked = false;
						}
					} 
				}
				if(i == 1) {
					$("#nominal_rating").val( keyVal[1] );
					energizr_commands.config_2.value_1_nominal_rating = keyVal[1];
				}
			}
		},
		engconfig3: function( data ) {
			var keyValArray = data.split(';');
			for(var i = 0 ; i < keyValArray.length ; i++) {
				
				var keyVal = keyValArray[i].split('=');
				
				if(i == 0) {
					var strBinaryBatteryInfo = Number(keyVal[1]).toString(2);
					if(strBinaryBatteryInfo.length < 2){
						pad = "00";
						strBinaryBatteryInfo = (pad + strBinaryBatteryInfo).slice(-pad.length);
					}
					strBinaryBatteryInfo = strBinaryBatteryInfo.split('');
					for (var j = 0; j < strBinaryBatteryInfo.length; j++) {
						if(strBinaryBatteryInfo[1]== 1){
							energizr_commands.config_3.value_01_24v = true;
							document.getElementById("24v").checked = true;
							document.getElementById("48v").checked = false;
						}
						else{
							energizr_commands.config_3.value_01_24v = false;
							document.getElementById("24v").checked = false;
							document.getElementById("48v").checked = true;
						}
						if(strBinaryBatteryInfo[0]== 1){
							energizr_commands.config_3.value_00_liion = true;
							document.getElementById("li-ion").checked = true;
							document.getElementById("lead-acid, flooded").checked = false;
						}
						else{
							energizr_commands.config_3.value_00_liion = false;
							document.getElementById("li-ion").checked = false;
							document.getElementById("lead-acid, flooded").checked = true;
						}
					} 
				}
				if(i == 1) {
					$("#minBatteryStateOfCharge").val( keyVal[1] );
					energizr_commands.config_3.value_1_minBatteryStateOfCharge = keyVal[1];
				}
				if(i == 2) {
					try{
						$("#maxBatteryVoltage").val( keyVal[1]/10 );
						energizr_commands.config_3.value_2_maxBatteryVoltage = keyVal[1] /10;//Make sure in the compare time
					}catch(exception){
						console.log("Max Battery voltage" + exception);
					}
				}
				if(i == 3) {
					try{
						$("#minBatteryVoltage").val( keyVal[1]/10 );
						energizr_commands.config_3.value_3_minBatteryVoltage = keyVal[1] /10;//Make sure in the compare time
					}catch(exception){
						console.log("Min Battery voltage" + exception);
					}
				}
				if(i == 4) {
					$("#maxBatteryCapacity").val( keyVal[1] );
					energizr_commands.config_3.value_4_maxBatteryCapacity = keyVal[1] ;
				}
				if(i == 5) {
					$("#maxBatteryCurrent").val( keyVal[1] );
					energizr_commands.config_3.value_5_maxBatteryCurrent = keyVal[1] ;
				}
			}
		},
		