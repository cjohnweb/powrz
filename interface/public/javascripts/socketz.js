var Socket = function(){
	var _this = this;
	
	this.socket = io.connect('/');
	this.queue = {};

	this.reqId = function(l){
		var id = "";
		while(l--){id += (1 + (Math.random() * 999)).toString()[0];}
		return parseInt(id);
	}

	this.callback = function(callback) {
		callback["reqId"] = this.reqId(8);
		this.registerCallback(callback, callback["function"]);
		return callback;
	}

	this.registerCallback = function(req, callback){
		this.queue[req.action] = (this.queue[req.action])?this.queue[req.action]:{};
		this.queue[req.action][req.reqId] = callback;
	}

	this.runCallback = function(res){
        if(!typeof(res)=='object') {
			return {"runCallback":"no object provided"};
		}
        if(!_this.queue.hasOwnProperty(res["action"])){
			return {"runCallback":"no action registered"};
		}
		try {
			this.queue[res.action][res.reqId](res.body); //this is the function call
			delete this.queue[res.action][res.reqId];
		} catch( e ) {
			return {"runCallback":e};
		}	
		return {"runCallback":true};
	}

	this.get = function(action, params, callback){
		var req = {
			action : action,
			params : params,
			reqId : this.reqId(8)
		}
		this.registerCallback(req, callback);
		this.socket.emit('get', req);
	};
        
	this.post = function(action, params, callback){
            
		var req = {
			action : action,
			params : params,
			reqId : this.reqId(8)
		}
		this.registerCallback(req, callback);
		this.socket.emit('post', req);
	};

	this.res = function(res){
		_this.socket.emit('res', res);
	}
        
	/* Universal response handler */
	this.socket.on('res', 
		function(res){
			_this.runCallback(res);
		}
	);
	
	//global broadcast agent	
	this.broadcast = function(broadcast){
                //console.log(broadcast);
            
		_this.socket.emit('broadcast', broadcast);
		return {"broadcast":true};
	}
	this.socket.on('broadcast', 
		function(broadcast){
                    
			_this.runCallback(broadcast);
		}
	);


};









