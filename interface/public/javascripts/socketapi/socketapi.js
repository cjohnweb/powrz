/*
	Super basic wrapper
	Simply put this lib mimics jQuery's $.get and $.post methods
	It creates a queue of requests with callbacks
*/
var Socket = function(){
	var _this = this;
	this.socket = io.connect('/',{'force new connection': true,'forceNew':true});
	this.queue = {};
	this.debug = false;
	
	/* 
		Could probably just increment a counter. 
		There was a reason I used this but I can't remember what it was.
	*/
	this.reqId = function(l){	
		var id = "";
		while(l--){id += (1 + (Math.random() * 8)).toString()[0];}
		return parseInt(id);
	}

	this.registerCallback = function(req, callback){
		_this.queue[req.action] = (this.queue[req.action])?this.queue[req.action]:{};
		_this.queue[req.action][req.reqId] = callback;
	}

	this.runCallback = function(res){
		this.queue[res.action][res.reqId](res.body);

		/* 
			WEIRD
			If you do NOT set a delay on this the browser will delete the callback and then attempt to run it.
			Is this a v8 optimization??

			WEIRDER
			Suddenly it stopped doing this.... idk wtf is going on.
		*/

		//window.setTimeout(function(){delete _this.queue[res.action][res.reqId];},0)
		delete _this.queue[res.action][res.reqId];
	}

	this.get = function(action, callback){
		
		var req = {
			action : action,
			reqId : this.reqId(8)
		}

		if(_this.debug === true){
			req.sent = Date.now();
			req.method = 'GET';
		}

		callback = (callback)?callback:function(){};
		this.registerCallback(req, callback);

		this.socket.emit('get', req);
	};

	this.post = function(action, body, callback){
		body = (body)?body:{};
		callback = (callback)?callback:function(){};
		var req = {
			action : action,
			reqId : this.reqId(8),
			body : body
		}

		if(_this.debug === true){
			req.sent = Date.now();
			req.method = 'POST';
		}

		// Make the body object optional for blank posts
		if(typeof body === 'function' && !callback){callback = body;}

		this.registerCallback(req, callback);
		this.socket.emit('post', req);
	};

	/* Universal response handler */
	this.responseHandler = function(res){

		if(_this.debug === true){
			if(res.method === 'GET'){
				console.groupCollapsed("%c"+res.method+" <- "+res.action, "color:green")
			}
			if(res.method === 'POST'){
				console.groupCollapsed("%c"+res.method+" -> "+res.action, "color:blue");
			}

			try{
				console.log("Request ID : %c"+res.reqId, "color:purple");
				console.log("round trip : %c"+(Date.now() - res.sent)+"ms", "color:purple");
				console.log("message length : %c"+res.body.length+" characters", "color:purple")
				var msg = (function(){
					try{
						return JSON.parse(res.body)
					}catch(err){
						return res.body
					}
				})
			}catch(e){
				console.warn(e)
			}

			console.groupEnd()
		}

		_this.runCallback(res);
	}
	
	this.socket.on('res', this.responseHandler);
};

var socket = new Socket();
socket.debug = false;