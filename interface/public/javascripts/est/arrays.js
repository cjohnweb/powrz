/*
	+---------------------------------------------------------------------------+
	|	Some array processors													|
	+---------------------------------------------------------------------------+
*/




function array_skip(arr, skip) {
	var l = arr.length;
	var r = [];
	for (var i = skip; i < l; i += skip) {
		r.push(arr[i]);
	}
	return r;
}

function aggregate_array_average(arr, points_per) {

	var l = arr.length;
	var r = [];

	var counter = 1;
	var tmp_agg = 0;

	for (var i = 0; i < l; i++) {

		tmp_agg += arr[i];

		if (counter === points_per) {
			var avg = tmp_agg / points_per;
			r.push(avg);
			counter = 1;
			tmp_agg = 0;
		} else {
			counter++;
		}
	}

	return r;
}

function aggregate_array_max(arr, points_per) {
	var l = arr.length;
	var r = [];

	var counter = 1;
	var tmp_agg = [];
	var tmp_max = 0;

	for (var i = 0; i < l; i++) {

		tmp_agg.push(arr[i]);

		if (counter === points_per) {
			var max = Math.max.apply(Math, tmp_agg);
			r.push(max);
			counter = 1;
			tmp_agg = [];

		} else {
			counter++;
		}

	}

	delete tmp_agg;
	return r;
}