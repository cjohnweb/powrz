/*
    request handler
    message.data = {
        action : '',
        sent : Date.now(),
        id : 12345678,
        body : {} || '' || [] || 0123
    }
*/

self.onmessage = function(message){
    var response = {
        sent : message.data.sent,
        action : message.data.action,
        id : message.data.id,
        body : false  
    };


    // update worker
    if(message.data.action === "update"){
        update(message.data.body);
        response.body = 'Sumulator Updated';
        response.completed = Date.now();
        postMessage(response)
    }

    // get
    if(message.data.action === "get"){
        response.body = simulator;
        response.completed = Date.now();
        postMessage(response)
    }    

    if(message.data.action === "optimize"){
        system.optimize(response);
    }

    // Simulate using some logic
    if(message.data.action === "simulate"){
        simulate[message.data.body.logic]();
        response.body = {
            shaved_interval_data : simulator.shaved_interval_data,
            soc_data : simulator.soc_data
        };

        response.completed = Date.now();
        postMessage(response)
    }


}


var simulate = {};
var system = {};

var simulator = {
    dates : [],
    pv_data : [],
    interval_data : [],
    interval_data_with_pv : [],
    adjusted_interval_data : [],
    shaved_interval_data : [],
    soc_data : [],
    system : {},
    rate_plan : {},
    apply_pv : true
}

/*
    Update the worker thread
*/
function update(O){
    for(var K in O){
        for(var k in O[K]){ 
            simulator[K][k] = O[K][k];
        }
    }
}



/*
    Power management Logic
    ---------------------------------------------------------------------------------
*/

/*
    Optimizing Baselines

    1.) set baselines to a point where you know they will fail (peak - (inverter capacity + 1))
    2.) test baselines, increment on fail and re test
    3.) if a simulation makes it through a year and does not fail, send the updated baselines to the main thread
*/
system.optimize = function(response){
    console.log(response)
    /* reset baselines
    --------------------------------*/
    var peaks = [0,0,0,0,0,0,0,0,0,0,0,0];
    var data = (simulator.interval_data_with_pv.length === simulator.interval_data.length)?simulator.interval_data_with_pv:simulator.interval_data;
    for(var i = 0; i < data.length; i++){
        var month = new Date(simulator.dates[i]).getMonth();
        var power = data[i];
        peaks[month] = (power > peaks[month])?power:peaks[month];
        simulator.system.baseline[month] = peaks[month] - (simulator.system.inverter.capacity + 1);
    }
    
    // kick off test
    system.test_baselines(response);
}

system.test_baselines = function(response){
    simulate.shave({
        on_baseline_fail : function(i){
            var date = new Date(simulator.dates[i]);
            var month = date.getMonth();
            simulator.system.baseline[month]+=2;
            setTimeout(function(){
                system.test_baselines(response);
            },0);
        },
        on_baseline_success : function(){
            
            console.log("completed")
            console.log(response)

            response.body = {
                system : simulator.system
            };

            response.completed = Date.now();
            postMessage(response)
        }
    })
}

/* 
    Demand Shaving 
*/
simulate.shoulder = function(){}

simulate.shave = function(options){

    var apply_pv = simulator.apply_pv;

    var inverter_capacity = simulator.system.inverter.capacity;
    var battery_capacity = simulator.system.storage.capacity;
    var RTE = 1 - ((1 - simulator.system.storage.round_trip_efficiency) * .5);

    // - for the sake of readability...
    var dates = simulator.dates;
    var interval_data = simulator.interval_data;
    var interval_data_with_pv = simulator.interval_data_with_pv;
    var pv_data = simulator.pv_data;

    var min_soc = battery_capacity - (battery_capacity * simulator.system.storage.max_dod);
    var current_soc = battery_capacity; // starting the year at a full charge
    var current_dod = 0; // Starts at 0 because we're starting FULLY CHARGED

    // If we're applying PV data and the PV data set is valid we want to shave the interval_data_with_pv series otherwise we can just shave the original data
    var data_to_shave = (apply_pv === true && pv_data.length === interval_data.length) ? interval_data_with_pv : interval_data;

    // loop through the year
    simulator.shaved_interval_data = [];
    simulator.soc_data = [];
    for (var datapoint = 0; datapoint < data_to_shave.length; datapoint++) {

        // DATE
        var current_date = new Date(dates[datapoint]);
        var current_month = current_date.getMonth();
        var current_hour = current_date.getHours();
        var day_is_backed_up = true;

        // POWER
        var max_load = simulator.system.baseline[current_month];
        var power = (apply_pv === true) ? data_to_shave[datapoint] * 1 : data_to_shave[datapoint] * 1;
        var power_needed = power * (1 + (1 - RTE));
        var current_load = power; // converted to watts

        /*
            Off peak & batteries arent full & current load is less than specified max
        */
        if (current_load < max_load && current_soc < battery_capacity) {

            var difference = max_load - current_load;
            var chargeby = (difference > inverter_capacity) ? inverter_capacity : difference;

            var dod = battery_capacity - current_soc;

            if ((chargeby * .25) > dod) {
                chargeby = dod;
            }

            current_soc += chargeby * .25;
            current_load += chargeby;

        }

        // Off peak & batteries aren't full & current load is greater than specified max
        if (current_load >= max_load) {

            var difference = current_load - max_load; // Power needed to maintain baseline
            var dischargeby = (difference > inverter_capacity) ? inverter_capacity : difference;

            if ((dischargeby * .25) > current_soc) {
                dischargeby = current_soc;
            }

            // if discharging would bring us below the min state of charge, alter that shit
            var future_soc = current_soc - (dischargeby * .25);
            if (future_soc < min_soc) {
                var diff = min_soc - future_soc;
                dischargeby -= diff * 4;
            }

            current_soc -= dischargeby * .25;
            current_load -= dischargeby;

        }

        // these aint no magic batteries. Yo ass gonna pay extra 4 dat.
        if (current_soc > battery_capacity) {
            current_soc = battery_capacity;
        }

        simulator.shaved_interval_data.push(current_load);
        simulator.soc_data.push(current_soc);

        if(options){
            if(options.on_baseline_fail){
                if(current_load > max_load){
                    return options.on_baseline_fail(datapoint)
                }
            }
        }
    }

    if(options){
        if(options.on_baseline_success){
            options.on_baseline_success(options.response)
        }
    }
}