var projectsTable;

/*	
	Customer Name
----------------------------------------------------------*/
$("#customer_name").unbind('change').on('change', function(){
	est.customer_name = this.value;
	est.save({
		customer_name : this.value
	});
})

$("#fork_project").unbind('click').on('click', function(){
	socket.post('/est/projects', {
		title : "Forked From "+est.title,
		system : JSON.stringify(est.system),
		rate_plan : est.rate_plan.id,
		customer_name : est.customer_name,
		proposals : JSON.stringify(est.proposals),
		interval_data : JSON.stringify(est.interval_data),
		pv_data : JSON.stringify(est.pv_data)
	},function(data){
		try{			
			data = JSON.parse(data)[0];
			window.location.href = '/est/'+data.id;
		}catch(err){
			console.warn('Unable to fork project!')
			console.log(error)
		}
	})
})

// /*	
// 	Delete project
// ----------------------------------------------------------*/
// $("#delete_projects").unbind('click').on('click', function(){
// 	var queue = [];

// 	$('.project_checkbox').each(function(){
// 		if($(this)[0].checked === true){
// 			queue.push($(this).data('id'));
// 		}
// 	})

// 	$.post('/est/projects/delete', {ids : queue}, function(){
// 		projectsTable.fetch(function(){
// 			projectsTable.render();
// 		})
// 	})
// })

// /* Datatable for projects 
// ----------------------------------------------------------*/
// projectsTable = new DataTable({
// 	container: $("#projectsTable")[0],
// 	navigationContainer : document.getElementById('projectsTableNav'),
// 	dataSource : "/est/projects",
// 	socket : socket,
// 	paginate : 10,
// 	filter : ['title', 'system', 'solar', 'proposals', 'id'],
// 	labels: {
// 		'title' : 'Title',
// 		'system' : 'Gridz',
// 		'solar' : 'Solar',
// 		'proposals' : 'Proposals'
// 	},
// 	header : {
// 		id : function(){
// 			var wrap = ele('div.right');
// 			var checkbox = ele("input");
// 			checkbox.type= 'checkbox';
// 			checkbox.onchange = function(){
// 				var checked = this.checked;
// 				$(".project_checkbox").each(function(){
// 					$(this)[0].checked = checked;
// 					$(this).change();
// 				})
// 			}
// 			wrap.appendChild(checkbox)
// 			return wrap
// 		}
// 	},
// 	row : function(row){
// 		var el = document.createElement('div');
// 		if(row){		
// 			if(row.id === est.id){
// 				el.className = "active";
// 			}
// 		}
// 		return el;
// 	},
// 	column : {
// 		solar : function(row){
// 			var wrap = document.createElement('div')
// 			wrap.innerHTML = kwhlabel(row.system.solar.capacity)
// 			return wrap
// 		},
// 		title : function(row){
// 			var wrap = ele("div")
// 			var link = ele("a")
// 			link.innerHTML = row.title;
// 			link.href = '/est/'+row.id;
// 			wrap.appendChild(link)
// 			return wrap
// 		},
// 		system : function(row){
// 			var wrap = document.createElement('div');
// 			wrap.innerHTML = row.system.inverter.capacity+" / "+row.system.storage.capacity;
// 			return wrap
// 		},
// 		proposals : function(row){
// 			var wrap = document.createElement('span');
// 			wrap.innerHTML = (row.proposals === null)? 0 : row.proposals.length;
// 			return wrap;
// 		},
// 		utility : function(row){
// 			var wrap = document.createElement('span');
// 			wrap.innerHTML = (row.utility === null)? "n/a" : row.utility;
// 			return wrap;
// 		},
// 		id : function(row){
// 			var wrap = ele('div.right');
// 			var input = ele('input.project_checkbox#project_'+row.id)
// 			input.type = 'checkbox'
// 			input.name = 'project_'+row.id;
// 			input.id = 'project_'+row.id;
// 			input.setAttribute("data-id", row.id)
			

// 			if(est.id){
// 				if(est.id !== row.id){	
// 					wrap.appendChild(input)
// 				}
// 			}

// 			if(row.created_by !== user.id){
// 				wrap.innerHTML += " created by :"+row.created_by;
// 			}

// 			return wrap
// 		}
// 	}
// })

// projectsTable.fetch(function(){
// 	projectsTable.render();
// })

/* Input events 
----------------------------------------------------------*/
/*	project title */
$("#project_title").on('change', function(){
	est.title = this.value;
	est.save({
		title : est.title
	}, function(){
		projectsTable.fetch(function(){
			projectsTable.render();
		})
	});
})