var projectsTable;
$(document).ready(function(){

	/*	Spawn a new project and refresh the table
	----------------------------------------------------------*/
	$("#spawn_project").unbind('click').on('click', function(){
		socket.post('/est/projects', {},function(data){
			projectsTable.fetch(function(){
				projectsTable.render();
			})
		})
	})

	/*	Delete project
	----------------------------------------------------------*/
	$("#delete_projects").unbind('click').on('click', function(){
		var queue = [];

		$('.project_checkbox').each(function(){
			if($(this)[0].checked === true){
				queue.push($(this).data('id'));
			}
		})

		$.post('/est/projects/delete', {ids : queue}, function(){
			projectsTable.fetch(function(){
				projectsTable.render();
			})
		})
	})

	/*	Datatable for projects 
	----------------------------------------------------------*/
	projectsTable = new DataTable({
		container: $("#projectsTable")[0],
		navigationContainer : document.getElementById('projectsTableNav'),
		dataSource : "/est/projects",
		socket : socket,
		paginate : 10,
		filter : ['title', 'system', 'solar', 'proposals', 'id'],
		labels: {
			'title' : 'Title',
			'system' : 'Gridz',
			'solar' : 'Solar',
			'proposals' : 'Proposals'
		},
		header : {
			id : function(){
				var wrap = ele('div.right');
				var checkbox = ele("input");
				checkbox.type= 'checkbox';
				checkbox.onchange = function(){
					var checked = this.checked;
					$(".project_checkbox").each(function(){
						$(this)[0].checked = checked;
						$(this).change();
					})
				}
				wrap.appendChild(checkbox)
				return wrap
			}
		},
		row : function(row){
			var el = document.createElement('div');
			return el;
		},
		column : {
			solar : function(row){
				var wrap = document.createElement('div')
				wrap.innerHTML = kwhlabel(row.system.solar.capacity)
				return wrap
			},
			title : function(row){
				var wrap = ele("div")
				var link = ele("a")
				link.innerHTML = row.title;
				link.href = '/est/'+row.id;
				wrap.appendChild(link)
				return wrap
			},
			system : function(row){
				var wrap = document.createElement('div');
				wrap.innerHTML = row.system.inverter.capacity+" / "+row.system.storage.capacity;
				return wrap
			},
			proposals : function(row){
				var wrap = document.createElement('span');
				wrap.innerHTML = (row.proposals === null)? 0 : row.proposals.length;
				return wrap;
			},
			utility : function(row){
				var wrap = document.createElement('span');
				wrap.innerHTML = (row.utility === null)? "n/a" : row.utility;
				return wrap;
			},
			id : function(row){
				var wrap = ele('div.right');
				var input = ele('input.project_checkbox#project_'+row.id)
				input.type = 'checkbox'
				input.name = 'project_'+row.id;
				input.id = 'project_'+row.id;
				input.setAttribute("data-id", row.id)
				wrap.appendChild(input);
				return wrap
			}
		}
	})

	projectsTable.fetch(function(){
		projectsTable.render();
	})

	/*
		HACK ATTACK!
		--------------------------------------------------------------------------------------------------------
		Known bug in google chrome causes the font to render at like 999999px size until the page is refreshed.
		Supposedly the issue is fixed in the next version BUT I've seen this issue a couple times since chrome started
		using the DirectWrite font rendering engine.

		I'm able to manually correct the issue by changing the font size of an element with chrome's element inspector.
		This simply sets and then resets the font size in hopes of forcing the browser to correct the font thing.

		Fortunately for us we're using rem everywhere so changing the html's font size should suffice.
	*/
	if(window.chrome){
		$('html').css('font-size', '5px'); 
		window.setTimeout(function(){
			$('html').css('font-size', '4px');
		}, 10)
	}
})