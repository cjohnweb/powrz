/*
	Simple data table script.

	This plugin has only enough style in it to form an actual grid.
	It simply generates a table-like html structure inside a container document.createElementment 
	so styling is entirely up to you.

	It can...
		*	Filter the data you feed it
		*	Label the data you feed it
		*	Generate custom row document.createElementments
		*	Generate custom column document.createElementments

	It will eventaully....
		*	Paginate results
		*	Sort by column
	
	This is how...
		
		Basic setup
			options = {
				container: document.getdocument.createElementmentById('container')
				data : [
					{k:v,k:v,k:v},
					{k:v,k:v,k:v}
				]
			}
		
		Optional settings
		filter : []
		labels : []
		input : {
			id : function(row){
				var document.createElementment  = document.createdocument.createElementment("wtf-ever-you-want")
				document.createElementment.onclick = function(){console.log(row.id)}
				return document.createElementment;
			}
		sdocument.createElementct : function(row){
			console.log(row.id);
		}
*/
var DataTable = function(options){

	var _this = this;
	this.labels = {};
	for(var o in options){_this[o] = options[o]};

	this.createColumn = function(i, key){
		try{
			/* if a function for creating the column exists, run it */
			if(_this.column){
				if(typeof _this.column[key] === 'function'){
					var column = _this.column[key](_this.data[i]);
					return column;
				}
			}

			/* Otherwise use a text input */
			var vanilla = document.createElement('span');
			vanilla.innerHTML = _this.data[i][key];
			return vanilla;
		}catch(err){
			var col = document.createElement('span');
			col.innerHTML = " - ";
			return col;
		}
	}
	this.createHeader = function(){
		var row = document.createElement('div');
		//row.style.display = 'flex';
		row.className += ' row';
		var set = (_this.filter)?_this.filter : Object.keys(_this.data[0]);

		for(var _i = 0; _i < set.length; _i++){
			var key = set[_i];
			
			// Default
			var col = document.createElement('div');
			col.className += ' col-1-'+set.length;
			col.innerHTML = (_this.labels[key])?_this.labels[key]:key;
			
			// Override?
			if(_this.header){
				if(typeof _this.header[key] === 'function'){
					col = _this.header[key]();
				}
			}
			
			// col.style.flex = '1';
			row.appendChild(col);
		}

		return row;
	}
	this.createRow = function(i){
		var row = (typeof _this.row === 'function')? _this.row(_this.data[i]) : document.createElement('div');
		//row.style.display = 'flex';
		row.className += ' row';
		var set = (_this.filter)?_this.filter : Object.keys(_this.data[i]);

		for(var _i = 0; _i < set.length; _i++){
			var col = document.createElement('div');
			//col.style.flex = '1';
			col.className += ' col-1-'+set.length;
			col.appendChild(_this.createColumn(i, set[_i]));
			row.appendChild(col);
		}

		return row;
	}
	
	/*
		get dat dat
	*/
	this.fetch = function(cb){
		if(_this.socket){
			_this.socket.get(_this.dataSource, function(result){
				_this.data = JSON.parse(result);
				if(typeof cb ==='function'){cb()}
			});
		}else{
			if($){
				$.get(_this.dataSource, function(result){
					_this.data = result;
					if(typeof cb ==='function'){cb()}
				});
			}
		}
	}

	this.generateNavigation = function(){

		if(!_this.navigationContainer){return;}
		_this.navigationContainer.style.display = 'flex';

		_this.navigationContainer.innerHTML = '';
		var pages = Math.ceil(_this.data.length / _this.paginate);

		if(pages < 2){
			_this.navigationContainer.style.opacity = "0";
			return
		}

		if(_this.navigationContainer.style.opacity != 1){
			_this.navigationContainer.style.opacity = "1";
		}

		for(var i = 0; i < pages; i++){
			(function(i){
				var element = document.createElement("div")
				element.style.flex = "1";
				if(i === _this.page){
					element.className += "lift-shadow active strong";
				}
				element.innerHTML = ((i * 1) + 1);

				element.onclick = function(){
					_this.render(i);
				}

				_this.navigationContainer.appendChild(element)
			})(i)
		}
	}


	/*
		append & prepend
		Add records to the top or bottom of the table.
		without destroying and re-rendering the table.

		prepend record to the data array
		r = createRow(0)
		container pre
	*/
	var duration = 600;
	this.append = function(){};
	this.prepend = function(records){

		// records is a single record
		if(!Array.isArray(records) && typeof records === 'object'){
			
			if(_this.page){
				if(_this.page > 0){
					_this.render(0)
				}
			}

			_this.data.unshift(records)
			var element = _this.createRow(0);
			var children = (typeof _this.container.children === 'object')?_this.container.children:_this.container.ChildNodes;
			

			$(element).css('z-index', 10000)
			$(element).css('font-weight', 'bold')
			_this.container.insertBefore(element,children[1]);

			$(element).slideToggle(0)
			$(element).slideToggle(duration, function(){
				window.setTimeout(_this.render,0)
			})

			/*
				For paginated tables with full containers we need to
				remove the extra node and update the navigation element 
			*/
			if(_this.paginate && _this.paginate >= (children.length - 2)){
				$(children[children.length-1]).slideToggle(duration,function(){
					$(children[children.length-1]).remove()
					// _this.generateNavigation();
				})
			}
		}

	};


	/*
		Clear the container document.createElementment and generate a new set of html document.createElementmenets
	*/
	this.render = function(page){
		
		if(!_this.data)
			return

		// empty
		_this.container.innerHTML = "";

		// header
		_this.container.appendChild(_this.createHeader());
		
		/*	Loop start and end	*/
		var start = 0;
		var end = _this.data.length;
		var pages = Math.ceil(_this.data.length / _this.paginate);
		
		if(pages < 2){
			page = 0;
		}

		/*	Paginate table (?)	*/
		if(page || page === 0){ // FUCK YOU 0!!! ...pretending like you aint be existing.
			_this.page = parseInt(page)
		}

		if(_this.paginate){

			if(!_this.page && _this.page !== 0){
				_this.page = 0;
			}

			start = _this.page * _this.paginate;
			end = start + _this.paginate;
		}

		/* Generate Table */
		for(var i = start; i < end; i++){
			_this.container.appendChild(_this.createRow(i));
		}

		_this.generateNavigation();
	}

}