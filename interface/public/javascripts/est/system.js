// System Spec sliders ---------------------------------------------------------------------------
$("#storage_size").val(est.system.storage.capacity+" kW");
$("#storage_size_slider").noUiSlider({
	start: est.system.storage.capacity,
	connect: "lower",
	behaviour: 'extend-tap',
	range: {
		'min': 0,
		'max': 3000
	}
});

$("#inverter_size").val(est.system.inverter.capacity+" kW");
$("#inverter_size_slider").noUiSlider({
	start: est.system.inverter.capacity,
	connect: "lower",
	behaviour: 'extend-tap',
	range: {
		'min': 0,
		'max': 2000
	}
});

$("#max_dod").val(parseInt(est.system.storage.max_dod * 100)+"%");
$("#max_dod_slider").noUiSlider({
	start: (est.system.storage.max_dod * 100),
	connect: "lower",
	behaviour: 'extend-tap',
	range: {
		'min': 0,
		'max': 100
	}
});

$("#round_trip_efficiency").val(parseInt(est.system.storage.round_trip_efficiency * 100)+"%");
$("#round_trip_efficiency_slider").noUiSlider({
	start: (est.system.storage.round_trip_efficiency * 100),
	connect: "lower",
	behaviour: 'extend-tap',
	range: {
		'min': 0,
		'max': 100
	}
});


// Inverter --
$("#inverter_size").on('change', function() {
	var v = parseInt($(this).val());

	$("#inverter_size_slider").val(v, {
		animate: true
	});

	est.system.inverter.capacity = v;

	$("#inverter_size_slider").val(v);

	$(this).val(kwlabel(v));

	est.save({system : JSON.stringify(est.system)});
	
	est.simulate(function() {
		est.update_result_outputs();
	});

});

$("#inverter_size_slider").on('change', function() {
	var num = parseInt($(this).val());
	est.system.inverter.capacity = num;
	$("#inverter_size")[0].value = kwlabel(num);
	est.save();

	est.simulate(function() {
		est.update_result_outputs();
	});
});

$("#inverter_size_slider").on('slide', function() {
	var num = parseInt($(this).val());
	$("#inverter_size").focus();
	$("#inverter_size")[0].value = kwlabel(num);
});


// DoD --
$("#max_dod").on('change', function() {
	var v = parseInt($(this).val());
	est.system.storage.max_dod = v / 100;
	$("#max_dod_slider").val(v, {
		animate: true
	});
	$(this).val(v + "%");
	est.save();

	est.simulate(function() {
		est.update_result_outputs();
	});
})

$("#max_dod_slider").on('change', function() {
	$("#max_dod").val(parseInt($(this).val()) + "%");
	est.system.storage.max_dod = parseInt($(this).val()) / 100;
	est.save();
	est.simulate(function() {
		est.update_result_outputs();
	});
});

$("#max_dod_slider").on('slide', function() {
	$("#max_dod").focus();
	$("#max_dod").val(parseInt($(this).val()) + "%");
});


// Round Trip Efficiency --
$("#round_trip_efficiency").on('change', function() {
	var v = parseInt($(this).val());
	console.log(v)
	est.system.storage.round_trip_efficiency = v / 100;
	$("#round_trip_efficiency_slider").val(v, {
		animate: true
	});
	$(this).val(v + "%");

	est.save();
	est.simulate(function() {
		est.update_result_outputs();
	});
})

$("#round_trip_efficiency_slider").on('change', function() {
	var v = parseInt($(this).val());
	$("#round_trip_efficiency").val(v + "%");
	est.system.storage.round_trip_efficiency = v / 100;
	est.save();
	est.simulate(function() {
		est.update_result_outputs();
	});
});

$("#round_trip_efficiency_slider").on('slide', function() {
	$("#round_trip_efficiency").focus();
	$("#round_trip_efficiency").val(parseInt($(this).val()) + "%");
});


// Storage capacity --
$("#storage_size").on('change', function() {
	var v = parseInt($(this).val());
	$("#storage_size_slider").val(v, {
		animate: true
	});
	est.system.storage.capacity = v;
	$(this).val(kwhlabel($(this).val()));

	est.save();
	est.simulate(function() {
		est.update_result_outputs();
	});
});

$("#storage_size_slider").on('change', function() {
	var v = parseFloat($(this).val());
	est.system.storage.capacity = v;
	$("#storage_size")[0].value = kwhlabel(v);
	est.save();
	est.simulate(function() {
		est.update_result_outputs();
	});
});

$("#storage_size_slider").on('slide', function() {
	var num = $(this).val();
	$("#storage_size").focus();
	$("#storage_size")[0].value = kwhlabel(num);
});

/*
	Optimize baselines
*/
$("#optimize")[0].onclick = function() {
	$(this).addClass('grey')
	this.innerHTML = "<i class='icon-spin5 animate-spin' style='margin-left: -2rem;'></i> Optimizing";
	est.async_optimize_baselines(function(optimization_results) {
		$("#optimize")[0].innerHTML = "Optimize";
		$("#optimize").removeClass('grey');
		for (var key in optimization_results) {
			est[key] = optimization_results[key];
		}
		est.save();
		est.simulate(function() {
			est.plot_all();
			est.update_result_outputs();
		});
	});
}

/*
	Toggle PV handling
*/
$("[name='pv_handling']").on('change', function() {

	if ($(this).val() == 'true') {
		est.apply_pv = true;
	} else {
		est.apply_pv = false;
	}

	est.retrieve_csv_input();
	est.save();
});