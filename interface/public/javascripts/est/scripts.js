

/*
	Optimize baselines button
*/
$("#interval_data_paste").on('change', function() {
	est.consume_power_csv(this.value, function(){
    	est.ready['interval_data'] = true;
    })

    // est.retrieve_csv_input(
    // 	function(){
	   //  	est.save({
		  //   	interval_data : est.interval_data.join(',')
		  //   });
	   //  }, function(){

	   //  }
    // );
})

$("#pv_data_paste").on('change', function() {
	est.consume_power_csv(this.value, function(){
    	est.ready['pv_data'] = true;
    })

    // est.retrieve_csv_input(
    // 	function(){

    // 	}, 
    // 	function(){
		  //   est.save({
		  //   	pv_data : est.pv_data.join(',')
		  //   });
    // 	}
    // );
});


/*
	Functions that run the charts in the bottom right of the view
*/
var peak_bar_chart = null;

function update_peak_chart(before, after) {

	if (!est.interval_data || est.interval_data.length < 500) {return;}

	var canvas = document.getElementById("peak_bar");
	var ctx = canvas.getContext("2d");
	var parent = $(canvas).parent()[0];
	var space = parent.getBoundingClientRect();
	var width = space.width;
	var height = 125;

	canvas.style.width = width + "px";
	canvas.style.height = height + "px";

	canvas.width = width;
	canvas.height = height;

	var data = {
		labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
		datasets: [{
			label: "Before",
			fillColor: "rgb(150,150,150)",
			strokeColor: "rgba(220,220,220,0.8)",
			highlightFill: "rgba(220,220,220,0.75)",
			highlightStroke: "rgba(220,220,220,1)",
			data: before
		}, {
			label: "After",
			fillColor: "#3498db",
			strokeColor: "rgba(151,187,205,0.8)",
			highlightFill: "rgba(151,187,205,0.75)",
			highlightStroke: "rgba(151,187,205,1)",
			data: after
		}]
	};

	if (peak_bar_chart === null) {
		peak_bar_chart = new Chart(ctx).Bar(data, {
			barShowStroke: false,
			responsive: false,
			animation: true,
			scaleShowLabels: false,
			tooltipFontSize: 12,
			scaleFontSize: 10,
			scaleFontColor: "#fafafa",
			tooltipFillColor: "rgba(74,74,74,1)",
			scaleFontFamily: "'Open Sans', 'Helvetica Neue', 'Helvetica', 'Arial', sans-serif",
			tooltipFontFamily: "'Open Sans', 'Helvetica Neue', 'Helvetica', 'Arial', sans-serif"
		});
	} else {
		for (var i = 0; i < before.length; i++) {
			peak_bar_chart.datasets[0].bars[i].value = before[i];
			peak_bar_chart.datasets[1].bars[i].value = after[i];
		}
		peak_bar_chart.update();
	}

	// weird... chart.js does not recover space when you turn off the labels
	ctx.setTransform(1, 0, 0, 1, -4, 0);
}

// -----------------------------------------------------------------------------------


var consumption_pi = null;
var canvas = $("#after_consumption_chart")[0];
function update_pi(data){
	var ctx = canvas.getContext("2d");
	canvas.style.width = 63 + "px";
	canvas.style.height = 63 + "px";
	canvas.width = 63;
	canvas.height = 63;
	consumption_pi = new Chart(ctx).Pie(data, {
		showTooltips: false,
		segmentShowStroke: false
	})

	consumption_pi.update();
	return;
		
	if(consumption_pi === null){
		return;
	}


	consumption_pi.segments[0].value = onpeak; // backwards?
	consumption_pi.segments[1].value = offpeak;
	consumption_pi.update();

}

// -----------------------------------------------------------------------------------

var consumption_charts = [
	null,
	null
];

function update_consumption_charts(before, after) {
	return;
	if (before.onpeak === 0) {
		before.onpeak = 1;
	}
	if (before.offpeak === 0) {
		before.offpeak = 1;
	}
	if (after.onpeak === 0) {
		after.onpeak = 1;
	}
	if (after.offpeak === 0) {
		after.offpeak = 1;
	}

	var canvas = $("#after_consumption_chart")[0];
	if (consumption_charts[1] === null) {

		canvas.style.width = 63 + "px";
		canvas.style.height = 63 + "px";

		canvas.width = 63;
		canvas.height = 63;

		var ctx = canvas.getContext("2d");

		var data = [{
			value: parseInt(after.onpeak),
			color: "#e74c3c",
			highlight: "#e74c3c",
			label: "On-peak"
		}, {
			value: parseInt(after.offpeak),
			color: "#3498db",
			highlight: "#3498db",
			label: "Off-peak"
		}]

		consumption_charts[1] = new Chart(ctx).Pie(data, {
			showTooltips: false,
			segmentShowStroke: false
		});

	} else {

		canvas.style.width = 63 + "px";
		canvas.style.height = 63 + "px";

		canvas.width = 63;
		canvas.height = 63;

		consumption_charts[1].segments[0].value = after.onpeak;
		consumption_charts[1].segments[1].value = after.offpeak;
		consumption_charts[1].update();
	}

}



/*
	Validators for the CSV data inputs
*/

function examineCSV(csv, cb){

	var data = csv.split(",");
	var min = 355; // Dec 1st
	var max = 366; // leap year
	var daysInData = data.length / 96;
	var valid = (daysInData >= min && daysInData <= max);

	var result = {
		valid : valid,
		days : daysInData,
		length : data.length,
		err : ""
	}

	if(valid === false){
		if(daysInData < min){
			result.err = "Not enough data. Found "+parseInt(result.days)+" in supplied data."
		}
		if(daysInData > max){
			result.err = "Too much data. Found "+parseInt(result.days)+" in supplied data."
		}
	}


	cb(result)
}

document.getElementById('interval_data_file').onchange = function(){
	var input = this;
    var reader = new FileReader();
    reader.onload = function(){
      var csvString = reader.result;
      var output = document.getElementById('interval_data_paste');
      output.value = csvString;
      $(output).change()
    };
    reader.readAsText(input.files[0]);
}

document.getElementById('pv_data_file').onchange = function(){
	var input = this;
    var reader = new FileReader();
    reader.onload = function(){
      var csvString = reader.result;
      var output = document.getElementById('pv_data_paste');
      output.value = csvString;
      $(output).change()
    };
    reader.readAsText(input.files[0]);
}

document.getElementById('interval_data_paste').onchange = function(){
	examineCSV(this.value, function(result){
		var icon = document.getElementById('interval_data_icon')
		var span = document.getElementById('interval_data_span')

		if(result.valid === true){
			icon.className = "icon-check-1 marg-w";
		}else{
			icon.className = "icon-warning-empty marg-w";
		}
		
		span.innerHTML = " "+result.err;

	})
}

document.getElementById('pv_data_paste').onchange = function(){
	examineCSV(this.value, function(result){
		var icon = document.getElementById('pv_data_icon')
		var span = document.getElementById('pv_data_span')

		if(result.valid === true){
			icon.className = "icon-check-1 marg-w";
		}else{
			icon.className = "icon-warning-empty marg-w";
		}
		
		span.innerHTML = " "+result.err;

	})
}

/* 
	Update Gants. Since they initialize in a div with display set to 'none' they assume they have no height.
*/


$(document).ready(function() {

	// if($("#interval_data_paste").val().length > 20000){
	// 	est.retrieve_csv_input();
	// }

	$("#est_tab_list > li").unbind('click').on('click', function() {

		// deactivate
		$("#est_tab_list > li").removeClass("active");
		$(".est-tab").removeClass("active");

		// activate
		$(this).addClass("active");
		var tab_selector = "#" + $(this).data('tab');
		$(tab_selector).addClass("active");

		if ($(this).data('tab') === 'finance-tab') {
			$('#initial_output_wrap').hide();
			$('#proposal_output_wrap').show();
		} else {
			$('#initial_output_wrap').show();
			$('#proposal_output_wrap').hide();
		}
		/*
			Gant charts are fluid by nature. One consequence is that if the last draw happened while the canvas was hidden the gant assumes it's height and width are 0. 
			This forces a draw call on all existing gant instances whenever a tab is clicked. 
		*/
		est.force_gant_draw();
	});
});



// Solar size
$("#quote_solar_size").on('change', function() {
	est.system.solar.capacity = parseFloat($(this).val());
});


/*
Hacky

soooooooooooooooo
so
hacky

*/
function style_date_pickers(){
	$('.ui-datepicker-prev').html("<i class='icon-left-open-3'></i>");
	$('.ui-datepicker-next').html("<i class='icon-right-open-3'></i>");
}

$("#zoom_start_dp").datepicker({
	altField: "#zoom_start",
	onSelect: function() {
		if ($("#zoom_start").val() !== "" && $("#zoom_end").val() !== "") {
			if (est.chart) {
				est.chart.zoom_to_date_range($("#zoom_start").val(), $("#zoom_end").val());
			}
		}
	},
	beforeShow: function() {
		var min_date = new Date("1/1/" + $("#dataset_start_date").val());
		var max_date = new Date("12/31/" + $("#dataset_start_date").val());

		$(".est-datepicker").datepicker("option", "minDate", min_date);
		$("#zoom_start").datepicker("option", "defaultDateType", min_date);

		$(".est-datepicker").datepicker("option", "maxDate", max_date);
		$("#zoom_end").datepicker("option", "defaultDateType", max_date);

	},
	minDate: new Date("1/1/" + $("#dataset_start_date").val()),
	maxDate: new Date("12/31/" + (parseInt($("#dataset_start_date").val()) + 1)),
});

$("#zoom_end_dp").datepicker({
	altField: "#zoom_end",
	onSelect: function() {
		$(this).change()
		if ($("#zoom_start").val() !== "" && $("#zoom_end").val() !== "") {
			if (est.chart) {
				est.chart.zoom_to_date_range($("#zoom_start").val(), $("#zoom_end").val());
			}
		}
	},
	beforeShow: function() {
		var min_date = new Date("1/1/" + $("#dataset_start_date").val());
		var max_date = new Date("12/31/" + $("#dataset_start_date").val());

		$(".est-datepicker").datepicker("option", "minDate", min_date);
		$("#zoom_start").datepicker("option", "defaultDateType", min_date);

		$(".est-datepicker").datepicker("option", "maxDate", max_date);
		$("#zoom_end").datepicker("option", "defaultDateType", max_date);

	},
	minDate: new Date("1/1/" + $("#dataset_start_date").val()),
	maxDate: new Date("12/31/" + (parseInt($("#dataset_start_date").val()) + 1)),
});

$(".ui-datepicker").on('click', style_date_pickers)

$("#zoom_start_dp").datepicker("setDate", "1/1/2014");
$("#zoom_end_dp").datepicker("setDate", "12/31/2014");

style_date_pickers()

/* Convert a text input into a 'ticker' input */
function tick(input) {

	input = $(input);
	var container = input.parent();

	// create elements
	var wrap = ele('div');
	wrap.className = "est-tick-input-wrap";

	var new_input = input.clone(true, true); // deep clone...
	new_input.className = "est-tick-input";

	var left_arrow = ele('i');
	left_arrow.input = new_input[0];
	left_arrow.className = "icon-left-open-4 est-tick-left";

	var right_arrow = ele('i');
	right_arrow.input = new_input[0];
	right_arrow.className = "icon-right-open-4 est-tick-right";

	// set events - using mousedown rather than click to prevent hilighting
	right_arrow.onmousedown = function(e) {
		e.preventDefault();
		var old_val = parseFloat(this.input.value.replace(/[^0-9\.]+/g, ""));
		if (!isNaN(old_val)) {
			var new_val = old_val + 1;
			this.input.value = new_val;
			$(this.input).change();
		}
	}

	left_arrow.onmousedown = function(e) {
		e.preventDefault();
		var old_val = parseFloat(this.input.value.replace(/[^0-9\.]+/g, ""));
		if (!isNaN(old_val)) {
			var new_val = old_val - 1;
			this.input.value = new_val;
			$(this.input).change();
		}
	}

	// append to le doc
	wrap.appendChild(new_input[0]);
	wrap.appendChild(left_arrow);
	wrap.appendChild(right_arrow);

	input.replaceWith(wrap); // scrap the original
	//container[0].appendChild(wrap);

}

$("#pv_generator, #quote_solar_size").on('change', function() {
	var parsed_val = parseFloat($(this).val().replace(/[^0-9\.]+/g, ""));

	$.get('/csv/1kwSolar.csv', function(data) {
		data = data.replace(/(?:\r\n|\r|\n)/g, ',');
		data = data.split(",");
		var l = data.length;
		for (var i = 0; i < l; i++) {

			data[i] = data[i] * parsed_val;

		}

		$("#pv_data_paste").val(data.toString());
		est.retrieve_csv_input();

	});
	est.system.solar.capacity = parsed_val;
	$("#pv_generator, #quote_solar_size").val(kwlabel(parsed_val));
	est.save();
});

$('.est-ticker-input').each(function() {

	if (!$(this).attr('type') || $(this).attr('type') == 'text') {
		tick($(this));
	}

});

$("#toggle_peak_dialogue").on('click', function(){
	est.peak_table.render();
	est.peak_dialogue.toggle();
})

$("#toggle_bar_property").on('click', function(){
	if(est.bar_settings.property === 'spend'){
		$(this)[0].innerHTML = "<i class='icon-flash-1'></i> Power";
		est.bar_settings.property = 'power';
	}else{
		$(this)[0].innerHTML = "<i class='icon-dollar'></i> Spend";
		est.bar_settings.property = 'spend';
	}
	est.update_result_outputs();
})

$("#toggle_bar_key").on('click', function(){
	if(est.bar_settings.key === 'demand'){
		$(this)[0].innerHTML = "<i class='icon-calendar'></i> Non-Coincidental";
		est.bar_settings.key = 'non_coincidental';
	}else{
		$(this)[0].innerHTML = "<i class='icon-chart-bar'></i> Demand";
		est.bar_settings.key = 'demand';
	}
	est.update_result_outputs();
})