// TARIFF EDITOR -----------------------------------------------------------------
/*
	THIS IS SOME FUUUU CODE RIGHT HERE!
	~ Shawn
*/
var tariff_editor;
function createTariffDialogue(){
	tariff_editor = new Dialogue({
		title : "Edit Rate Plan",
		content : [
			ele("#tariff_warnings.center"),
			ele("#tariff_editor")
		],
		onShow : function(){
			est.force_gant_draw()
			$("#rate_plan_tab_list").click(est.force_gant_draw)
		},
		onHide : function(){
			est.simulate(function(simulation_results){
				// for(var ki in simulation_results){est[ki] = simulation_results[ki];}
				est.create_tariff_chart_data();
				est.save_rate_plan();
				est.update_result_outputs();
            });
		}
	});
}

createTariffDialogue();
// ------------------------------------------------------------------------------------



$("#edit_rate_plan").on('click', function(){
	tariff_editor.toggle();
})

$("#rate_plan_title").on('change', function(){
	est.rate_plan.title = $(this).val();
	est.save_rate_plan(function(){
		tariff_datatable.fetch(function(){
			tariff_datatable.render()
		});
	});
})

$("#rate_plan_utility").on('change', function(){
	est.rate_plan.utility = $(this).val();
	est.save_rate_plan(function(){
		tariff_datatable.fetch(function(){
			tariff_datatable.render()
		});
	});
})

$("#fork_rate_plan").on('click', function(){

	var utility = (est.rate_plan.utility) ? est.rate_plan.utility : " - ";
	var rate_plan = {};

	for(var  p in est.rate_plan){
		if(p !== 'id'){
			if(typeof est.rate_plan[p] === "object"){
				rate_plan[p] = JSON.stringify(est.rate_plan[p]);
			}else{	
				rate_plan[p] = est.rate_plan[p];
			}
		}
	}

	rate_plan.title = "Forked from "+rate_plan.title+"";

	socket.post('/est/rateplans', rate_plan, function(data){
		data = JSON.parse(data);
		est.load_rate_plan(data[0].id)
		window.setTimeout(function(){
			tariff_datatable.fetch(function(){
				tariff_datatable.render()
			});
		}, 100)
	})
})

/* Autocomplete for the utilities input
----------------------------------------------------------*/
socket.get('/est/utilities', function(utils){
	
	if(typeof utils === 'string')
		utils = JSON.parse(utils)

	/* Trim that mess */
	utils = utils.map(function(util){return util['title']})

	$( ".utility_select" ).autocomplete({
		source : utils,
		minLength: 2,
		change : function(){
			est.rate_plan.utility = $(this).val();
			est.save_rate_plan(function(){
				tariff_datatable.fetch(function(){
					tariff_datatable.render()
				});
			});
		}
    });
}) 


/*
	RATE PLAN BROWSER
*/

/*CREATE ----------------------------------------------*/
var new_rateplan_label = ele('label');
new_rateplan_label.innerHTML = "Create New Rate Plan";

var form = ele('div.row.flex');

var name_col = ele("div.col-auto.pad-e");
var name_input = ele('input');
name_input.placeholder = "Untitled Rate Plan";

var utility_col = ele("div.col-auto.pad-w");
var utility_input = ele('input');
utility_input.placeholder = "Untitled Utility";


name_col.appendChild(name_input);
utility_col.appendChild(utility_input);
form.appendChild(name_col);
form.appendChild(utility_col);


/*BROWSE ----------------------------------------------*/
var table_label = ele('label.marg-n');
table_label.innerHTML = "Existing Rate Plans";

var table = ele('div');
table.className = 'data-table';

var tariff_datatable = new DataTable({
	container: document.getElementById("rate_plan_browser"),
	socket : socket,
	navigationContainer : document.getElementById('rate_plan_browser_navigation'),
	paginate : 10,
	filter : ['title', 'utility','public','id'],
	labels : {
		title : "Rate Plan",
		utility : "Utility",
		public : "JLM Official",
		id : '<input type="checkbox">'
	},
	header : {
		id : function(){
			var wrap = ele('div.right');
			var checkbox = ele("input");
			checkbox.type= 'checkbox';
			checkbox.onchange = function(){
				var checked = this.checked;
				$(".rate_plan_checkbox").each(function(){
					$(this)[0].checked = checked;
					$(this).change();
				})
			}
			wrap.appendChild(checkbox)
			return wrap
		}
	},
	row : function(data){
		var row = ele();
		if(data){		
			if(data.id === est.rate_plan.id){
				row.className = 'active';
			}
		}
		return row;
	},
	column : {
		title : function(data){
			var el = ele('a');
			el.href = "#";
			el.innerHTML = data.title;
			el.onclick = function(){
				$(".est-window").remove()
				est.load_rate_plan(data.id, function(){

					/*Update rateplan title input*/
					$("#rate_plan_title")[0].value = est.rate_plan.title;

					/*Save project*/
					est.save({
						rate_plan : est.rate_plan.id
					});
					
					window.setTimeout(function(){
						tariff_datatable.render()
					}, 100)
				});
			}
			return el;
		},
		public : function(data){
			var el = ele();
			el.innerHTML = (data.public === true)? "Yes" : "User Created";
			return el;
		},
		id : function(data){
			var el = ele('input');
			el.type = "checkbox";
			el.className = "rate_plan_checkbox";
			el.setAttribute("data-id", data.id)
			return ele({'div.right': el});
		}
	},
	dataSource : "/est/rateplans"
});

tariff_datatable.fetch(function(){
	tariff_datatable.render()
});

// /* 
// 	Create dialogue for rate plans
// */
// var tab = $("#utility-tab")[0];
// var container_size = tab.getBoundingClientRect();
// var rate_plan_browser = new Dialogue({
// 	title : "Rate Plans",
// 	content : [table]
// });

// rate_plan_browser.dialogue.style.minWidth = "400px";
// rate_plan_browser.dialogue.style.width = "50%";
// rate_plan_browser.dialogue.style.left = "25%";




// /*
// 	Activate
// */
// $("#browse_rate_plans").on('click', function(){
// 	rate_plan_browser.toggle();
// 	tariff_datatable.fetch(function(){
// 		tariff_datatable.render()
// 	});
// })