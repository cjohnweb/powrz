    /*
        Second Thread
    */
    onmessage = function(e) {

        // Become the update!
        for (var key in e.data.data) {
            _this[key] = e.data.data[key];
        }

        if (e.data.request === "optimize_baselines") {

            // can't pass date objects but converting these a trillion times in a row is silly. Going to just convert these all at once and stop wasting cycles
            _this.prep_dates();

            _this.optimize_baselines();

        }

        if (e.data.request === "simulate") {
            _this.prep_dates();
            _this.simulate(function() {
                postMessage({
                    shaved_interval_data: _this.shaved_interval_data,
                    soc_data: _this.soc_data,
                });
            })
        }

        if (e.data.request === "roi") {
            _this.prep_dates();
            var roi = _this.calc.roi();
            postMessage({
                roi: roi
            });
        }

    }

    Date.prototype.dayOfYear = function() {
        var j1 = new Date(this);
        j1.setMonth(0, 0);
        return Math.round((this - j1) / 8.64e7);
    }

    Date.prototype.isWeekEnd = function() {
        return (this.getDay() === 6 || this.getDay() === 0);
    }

    Date.prototype.getHalfHour = function() {

        var minutes = this.getMinutes();
        var hours = this.getHours();

        var ret = hours;

        if (hours === 0) {
            if (minutes >= 29) {
                ret += 1;
                return ret;
            }
            return ret;
        }

        ret += ret;
        if (minutes >= 29) {
            ret += 1;
        }

        return ret;
    }

    var _this = {};
    var results = {}; // returned

    _this.prep_dates = function() {
            var dates_length = _this.dates.length;
            for (var i = 0; i < dates_length; i++) {
                _this.dates[i] = new Date(_this.dates[i]);
            }
    }  

    _this.test_baselines = function(step) {

        // results
        _this.shaved_interval_data = [];
        _this.soc_data = [];

        var inverter_size = _this.system.inverter.capacity;
        var battery_capacity = _this.system.storage.capacity;
        var RTE = 1 - ((1 - _this.system.storage.round_trip_efficiency) * .5);
        var backup_days = _this.system.backup_days;

        var inverter_capacity = inverter_size; // inverter capacity in watts
        var battery_capacity = battery_capacity; // battery capacity in watt minutes

        var min_soc = battery_capacity - (battery_capacity * _this.system.storage.max_dod);
        var current_soc = battery_capacity; // starting the year at a full charge
        var current_dod = 0; // Starts at 0 because we're starting FULLY CHARGED
        var dod_infractions = 0;


        // If we're applying PV data and the PV data set is valid we want to shave the interval_data_with_pv series otherwise we can just shave the original data
        var data_to_shave = (_this.apply_pv === true && _this.pv_data.length === _this.interval_data.length) ? _this.interval_data_with_pv : _this.interval_data;

        // loop through the year
        var l = data_to_shave.length;
        for (var datapoint = 0; datapoint < l; datapoint++) {

            // DATE
            var current_date = _this.dates[datapoint];
            var current_month = current_date.getMonth();
            var current_hour = current_date.getHours();
            var current_half_hour = current_date.getHalfHour();
            var is_on_peak = _this.is_on_peak(current_date);
            var day_is_backed_up = _this.system.backup_days[current_date.getDay()];

            // POWER
            var max_load = _this.system.baseline[current_month];
            var power = (_this.apply_pv === true) ? data_to_shave[datapoint] * 1 : data_to_shave[datapoint] * 1;
            var power_needed = power * (1 + (1 - RTE));
            var current_load = power; // converted to watts

            var rp_index = _this.get_rate_plan_schedule(current_date);
            var handle_peaks = _this.rate_plan.schedules[rp_index].onpeak_handling;

            // if on peak...
            if (is_on_peak === true && day_is_backed_up === true) {

                if (handle_peaks === true) {
                    // if the load is greater than the inverter's capacity discharge at the inverter's capacity
                    var dischargeby = (power_needed > inverter_capacity) ? inverter_capacity : power_needed;

                    if ((dischargeby * .25) > current_soc) {
                        dischargeby = current_soc;
                    }

                    // if discharging would bring us below the min state of charge, alter that messss
                    var future_soc = current_soc - (dischargeby * .25);
                    if (future_soc < min_soc) {
                        var diff = min_soc - future_soc;
                        dischargeby -= diff * 4;
                    }

                    current_load -= dischargeby; // trim the power spend by available backup
                    current_soc -= dischargeby * .25; // reduce soc by amount consumed

                } else {
                    if (power_needed > max_load) {
                        // Off peak & batteries aren't full & current load is greater than specified max
                        var difference = power_needed - max_load; // Power needed to maintain baseline
                        var dischargeby = (difference > inverter_capacity) ? inverter_capacity : difference;

                        if ((dischargeby * .25) > current_soc) {
                            dischargeby = current_soc;
                        }

                        // if discharging would bring us below the min state of charge, alter that shit
                        var future_soc = current_soc - (dischargeby * .25);
                        if (future_soc < min_soc) {
                            var diff = min_soc - future_soc;
                            dischargeby -= diff * 4;
                        }

                        current_soc -= dischargeby * .25;
                        current_load -= dischargeby;
                    }
                }

            } else {

                // Off peak & batteries arent full & current load is less than specified max
                if (current_load < max_load && current_soc < battery_capacity) {

                    var difference = max_load - current_load;
                    var chargeby = (difference > inverter_capacity) ? inverter_capacity : difference;

                    var dod = battery_capacity - current_soc;

                    if ((chargeby * .25) > dod) {
                        chargeby = dod;
                    }

                    current_soc += chargeby * .25;
                    current_load += chargeby;

                }

                // Off peak & batteries aren't full & current load is greater than specified max
                if (current_load >= max_load) {

                    var difference = current_load - max_load; // Power needed to maintain baseline
                    var dischargeby = (difference > inverter_capacity) ? inverter_capacity : difference;

                    if ((dischargeby * .25) > current_soc) {
                        dischargeby = current_soc;
                    }

                    // if discharging would bring us below the min state of charge, alter that shit
                    var future_soc = current_soc - (dischargeby * .25);
                    if (future_soc < min_soc) {
                        var diff = min_soc - future_soc;
                        dischargeby -= diff * 4;
                    }

                    current_soc -= dischargeby * .25;
                    current_load -= dischargeby;

                }

            }

            // these aint no magic batteries. Yo ass gonna pay extra 4 dat.
            if (current_soc > battery_capacity) {
                current_soc = battery_capacity;
            }

            // if we've failed to maintain the baselines, stop immediately and mod the offending baseline.
            if (current_load > max_load) {
                _this.system.baseline[current_month] += step; // push the baseline up by step
                //_this.test_baselines(step)
                setTimeout(function() {
                        _this.test_baselines(step)
                    },
                    0);
                return;
            } else if (step !== 1) {

                // reduce all baselines by step  +1
                for (var i = 0; i < _this.system.baseline.length; i++) {
                    _this.system.baseline[i] -= step + 1;
                }

                // test again
                setTimeout(function() {
                        _this.test_baselines(step * .5);
                    },
                    0);
                return;
            }

        }

        // SUCCESS MOTHER FUCKER!
        postMessage({
            system: _this.system,
            message_id: _this.message_id
        });
    }

    _this.simulate = function(callback) {

        // results
        _this.shaved_interval_data = [];
        _this.soc_data = [];

        var inverter_size = _this.system.inverter.capacity;
        var battery_capacity = _this.system.storage.capacity;
        var RTE = 1 - ((1 - _this.system.storage.round_trip_efficiency) * .5);
        var backup_days = _this.system.backup_days;

        var inverter_capacity = inverter_size; // inverter capacity in watts
        var battery_capacity = battery_capacity; // battery capacity in watt minutes

        var min_soc = battery_capacity - (battery_capacity * _this.system.storage.max_dod);
        var current_soc = battery_capacity; // starting the year at a full charge
        var current_dod = 0; // Starts at 0 because we're starting FULLY CHARGED
        var dod_infractions = 0;


        // If we're applying PV data and the PV data set is valid we want to shave the interval_data_with_pv series otherwise we can just shave the original data
        var data_to_shave = (_this.apply_pv === true && _this.pv_data.length === _this.interval_data.length) ? _this.interval_data_with_pv : _this.interval_data;

        // loop through the year
        var l = data_to_shave.length;
        for (var datapoint = 0; datapoint < l; datapoint++) {

            // DATE
            var current_date = _this.dates[datapoint];
            var current_month = current_date.getMonth();
            var current_hour = current_date.getHours();
            var current_half_hour = current_date.getHalfHour();
            var is_on_peak = _this.is_on_peak(current_date);
            var day_is_backed_up = _this.system.backup_days[current_date.getDay()];

            // POWER
            var max_load = _this.system.baseline[current_month];
            var power = (_this.apply_pv === true) ? data_to_shave[datapoint] * 1 : data_to_shave[datapoint] * 1;
            var power_needed = power * (1 + (1 - RTE));
            var current_load = power; // converted to watts

            var rp_index = _this.get_rate_plan_schedule(current_date);
            var handle_peaks = _this.rate_plan.schedules[rp_index].onpeak_handling;

            // if on peak...
            if (is_on_peak === true && day_is_backed_up === true) {

                if (handle_peaks === true) {
                    // if the load is greater than the inverter's capacity discharge at the inverter's capacity
                    var dischargeby = (power_needed > inverter_capacity) ? inverter_capacity : power_needed;

                    if ((dischargeby * .25) > current_soc) {
                        dischargeby = current_soc;
                    }

                    // if discharging would bring us below the min state of charge, alter that messss
                    var future_soc = current_soc - (dischargeby * .25);
                    if (future_soc < min_soc) {
                        var diff = min_soc - future_soc;
                        dischargeby -= diff * 4;
                    }

                    current_load -= dischargeby; // trim the power spend by available backup
                    current_soc -= dischargeby * .25; // reduce soc by amount consumed

                } else {
                    if (power_needed > max_load) {
                        // Off peak & batteries aren't full & current load is greater than specified max
                        var difference = power_needed - max_load; // Power needed to maintain baseline
                        var dischargeby = (difference > inverter_capacity) ? inverter_capacity : difference;

                        if ((dischargeby * .25) > current_soc) {
                            dischargeby = current_soc;
                        }

                        // if discharging would bring us below the min state of charge, alter that shit
                        var future_soc = current_soc - (dischargeby * .25);
                        if (future_soc < min_soc) {
                            var diff = min_soc - future_soc;
                            dischargeby -= diff * 4;
                        }

                        current_soc -= dischargeby * .25;
                        current_load -= dischargeby;
                    }
                }

            } else {

                // Off peak & batteries arent full & current load is less than specified max
                if (current_load < max_load && current_soc < battery_capacity) {

                    var difference = max_load - current_load;
                    var chargeby = (difference > inverter_capacity) ? inverter_capacity : difference;

                    var dod = battery_capacity - current_soc;

                    if ((chargeby * .25) > dod) {
                        chargeby = dod;
                    }

                    current_soc += chargeby * .25;
                    current_load += chargeby;

                }

                // Off peak & batteries aren't full & current load is greater than specified max
                if (current_load >= max_load) {

                    var difference = current_load - max_load; // Power needed to maintain baseline
                    var dischargeby = (difference > inverter_capacity) ? inverter_capacity : difference;

                    if ((dischargeby * .25) > current_soc) {
                        dischargeby = current_soc;
                    }

                    // if discharging would bring us below the min state of charge, alter that shit
                    var future_soc = current_soc - (dischargeby * .25);
                    if (future_soc < min_soc) {
                        var diff = min_soc - future_soc;
                        dischargeby -= diff * 4;
                    }

                    current_soc -= dischargeby * .25;
                    current_load -= dischargeby;

                }

            }

            // these aint no magic batteries. Yo ass gonna pay extra 4 dat.
            if (current_soc > battery_capacity) {
                current_soc = battery_capacity;
            }

            _this.shaved_interval_data.push(current_load);
            _this.soc_data.push(current_soc);

        }

        if (typeof(callback === 'function')) {
            callback();
        }
    }

    _this.get_rate_plan_schedule = function(date) {
        var date = new Date(date);
        var month = date.getMonth();
        for (var schedule = 0; schedule < _this.rate_plan.schedules.length; schedule++) {
            for (var i = 0; i < _this.rate_plan.schedules[schedule].months.length; i++) {
                if (this.rate_plan.schedules[schedule].months[i] === month) {
                    return schedule;
                }
            }
        }
    }

    _this.get_tariff = function(date) {
        var date = new Date(date);
        var rate_plan = _this.get_rate_plan_schedule(date);
        var halfHour = date.getHalfHour();
        var wknd = (date.isWeekEnd()) ? "weekend" : "weekday";

        return {
            kw: _this.rate_plan.schedules[rate_plan][wknd].kw[halfHour] * _this.tariff_scale,
            kwh: _this.rate_plan.schedules[rate_plan][wknd].kwh[halfHour] * _this.tariff_scale
        };
    }

    _this.is_on_peak = function(date) {

        var month = date.getMonth();
        var halfHour = date.getHalfHour();
        var day = date.getDay();
        var rate_plan = _this.rate_plan.schedules[_this.get_rate_plan_schedule(date)];
        var isWeekend = date.isWeekEnd();

        if (isWeekend === true) {
            return rate_plan.weekend.onpeak[halfHour];
        } else {
            return rate_plan.weekday.onpeak[halfHour];
        }
    }

    _this.calc = {
        highest_peaks: function(dataset) {
            var dates = _this.dates;
            var ret = [];
            var l = dataset.length;
            for (var i = 0; i < l; i++) {
                var key = dates[i].getMonth();
                if (!ret[key]) {
                    ret[key] = {
                        date: dates[i],
                        power: dataset[i]
                    }
                } else {
                    if (dataset[i] > ret[key].power) {
                        ret[key] = {
                            date: dates[i],
                            power: dataset[i]
                        }
                    }
                }
            }
            return ret;
        },
        consumption_spend: function(dataset) {

            var dates = _this.dates;

            var result = {
                total: 0,
                onpeak: 0,
                offpeak: 0
            };

            for (var i = 0; i < dataset.length; i++) {

                var power = dataset[i] * .25; // iP * 1/4 hour = tP

                var tariff = _this.get_tariff(dates[i]).kwh * 1;

                var on_peak = _this.is_on_peak(date);

                var cost = (power > 0) ? power * tariff : 0;

                result.total += cost;

                if (on_peak === true) {
                    result.onpeak += cost;
                } else {
                    result.offpeak += cost;
                }
            }
            return result;

        },
        total_peak_spend: function(dataset) {
            var peaks = _this.calc.highest_peaks(dataset);

            var result = {
                total: 0,
                month: []
            }

            for (var p = 0; p < peaks.length; p++) {
                var tariff = _this.get_tariff(peaks[p].date).kw;
                var spend = (peaks[p].power > 0) ? peaks[p].power * tariff : 0;
                result.total += spend;
                result.month[p] = spend;
            }

            return result;
        },
        roi: function() {
            var start = Date.now();
            var cost = _this.system_cost;
            var balance = cost.total * -1;

            var macrs_payback_schedule = [0, .2, .32, .192, .1152, .1152, .0576];
            var sgip_payback_schedule = [0, .5, .1, .1, .1, .1, .1];

            for (var year = 0; year < 20; year++) {

                // Bump up tariffs
                _this.tariff_scale = 1 + (_this.annual_tariff_increase * year);

                // onpeak spend
                var original_peak_spend = _this.calc.total_peak_spend(_this.interval_data);
                var adjusted_peak_spend = _this.calc.total_peak_spend(_this.shaved_interval_data);


                // loop over the entire dataset to find the exact savings
                var month = 0;
                for (var d = 0; d < _this.dates.length; d++) {

                    // thingz
                    var date = new Date(_this.dates[d]);
                    var tariff = _this.get_tariff(date);
                    var original_demand = _this.interval_data[d];
                    var adjusted_demand = _this.shaved_interval_data[d];
                    var consumption_saved = (original_demand - adjusted_demand) * (tariff.kwh * .25);

                    // Add consumption savings at the end of each 
                    balance += consumption_saved;

                    if (date.getMonth() !== month) {
                        var peak_savings = (original_peak_spend.month[month] - adjusted_peak_spend.month[month]) * (tariff.kw * .25);
                        month++;
                        balance += peak_savings;
                    }


                    // Check if we're still negative
                    if (balance >= 0) {
                        _this.tariff_scale = 1;
                        return {
                            years: year,
                            days: date.dayOfYear()
                        }
                    }
                }
            }
        }
    }

    _this.optimize_baselines = function() {
        var original_peaks;
        if (_this.pv_data.length === _this.interval_data.length && _this.apply_pv === true) {
            var original_peaks = _this.calc.highest_peaks(_this.interval_data_with_pv);
        } else {
            var original_peaks = _this.calc.highest_peaks(_this.interval_data);
        }

        // reset baselines
        for (var i = 0; i < original_peaks.length; i++) {
            var baseline = original_peaks[i].power - (_this.system.inverter.capacity);
            baseline = (baseline < 0) ? 0 : baseline;
            _this.system.baseline[i] = baseline;
        }


        // optimize
        _this.test_baselines(2);
        //_this.optimize_baseline(0);
    }

    _this.optimize_baseline = function(month) {
        _this.simulate(function() {
            var final_peaks = _this.calc.highest_peaks(_this.shaved_interval_data);
            if (final_peaks[month].power > _this.system.baseline[month]) {
                _this.system.baseline[month] ++;
                _this.optimize_baseline(month);
            } else {
                if (month < 11) {
                    _this.optimize_baseline((month + 1));
                    return;
                }
            }
        });
    }