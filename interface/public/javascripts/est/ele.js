/*
    Rapid HTML templating in javascript

    var title = "Home Page!";
    var template {
        "#header" : [
            "a.nav:Home(href='/')",
            "a.nav:Contact(href='/contact')",
        ],
        "#body" : [
            "h1:{title}",
            "p:Some plop about the site!"
        ],
        "#footer.row" : [
            {"ul.col-1-3": [
                "li:a",
                "li:b",
                "li:c"
            ]},
            {"ul.col-1-3": [
                "li:a",
                "li:b",
                "li:c"
            ]},
            {"ul.col-1-3": [
                "li:a",
                "li:b",
                "li:c"
            ]}
        ]
    }

    ele(template)
*/

function ele(e,s){

    if(!e || e === ""){return document.createElement('div');}
    
    if(e.nodeName){return e;}

    if(Array.isArray(e)){
      var frag = document.createDocumentFragment();
      for(var i = 0; i < e.length; i++){
        frag.appendChild(ele(e[i]));
      }
      return frag;
    }
  
    if(typeof e === 'object'){
      var frag = document.createDocumentFragment();
      for(var node in e){
        var parent = ele(node);
        parent.appendChild(ele(e[node]));
        frag.appendChild(parent);
      }
      return frag;
    }
  
    if(
        e[0] === "#" 
        || e[0] ===  "." 
        || e[0] ===  ":" 
        || e[0] === "("
    ){
        e = "div"+e
    }
    

    /*
        {templates} are {evaluated} {here}
    */
    var vars = e.match(/{([^}]*)}/g);
    if(vars){
        for(var i = 0; i < vars.length; i++){
            var scope = (s)?s:window;
            // with(scope){            
                try{
                    var replacement = scope.eval(vars[i].replace("{","").replace("}",""));
                    e = e.replace(vars[i], replacement);
                }catch(err){
                    console.log(err)
                    var replacement = "ERR";
                    e = e.replace(vars[i], replacement);
                }
            // }
        }
    }
    

    /*
        (attrs="handled" here="awesome")
    */
    var attrs = e.match(/\((.*?)\)/);
    var keys = [];
    var values = [];
    if(attrs){
        e = e.replace(attrs[0], '')

        // var attributes = attrs[1].match(/(["'])(?:(?=(\\?))\2.)*?\1/)
        // console.log(attrs[1])
        keys = attrs[1];
        values = attrs[1];

        keys = keys.match(/(\S)+=/g)
        if(keys){        
            keys = keys.map(function(key){
                return key.replace("=","");
            })
        }

        values = values.match(/['"]+([^'"]+)+['"]/g)
    }
    
    // inner html
    var parts = e.split(/[:](.+)?/)
    var html = (parts[1])?parts[1]:undefined;
    e = parts[0]

    // id
    var re = new RegExp('#', 'g');
    var str = e.replace(re, ';#');

    // Class names
    re = new RegExp('[.]', 'g');
    str = str.replace(re, ';.');

    var str = str.split(';');

    var element = document.createElement(str[0]);

    // set html
    if(html){element.innerHTML += html;}

    //set attributes
    if(values && keys){    
        for(var i = 0; i < values.length; i++){
            if(keys[i] && values[i]){
                element.setAttribute(keys[i], values[i].replace("'","").replace("'",""))
            }        
        }
    }


    // ID and class names
    for(var i = 1; i < str.length; i++){
        if(str[i][0] === "#"){element.id = str[i].replace("#", '');}
        if(str[i][0] === "."){
            if(element.className.length > 0){element.className += " ";}
            element.className += str[i].replace(".", '');
        }
    }

    return element;
}