
/*  
    ------------------------------------------------------------------
    Pile of minified utils. None of this need to be in this library.
    ------------------------------------------------------------------
*/
function safeParseJSON(a){try{return JSON.parse(a)}catch(b){return console.warn(b),{}}}function daysInMonth(a,b){return(new Date(b,a,0)).getDate()}function kwhlabel(a){a=parseFloat(a);return parseInt(a)+" kWh"}function kwlabel(a){a=parseFloat(a);return parseInt(a)+" kW"}Array.prototype.sum=function(){for(var a=0,b=0;a<this.length;b+=this[a++]);return b};Array.prototype.max=function(){return Math.max.apply({},this)};Array.prototype.min=function(){return Math.min.apply({},this)};
Date.prototype.dayOfYear=function(){var a=new Date(this);a.setMonth(0,0);return Math.round((this-a)/864E5)};Date.prototype.isWeekEnd=function(){return 6===this.getDay()||0===this.getDay()};Date.prototype.getHalfHour=function(){date=this;var a=date.getMinutes(),b=date.getHours(),c=b;if(0===b)return 29<=a&&(c+=1),c;c+=c;29<=a&&(c+=1);return c};
Date.prototype.getWeekNumber=function(){var a=new Date(+this);a.setHours(0,0,0);a.setDate(a.getDate()+4-(a.getDay()||7));return Math.ceil(((a-new Date(a.getFullYear(),0,1))/864E5+1)/7)};
Number.prototype.makeMoney=function(a,b,c){var e=this;a=isNaN(a=Math.abs(a))?2:a;b=void 0==b?".":b;c=void 0==c?",":c;var g=0>e?"-":"",f=parseInt(e=Math.abs(+e||0).toFixed(a))+"",d=3<(d=f.length)?d%3:0;return g+"$"+(d?f.substr(0,d)+c:"")+f.substr(d).replace(/(\d{3})(?=\d)/g,"$1"+c)+(a?b+Math.abs(e-f).toFixed(a).slice(2):"")};function isNumber(a){return a===parseFloat(a)}function isEven(a){return isNumber(a)&&0==a%2}function isOdd(a){return isNumber(a)&&1==Math.abs(a)%2};
function cloneObject(obj) {
    if(obj == null || typeof(obj) != 'object')
        return obj;

    var temp = obj.constructor(); // changed

    for(var key in obj) {
        if(obj.hasOwnProperty(key)) {
            temp[key] = clone(obj[key]);
        }
    }
    return temp;
}

/*
    prototype both so I don't have to sanity check lol
*/
String.prototype.toNumber=function(){
    var numb = this.replace(/[^0-9\.]/g, '')
    numb = (numb!=='')?numb:0;
    return parseFloat(numb)
}
Number.prototype.toNumber=function(){return this}

/*
    SIMULATOR THREAD
    --------------------------------------------------------
    Simulator thread. Built to work a little like the socket io wrapper.
    Probably not necessary to wrap it up in a constructor but I like how 
    it feels from a developer's point of view.

    Additionally, if we decide to run multiple variations of the 
    simulator at once we can eailly spawn additional simulators.
*/
var Simulator = function(){
    var _this = this;
    this.worker = new Worker('/javascripts/est/simulator.js');
    this.queue = {};

    /*
        Req handler
    */
    this.worker.onmessage = function(message){
        var res = message.data;
        res.received = Date.now();
        _this.run_callback(res);
    }

    this.req_id = function(){
        var id = "";
        var len = 16;
        while(len--){id += (1 + (Math.random() * 8)).toString()[0];}
        return parseInt(id);
    }

    this.register_callback = function(req, callback){
        this.queue[req.action] = (this.queue[req.action])?this.queue[req.action]:{};
        this.queue[req.action][req.id] = callback;
    }

    this.run_callback = function(res){
        try{
            this.queue[res.action][res.id](res.body);
            delete this.queue[res.action][res.id];
        }catch(er){

        }
    }

    /*
        send data to the worker thread
    */
    this.post = function(action, body, callback){
        var req = {action:action,id:this.req_id(),body:body,sent:Date.now()};
        if(typeof callback === 'function'){this.register_callback(req, callback);}
        this.worker.postMessage(req);
    }

    /*pre canned*/
    this.update = function(obj, callback){
        this.post('update', obj, callback);
    }

    this.get = function(){
        this.post('update', obj, function(r){console.log(r)});
    }
}

/*
    EST
    -----------------------------------------------------------------------------------------------
*/
var EST = function(options) {

    /* SCOPE */
    var _this = this;


    /*  
        System 
        -----------------------------------------------------------------------------------------------
        Thinking about moving this object to the simulator thread.
        It exists in both contexts it feels inky
    */
    this.system = {
        backup_days: [true, true, true, true, true, true, true], // depreciated
        baseline: [],
        max_load: 0, // depreciated
        solar: {
            capacity: 0
        },
        inverter: {
            capacity: 100
        },
        storage: {
            capacity: 200,
            round_trip_efficiency: .8,
            max_dod: .8
        }
    }
    

    /*  Project 

    ----------------------------------------------------------------------------------------------- */
    this.name = "New EST Project";      // duh
    this.customer_name = "EST Customer";// duh
    this.id = null;                     // duh
    this.year = 2013;                   // sample data starts on jan1st of this year

    /*  Data 
    -------------------------------------------------------------------------------------------------- */
    this.optimal_data_length = 35040;   // If data input is less or more than this number EST will trim / append 0 to the data until it fits.
    this.minimum_data_length = 35040;   // If data input is less or more than this number EST will trim / append 0 to the data until it fits.
    this.maximum_data_length = 35040;   // If data input is less or more than this number EST will trim / append 0 to the data until it fits.
    this.dates = [];                    // Date array in milisecond integers
    this.interval_data = [];            // Raw interval data
    this.pv_data = [];                  // Raw PV data
    this.interval_data_with_pv = [];    // Original data after pv has been applied
    this.shaved_interval_data = [];     // The blue gradient line that represents the result of our simulate method
    this.tariff_data = [];              // The current rate plan precalculated and flattened
    this.soc_data = [];                 // Array representing the battery SOC
    this.apply_pv = true;               // Should EST subtract solar production from the load when simulating?

    /*  Tariffs 
    ----------------------------------------------------------------------------------------------- */
    this.annual_tariff_increase = 0.05; // Assume a 5% increase in costs
    this.tariff_scale = 1.0;            // multiplier applied to each tariff.
    this.rate_plan_gants = [];          // Each schedule in a rate plan has two gant chart instances which are referenced here
    this.rate_plan_id = null;              // Need to fix the weirdness
    this.rate_plan = {
        name : "Default",
        non_coincidental : {period : 6, tariff : 0},
        calendar : [1,1,1,1,0,0,0,0,0,1,1,1],
        peak_statuses : ['Off-peak', 'Mid-peak', 'On-peak'],
        schedules : [
        {
            "name": "Summer",
            "onpeak_handling": false,
            "months": [4, 5, 6, 7, 8],
            "weekday": {
                "kw": [10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 12, 12, 12, 12, 12, 12, 13, 13, 13, 13, 13, 13, 13, 13, 13, 12, 12, 12, 12, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10],
                "kwh": [.1, .1, .1, .1, .1, .1, .1, .1, .1, .1, .1, .1, .1, .1, .2, .2, .2, .2, .2, .2, .3, .3, .3, .3, .3, .3, .3, .3, .3, .2, .2, .2, .2, .1, .1, .1, .1, .1, .1, .1, .1, .1, .1, .1, .1, .1, .1, .1],
                "onpeak": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
            },
            "weekend": {
                "kw": [10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 12, 12, 12, 12, 12, 12, 13, 13, 13, 13, 13, 13, 13, 13, 13, 12, 12, 12, 12, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10],
                "kwh": [.1, .1, .1, .1, .1, .1, .1, .1, .1, .1, .1, .1, .1, .1, .2, .2, .2, .2, .2, .2, .3, .3, .3, .3, .3, .3, .3, .3, .3, .2, .2, .2, .2, .1, .1, .1, .1, .1, .1, .1, .1, .1, .1, .1, .1, .1, .1, .1],
                "onpeak": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
            }
        },
        {
            "name": "Winter",
            "months": [0, 1, 2, 3, 9, 11, 10],
            "onpeak_handling": false,
            "weekday": {
                "kw": [10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 12, 12, 12, 12, 12, 12, 13, 13, 13, 13, 13, 13, 13, 13, 13, 12, 12, 12, 12, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10],
                "kwh": [.1, .1, .1, .1, .1, .1, .1, .1, .1, .1, .1, .1, .1, .1, .2, .2, .2, .2, .2, .2, .3, .3, .3, .3, .3, .3, .3, .3, .3, .2, .2, .2, .2, .1, .1, .1, .1, .1, .1, .1, .1, .1, .1, .1, .1, .1, .1, .1],
                "onpeak": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
            },
            "weekend": {
                "kw": [10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 12, 12, 12, 12, 12, 12, 13, 13, 13, 13, 13, 13, 13, 13, 13, 12, 12, 12, 12, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10],
                "kwh": [.1, .1, .1, .1, .1, .1, .1, .1, .1, .1, .1, .1, .1, .1, .2, .2, .2, .2, .2, .2, .3, .3, .3, .3, .3, .3, .3, .3, .3, .2, .2, .2, .2, .1, .1, .1, .1, .1, .1, .1, .1, .1, .1, .1, .1, .1, .1, .1],
                "onpeak": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
            }
        }]
    };

    /*  Proposals 
    --------------------------------------------------------------------------------------------- */
    var macrs_payback_schedule = [0, .2, .32, .192, .1152, .1152, .0576];
    var sgip_payback_schedule = [0, .5, .1, .1, .1, .1, .1];
    this.proposals = [];
    
    /*  Misc 
    -------------------------------------------------------------------------------------------------- */
    this.month_labels = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    this.short_month_labels = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    this.day_labels = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];

    /*  Chart 
    -------------------------------------------------------------------------------------------------- */
    this.colors = [
        '#eb6a5b',
        '#3498db',
        '#9b59b6',
        '#e67e22',
        '#f1c40f',
        '#34495e',
        '#16a085',
        '#8e44ad',
        '#c0392b',
        '#f39c12'];


    /*
        CSV Data Input!
        --------------------------------------------
    */

    /*
        treatCSV
    */
    this.treatCSV = function(csv_string, callback){
        
        var arr = csv_string;
        if(typeof csv_string === 'string'){        
            //'\n' '\r' to ','
            csv_string = csv_string.replace(/(?:\r\n|\r|\n)/g, ',');
            arr = csv_string.split(',');
        }


        var data = new Parallel(arr);
        data.map(function(point){

            if (isNaN(point) || !parseFloat(point) && point != 0)
                return 0

            // parse
            point = parseFloat(point)
            point = point.toFixed(1)
            point = parseFloat(point)

            return point;
        }).then(function(){
            var data = arguments[0];
            
            while(data.length > _this.optimal_data_length){
                data.pop();
            }
            while(data.length < _this.optimal_data_length){
                data.push(0);
            }

            if(typeof callback === 'function'){
                callback(data)
            }
        })
    }

    this.consume_power_csv = function(string, callback) {
        _this.treatCSV(string, function(treated_data){
            _this.interval_data = [];

            if(!!treated_data)
                _this.interval_data = treated_data;
            
            _this.plot_all(true);
            if(!!_this.interval_data)
                $("#interval_data_paste").val(_this.interval_data.join(','))
            
            if(typeof callback === 'function'){
                callback()
            }
        });
        return
    };

    this.consume_pv_csv = function(string, callback) {
        _this.treatCSV(string, function(treated_data){
            if(!treated_data){
                return
            }
            _this.pv_data = treated_data;
            _this.plot_all(false);
            $("#pv_data_paste").val(_this.pv_data.join(','))

            if(typeof callback === 'function'){
                callback()
            }
        });
    };
 
    this.prep_data_for_plot = function(callback) {

        // create interval +pv data series
        if (_this.interval_data.length !== 0 && _this.pv_data.length === _this.interval_data.length) {
            _this.interval_data_with_pv = []; // reset
            for (var i = 0; i < _this.interval_data.length; i++) {
                var reduced = _this.interval_data[i] - _this.pv_data[i];
                reduced = (reduced < 0)? 0 : reduced; // clamp negative numbers. They cound as negative consumption. We arent net metering yet.
                _this.interval_data_with_pv.push(reduced);
            }
        }

        if (_this.shaved_interval_data.length !== _this.interval_data.length) {
            _this.shaved_interval_data = [];
            for (var i = 0; i < _this.interval_data.length; i++) {
                _this.shaved_interval_data.push(_this.interval_data[i] * 1);
            }
        }
    };

    this.force_gant_draw = function(){
        for(var i = 0 ; i < _this.rate_plan_gants.length; i++){
            est.rate_plan_gants[i].weekend_gant.resize();
            est.rate_plan_gants[i].weekday_gant.resize();
        }
    }
    
    this.save_rate_plan = function(cb){

        // Don't knowbody know where that rate plan goes...
        if(!_this.rate_plan.id){
            console.warn("Rate plan has no id")
            return 
        }
        
        /* Prepare to save */
        var utility = (_this.rate_plan.utility)? _this.rate_plan.utility : " - ";
        var url = '/est/rateplans/'+_this.rate_plan.id;
        
        var update_data = {
            title: _this.rate_plan.title,
            utility : utility,
            calendar : JSON.stringify(_this.rate_plan.calendar),
            peak_statuses : JSON.stringify(_this.rate_plan.peak_statuses),
            non_coincidental : JSON.stringify(_this.rate_plan.non_coincidental),
            schedules : JSON.stringify(_this.rate_plan.schedules)
        };

        socket.post(url, update_data, function(d){
            if(typeof cb === 'function')
                cb()
        })
    }
    
    this.fork_rate_plan = function(cb){
        
        /* 
            Stringify the rate plan schedules. 

            The rate plan schedules have references to their gant charts in them so we need this loop to avoid stringifying a function and saving it.
            Eventually the gant instances will be referenced from elsewhere but for the time being this is how it's done.
            
            backslash regret
        */
        var _rp = [];
        
        for(var i = 0; i < _this.rate_plan.schedules.length; i++){
            var schedule = _this.rate_plan.schedules[i];
            _rp.push({
                name : schedule.name,
                months : schedule.months,
                onpeak_handling : schedule.onpeak_handling,
                weekday : schedule.weekday,
                weekend : schedule.weekend
            });
        }
        
        /* Prepare to save */
        var utility = (_this.rate_plan.utility)? _this.rate_plan.utility : " - ";
        var url = '/est/rateplans';
        var update_data = {
            title: "Forked from "+_this.rate_plan.title,
            utility : utility,
            non_coincidental : JSON.stringify(_this.rate_plan.non_coincidental),
            schedules : JSON.stringify(_rp)
        };

        socket.post(url, update_data, function(d){
            if(typeof cb === 'function')
                cb(d)
        })
    }

    this.load_rate_plan = function(id, cb, update_outputs){

        if(!id){return console.warn("An ID is required to load a rate plan")}
        var pending_save = false;
        socket.get('/est/rateplans/'+id, function(r){

            /* 
                Parse
                ---------------------------------------------------------------------------
            */
            var rate_plan = JSON.parse(r);
            rate_plan = rate_plan[0];

            /* 
                support for older rate plans
                --------------------------------------------------------------------------- 
            */
            if(!rate_plan.non_coincidental){
                rate_plan.non_coincidental = {
                    period : 6,
                    tariff : 0
                };
            }

            if(!rate_plan.peak_statuses){
                rate_plan["peak_statuses"] = ['Off-peak', 'Mid-peak', 'On-peak'];
            }

            /*---------------------------------------------------------------------------*/

            /*
                Update EST
                ---------------------------------------------------------------------------
            */
            _this.rate_plan = rate_plan;
            
            for(var s = 0 ; s < _this.rate_plan.schedules.length; s++){
                // console.log(_this.rate_plan.schedules[s])
                for(var p = 0 ; p < _this.rate_plan.schedules[s].weekday.onpeak.length; p++){

                    if(_this.rate_plan.schedules[s].weekday.onpeak[p] === true){
                        _this.rate_plan.schedules[s].weekday.onpeak[p] = 1;
                        pending_save = true
                    }
                    if(_this.rate_plan.schedules[s].weekend.onpeak[p] === true){
                        _this.rate_plan.schedules[s].weekend.onpeak[p] = 1;
                        pending_save = true
                    }

                    if(_this.rate_plan.schedules[s].weekday.onpeak[p] === false){
                        _this.rate_plan.schedules[s].weekday.onpeak[p] = 0;
                        pending_save = true
                    }
                    if(_this.rate_plan.schedules[s].weekend.onpeak[p] === false){
                        _this.rate_plan.schedules[s].weekend.onpeak[p] = 0;
                        pending_save = true
                    }
                }
            }


            _this.rate_plan_ready = false;


            /*
                Update the tariff editor window
                ---------------------------------------------------------------------------
            */
            if(est.rate_plan.public === true){
                $(tariff_editor.contentWrap).children().css('display', 'none');
                $("#tariff_warnings").css("display", "")
                
                var warning = ele("h2:Warning!");
                warning.style.fontSize = "10rem";
                warning.style.color = "#e74c3c";
                var details = ele([
                    'p:You are attmpting to edit a rate plan you do not own.',
                    'p:To continue editing please click "Fork Now".'
                    ]);

                var forkButton = ele('button.grey:<i class="icon-fork"></i> Fork Now');

                forkButton.onclick = function(){
                    _this.fork_rate_plan(function(forked_rate_plan){
                        _this.load_rate_plan(forked_rate_plan[0].id, function(){

                            if(tariff_datatable){
                                tariff_datatable.fetch(function(){
                                    tariff_datatable.render();
                                })
                            }
                        })
                    });
                }


                $("#tariff_warnings").html("")
                $("#tariff_warnings")[0].appendChild(warning);
                $("#tariff_warnings")[0].appendChild(details);
                $("#tariff_warnings")[0].appendChild(forkButton);

            }else{
                $(tariff_editor.contentWrap).children().css('display', '')
                $("#tariff_warnings").html("")
            }

            _this.create_rate_plan_inputs();

            $("#rate_plan_title").val(rate_plan.title)
            $("#rate_plan_utility").val(rate_plan.utility)
            $("#non_coincidental_price").val(rate_plan.non_coincidental.tariff).change()
            $("#non_coincidental_period").val(rate_plan.non_coincidental.period).change()

            for (var plan = 0; plan < _this.rate_plan.schedules.length; plan++) {
                var rp = _this.rate_plan.schedules[plan];
               _this.rate_plan_gants[plan].weekday_gant.blocks = _this.rates_to_gant_blocks(rp.weekday);
               _this.rate_plan_gants[plan].weekend_gant.blocks = _this.rates_to_gant_blocks(rp.weekend);
            }
            
            _this.force_gant_draw();

            if(!update_outputs || update_outputs !== false){
                _this.update_result_outputs();
            }

            if(typeof cb === 'function'){
                cb();
            }
            /*
                Again. Does not work on the initial call unless I qeue it up with a timeout. weird.
            */
            if(pending_save === true){
                est.save_rate_plan();
            }
            _this.create_tariff_chart_data();
        })
    }
    
    
    this.generate_dates = function(callback) {

        var now = new Date('1/1/' + _this.year); // using this date because I know DST is not in effect
        var offset = now.getTimezoneOffset() * 60000;
        var start = new Date(now.getUTCFullYear(), now.getUTCMonth(), now.getUTCDate(), now.getUTCHours(), now.getUTCMinutes(), now.getUTCSeconds());
        var interval = 900000;

        start = start.getTime() - offset;
        _this.dates = [];
        for (var i = 0; i < _this.optimal_data_length; i++) {
            var date = start + (i * interval);
            _this.dates.push(date)
        }

        if (typeof callback === 'function') {
            callback();
        }
    }

    /* THIS is a mess. It didn't start so intensely intense but it's gotten out of hand */
    this.create_rate_plan_inputs = function() {

        var wrap = $("#tariff_editor")[0];
        var $wrap = $(wrap);
        
        // destroy tabs
        if ($wrap.hasClass("ui-tabs")) {
            $wrap.tabs('destroy');
        }

        // Clear the container element
        wrap.innerHTML = "";

        /*
            The template used to generate these 
        */
        var non_coincidental_tariff_input = ele("input(value='"+_this.rate_plan.non_coincidental.tariff.makeMoney()+"')");
        var non_coincidental_period_input = ele("input(value='"+_this.rate_plan.non_coincidental.period+" Months')");
        
        non_coincidental_tariff_input.addEventListener('change', function(){
            _this.rate_plan.non_coincidental.tariff = this.value.toNumber();
            this.value = "$"+this.value.toNumber();
        })

        non_coincidental_period_input.addEventListener('change', function(){
            _this.rate_plan.non_coincidental.period = this.value.toNumber();
            this.value = this.value.toNumber()+" Months";
        })


        /*
            This gets stuffed and then compiled into html
        */
        var template = {
            "label:Calendar" : "",
            "#calendar_wrap.row.center.white.pill-box-h": [],
            "#non_coincidental_wrap.row.marg-n": [
                {'.col-1-2.pad-e': [
                    "label:Non-Coincidental Tariff($/kW)",
                    non_coincidental_tariff_input,
                ]},
                {'.col-1-2.pad-w': [
                    "label:Non-Coincidental Period",
                    non_coincidental_period_input
                ]}
            ],
            "#rateplan_schedules" : [
                {"ul#rate_plan_tab_list" : []}
            ]
        }

        /*
            create calendar
        */
        for(var m = 0; m < _this.rate_plan.calendar.length; m ++){

            if(!_this.rate_plan.schedules[_this.rate_plan.calendar[m]]){
                _this.rate_plan.calendar[m] = 0
            }

            var schedule = _this.rate_plan.schedules[_this.rate_plan.calendar[m]];
            var month = ele(".col-1-12(data-month='"+m+"' href='#' data-schedule='"+_this.rate_plan.calendar[m]+"')");
            var label = ele("a.black.hover-blue:"+_this.month_labels[m]+" <br /> "+schedule.name)
            month.appendChild(label)
            month.onclick = function(){

                if(this.dataset.schedule === (_this.rate_plan.schedules.length - 1)){
                    this.dataset.schedule = 0;
                }else{
                    this.dataset.schedule++;
                }

                _this.rate_plan.calendar[this.dataset.month] = this.dataset.schedule.toNumber();
                console.log(_this.rate_plan.calendar)
                _this.create_rate_plan_inputs()
            }
            template["#calendar_wrap.row.center.white.pill-box-h"].push(month)
        }


        /*
            Create tabs and gants for schedules
        */
        for(var s = 0; s < _this.rate_plan.schedules.length; s++){

            var tab_list = template["#rateplan_schedules"][0]["ul#rate_plan_tab_list"];
            var tabs_wrap = template["#rateplan_schedules"];
            var schedule = _this.rate_plan.schedules[s];
            var tab_identifier = "tariff_schedule_"+s;
            var anchor = ele("a(href='#"+tab_identifier+"' style='position:relative;')")
            var input = ele("input(value='"+schedule.name+"' data-schedule='"+s+"')")
            var del = ele("i.icon-trash-2.grey.hover-red(data-schedule='"+s+"' style='position:absolute; top:1px; right:4px;')");
            
            del.onclick = function(){

                if(_this.rate_plan.schedules.length === 1){
                    return alert("Rate plans must have at least one schedule.")
                }

                _this.rate_plan.schedules.splice((this.dataset.schedule.toNumber()), 1)
            
                for(var m = 0; m < _this.rate_plan.calendar.length; m++){
                    if(_this.rate_plan.calendar[m] === this.dataset.schedule){
                        _this.rate_plan.calendar[m] = 0
                    }
                }
                _this.create_rate_plan_inputs()
            }

            input.onchange = function(){
                _this.rate_plan.schedules[this.dataset.schedule].name = this.value;
            }
            anchor.appendChild(input)
            anchor.appendChild(del)
            tab_list.push({
                "li": [
                anchor
            ]})

            var tabs = {};
            var weekday_gant = ele("#schedule_"+s+"_weekday_gant");
            var weekend_gant = ele("#schedule_"+s+"_weekend_gant");

            var labels = [];
            for (var i = 0; i < 24; i++) {
                var className = (i === 0) ? ".col-1-48.center.small" : ".col-2-48.center.small";
                className += "(style='line-height:5rem;')";
                var html = (i !== 0) ? i + ":00" : " - ";
                labels.push(className+":"+html)
            }

            tabs[".pad#"+tab_identifier] = [
                "label.marg-n:Weekday Tariff",
                {".row.white":labels},
                weekday_gant,
                "label.marg-n:Weekend Tariff",
                {".row.white":labels},
                weekend_gant
            ]

            /*
                Gant Charts
            */
            _this.rate_plan_gants[s] = {};

            _this.rate_plan_gants[s].weekday_gant = new Gantron({
                container: weekday_gant,
                inputs: [{
                    placeholder: 'kW Tariff',
                    prefix: '$',
                    suffix: ' / kW'
                }, {
                    placeholder: 'kWh Tariff',
                    prefix: '$',
                    suffix: ' / kWh'
                }, {
                    placeholder: 'Off-peak',
                    type: 'toggle',
                    values: _this.rate_plan.peak_statuses
                }],
                onchange: function() {
                    var values = this.serialize();
                    var rateplan = this.rate_plan;

                    // apply values to rate plans
                    for (var i = 0; i < values.length; i++) {
                        if (values[i]) {

                            var kw = values[i][0].toNumber(); // remove currency formatting
                            var kwh = values[i][1].toNumber(); // remove currency formatting
                            var onpeak = values[i][2];

                            rateplan.weekday.kw[i] = (isNaN(kw)) ? 0 : kw;
                            rateplan.weekday.kwh[i] = (isNaN(kwh)) ? 0 : kwh;
                            rateplan.weekday.onpeak[i] = onpeak;

                        } else {

                            rateplan.weekday.kw[i] = 0;
                            rateplan.weekday.kwh[i] = 0;
                            rateplan.weekday.onpeak[i] = 0;

                        }
                    }
                }
            });

            _this.rate_plan_gants[s].weekend_gant = new Gantron({
                container: weekend_gant,
                inputs: [{
                    placeholder: 'kW Tariff',
                    prefix: '$',
                    suffix: ' / kW'
                }, {
                    placeholder: 'kWh Tariff',
                    prefix: '$',
                    suffix: ' / kWh'
                }, {
                    placeholder: 'Off-peak',
                    type: 'toggle',
                    values: _this.rate_plan.peak_statuses
                }],
                onchange: function(values) {
                    var values = this.serialize();
                    var rateplan = this.rate_plan;

                    // apply values to rate plans
                    for (var i = 0; i < values.length; i++) {

                        if (values[i]) {

                            var kw = parseFloat(values[i][0].replace(/[^0-9\.]+/g, "")); // remove currency formatting
                            var kwh = parseFloat(values[i][1].replace(/[^0-9\.]+/g, "")); // remove currency formatting
                            var onpeak = values[i][2];

                            rateplan.weekend.kw[i] = (isNaN(kw)) ? 0 : kw;
                            rateplan.weekend.kwh[i] = (isNaN(kwh)) ? 0 : kwh;
                            rateplan.weekend.onpeak[i] = onpeak;

                        } else {

                            rateplan.weekend.kw[i] = 0;
                            rateplan.weekend.kwh[i] = 0;
                            rateplan.weekend.onpeak[i] = 0;

                        }

                    }
                }
            });

            _this.rate_plan_gants[s].weekday_gant.rate_plan = schedule;
            _this.rate_plan_gants[s].weekend_gant.rate_plan = schedule;
            _this.rate_plan_gants[s].weekday_gant.blocks = _this.rates_to_gant_blocks(_this.rate_plan.schedules[s].weekday);
            _this.rate_plan_gants[s].weekend_gant.blocks = _this.rates_to_gant_blocks(_this.rate_plan.schedules[s].weekend);

            tabs_wrap.push(tabs)
        }

        template["#rateplan_schedules"][0]["ul#rate_plan_tab_list"].push({"li#new_schedule": {"a":"i.icon-plus-6.grey.button.hover-green"}})
        
        // compile and append
        wrap.innerHTML = "";
        wrap.appendChild(ele(template));
        $("#rateplan_schedules").tabs({
            activate: _this.force_gant_draw
        })
        $("#new_schedule").unbind('click').on('click',function(){
            _this.rate_plan.schedules.push(_this.rate_plan.schedules[_this.rate_plan.schedules.length -1])
            _this.create_rate_plan_inputs()
        })
        _this.force_gant_draw()

        return
    }

    this.get_rate_plan_schedule_by_date = function(date) {
        var month = new Date(date).getMonth();

        if(!_this.rate_plan.calendar[month]){
            _this.rate_plan.calendar[month] = 0
        }

        return _this.rate_plan.calendar[month]
    }

    this.peak_status = function(date) {
        // var time = new Delta("Is On Peak");
        var month = date.getMonth();
        var halfHour = date.getHalfHour();
        var day = date.getDay();
        var rate_plan = _this.rate_plan.schedules[_this.get_rate_plan_schedule_by_date(date)];
        var isWeekend = date.isWeekEnd();

        // time.end();
        if (isWeekend === true) {
            return rate_plan.weekend.onpeak[halfHour];
        } else {
            return rate_plan.weekday.onpeak[halfHour];
        }
    }

    /*
        Refactored from get_tariff
    */
    this.get_tariff_by_date = function(date) {
        var date = new Date(date);
        var rate_plan = _this.get_rate_plan_schedule_by_date(date);
        var halfHour = date.getHalfHour();
        var wknd = (date.isWeekEnd()) ? "weekend" : "weekday";

        tariff = {
            kw: _this.rate_plan.schedules[rate_plan][wknd].kw[halfHour] * _this.tariff_scale,
            kwh: _this.rate_plan.schedules[rate_plan][wknd].kwh[halfHour] * _this.tariff_scale,
            peak_status: _this.rate_plan.schedules[rate_plan][wknd].onpeak[halfHour]
        }

        /*
            Create non_coincidental for old rate plans
        */
        if(_this.rate_plan.non_coincidental){
            tariff.non_coincidental =  _this.rate_plan.non_coincidental.tariff;
        }else{
            tariff.non_coincidental = 0;
        }

        return tariff;
    }

    this.get_tariff = function(i){
        if(!_this.tariff_data[i]){
            _this.tariff_data[i] = _this.get_tariff_by_date(_this.dates[i])
        }
        return _this.tariff_data[i];
    }

    var elements = {
        non_coincidental_spend_total :   $("#non_coincidental_spend_total")[0],
        before_peak_spend_total :   $("#before_peak_spend_total")[0],
        after_peak_spend_total :    $("#after_peak_spend_total")[0],
        before_spend :              $("#before_spend")[0],
        before_spend_onpeak :       $("#before_spend_onpeak")[0],
        before_spend_offpeak :      $("#before_spend_offpeak")[0],
        after_spend :               $("#after_spend")[0],
        after_spend_onpeak :        $("#after_spend_onpeak")[0],
        after_spend_offpeak :       $("#after_spend_offpeak")[0],
        total_after_output :        $("#total_after_output")[0],
        peak_data_table :           ele("#peak_table.data-table.center")
    }

    /*
        Is part of the interface. Needs to be removed from EST.
        Is part of the interface. Needs to be removed from EST.
    */
    this.peak_table = new DataTable({
        container : elements.peak_data_table,
        labels : {
            'month' : 'Month',
            'demand' : 'Demand',
            'demand_adjusted' : 'Demand Adjusted',
            'non_coincidental' : 'Non-Coincidental',
            'non_coincidental_adjusted' : 'Non-Coincidental Adjusted',
            'total' : 'Total',
            'total_adjusted' : 'Total Adjusted',
        },
        data : []
    });

    this.peak_dialogue = new Dialogue({
        title : "Peak Spend Details",
        content : [elements.peak_data_table]
    });

    this.bar_settings = {
        key : 'demand',
        property : 'spend'
    }

    this.update_result_outputs = function() {

        if(_this.interval_data.length < _this.optimal_data_length){return;}

        // var time = new Delta("Update Result Outputs");
        // GET SPEND
        // -------------------------------------------------------------------------------------------
        var result = _this.calc.master();
        var original = result.original;
        var adjusted = result.adjusted;

        // PREP DATA FOR CHART.JS
        // -------------------------------------------------------------------------------------------
        var pi = [];
        var plot = 'spend';
        var labels = [];
        var colors = [
            ["#0096ff", "#008bff"],
            ["#f1c40f", "#f39c12"],
            ["#e74c3c", "#d34334"],
            ["#25C225", "#35BA35"],
        ]
        
        for(var s = 0; s < adjusted.total.consumption.peak_status.length; s++){
            pi[s] = {
                value: adjusted.total.consumption.peak_status[s].spend,
                color:colors[s][0],
                highlight: colors[s][1],
                label: _this.rate_plan.peak_statuses[s]
            }

            var swatch = ele("div.est-swatch");
            swatch.style.backgroundColor = colors[s][0];

            labels.push({
                'div.row.flex.middle': [
                    {'div.col-1-2':[
                        swatch,
                        "span:"+_this.rate_plan.peak_statuses[s]
                    ]},
                    'div.col-1-2:'+adjusted.total.consumption.peak_status[s].spend.makeMoney()
                ]
            })
        }


        $("#labels")[0].innerHTML = '';
        $("#labels")[0].appendChild(ele(labels));

        update_pi(pi)
        
        /*
            peak data table update
        */
        var grand_total = 0;
        var adjusted_total = [];
        var original_total = [];

        for(var m = 0; m < adjusted.month.length; m++){

            adjusted_total[m] =  {
                demand : 0,
                consumption : 0,
                non_coincidental : 0,
            }
            original_total[m] = {
                demand : 0,
                consumption : 0,
                non_coincidental : 0,
            }

            for(var metric in adjusted.month[m]){

                if(metric === 'non_coincidental'){

                    grand_total += adjusted.month[m][metric].spend;

                    adjusted_total[m].non_coincidental += adjusted.month[m][metric].spend;
                    original_total[m].non_coincidental += original.month[m][metric].spend;

                }else{
                    for(var s = 0; s < adjusted.month[m][metric].peak_status.length; s++){
                        if(typeof adjusted.month[m][metric].peak_status[s] !== 'undefined'){                        
                            grand_total += adjusted.month[m][metric].peak_status[s].spend;

                            adjusted_total[m][metric] += adjusted.month[m][metric].peak_status[s].spend;
                            original_total[m][metric] += original.month[m][metric].peak_status[s].spend;
                            
                        }
                    }   
                }
                
            }
        }

        elements.total_after_output.innerHTML = grand_total.makeMoney();

        var peak_table_data = [];
        for(var m = 0; m < adjusted_total.length; m++){

             peak_table_data.push({
                month : _this.month_labels[m],
                
                demand : original_total[m].demand.makeMoney(),
                demand_adjusted : adjusted_total[m].demand.makeMoney(),

                non_coincidental : original_total[m].non_coincidental.makeMoney(),
                non_coincidental_adjusted : adjusted_total[m].non_coincidental.makeMoney(),

                total : (original_total[m].non_coincidental + original_total[m].demand).makeMoney(),
                total_adjusted : (adjusted_total[m].non_coincidental + adjusted_total[m].demand).makeMoney(),
            })    
        }

        _this.peak_table.data = peak_table_data;
        _this.peak_table.render();

        var pchart = {
            original : [],
            adjusted : [],
            non_coincidental : []
        }

        for(var m = 0; m < original_total.length; m++){
            pchart['original'].push(Math.round(original_total[m].demand));
            pchart['adjusted'].push(Math.round(adjusted_total[m].demand));
            pchart['non_coincidental'].push(Math.round(adjusted_total[m].non_coincidental));
        }

        update_peak_chart(
            pchart.original,
            pchart.adjusted
        )
        
        $("#before_peak_spend_total")[0].innerHTML = original.total.demand.spend.makeMoney()
        $("#after_peak_spend_total")[0].innerHTML = adjusted.total.demand.spend.makeMoney()
        $("#non_coincidental_spend_total")[0].innerHTML = adjusted.total.non_coincidental.spend.makeMoney()

        /*
            vvvvvvvvvvvvvvvvvvvvvvv THIS IS A PROBLEM vvvvvvvvvvvvvvvvvvvvv
            vvvvvvvvvvvvvvvvvvvvvvv THIS IS A PROBLEM vvvvvvvvvvvvvvvvvvvvv
            vvvvvvvvvvvvvvvvvvvvvvv THIS IS A PROBLEM vvvvvvvvvvvvvvvvvvvvv
        */
        
        var original_with_solar = [];
        if (_this.pv_data.length === _this.interval_data.length) {
            var l = _this.interval_data.length;
            for (var i = 0; i < l; i++) {
                original_with_solar.push(_this.interval_data[i] - _this.pv_data[i]);
            }
            var after_pv_peaks = _this.calc.highest_peaks(original_with_solar); // original + pv
        }

        /*
            ^^^^^^^^^^^^ THIS IS A PROBLEM ^^^^^^^^^^^^^^^^^^^^
            ^^^^^^^^^^^^ THIS IS A PROBLEM ^^^^^^^^^^^^^^^^^^^^
            ^^^^^^^^^^^^ THIS IS A PROBLEM ^^^^^^^^^^^^^^^^^^^^
        */


        // BASELINES
        // -------------------------------------------------------------------------------------------
        // Need to create a cache for these elements!
        // Actually I don't. Ran some tests and it takes 1 - 3 ms to complete.
        // -------------------------------------------------------------------------------------------
        for (var i = 0; i < adjusted.month.length; i++) {
            var original_month_peak = {
                power : 0,
                spend : 0
            };
            var adjusted_month_peak = {
                power : 0,
                spend : 0
            };

            /*
                Want the tallest peak from all of the month's peaks
            */
            for(var s = 0; s < adjusted.month[i].demand.peak_status.length; s++){
                if(adjusted.month[i].demand.peak_status[s].power > adjusted_month_peak.power){
                    adjusted_month_peak.power = adjusted.month[i].demand.peak_status[s].power;
                    adjusted_month_peak.spend = adjusted.month[i].demand.peak_status[s].spend;
                    adjusted_month_peak.date = adjusted.month[i].demand.peak_status[s].date;
                }

                if(original.month[i].demand.peak_status[s].power > original_month_peak.power){
                    original_month_peak.power = original.month[i].demand.peak_status[s].power;
                    original_month_peak.spend = original.month[i].demand.peak_status[s].spend;
                    original_month_peak.date = original.month[i].demand.peak_status[s].date;
                }
            }

            if($("#baseline_" + i)[0]){

                $("#baseline_" + i)[0].innerHTML = "(" + kwlabel(_this.system.baseline[i]) + ")";
                $("#baseline_input_" + i)[0].value = kwlabel(_this.system.baseline[i]);
                $("#original_peak_" + i)[0].innerHTML = kwlabel(original_month_peak.power);
                // $("#after_pv_peak_" + i)[0].innerHTML = (original_with_solar.length > 0) ? kwlabel(after_pv_peaks[i].power) : " - ";
                $("#final_peak_" + i)[0].innerHTML = kwlabel(adjusted_month_peak.power);

                if (_this.system.baseline[i] < adjusted_month_peak.power) {
                    console.log(_this.system.baseline[i])
                    console.log(adjusted_month_peak.power)
                    $("#baseline_input_" + i)[0].style.color = "red";
                    $("#final_peak_" + i)[0].style.color = "red";
                    $("#final_peak_" + i)[0].style.cursor = "pointer";
                    $("#baseline_" + i)[0].style.color = "red";
                    $("#final_peak_" + i)[0].date = adjusted_month_peak.date;
                    $("#final_peak_" + i)[0].onclick = function(){
                        _this.chart.zoom_to_date_range(this.date - 90000000, this.date + 90000000);
                    };

                } else {
                    $("#baseline_input_" + i)[0].style.color = "";
                    $("#final_peak_" + i)[0].style.color = "";
                    $("#baseline_" + i)[0].style.color = "";
                    $("#final_peak_" + i)[0].style.cursor = "";
                    $("#final_peak_" + i)[0].onclick = function(){};
                }
                
            }

        }

        // return
        _this.update_roi();
    }

    this.update_roi = function(){
        $("#system_cost")[0].innerHTML = _this.calc.system_cost().total.makeMoney();
        var roi = _this.calc.roi();
        if(roi){
            var years = (((roi.years * 365) + roi.days) / 365 ).toFixed(2);
            if(!years || isNaN(years) || typeof years !== 'string'){
                $(".est-output-roi").html("Calculating...");
                console.log("ROI failed")
                console.log(roi)
                console.log(years)
                return window.setTimeout(_this.update_roi, 2000);
            }else{
                $(".est-output-roi").html(years+" Years");
            }
        }else{
            console.warn("ROI calculation failed")
        }
    }

    /*
        Translates a rate plan object into gant chart block data
    */
    this.rates_to_gant_blocks = function(rates) {
        var result = [];
        var tmp = null;

        for (var i = 0; i < rates.kw.length; i++) {

            var kw = rates.kw[i];
            var kwh = rates.kwh[i];
            var onpeak = rates.onpeak[i];

            if (tmp === null) {
                tmp = {
                    location: [i, i],
                    style: "#eeeeee",
                    value: [kw, kwh, onpeak]
                };
            }

            tmp.location[1] = i;

            if (
                tmp.value[0] !== kw ||
                tmp.value[1] !== kwh ||
                tmp.value[2] !== onpeak
            ) {
                result.push(tmp);
                tmp = {
                    location: [i, i],
                    style: "#eeeeee",
                    value: [kw, kwh, onpeak]
                };
            }

        }

        result.push(tmp);


        /*
            Reformats again to something more human readable...
        */
        for (var i = 0; i < result.length; i++) {

            result[i].value[0] = "$" + result[i].value[0] + " / kW";
            result[i].value[1] = "$" + result[i].value[1] + " / kWh";


            /*
                -----------------------------------------------------------------------------------------------------------
                Leaving this in for older rate plans that might be using a boolean to tell if we're on or off peak
            */

            if (result[i].value[2] === true) {
                result[i].value[2] = 1;
            }
            if (result[i].value[2] === false) {
                result[i].value[2] = 0;
            }
            
            /*
                Leaving this in for older rate plans that might be using a boolean to tell if we're on or off peak
                -----------------------------------------------------------------------------------------------------------
            */

        }

        result[result.length - 1].location[1] = rates.kw.length;

        return result;
    }

    this.plot_styles = [{
        color: "rgba(150,150,150,.8)",
        style: "fill"
    }, {
        color: "rgba(91,155,213,.65)",
        style: "fill"
    }, {
        color: "#e59855",
        style: "line"
    }, {
        color: "#72cc98",
        style: "line"
    }];

    this.plot_all = function(resetZoom) {

        if (!_this.chart) {
            return;
        }

        var resetZoom = (resetZoom) ? resetZoom : false;
        var datasets = [];

        this.prep_data_for_plot();
        window.setTimeout(function() {

            if (_this.interval_data.length !== 0) {
                datasets.push(_this.interval_data);
                _this.chart.colors[datasets.length - 1] = _this.plot_styles[0].color;
                _this.chart.plot_styles[datasets.length - 1] = _this.plot_styles[0].style;
            }

            if (_this.shaved_interval_data.length === _this.interval_data.length) {

                var gradient = _this.chart.tariff_ctx.createLinearGradient(0, 0, 0, (_this.chart.size.height - _this.chart.scrubber.height));
                gradient.addColorStop(0, "rgba(0,212,255,.75)");
                gradient.addColorStop(1, "rgba(0,151,255,.7)");

                datasets.push(_this.shaved_interval_data);
                _this.chart.colors[datasets.length - 1] = gradient;
                _this.chart.plot_styles[datasets.length - 1] = _this.plot_styles[1].style;
            }

            if (_this.pv_data.length === _this.interval_data.length) {
                datasets.push(_this.pv_data);
                _this.chart.colors[datasets.length - 1] = _this.plot_styles[2].color;
                _this.chart.plot_styles[datasets.length - 1] = _this.plot_styles[2].style;
            }

            if (_this.soc_data.length === _this.interval_data.length) {
                datasets.push(_this.soc_data);
                _this.chart.colors[datasets.length - 1] = _this.plot_styles[3].color;
                _this.chart.plot_styles[datasets.length - 1] = _this.plot_styles[3].style;
            }

            _this.chart.overlay_needs_update = true;
            _this.chart.update_data(datasets, resetZoom);

        },0);
    }

    /*
        
    */
    this.create_baseline_inputs = function() {
        if (!options.baseline_container) {
            return;
        }

        options.baseline_container.innerHTML = "";
    
        var tab_parent = ele();
        var ul = ele("ul");
        tab_parent.appendChild(ul);
        
        var result = _this.calc.master();

        var peaks = {
            original : [],
            adjusted : []
        };

        var original_peaks = [];
        var shaved_peaks = [];

        /*
            find the highest peak of x peaks where x is the number of peak statuses in the rate plan.
        */
        for(var thing in result){

            for(var m = 0; m < result[thing].month.length; m++){
                for(var metric in result[thing].month[m]){
                    if(metric === 'demand'){

                        if(!peaks[thing][m]){
                            peaks[thing][m] = {
                                power : 0,
                                spend : 0
                            }
                        }

                        for(var s = 0; s < result[thing].month[m][metric].peak_status.length; s++){
                            if(result[thing].month[m][metric].peak_status.power > peaks[thing][m].power){
                                peaks[thing][m].power = result[thing].month[m][metric].peak_status.power;
                                peaks[thing][m].spend = result[thing].month[m][metric].peak_status.spend;
                                peaks[thing][m].date = result[thing].month[m][metric].peak_status.date;
                            }
                        }
                    }
                }
            }

        }


        for (var i = 0; i < 12; i++) {
            
            // If baselines don't exist for whatever reason just assume they are the demand peak (no shaving)
            if (!_this.system.baseline[i]) {
                _this.system.baseline[i] = peaks.adjusted[i].power;
            }

            var row = ele('div.row');
            var left_col = ele('div.col-2-4');
            var right_col = ele('div.col-2-4');

            left_col.style.borderRight = "10px solid transparent";

            row.appendChild(left_col);
            row.appendChild(right_col);

            var li = ele("li");
            li.month = i + 1;
            li.year = _this.year;

            li.style.width = 100 / 12 + "%";

            var a = ele("a");

            a.innerHTML = _this.short_month_labels[i] + " <br /><small id='baseline_" + i + "'>(" + kwlabel(_this.system.baseline[i]) + ")</small>";
            a.href = "#baseline-" + _this.month_labels[i];

            li.appendChild(a);
            ul.appendChild(li);

            // zoom chart on tab click
            li.onclick = function() {
                var days_in_month = daysInMonth(this.month, this.year);

                var start = this.month + "/1/" + this.year;
                var end = this.month + "/" + days_in_month + "/" + this.year;

                _this.chart.zoom_to_date_range(start, end);
            }

            var tab = ele();
            tab.id = "baseline-" + _this.month_labels[i];

            var inner_tab = ele("div.est-component-wrap");
            tab.appendChild(inner_tab);

            var label = ele("h3:"+_this.month_labels[i]); // The tab label
            // label.innerHTML = _this.month_labels[i];

            // baseline text input and label
            var input = ele("input");
            input.label = a;
            input.id = "baseline_input_" + i;

            var baseline_input_label = ele('label');
            baseline_input_label.innerHTML = 'Baseline';
            // ----------------------------------------------


            /*  this should not be in the loop!  12 * 12 * 2 * 37040 = redonk loops */
            // PEAK OUTPUTS ---------------------------------------------
            var original_peak = peaks.original[i];
            var shaved_peak = peaks.adjusted[i];

            // original peak output --------------------------------
            var original_peak_container = ele("div.col-1-3");

            var original_peak_label = ele('label:Original Peak');
            var original_peak_output = ele('span.est-baseline-output#original_peak_'+i+':'+kwlabel(original_peak.power));

            original_peak_container.appendChild(original_peak_label);
            original_peak_container.appendChild(original_peak_output);
            right_col.appendChild(original_peak_container);

            // PV peak output --------------------------------
            var pv_peak_container = ele('div.col-1-3');

            var pv_peak_label = ele('label');
            var pv_peak_output = ele('span.est-baseline-output');

            pv_peak_label.innerHTML = 'With PV Peak';
            pv_peak_output.innerHTML = " - ";
            pv_peak_output.id = "after_pv_peak_" + i;

            pv_peak_container.appendChild(pv_peak_label);
            pv_peak_container.appendChild(pv_peak_output);
            right_col.appendChild(pv_peak_container);

            // PV and storage peak output --------------------------------
            var final_peak_container = ele("div.col-1-3");

            var final_peak_label = ele('label:Estimated Peak');
            var final_peak_output = ele('span.est-baseline-output#final_peak_'+i+':'+kwlabel(shaved_peak.power));

            final_peak_container.appendChild(final_peak_label);
            final_peak_container.appendChild(final_peak_output);
            right_col.appendChild(final_peak_container);

            var spacer = ele('div.est-spacer-2');

            // Baseline slider ---------------------------------------
            var slider = ele("div#"+_this.month_labels[i] + "_baseline_slider.est-slider");
            slider.month = i;
            slider.input = input;
            slider.label = a;
            slider.final_peak_output = final_peak_output;
            // ----------------------------------------------


            var backup_days = ele('div:<p> - </p>');

            //inner_tab.appendChild(label);
            inner_tab.appendChild(row);
            left_col.appendChild(baseline_input_label);
            left_col.appendChild(input);
            inner_tab.appendChild(spacer);
            left_col.appendChild(slider);

            tab_parent.appendChild(tab);
            input.value = kwlabel(_this.system.baseline[i]);

            $(slider).noUiSlider({
                start: _this.system.baseline[i],
                connect: 'lower',
                behaviour: 'extend-tap',
                range: {
                    'min': 0,
                    'max': 5000
                }
            });

            $(slider).on('change', function() {
                var month = $(this)[0].month;
                var input = $("#baseline_input_" + month)[0];
                var label = $(this)[0].label;
                var final_peak_output = $(this)[0].final_peak_output;
                var newBaseline = parseInt($(this).val());

                _this.system.baseline[month] = newBaseline;

                input.value = kwlabel(newBaseline);

                _this.simulate(function() {
                    _this.update_result_outputs();
                });
            });

            $(slider).on('slide', function() {
                var month = $(this)[0].month;
                var input = $("#baseline_input_" + month)[0];

                var v = parseInt($(this).val());

                input.value = kwlabel(v);
            });

            $(input).data('month', i);
            $(input).data('slider', $(slider));

            $(input).on('change', function() {
                var month = $(this).data('month');
                var slider = $(this).data('slider');
                var label = $(this)[0].label;

                var v = parseInt($(this)[0].value);
                _this.system.baseline[month] = v;

                $(this)[0].value = kwlabel(v);
                $(slider).val(v, {
                    animate: true
                });

                _this.system.baseline[month] = v;

                _this.simulate(function() {
                    _this.update_result_outputs();
                });

            });


            tick(input);
        }
        options.baseline_container.appendChild(tab_parent);
        

        if ($(options.baseline_container).hasClass("ui-tabs")) {
            $(options.baseline_container).tabs('destroy');
        }

        $(options.baseline_container).tabs();
    }

    /*
        Pass a system and some interval data to a dedicated thread.
    */
    _this.simulator = new Simulator();
    

    /*
        Simulates a year with the current system params on a 2nd thread.
    */
    this.simulate = function(callback, plot) {

        /*
            updating everything in the thread on every simulate call is unnecessary.
            The biggest performance hit comes from

                *   interval_data
                *   pv_data
                *   dates
            These arrays are massive and should only update once
        */

        _this.simulator.update({
            system:_this.system,
            interval_data:_this.interval_data,
            pv_data:_this.pv_data,
            apply_pv:_this.apply_pv,
            interval_data_with_pv:_this.interval_data_with_pv,
            rate_plan:_this.rate_plan,
            dates:_this.dates
        }, function(r){
            _this.simulator.post('simulate',{logic:'shave'}, function(r){

                _this.shaved_interval_data = r.shaved_interval_data;
                _this.soc_data = r.soc_data;
                _this.plot_all();
                _this.resize();

                if (typeof callback === 'function') {
                    callback();
                }
                
            })
        });

        return;
    }


    /*
        Baseline solver
    */
    var optimizing = false;
    var perpetual = false;
    this.async_optimize_baselines = function(callback) {
        if (_this.system.inverter.capacity < 1 || _this.system.storage.capacity < 1 || optimizing === true) {
            callback();
            return;
        }
        optimizing = true;
        _this.simulator.post('optimize', {}, function(result){
            optimizing = false;
            if(typeof callback === 'function'){
                console.log(callback)
                callback(result)
            }
        })
        return
    }    
    
    this.init_chart = function() {
        _this.chart = new Triceraplots({
            container: options.chart_container,
            // data: [_this.interval_data, _this.pv_data],
            data: [
                [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0], 
                [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
            ],
            crunch_file: '/javascripts/triceraplots/triceraplots-worker.js',
            dates: _this.dates,
            y_axis_align : 'right',
            label_format : function(val){
                if(val > 1000){return (val / 1000).toFixed(1)+'MWt(h)';}
                return parseInt(val)+' kW(h)';
            },
            autosort: false // the default behavior of the chart is to stack the lines with the largest in the back and the smallest in the front
        });
    }

    this.resize = function() {

        var mq = window.matchMedia("(max-width: 960px)");

        var chart = $(options.chart_container);
        var tabs = $(options.tab_wrap);
        var new_height = (mq.matches) ? "auto" : $(window).height() - chart.outerHeight() - (20 + 64 + 16) + "px";

        tabs[0].style.height = new_height;
        $("#est_bottom_line")[0].style.height = new_height;

        var tab_list_rect = options.tab_list.getBoundingClientRect();
        options.tab_list.style.clip = "rect(-3px, 64px, " + tab_list_rect.height + 5 + "px, -4px)";

        if (mq.matches) {
            $("#output_container")[0].style.height = "auto";
        } else {
            $("#output_container")[0].style.height = "100%";
        }
        
        _this.force_gant_draw();
    }

    this.retrieve_csv_input = function(intv_cb, pv_cb) {

        _this.generate_dates();

        /*Just Hold off until we have a decent amount of interval data*/
        if($("#interval_data_paste").val().split(',').length < 30000){
            return;
        }


        if ($("#interval_data_paste").val().split(',').length > 30000) {

            
            _this.consume_power_csv($("#interval_data_paste").val(), function() {
                
                _this.simulate(function() {
                    _this.init_chart();
                    _this.plot_all(true);
                    _this.create_baseline_inputs();
                    _this.update_result_outputs();
                });

                if(typeof intv_cb === 'function'){
                        intv_cb();
                }
            });

        }

        // PV and interval data are long
        if ($("#pv_data_paste").val().split(',').length > 30000) {

            _this.consume_pv_csv($("#pv_data_paste").val(), function() {
                _this.simulate(function() {
                    _this.plot_all(false);
                    _this.create_baseline_inputs();
                    _this.update_result_outputs();
                });
                if(typeof pv_cb === 'function'){
                    pv_cb();
                }
            });

        }     
    }

    /*
        This method flattens the rate plan into a long array
        It's similar to rasterizing a vector to a bitmap.
    */
    this.create_tariff_chart_data = function() {

        if (!_this.chart) {
            return window.setTimeout(_this.create_tariff_chart_data, 500);
        }

        // The chart uses this to calculate the color of the rate plan
        var max = {
            kw: 0,
            kwh: 0
        };

        var tmp = [];

        for (var i = 0; i < _this.dates.length; i++) {
            var tariff = _this.get_tariff_by_date(_this.dates[i]);
            if (tariff.kw > max.kw) {
                max.kw = tariff.kw;
            };
            if (tariff.kwh > max.kwh) {
                max.kwh = tariff.kwh;
            };
            tmp.push(tariff);
        }
        _this.tariff_data = tmp;
        _this.chart.tariff_data = tmp;
        _this.chart.tariff_max = max;
    }

    /*
        Proposals 
    */

    /* create a proposal object */
    this.generate_proposal_object = function(title, years){

        title |= "Gridz : "+kwlabel(this.system.inverter.capacity)+" / "+kwlabel(this.system.storage.capacity)+" Solarz: "+kwlabel(this.system.solar.capacity);
        years |= 20;

        var cashflow = _this.calc.cashflow(years);
        var proposal = {
            "title" : "Untitled",
            "created" : Date.now(),
            "system_installed" : _this.calc.system_cost().total,
            "estimated_incentives" : _this.calc.incentives(),
            "roi" : _this.calc.roi(),
            "savings" : cashflow[cashflow.length - 1].total,
            "years" : years,
            "inverter" : _this.system.inverter.capacity,
            "storage" : _this.system.storage.capacity,
            "solar" : _this.system.solar.capacity,
            "cashflow" : cashflow
        }

        return proposal;
    }

    /*this...*/
    this.render_proposals = function(){}


    this.generate_system_cost_element = function(proposal) {

        var cf = proposal.cashflow;
        var inc = proposal.estimated_incentives;
        var ROI = proposal.roi;
        ROI = ROI.years + (ROI.days / 365);
        ROI = "h2:"+ROI.toFixed(2) +" Years";

        var template = {
            ".row" : [
                {".col-1-4": [
                    "label:System Installed",
                    "h2:"+proposal.system_installed.makeMoney()
                ]},
                {".col-1-4": [
                    "label:Estimated Incentives",
                    "h2:"+(inc.sgip + inc.itc + inc.macrs_federal + inc.macrs_state).makeMoney()
                ]},
                {".col-1-4": [
                    "label:ROI",
                    ROI
                ]},
                {".col-1-4": [
                    "label:"+cf.length+"yr Savings",
                    "h2:"+cf[cf.length - 1].total.makeMoney()
                ]},

            ]
        };

        return ele(template)
        
    }

    this.generate_system_spec_element = function(proposal) {
        var start = Date.now();
        var row = ele();
        row.className = 'row';

        var system = _this.system;

        var inverter_column = ele("div.col-1-3");
        var inverter_label = ele('label');
        var inverter_value = ele('h2');
        inverter_label.innerHTML = "Inverter";
        inverter_value.innerHTML = kwlabel(proposal.inverter);
        inverter_column.appendChild(inverter_label);
        inverter_column.appendChild(inverter_value);

        var storage_column = ele("div.col-1-3");
        var storage_label = ele('label');
        var storage_value = ele('h2');
        storage_label.innerHTML = "Storage";
        storage_value.innerHTML = kwhlabel(proposal.storage);
        storage_column.appendChild(storage_label);
        storage_column.appendChild(storage_value);

        var solar_column = ele("div.col-1-3");
        var solar_label = ele('label');
        var solar_value = ele('h2');
        solar_label.innerHTML = "Solar";
        solar_value.innerHTML = kwlabel(proposal.solar);
        solar_column.appendChild(solar_label);
        solar_column.appendChild(solar_value);

        row.appendChild(inverter_column);
        row.appendChild(storage_column);
        row.appendChild(solar_column);

        return row;
    }

    this.generate_cashflow_table_element = function(cashflow) {
        var table = ele("div.cashflow-table");
        var dt = new DataTable({
            container : table,
            filter : ['energy_savings', 'demand_savings', 'non_coincidental_savings', 'itc', 'sgip', 'macrs', 'annual'],
            labels : {
                'energy_savings': "Energy Savings", 
                'demand_savings' : "Demand Savings", 
                'non_coincidental_savings' : "Non-Coincidental Savings", 
                'itc' : "ITC", 
                'sgip' : "SGIP", 
                'macrs' : "MACRS", 
                'annual' : "Annual"
            },
            data : cashflow,
            column : {
                non_coincidental_savings : function(n){
                    var col = ele('div');
                    col.innerHTML = Math.round(n['non_coincidental_savings']).makeMoney();
                    return col
                },
                energy_savings : function(n){
                    var col = ele('div');
                    col.innerHTML = Math.round(n['energy_savings']).makeMoney();
                    return col
                },
                demand_savings : function(n){
                    var col = ele('div');
                    col.innerHTML = Math.round(n['demand_savings']).makeMoney();
                    return col
                },
                itc : function(n){
                    var col = ele('div');
                    col.innerHTML = Math.round(n['itc']).makeMoney();
                    return col
                },
                sgip : function(n){
                    var col = ele('div');
                    col.innerHTML = Math.round(n['sgip']).makeMoney();
                    return col
                },
                macrs : function(n){
                    var col = ele('div');
                    col.innerHTML = Math.round(n['macrs']).makeMoney();
                    return col
                },
                annual : function(n){
                    var col = ele('div');
                    col.innerHTML = Math.round(n['annual']).makeMoney();
                    return col
                },
            }
       });
       dt.render();
       return table;
    }

    /* Save a project */
    this.save = function(post, cb){

        if(!_this.id)
            return console.warn("Could not save. EST has no ID.")

        // Save project
        post = (post)? post : {
            title : this.title,
            proposals : JSON.stringify(this.proposals),
            system : JSON.stringify(_this.system),
            interval_data : JSON.stringify(_this.interval_data),
            pv_data : JSON.stringify(_this.pv_data),
            rate_plan : _this.rate_plan.id, // Is relational so we only need to save the ID. This should probably be changed.
            customer_name : _this.customer_name,
            finance : JSON.stringify({
                quote_inverter_price : $("#quote_inverter_price")[0].value,
                quote_storage_price : $("#quote_storage_price")[0].value,
                quote_solar_price : $("#quote_solar_price")[0].value,
                quote_warranty_rate : $("#quote_warranty_rate")[0].value,
                quote_sgip_rate : $("#quote_sgip_rate")[0].value,
                quote_itc_rate : $("#quote_itc_rate")[0].value,
                quote_macrs_federal_rate : $("#quote_macrs_federal_rate")[0].value,
                quote_macrs_state_rate : $("#quote_macrs_state_rate")[0].value,
                quote_macrs_state_rate : $("#quote_macrs_state_rate")[0].value,
                quote_installation_price : $("#quote_installation_price")[0].value,
                quote_dealer_profit_rate : $("#quote_dealer_profit_rate")[0].value,
                annual_tariff_increase : $("#annual_tariff_increase")[0].value
            })
        }
        
        socket.post('/est/projects/'+_this.id, post, function(d){
            d = JSON.parse(d)
            if(typeof cb === 'function')
                cb(d)
        })
    }


    /*
        COMPUTE
        ------------------------------------------------------------
    */
    this.calc = {
        master: function(){

            if(_this.interval_data.length < _this.optimal_data_length){
                return; 
            }
            var start = Date.now();

            var results = {}

            results.original = {
                total : {
                    consumption : {
                        spend : 0,
                        energy : 0,
                        peak_status : []
                    },
                    demand : {
                        spend : 0,
                        power : 0,
                        peak_status : []
                    },
                    non_coincidental :  {
                        spend : 0
                    }
                },
                month : []
            };

            results.adjusted = {
                total : {
                    consumption : {
                        spend : 0,
                        energy : 0,
                        peak_status : []
                    },
                    demand : {
                        spend : 0,
                        power : 0,
                        peak_status : []
                    },
                    non_coincidental :  {
                        spend : 0
                    }
                },
                month : []
            };
            

            function month(i, tariff, peak_status){

                var date = new Date(_this.dates[i]);
                var month = date.getMonth();
                var data = {
                    'original' : _this.interval_data[i],
                    'adjusted' : _this.shaved_interval_data[i]
                }
                
                for(var thing in results){
                    var power = data[thing];
                    var energy = power * .25;
                    var energy_spend = energy * (tariff.kwh * _this.tariff_scale);

                    if(!results[thing].month[month]){

                        results[thing].month[month] = {
                            demand : {
                                peak : {power : 0, spend : 0, date : null},
                                peak_status : [] // by peak status
                            },
                            non_coincidental : {
                                spend : 0,
                                power : 0
                            },
                            consumption : {
                                peak_status : [] // by peak status
                            }
                        };
                        
                    }

                    /*
                        track peak spend and power by peak_status
                    */
                    if(!results[thing].month[month].demand.peak_status[peak_status]){
                        results[thing].month[month].demand.peak_status[peak_status] = {
                            spend : 0,
                            power : 0
                        }                       

                        results[thing].month[month].consumption.peak_status[peak_status] = {
                            spend : 0,
                            energy : 0
                        }
                    }

                    results[thing].month[month].consumption.peak_status[peak_status].energy += energy;
                    results[thing].month[month].consumption.peak_status[peak_status].spend += energy_spend;

                    /*
                        update peak by peak_status
                    */
                    if(power >= results[thing].month[month].demand.peak_status[peak_status].power){
                        results[thing].month[month].demand.peak_status[peak_status].power = power;
                        results[thing].month[month].demand.peak_status[peak_status].spend = power * (tariff.kw * _this.tariff_scale);
                        results[thing].month[month].demand.peak_status[peak_status].date = date;
                    }

                }
            }

            /*
                Loop over all the interval data at once
            */
            var l = _this.interval_data.length;
            for(var i = 0; i < l; i++){
                
                /*
                    Get tariff object
                */
                var tariff = _this.get_tariff(i);
                var peak_status = tariff.peak_status;

                /*
                   wtf?!
                */
                if(peak_status === false){
                    peak_status = 0;
                }

                month(i, tariff, peak_status);
            }

            /*
                loop over each month to calculate Non-Coincidental spend / power
            */
            for(var thing in results){

                var period_peak = 0;
                var period_length = _this.rate_plan.non_coincidental.period;
                var period_duration = 0;
                var non_coincidental = [];
                var peak = 0;

                for(var m = 0; m < 12; m++){

                    var month = results[thing].month[m].demand;
                    for(var s = 0; s < month.peak_status.length; s++){
                        if(month.peak_status[s].power > peak){
                            peak = month.peak_status[s].power;
                        }
                    }

                    results[thing].month[m].non_coincidental = {
                        power : peak,
                        spend : peak * _this.rate_plan.non_coincidental.tariff
                    }

                    period_duration++
                    if(period_duration === period_length){
                        period_duration = 0;
                        peak = 0;
                    }
                }

                /*
                    Once more for our totals now that we've figured out our non_coincidental
                */
                for(var m = 0; m < 12; m++){
                    for(var metric in results[thing].month[m]){

                        if(metric === 'demand'){

                            for(var s = 0; s < results[thing].month[m].demand.peak_status.length; s++){

                                // Total by peak_status as well as flat out total
                                if(!results[thing].total.demand.peak_status[s]){
                                    results[thing].total.demand.peak_status[s] = {
                                        spend : 0,
                                        power : 0
                                    }
                                }
                                
                                results[thing].total.demand.peak_status[s].spend += results[thing].month[m].demand.peak_status[s].spend;
                                results[thing].total.demand.peak_status[s].power += results[thing].month[m].demand.peak_status[s].power;

                                /*
                                    Total up demand spend
                                */
                                results[thing].total.demand.spend += results[thing].month[m].demand.peak_status[s].spend;
                                results[thing].total.demand.power += results[thing].month[m].demand.peak_status[s].power;

                                /*
                                    Were also after the highest peak of the month regardless of the peak status so....
                                */
                                if(results[thing].month[m].demand.peak_status[s].power > results[thing].month[m].demand.peak.power){
                                    results[thing].month[m].demand.peak.power = results[thing].month[m].demand.peak_status[s].power;
                                    results[thing].month[m].demand.peak.spend = results[thing].month[m].demand.peak_status[s].spend;
                                    results[thing].month[m].demand.peak.date = results[thing].month[m].demand.peak_status[s].date;
                                }

                            }

                        }

                        if(metric === 'consumption'){

                            for(var s = 0; s < results[thing].month[m].consumption.peak_status.length; s++){

                                // Total by peak_status as well as flat out total
                                if(!results[thing].total.consumption.peak_status[s]){
                                    results[thing].total.consumption.peak_status[s] = {
                                        spend : 0,
                                        energy : 0
                                    }
                                }

                                results[thing].total.consumption.peak_status[s].spend += results[thing].month[m].consumption.peak_status[s].spend;
                                results[thing].total.consumption.peak_status[s].energy += results[thing].month[m].consumption.peak_status[s].energy;

                                results[thing].total.consumption.spend += results[thing].month[m].consumption.peak_status[s].spend;
                                results[thing].total.consumption.energy += results[thing].month[m].consumption.peak_status[s].energy;
                            }
                            
                        }
                        
                        if(metric === 'non_coincidental'){
                            results[thing].total.non_coincidental.spend += results[thing].month[m].non_coincidental.spend;
                        }

                    }
                }
            }

            //console.log("Master completed in "+(Date.now() - start)+"ms")
            return results;

        },
        cashflow: function(yrs, break_on_roi) {
            var cashflow = [];
            var years = (yrs)? yrs: 20;
            // COST
            var cost = _this.calc.system_cost();
            // INCENTIVES
            var incentives = _this.calc.incentives();
            var balance = cost.total * -1;
            
            for (var i = 0; i < years; i++) {
                var _start = Date.now();
                if (i === 0) {

                    var year = {
                        energy_savings: 0,
                        demand_savings: 0,
                        non_coincidental_savings: 0,
                        itc: 0,
                        sgip: 0,
                        macrs: 0,
                        annual: 0,
                        total: balance
                    };

                    cashflow.push(year);

                } else {
                    var year = {};
                    // adjust tariff prices for inflation
                    _this.tariff_scale = 1 + (_this.annual_tariff_increase * i);

                    // Map spending
                    var MASTER = _this.calc.master();
                    var spend = MASTER.original;
                    var adjusted_spend = MASTER.adjusted;

                    // Compare consumption
                    var consumption_spend = spend.total.consumption.spend;
                    var adjusted_consumption_spend = adjusted_spend.total.consumption.spend;
                    var energy_savings = (consumption_spend - adjusted_consumption_spend);
                    year.energy_savings = energy_savings;

                    // Compare non-conincidental
                    var non_coincidental_spend = spend.total.non_coincidental.spend;
                    var adjusted_non_coincidental_spend = adjusted_spend.total.non_coincidental.spend;
                    var non_coincidental_savings = non_coincidental_spend - adjusted_non_coincidental_spend;
                    year.non_coincidental_savings = non_coincidental_savings;

                    // Compare demand
                    var peak_spend = spend.total.demand.spend;
                    var adjusted_peak_spend = adjusted_spend.total.demand.spend;
                    var demand_savings = (peak_spend - adjusted_peak_spend);
                    year.demand_savings = demand_savings;

                    // ITC return
                    var itc = (i === 1) ? incentives.itc : 0;
                    year.itc = itc;

                    // SGIP return
                    var sgip = (sgip_payback_schedule[i]) ? (incentives.sgip * sgip_payback_schedule[i]) : 0;
                    year.sgip = sgip;

                    // MACRS return
                    var macrs = (macrs_payback_schedule[i]) ? ((incentives.macrs_federal + incentives.macrs_state) * macrs_payback_schedule[i]) : 0;
                    year.macrs = macrs;

                    // total
                    var annual = energy_savings + non_coincidental_savings + demand_savings + itc + sgip + macrs;
                    balance += annual;
                    year.annual = annual;
                    year.total = balance;

                    cashflow.push(year);

                    if(balance >= 0 && break_on_roi === true){
                        // reset tariff scale                   
                        _this.tariff_scale = 1;
                        return cashflow;
                    }
                }
            }

            // reset tariff scale                   
            _this.tariff_scale = 1;

            return cashflow;
        },
        roi: function() {

            var cashflow = _this.calc.cashflow(100, true);
            console.log(cashflow)
            var cost = _this.calc.system_cost();
            var balance = cost.total * -1;
            var years = 0;
            
            for(var i = 0; i < cashflow.length; i++){

                years++;
                var year = cashflow[i];
                var avg_daily_savings = year.annual / 365;

                balance += year.annual;
                
                if(balance >= 0){
                    var slack_days = Math.round(balance / avg_daily_savings);
                    var yrs = years;
                    var days = 0;

                    if(slack_days > 0){
                        yrs--
                        days = 365 - slack_days;
                    }
                    
                    return {
                        years: yrs,
                        days: days
                    };
                }


            }
            return { years: 0, days: 0 };
        },
        incentives: function() {

            var cost = _this.calc.system_cost();

            // SGIP
            var sgip_rate = $("#quote_sgip_rate").val();
            sgip_rate = parseFloat(sgip_rate.replace(/[^0-9\.]+/g, ""));
            var sgip_rating;

            // our inverter can drain the entire system in less than 2 hours, use the battery size * .5 as the sgip value
            if ((_this.system.inverter.capacity * 2) > _this.system.storage.capacity) {
                sgip_rating = (_this.system.storage.capacity * .5) * 1000; // convert to watts
            } else {
                // our inverter is not going to empty the battery bank in less than 2 hours, use the inverter size
                sgip_rating = (_this.system.inverter.capacity) * 1000; // convert to watts
            }

            var sgip_available = sgip_rate * sgip_rating;
            var sgip_max = cost.total * .6;
            var sgip_return = (sgip_available > sgip_max) ? sgip_max : sgip_available;

            // ITC
            var itc_rate = $("#quote_itc_rate").val();
            itc_rate = parseFloat(itc_rate.replace(/[^0-9\.]+/g, "")) / 100;
            var roi = cost.solar_cost * itc_rate;

            // MACRS
            var macrs_federal_rate = $("#quote_macrs_federal_rate").val();
            macrs_federal_rate = parseFloat(macrs_federal_rate.replace(/[^0-9\.]+/g, "")) / 100;
            macrs_federal_return = (cost.total - (sgip_return + roi)) * macrs_federal_rate;

            var macrs_state_rate = $("#quote_macrs_state_rate").val();
            macrs_state_rate = parseFloat(macrs_state_rate.replace(/[^0-9\.]+/g, "")) / 100;
            macrs_state_return = (cost.total - (sgip_return + roi)) * macrs_state_rate;

            var result = {
                sgip: sgip_return,
                itc: roi,
                macrs_federal: macrs_federal_return,
                macrs_state: macrs_state_return
            };

            return result;
        },
        system_cost: function() {
            // inverter
            var inverter_size = _this.system.inverter.capacity * 1000; // watts
            var inverter_price = $("#quote_inverter_price").val(); // $ / watts
            inverter_price = parseFloat(inverter_price.replace(/[^0-9\.]+/g, ""));
            var inverter_cost = inverter_price * inverter_size;

            // solar
            var solar_size = $("#pv_generator").val().replace(/[^0-9\.]+/g, "") * 1000; // kw
            var solar_price = parseFloat($("#quote_solar_price").val().replace(/[^0-9\.]+/g, "")); // $ / kw
            var solar_cost = solar_size * solar_price;

            // storage
            var storage_size = _this.system.storage.capacity; // kwh
            var storage_price = $("#quote_storage_price").val().replace(/[^0-9\.]+/g, ""); // $ / kwh
            storage_price = parseFloat(storage_price.replace(/[^0-9\.]+/g, ""));
            var storage_cost = storage_size * storage_price;

            //installation
            var installation_price = $("#quote_installation_price").val().replace(/[^0-9\.]+/g, ""); // $ / kwh
            installation_price = parseFloat(installation_price.replace(/[^0-9\.]+/g, ""));

            //warranty
            var warranty_rate = $("#quote_warranty_rate").val().replace(/[^0-9\.]+/g, ""); // $ / kwh
            warranty_rate = parseFloat(warranty_rate.replace(/[^0-9\.]+/g, "")) / 100;
            var warranty_cost = (solar_cost + inverter_cost + storage_cost) * warranty_rate;

            //dealer profit
            var dealer_profit_rate = $("#quote_dealer_profit_rate").val().replace(/[^0-9\.]+/g, ""); // $ / kwh
            dealer_profit_rate = parseFloat(dealer_profit_rate.replace(/[^0-9\.]+/g, "")) / 100;
            var dealer_profit_cost = (solar_cost + inverter_cost + storage_cost) * dealer_profit_rate;

            var result = {
                total: (inverter_cost + solar_cost + storage_cost + installation_price + warranty_cost + dealer_profit_cost),
                dealer_profit_cost: dealer_profit_cost,
                warranty_cost: warranty_cost,
                installation_cost: installation_price,
                storage_cost: storage_cost,
                solar_cost: solar_cost,
                inverter_cost: inverter_cost
            }
            return result;
        },
        highest_peaks: function(dataset) {
            var dates = _this.dates;
            var ret = [{date:0,power:0},{date:0,power:0},{date:0,power:0},{date:0,power:0},{date:0,power:0},{date:0,power:0},{date:0,power:0},{date:0,power:0},{date:0,power:0},{date:0,power:0},{date:0,power:0},{date:0,power:0}];
            var l = dataset.length;
            for (var i = 0; i < l; i++) {
                var key = new Date(dates[i]).getMonth();
                if (!ret[key]) {
                    ret[key] = {
                        date: dates[i],
                        power: dataset[i]
                    }
                } else {
                    if (dataset[i] > ret[key].power) {
                        ret[key] = {
                            date: dates[i],
                            power: dataset[i]
                        }
                    }
                }
            }
            return ret;
        }
    }

    /*  Ready Check 
    -------------------------------------------------------------------------------------------------- */
    if(options.project.interval_data === _this.optimal_data_length){
        TweenLite.to(options.chart_container, 0, {opacity: 0,backgroundColor: "#fafafa"})
    }

    this.ready = {
        project : false,
        dates : false,
        interval_data : false,
        rate_plan : false,
        system : false,
    }

    this.ready_check = function(){

        for(var ready in _this.ready){
            if(_this.ready[ready] === false){
                return window.setTimeout(_this.ready_check, 100)
            }
        }

        console.log("READY!")
        _this.init_chart();

        /*
            Running 3 complete sims before starting the chart.
        */
        _this.simulate(function(){                        
            _this.simulate(function(){  
                _this.simulate(function(){ 


                    _this.plot_all(true);
                    _this.create_baseline_inputs();
                    _this.update_result_outputs();

                    TweenLite.to(options.chart_container, 1, {
                        opacity:1, 
                        delay : 1
                    })

                })                      
            })                     
        })



    }

    this.initialize = {
        dates : function(){
            _this.generate_dates();
            _this.ready['dates'] = true;
        },
        project : function(){
            _this.id = options.project.id;
            _this.title = options.project.title;
            _this.proposals = options.project.proposals;
            _this.ready['project'] = true;
        },
        system : function(){
            // Parse the system object if necessary
            if(typeof options.project.system === 'string'){
                options.project.system = safeParseJSON(options.project.system)
            }

            // Parse the system baseline object if necessary
            if(typeof options.project.system.baseline === 'string'){
                options.project.system.baseline = safeParseJSON(options.project.system.baseline);
            }

            // Parse the system backup_days object if necessary
            if(typeof options.project.system.backup_days === 'string'){
                options.project.system.backup_days = safeParseJSON(options.project.system.backup_days)
            }

            _this.system = options.project.system;
            _this.ready['system'] = true;
        },
        interval_data : function(){
            if(options.project.interval_data.length > 30000){            
                _this.consume_power_csv(options.project.interval_data, function(){
                    _this.ready['interval_data'] = true;
                })
            }
        },
        pv_data : function(){
            if(options.project.pv_data.length > 30000){
                _this.ready['pv_data'] = false;
                _this.consume_pv_csv(options.project.pv_data, function(){
                    _this.ready['pv_data'] = true;
                })
            }
        },
        rate_plan : function(){
            _this.load_rate_plan(options.project.rate_plan, function(argument) {
                _this.ready['rate_plan'] = true;
            })
        }
    };

    
    _this.resize();
    _this.ready_check();

    for(var i in _this.initialize){
        _this.initialize[i]()
    }

    $(document).ready(function(){
        window.onresize = _this.resize;
    })

}