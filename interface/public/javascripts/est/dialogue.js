/*
	Dialoge
	--------------------------------------------------------

	Bare bones dialogue window. Verry little style and very 
	few options. Can be initialized with no options.
	
	Uses jQuery drag - n drop / resize 

	--------------------------------------------------------
	options {}
		title : string
			Displayed in the title bar
		container : html element
			A container element. The container is set to the document body by default.
		content : [html element, html element, html element, html element]
			An array of html elements to append to the contents of the window
		resize : true || false
			Can the window be resized? Default is true;
		minimize : true || false
			Can the window be minimized? Default is true;
		size : [x,y]
			The size of the window ['100px', 'auto']. Default is ['80%', 'auto']
		location : [x,y]
			Distance from the top left of the container (whatever it may be) to create the dialogue

*/
var DialogueCache = [];
var Dialogue = function(options){

	var _this = this;
	this.count = {
		hide : 0,
		show: 0,
		focus : 0
	}
	for(var option in options){this[option] = options[option]}

	/*
		Methods
		---------------------------------------------------------------------
	*/

	var delay = .1;
	this.show = function(){
		_this.dialogue.style.display = '';
		TweenLite.to(_this.dialogue, delay, {
			opacity: 1,
			scale : 1,
			onComplete : function(){
				if(_this.onShow)
					_this.onShow()
			}
		})
	}	
	this.focus = function(){
		for(var i = 0; i < DialogueCache.length; i++){
			DialogueCache[i].dialogue.style.zIndex = "";
		}
		_this.dialogue.style.zIndex = 5001;
		if(_this.onFocus)
			_this.onFocus()
	}
	this.hide = function(){
		console.log("HIDE ", _this.title)
		TweenLite.to(_this.dialogue, delay, {
			opacity: 0,
			scale : .95,
			onComplete : function(){
				_this.dialogue.style.display = 'none';
				if(_this.onHide)
					_this.onHide()
			}
		})
	}
	this.toggle = function(){
		if(_this.dialogue.style.display === 'none')
			return _this.show();
		_this.hide();
	}


	/* 
		Generate / style / assemble elements 
		---------------------------------------------------------------------
	*/
	var elements = ['dialogue', 'titlebar', 'contentWrap', 'minimize', 'close'];
	for(var i = 0; i < elements.length; i++){
		this[elements[i]] = document.createElement('div')
		this[elements[i]].className = elements[i];
		if(i > 0)
			this.dialogue.appendChild(this[elements[i]])
	}

	// title
	if(this.title)
		this.titlebar.innerHTML = this.title
	
	// events
	this.dialogue.onmousedown = this.focus
	this.close.onclick = this.hide

	/* 
		Init 
		---------------------------------------------------------------------
	*/

	DialogueCache.push(this);

	/* Append content to window */
	for(var i = 0; i < this.content.length; i++){
		this.contentWrap.appendChild(this.content[i])
	}

	document.body.appendChild(this.dialogue);

	$(this.dialogue).resizable({
        minHeight: (4 * 10),
        minWidth: 196,
        handles: "n, e, s, w, ne, se, sw, nw",
        resize : (this.onResize)? this.onResize : function(){}
    });

	$(this.dialogue).draggable({ 
		containment: "parent",
		handle: "> .titlebar"
	});

	//this.hide();

	TweenLite.to(_this.dialogue, 0, {
		opacity: 0,
		scale : .95,
		onComplete : function(){
			_this.dialogue.style.display = 'none';
		}
	})
}