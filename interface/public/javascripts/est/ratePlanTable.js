var tariff_datatable;
$(document).ready(function(){

	$("#new_rate_plan").on('click', function(){
		$.post('/est/rateplans', {}, function(data){
			if(typeof est === 'object'){			
				est.load_rate_plan(data[0].id, function(){		
					window.setTimeout(function(){
						tariff_datatable.fetch(function(){
							tariff_datatable.render()
						});
					}, 250)
				})
			}else{
				tariff_datatable.fetch(function(){
					tariff_datatable.render()
				});
			}
		})
	})

	$("#delete_rate_plans").on('click', function(){
		var queue = [];

		$('.rate_plan_checkbox').each(function(){
			if($(this)[0].checked === true){
				queue.push($(this).data('id'));
			}
		})

		$.post('/est/rateplans/delete', {ids : queue}, function(){
			tariff_datatable.fetch(function(){
				tariff_datatable.render();
			})
		})
	})

	tariff_datatable = new DataTable({
		container: document.getElementById("rate_plan_browser"),
		socket : socket,
		navigationContainer : document.getElementById('rate_plan_browser_navigation'),
		paginate : 10,
		filter : ['title', 'utility','public','id'],
		labels : {
			title : "Rate Plan",
			utility : "Utility",
			public : "JLM Official",
			id : '<input type="checkbox">'
		},
		header : {
			id : function(){
				var wrap = ele('div.right');
				var checkbox = ele("input");
				checkbox.type= 'checkbox';
				checkbox.onchange = function(){
					var checked = this.checked;
					$(".rate_plan_checkbox").each(function(){
						$(this)[0].checked = checked;
						$(this).change();
					})
				}
				wrap.appendChild(checkbox)
				return wrap
			}
		},
		row : function(data){
			var row = ele();
			if(data){
				if(typeof est === 'object'){
					if(data.id === est.rate_plan.id){
						row.className = 'active';
					}
				}
			}
			return row;
		},
		column : {
			title : function(data){
				var el = ele('a');
				el.href = "#";
				el.innerHTML = data.title;
				el.onclick = function(){
					if(typeof est === 'object'){					
						est.load_rate_plan(data.id, function(){

							/*Update rateplan title input*/
							$("#rate_plan_title")[0].value = est.rate_plan.title;

							/*Save project*/
							est.save({
								rate_plan : est.rate_plan.id
							});
							
							window.setTimeout(function(){
								tariff_datatable.render()
							}, 100)
						});
					}
				}
				return el;
			},
			public : function(data){
				var el = ele();
				el.innerHTML = (data.public === true)? "Yes" : "User Created";
				return el;
			},
			id : function(data){
				var el = ele('input');
				el.type = "checkbox";
				el.className = "rate_plan_checkbox";
				el.setAttribute("data-id", data.id)
				return ele({'div.right': el});
			}
		},
		dataSource : "/est/rateplans"
	});

	tariff_datatable.fetch(function(){
		tariff_datatable.render()
	});
})