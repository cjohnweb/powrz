var proposalTable; 
function render_proposal_table(){
	proposalTable = new DataTable({
		filter : ['title', 'roi', 'system', 'id'],
		container : document.getElementById('proposal_container'),
		data : est.proposals,
		labels : {
			system : "System",
			id : " ",
			title : "Title",
			roi : "ROI",
		},
		row : function(data){
			var wrap = ele();
			return wrap
		},
		column : {
			title : function(data){
				var wrap = ele();
				wrap.innerHTML = data.title;
				wrap.setAttribute("contentEditable", true);
				wrap.onkeyup = function(){
					data.title = this.innerHTML;
				}
				wrap.onblur = function(){
					est.save({
						proposals : JSON.stringify(est.proposals)
					});
				}
				return wrap;
			},
			roi : function(data){
				var wrap = ele();
				wrap.innerHTML = (((data.roi.years * 365) + data.roi.days) / 365).toFixed(2)+" Yr";
				return wrap;
			},
			system : function(data){
				//console.log(data)
				var wrap = ele();
				wrap.innerHTML = data.inverter+"/"+data.storage+"/"+data.solar;
				return wrap;
			},
			id : function(data){
				//console.log(data)
				var wrap = ele('div.center');

				var view = ele("i.icon-eye-5.marg-e")
				view.style.cursor = 'pointer';
				var d = render_proposal(data);
				view.onclick = function(){
					d.toggle();
				}
				wrap.appendChild(view) 

				var del = ele("i.icon-trash-2")
				del.style.cursor = 'pointer';
				del.onclick = function(){
					for(var i = 0; i < est.proposals.length; i++){
						if(est.proposals[i].created === data.created){
							est.proposals.splice(i, 1);
						}
					}
					proposalTable.render();
				}
				wrap.appendChild(del)
				
				return wrap;
			}

		}
	});
	proposalTable.render();
}

var dialogue_cache = {};
function render_proposal(proposal){

	var headers = ele('div.row');

	var system_cost = ele('div.col-2-3.pad-e');
	var _system_cost = ele('h1');
	_system_cost.innerHTML = "System Cost & Incentives";
	system_cost.appendChild(_system_cost)
	headers.appendChild(system_cost)

	var system_specs = ele('div.col-1-3.pad-w');
	var _system_specs = ele('h1');
	_system_specs.innerHTML = "System Specs";
	system_specs.appendChild(_system_specs)
	headers.appendChild(system_specs)

	var top_row = ele('div.row');
	var money = ele('div.col-2-3.pad-e');
	money.appendChild(est.generate_system_cost_element(proposal));
	top_row.appendChild(money);

	var system = ele('div.col-1-3.pad-w');
	system.appendChild(est.generate_system_spec_element(proposal));
	top_row.appendChild(system);

	var cashflow = ele('div.row');
	var cashflow_header = ele('h1');
	cashflow_header.innerHTML = 'Cash Flow';
	
	/* ---------------------------------------------------------------- */
	/* CHART                                                            */
	/* ---------------------------------------------------------------- */
	var chart = ele('div.col-2-5.pad-e');
	cashflow.appendChild(chart);

	var data = [
		['Year', 'Total', { role: 'style' }]
	];

	for(var _i = 0; _i < proposal.cashflow.length; _i++){
		var record = ["Yr "+(_i + 1), proposal.cashflow[_i].total, "#0096ff"];
		data.push(record)
	}

	var data = google.visualization.arrayToDataTable(data);
	var bar_chart = new google.visualization.ColumnChart(chart);

	var opts = {
		width: "100%",
		height: 325 + 12,
		chartArea: {
			width: '100%', 
			height: '100%'
		},
		legend: 'none',
		titlePosition: 'in', 
		axisTitlesPosition: 'in',
		hAxis: {
			textPosition: 'in'
		}, vAxis: {
			textPosition: 'in'
		}
	};

	var formatter = new google.visualization.NumberFormat({
		negativeColor: 'red', 
		negativeParens: true, 
		pattern: '$###,###'
	});

	formatter.format(data, 1);    
	bar_chart.draw(data,opts);
	/* ---------------------------------------------------------------- */

	var table = ele('div.col-3-5.pad-w');
	table.appendChild(est.generate_cashflow_table_element(proposal.cashflow))
	cashflow.appendChild(table);

	var id = "dialogue_"+Math.round(Math.random() * 1000000);

	var title = ele('input');
	title.value = proposal.title;
	title.onchange = function(){
		proposal.title = this.value;
		est.save({
			proposals : JSON.stringify(est.proposals)
		})
	}

	var pdf = ele('button')
	pdf.innerHTML = "Generate PDF";
	pdf.onclick = function(){
		// Why the fuuuuuuh am I generating a form???
		var form = ele('form.hide');
		form.action = '/est/proposal';
		form.method = 'post';

		// input
		var input = ele('textarea');
		input.name = 'proposal';
		input.value = JSON.stringify(proposal);

		// input
		var customer_name = ele('input');
		customer_name.name = 'customer_name';
		customer_name.value = est.customer_name;

		// add it to the doc
		form.appendChild(input);
		form.appendChild(customer_name);
		document.body.appendChild(form)

		form.submit();
		window.setTimeout(function(){
			form.parentNode.removeChild(form)
		}, 100)

	}

	var d = new Dialogue({
		title : proposal.title,
		onShow : function(){
			bar_chart.draw(data,opts);
		},
		onResize : function(){
			bar_chart.draw(data,opts);
		},
		content : [
			headers,
			top_row,
			ele('div.est-spacer-6'),
			cashflow_header,
			cashflow,
			pdf
		]
	});
	d.dialogue.id = id;
	return d;
}
$(document).ready(function(){
	render_proposal_table();

	$("#finance-tab").find('input').on('change', function() {
		// est.update_result_outputs();
		// est.save(); // WOAH!!!! This is going to post like 2MB of data onchange!!!
	})

	$("#annual_tariff_increase").on('change', function() {
		var parsed_val = parseFloat($(this).val().replace(/[^0-9\.]+/g, ""));
		est.annual_tariff_increase = parsed_val / 100;
		$(this).val(parsed_val + "%")
		est.save();
	});

	$("#quote_sgip_rate, #quote_inverter_price, #quote_storage_price, #quote_solar_price").on('change', function() {
		var parsed_val = parseFloat($(this).val().replace(/[^0-9\.]+/g, ""));
		$(this).val("$" + parsed_val)
		est.save();
	});

	$("#quote_dealer_profit_rate, #quote_itc_rate, #quote_macrs_federal_rate, #quote_macrs_state_rate, #quote_warranty_rate").on('change', function() {
		var parsed_val = parseFloat($(this).val().replace(/[^0-9\.]+/g, ""));
		$(this).val(parsed_val + "%")
		est.save();
	});

	$("#cashflow_duration").on('change', function() {
		var parsed_val = parseFloat($(this).val().replace(/[^0-9\.]+/g, ""));
		$(this).val(parsed_val + " Years")
		est.save();
	});

	$("#quote_installation_price").on('change', function() {
		var parsed_val = parseFloat($(this).val().replace(/[^0-9\.]+/g, ""));
		$(this).val(parsed_val.makeMoney())
		est.save();
	});

	$("#quote").click(function() {
		$(this).removeClass('blue')
		$(this).addClass('grey')
		$(this).html("<i class='icon-spin5 animate-spin'></i> Generating Proposal...");
		var parsed_val = parseFloat($("#cashflow_duration").val().replace(/[^0-9\.]+/g, ""));
		
		window.setTimeout(function(){		
			var proposal = est.generate_proposal_object();
			proposalTable.prepend(proposal);
			$("#quote").addClass('blue')
			$("#quote").removeClass('grey')
			$("#quote").html("Generate Proposal");
			socket.debug = true
			est.save({proposals: JSON.stringify(est.proposals)})
		},250)
	});
})

