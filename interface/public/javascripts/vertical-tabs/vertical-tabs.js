$(document).ready(function() {


$(".tab-list > li:first-child").addClass('active');

function resize_tab_list(){
    var header = $("#header"); 
    var chart = $("#visualization");
    var io = $("#io");
    var tab_wrap = $(".tab-wrap");
    var tab_list = $(".tab-list");

    if(tab_wrap.length === 0){
        return;
    }

    var new_height = $(window).height() - chart.outerHeight() - header.outerHeight() - 36 + "px"; // 36 = 9rem

    tab_wrap[0].style.height = new_height;
    $("#o")[0].style.height = new_height;

    var tab_list_rect = tab_list[0].getBoundingClientRect();
    tab_list[0].style.clip = "rect(-3px, 64px, " + tab_list_rect.height + 5 + "px, -4px)";


    $(".tab-list > li").on('click', function() {
        // deactivate
        $(".tab-list > li").removeClass("active");
        $(".tab").removeClass("active");

        // activate
        $(this).addClass("active");
        var tab_selector = "#" + $(this).data('tab');
        console.log(tab_selector)
        $(tab_selector).addClass("active");

    });
}

$(window).on('resize', resize_tab_list);
resize_tab_list();

});