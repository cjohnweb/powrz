var Gantron = function(options){

	var _this = this; 	
	this.onchange = options.onchange;
	this.container = options.container;

	
	this.init = function(newOptions){
	
		if(newOptions.container){
			_this.container = newOptions.container;
		}
		
		this.canvas = document.createElement('canvas');
		this.ctx = this.canvas.getContext("2d");
		
		_this.container.ondblclick = function(e){
			if(_this.mouse.target.index !== null){
				_this.openPop(_this.mouse.target.index);
			}
		}
	
		_this.container.onmouseenter = function(e){
			_this.resize();
			_this.setIntent['create']();
			_this.mouse.isOver = true;
			_this.draw();
		}
		
		_this.container.onmouseleave = function(e){
			_this.resize();
			_this.setIntent['idle']();
			_this.mouse.isOver = false;
			_this.draw();
		}
		
		_this.container.onmousemove = function(e){
		
			var offset = getPosition(_this.container);
			
			var location = [e.clientX - offset.x, e.clientY - offset.y];
			_this.mouse.location = location;
			_this.blockStyles.grab = _this.generateGrabGradient();
			
			if(_this.mouse.isDown === true){
			
				_this.mouse.delta[0] = Math.round(_this.mouse.down[0]) - _this.mouse.location[0];
				
				if(_this.mouse.intent === 'grab'){
					_this.dragTargetBlock();
				}
				
				if(_this.mouse.intent === 'resize'){
					_this.resizeTargetBlock();
					
				}
				
				_this.draw();
				return;
			}
			
			_this.updateIntent();
			_this.draw();
		}
		
		_this.container.onmouseup = function(e){
		
			var offset = getPosition(_this.container);
			var location = [e.clientX - offset.x, e.clientY - offset.y];
			
			_this.mouse.up[0] = _this.mouse.snapSize * Math.round(location[0]/_this.mouse.snapSize);
			_this.mouse.up[1] = 0;
			
			if(Math.abs(_this.mouse.delta[0]) > 0 && _this.mouse.intent === 'create'){_this.createBlock();}
			_this.sortBlocks();
			_this.mouse.isDown = false;
			_this.cleanup();
			_this.detectChange();
		}
		
		_this.container.onmousedown = function(e){
			e.preventDefault();
			
			_this.destroyPop();
			
			var offset = getPosition(_this.container);
			var location = [e.clientX - offset.x, e.clientY - offset.y];
			
			_this.mouse.down[0] = _this.mouse.snapSize * Math.round(location[0]/_this.mouse.snapSize);
			_this.mouse.down[1] = 0;
			
			if(_this.mouse.intent === 'grab' || _this.mouse.intent === 'resize'){
				_this.mouse.target.tmp = [
					_this.blocks[_this.mouse.target.index].location[0],
					_this.blocks[_this.mouse.target.index].location[1]
				];
			}
			
			_this.mouse.isDown = true;
			
		}
		
		document.body.addEventListener('click', function(e){
			var close = true;
			var element = e.target;
			
			while(element){
				if(element.className === 'gantron-pop'){
					close = false;
					element = 0;
				}else{
					element = element.parentNode;
				}
			}
			
			if(close === true){
				_this.destroyPop();
			}
		});
		
		window.addEventListener('resize', function(){
			_this.resize();
		});
		
		_this.container.appendChild(_this.canvas);
		this.resize();
		
	}
	


	/* 
		This is probably the weirdest part of this program because it breaks the [x,y] convention.
		It's an array of arrays, each array contains the [startIndex, endIndex, fillStyle] of the time span it represents.
		These indexes are currently set in 30 minute intervals or 0 - 47
		Additionally the 3rd point in the array is an optional paramater which sets the fill style of the block
	*/
	this.blocks = [];
	
	this.divisions = 48;
	
	/* 
		canvas size [x,y] 
	*/
	this.size = [0,64];
	
	/*
		though the y axis is unused I added support for it any way. Mouse values can be read as [x,y].
		Array access is faster than ob access. This is simply the result of compulsive, unnecessary optimization.

	*/
	this.activePop = null;
	
	this.mouse = {
		location : [0,0],
		grid : [0,0],
		delta : [0,0],
		down: [0,0],
		up: [0,0],
		isDown: false,
		isOver : false,
		intent : null,
		snapSize : 4,
		intent: 'create',
		target : {
			resize : null,
			index: null,
			offset: [0,0],
			tmp: [0,0]
		},
		getGrid : function(){
			return _this.locationToGrid(_this.mouse.location);
		}
	}
	
	this.locationToGrid = function(location){
		return [Math.round(location[0]/_this.mouse.snapSize), Math.round(location[1]/_this.mouse.snapSize)];
	}
	
	this.gridToLocation = function(grid){
		return [Math.round(_this.mouse.snapSize * grid[0]), Math.round(_this.mouse.snapSize * grid[1])];
	}
	
	this.snapLocation = function(location){
		var grid = _this.locationToGrid(location);
		return _this.gridToLocation(grid);
	}
	
	this.blockStyles = {
		'create' : '#eeeeee',
		'grab' : '#ffffff',
		'resize' : 'rgba(150,150,150,.75)'
	};
	
	this.roundRect = function(ctx, x, y, width, height, radius, fill, stroke) {
		if (typeof stroke == "undefined" ) {
			stroke = true;
		}
		if (typeof radius === "undefined") {
			radius = 4;
		}
		ctx.beginPath();
		ctx.moveTo(x + radius, y);
		ctx.lineTo(x + width - radius, y);
		ctx.quadraticCurveTo(x + width, y, x + width, y + radius);
		ctx.lineTo(x + width, y + height - radius);
		ctx.quadraticCurveTo(x + width, y + height, x + width - radius, y + height);
		ctx.lineTo(x + radius, y + height);
		ctx.quadraticCurveTo(x, y + height, x, y + height - radius);
		ctx.lineTo(x, y + radius);
		ctx.quadraticCurveTo(x, y, x + radius, y);
		ctx.closePath();
		ctx.stroke();
		ctx.fill();
	}
	
	this.serialize = function(){
	
		var result = [];
		
		for(i = 0; i < _this.divisions; i++){
			
			for(var b = 0; b < _this.blocks.length; b++){
			
				var block = _this.blocks[b];
				
				if(
					block.location[0] <= i && 
					block.location[1] > i
				){
					result.push(block.value);
				}
			}
			if(!result[i]){
				result[i] = null;
			}
		}
		
		return result;
	}
	
	this.updateIntent = function(){
		
		if(_this.mouse.isOver === false){
			_this.setIntent['idle']();
		}
		
		// if mouse is over a box - intent is to drag
		var mouseGrid = _this.mouse.getGrid();
		var gridSize = Math.round(_this.size[0]/_this.divisions);
		var resizeRange = 4; // pixel area that acts as a resize handle
		var margin = 4;
		
		for(var block = 0; block < _this.blocks.length; block++){
		
			var _block = _this.blocks[block];
			var blockLocation = _this.gridToLocation(_block.location)
			
			// resize left
			if(
				Math.abs((_this.mouse.location[0] - margin) - blockLocation[0]) <= resizeRange &&
				Math.abs((_this.mouse.location[0] - margin) - blockLocation[0]) >= 0
			){
				_this.setIntent['resize'](block, 'left');
				break;
			}
			
			// resize right
			if(
				Math.abs(_this.mouse.location[0] - blockLocation[1]) <= resizeRange &&
				Math.abs(_this.mouse.location[0] - blockLocation[1]) >= 0
			){
				_this.setIntent['resize'](block, 'right');
				break;
			}
			
			// grab
			if(
				_this.mouse.location[0] >= (blockLocation[0]+ resizeRange) && 
				_this.mouse.location[0] <= (blockLocation[1] - resizeRange)
			){
				_this.setIntent['grab'](block);
				break;
			}
			
			_block.style = 'rgba(100,100,100,1)';
			_this.setIntent['create']();
		}
		
		
		// if mouse is 8px or less from the edge of a block - intent is to resize
		// if mouse is not over a box - intent is to create a block
	}
	
	this.setIntent = {
		'idle': function(){
			_this.mouse.intent = 'idle';
			for(var b = 0; b < _this.blocks.length;b ++){
				_this.blocks[b].style= _this.blockStyles['create'];
			}
			_this.mouse.target.index = null;
			_this.container.className = "gantron";
		},
		'create': function(){
			_this.mouse.intent = 'create';
			for(var b = 0; b < _this.blocks.length;b ++){
				_this.blocks[b].style = _this.blockStyles['create'];
			}
			_this.mouse.target.index = null;
			_this.container.className = "gantron";
		},
		'grab': function(block){
			_this.mouse.intent = 'grab';
			_this.mouse.target.index = block;
			for(var b = 0; b < _this.blocks.length;b ++){
				_this.blocks[b].style = _this.blockStyles['create'];
			}
			_this.blocks[_this.mouse.target.index].style = _this.generateGrabGradient();
			_this.container.className = "gantron grab";
		},
		'resize': function(block, direction){
			_this.mouse.intent = 'resize';
			_this.mouse.target.resize = direction;
			for(var b = 0; b < _this.blocks.length;b ++){
				_this.blocks[b].style = _this.blockStyles['create'];
			}
			_this.mouse.target.index = block;
			_this.blocks[_this.mouse.target.index].style = _this.generateResizeGradient();
			_this.container.className = "gantron resize";
		}
	}
	
	this.createBlock = function(){
		if(_this.mouse.intent !== 'create'){return;};
		var start = (_this.mouse.down[0] < _this.mouse.up[0])? _this.mouse.down[0] : _this.mouse.up[0];
		var end = (_this.mouse.down[0] < _this.mouse.up[0])? _this.mouse.up[0] : _this.mouse.down[0];
		
		var vals = [];

		for(var i = 0 ; i < options.inputs.length; i++){
			var inputOptions = options.inputs[i];

			if(inputOptions === "toggle"){
				vals.push(0);
			}else if(inputOptions.placeholder){
				vals.push(inputOptions.placeholder);
			}
		}
		
		var newBlock = {
			location : _this.locationToGrid([start, end]),
			value: vals
		};
			
		_this.blocks.push(newBlock);
		console.log(_this.blocks)
	}

	this.cleanup = function(){
	
		var gridSize = Math.round(_this.size[0]/_this.divisions);
		
		// remove boxes with 0 width
		for(var block = 0; block < _this.blocks.length; block++){
		
			var _block = _this.blocks[block];
			
			if(_block.location[0] < 0){_block.location[0] = 0;}
			if(_block.location[1] > _this.divisions){_block.location[1] = _this.divisions;}
			
			var blockLocation = _this.gridToLocation(_block.location)
			var start = blockLocation[0];
			var end = blockLocation[1];
			var width = end - start;
			
			if(width < 1 || _block.location[0] > _block.location[1]){
				_this.blocks.splice(block, 1);
				_this.sortBlocks();
			}
			
		}
		
	} 
	
	this.drawBlocks = function(){
		// draw blocks
	 	var gridSize = Math.round(_this.size[0]/_this.divisions);
		
		for(var block = 0; block < _this.blocks.length; block++){
		
			var _block = _this.blocks[block];
			
			var margin = 4;
			var location = _this.gridToLocation(_block.location);
			var start = location[0] + margin;
			var end = location[1] - margin;
			
			var width = (start < end)? end - start:start - end;
			
			_this.ctx.strokeStyle = "rgba(0,0,0,.25)";
			_this.ctx.lineWidth = 1;
			_this.ctx.fillStyle = (_block.style)?_block.style : "rgba(100,100,100,1)";
			
			if(_this.mouse.target.index === block){
				_this.ctx.strokeStyle = "rgba(0,150,255,1)";
			}

			_this.roundRect(_this.ctx,start, margin, width, _this.size[1] - (margin * 2));
			
			if(_block.value){
				var fontSize = 10;
				var step = _this.size[1] / _block.value.length - 4;
				_this.ctx.font = fontSize+"px Open Sans";
				_this.ctx.fillStyle = "rgba(74,74,74,.83)";
				_this.ctx.shadowColor = '#999';
				_this.ctx.shadowBlur = 0;

				for(var i = 0; i < _block.value.length; i++){

					var text = (!_block.value[i] || _block.value[i] === null)?  ' - ' : _block.value[i];
					var type = options.inputs[i].type;

					if(type === 'toggle'){
						text = options.inputs[i].values[_block.value[i]];
					}

					if((_this.ctx.measureText(text).width + 8) < width){
						_this.ctx.fillText(text,start + margin, 18 + (step * i)) - fontSize * .5;
					}
				}
			}
			
			
		}
	};
	
	this.drawCursor = function(){
	
		if(_this.mouse.isOver === false){return;}
		
		var x = _this.snapLocation(_this.mouse.location)[0];
		
		// draw cursor / stretch new blocks
		if(_this.mouse.isDown === false && _this.mouse.intent === 'create'){
			_this.ctx.beginPath();
			_this.ctx.strokeStyle = "#63C0FF";
			_this.ctx.lineWidth = 2;
			
			_this.ctx.moveTo(x, 0);
			_this.ctx.lineTo(x, _this.size[1]);
			_this.ctx.stroke();
			return;
		}
		
		if(_this.mouse.isDown === true && _this.mouse.intent === 'create'){
			_this.ctx.fillRect(_this.mouse.down[0],0, _this.mouse.delta[0] * -1, _this.size[1]);
		}
		
	};
	
	this.drawLines = function(){
		
		_this.ctx.beginPath();
		_this.ctx.strokeStyle = "rgba(0,0,0,.25)";
		_this.ctx.lineWidth = 1;
		
		for(var i = 1; i < _this.divisions; i++){
			var x = _this.gridToLocation([i,0])[0];
			_this.ctx.moveTo(x, 4);
			_this.ctx.lineTo(x, _this.size[1] - 4);
		}
		
		_this.ctx.stroke();
	}
	
	this.draw = function(){
		_this.collisionHandler();
		_this.ctx.clearRect(0,0,_this.size[0],_this.size[1]);
		_this.drawLines();
		_this.drawBlocks();
		_this.drawCursor();
	}
	
	this.collisionHandler = function(){
		var itterations = 24;
		if(_this.blocks.length > 0){
			while(itterations--){				
				for(var b = 0; b < _this.blocks.length; b++){
					var block = _this.blocks[b];
					var leftNeighbor = (b === 0)? null:_this.blocks[b-1];
					var rightNeighbor = (b === _this.blocks.length-1)? null:_this.blocks[b+1];
					
					if(leftNeighbor !== null && _this.mouse.delta[0] > 0){
						if(leftNeighbor.location[1] > block.location[0]){
							leftNeighbor.location[0]--;
							leftNeighbor.location[1]--;
						}
					}
					
					if(rightNeighbor !== null && _this.mouse.delta[0] < 0){
						if(rightNeighbor.location[0] < block.location[1]){
							rightNeighbor.location[0]++;
							rightNeighbor.location[1]++;
						}
					}
					
				}
			}
		}
	}
	
	this.generateCreateGradient = function(base){
		var base = (base)? base : "#eeeeee";
		var grab = _this.ctx.createLinearGradient(0,0,0,64);
		grab.addColorStop(0,"#fafafa");
		grab.addColorStop(1,base);
		return grab;
	}
	
	this.generateGrabGradient = function(){
		var grab = _this.ctx.createRadialGradient(_this.mouse.location[0],_this.mouse.location[1],5,_this.mouse.location[0],_this.mouse.location[1],250);
		grab.addColorStop(0,"#fafafa");
		grab.addColorStop(1,"#eeeeee");
		return grab;
	}
	
	this.generateResizeGradient = function(){
		var resize = _this.ctx.createRadialGradient(_this.mouse.location[0],_this.mouse.location[1],5,_this.mouse.location[0],_this.mouse.location[1],250);
		resize.addColorStop(0,"#ffffff");
		resize.addColorStop(1,"#eeeeee");
		return resize;
	}
	
	this.dragTargetBlock = function(){
		
		_this.destroyPop();
		
		var target = _this.blocks[_this.mouse.target.index];
		target.style = _this.generateGrabGradient();
		
		var originalStart = _this.mouse.target.tmp[0];
		var originalEnd = _this.mouse.target.tmp[1];
		
		var gridDelta = _this.locationToGrid(_this.mouse.delta)[0];
		var mouseGrid = _this.locationToGrid(_this.mouse.location)[0];
		
		var newStart = originalStart - gridDelta;
		var newEnd = originalEnd - gridDelta;
		
		target.location[0] = (newStart < 0) ? 0 : newStart;
		target.location[1] = (newEnd > _this.divisions) ? _this.divisions : newEnd;
		
		// collisions
		_this.collisionHandler();
		
	}
	
	this.resizeTargetBlock = function(){
		
		_this.destroyPop();
		
		var target = _this.blocks[_this.mouse.target.index];
		
		
		target.style = _this.generateResizeGradient();
		
		var originalStart = _this.mouse.target.tmp[0];
		var originalEnd = _this.mouse.target.tmp[1];
		
		var gridDelta = _this.locationToGrid(_this.mouse.delta)[0];
		
		var newStart = originalStart - gridDelta;
		var newEnd = originalEnd - gridDelta;
		
		if(_this.mouse.target.resize === 'left'){
			target.location[0] = newStart;
		}else{
			target.location[1] = newEnd;
		}
		
		// collisions
		_this.collisionHandler();
	}
	
	this.resize = function(){
		var rect = _this.container.getBoundingClientRect();
		
		_this.container.style.height = "64px";
		_this.container.style.background = "#ffffff";
		
		var newWidth = rect.width;
		
		_this.canvas.height = rect.height;
		_this.canvas.width = newWidth;		
		
		_this.mouse.snapSize = rect.width/_this.divisions;
		
		_this.size[0] = newWidth;
		_this.size[1] = rect.height;
		
		_this.draw();
	}
	
	this.initStyle = function(){ 
		if(document.getElementById('gantron_style')){
			return;
		}
		var css = ".gantron{ position: relative; -webkit-box-shadow: 0 3px 5px -2px rgba(0,0,0,.1); box-shadow: 0 3px 5px -2px rgba(0,0,0,.1);}";
		css += ".gantron.resize{cursor: e-resize; cursor: col-resize;}";
		css += ".gantron.grab{cursor: move; cursor: -webkit-grab; cursor: -moz-grab;}";
		css += ".gantron.grab:active{cursor: -webkit-grabbing; cursor: -moz-grabbing;}";
		css += ".gantron-pop{-webkit-box-shadow: 0 5px 25px -5px rgba(0,0,0,.5);box-shadow: 0 5px 25px -5px rgba(0,0,0,.5);z-index: 9999; position:absolute; top: 0px; left: 0px; width:128px;padding:4px;border-radius:1rem;background:rgba(74,74,74,1)}.gantron-pop .input,.gantron-pop input{padding:4px 8px;font-size:10px;margin:0;min-height:23px;background:#fff;border-radius:0;border:none;border-top:1px solid #ddd;border-left:1px solid #ddd;border-right:1px solid #ddd}.gantron-pop input:focus{border-color:#ddd}.gantron-pop .input:first-child,.gantron-pop input:first-child{border-radius:4px 4px 0 0;border-top:1px solid #ddd}.gantron-pop .input:last-child,.gantron-pop input:last-child{border-radius:0 0 4px 4px;border-top:1px solid #ddd;border-bottom:1px solid #ddd}.gantron-tri{position:absolute;width:0;top:100%;left:calc(50% - 15px);height:0;border-left:10px solid transparent;border-right:10px solid transparent;border-top:7px solid rgba(74,74,74,1)}.gantron-pop .input{cursor: pointer;}";
		
		_this.container.style.borderRadius = "4px";
		
		var style = document.createElement('style');
		style.innerHTML = css;
		style.id = 'gantron_style';
		document.body.appendChild(style);
	}
	
	this.sortBlocks = function(){
	
		_this.blocks.sort(function(a, b){
		
			var x= a.location[0];
			var y= b.location[0];
			
			var r = 0;
			
			if(x > y){
				r = 1;
			}
			
			if(x < y || x === y){
				r - 1
			}
			
			return r;
			
		});
		
		
	}
	
	this.destroyPop = function(callback){
		_this.activePop = null;
		var similar = document.querySelectorAll('.gantron-pop');
		for(var i = 0; i < similar.length; i++){
			similar[i].parentNode.removeChild(similar[i])
		}
	}
	
	this.positionPop = function(block){
		var block = (typeof block === 'number')?_this.blocks[block]: block;
		var location = _this.gridToLocation(block.location);
		var blockPos = location;
		var center = ((blockPos[1] - blockPos[0]) * .5) - 64;
		
		
		var offset = getPosition(_this.container);
		var wrap = _this.activePop;
		
		wrap.style.top = offset.y - 77+'px';
		wrap.style.left = (offset.x+location[0])+center+'px';
	}
	
	this.openPop = function(block){
		
		_this.destroyPop();
		
		var _block = _this.blocks[block];
		
		var wrap = document.createElement('div');
		wrap.className = 'gantron-pop';
		
		var tri = document.createElement('div');
		tri.className = 'gantron-tri';
		
		var inputWrap = document.createElement('div');
		
		for(var i = 0 ; i < options.inputs.length; i++){
			
			var input;
			var inputOptions = options.inputs[i];
			/*
				I must have been in a hurry cause I don't know what the fuck I'm doing here....
			*/

			if(inputOptions.type === 'toggle'){ // this shouldn't be called a toggle any more. It allows inifinite options and will return the index of the selected option rather than the label...
			
				input = document.createElement('div');
				input.className = 'input';
				input._block = block;
				input._index = i;
				input._values = inputOptions.values; // array options
				var current_value = _this.blocks[input._block].value[input._index]; // option index
				input._value = current_value; // array options
				input.innerHTML = input._values[input._value];
				
				/*
					cycle through options
				*/
				input.onclick = function(){

					if(this._value === (this._values.length - 1)){
						this._value = 0;
					}else{
						this._value++;
					}
					_this.blocks[this._block].value[this._index] = this._value;
					this.innerHTML = this._values[this._value]; 
					_this.draw();
					_this.detectChange();
				}
				
			}else{
			
				input = document.createElement('input');
				input._block = block;
				input._index = i;
				
				if(_block.value[i]){
					input.value = _block.value[i];
				}
				
				if(inputOptions.placeholder){
					input.placeholder = inputOptions.placeholder;
				}
			
				input.onkeyup = function(){
					_this.blocks[this._block].value[this._index] = this.value;
					_this.draw();
				}
				
				if(inputOptions.prefix){
					input._prefix = inputOptions.prefix;
				}
				
				if(inputOptions.suffix){
					input._suffix = inputOptions.suffix;
				}
				
				input.onmouseup = function(e){
					e.preventDefault();
				}
				
				input.onfocus = function(e){
					e.preventDefault();
					this.select();
				}
				
				input.onchange = function(){
					var newVal = '';
					
					if(this._prefix){
						newVal += this._prefix;
					}
					
					newVal += parseFloat(this.value);
					
					if(this._suffix){
						newVal += this._suffix;
					}
					
					this.value = newVal;
					_this.blocks[this._block].value[this._index] = this.value;
					_this.draw();
					_this.detectChange();
				}
			}
			
			
			inputWrap.appendChild(input);
		}
		
		
		wrap.appendChild(inputWrap);
		wrap.appendChild(tri);
		
		TweenLite.to(wrap, 0, {
			opacity: 0,
			scale: .75,
			onComplete: function(){
				_this.activePop = wrap;
				document.body.appendChild(wrap);
				_this.positionPop(block);
				TweenLite.to(wrap, .75, {
					opacity: 1,
					ease:Expo.easeOut
				})
				TweenLite.to(wrap, .75, {
					scale: 1,
					ease:Elastic.easeOut
				});
			}
		});
	}
	
	var previousVal = "";
	this.detectChange = function(){
		// this probably isn't the best way to do this but it works for now
		var newVal = _this.serialize().toString();
		if(previousVal !== newVal){
			previousVal = newVal;
			
			if(typeof options.onchange === 'function'){
				_this.onchange.apply(_this, _this.serialize());
			}
		}
	}
	
	// init ----------------------------------------------
	this.initStyle();
	this.init({});
	this.draw();
}