var Triceraplots = function(options) {

    var _this = this;

    this.fps = 15;
    this.container = options.container;
    this.overlay_enabled = (options.overlay) ? options.overlay : true;
    this.overlay_needs_update = true;
    this.overlay_data = []; // Visual data max compression
    this.size = options.container.getBoundingClientRect();
    this.cap_y = (options.cap_y) ? options.cap_y : null; // Used for demand shaving. 
    this.peak_y = 0; // highest peak in the series
    this.shaving = (options.shaving) ? options.shaving : false;
    this.dragging = false; // true when the mouse button is down and moving
    this.perpetual_draw = false; // Set to true for perpetual perpetual_draw
    this.canvases = []; // HTML canvas elements
    this.contexts = []; // canvas 2D contexts
	
	this.tariff_data = []; 
	
	this.plot_styles = [
		"fill",
		"fill",
		"fill",
		"fill",
		"fill",
		"fill",
		"fill",
		"fill",
		"fill",
		"fill",
		"fill",
		"fill",
		"fill",
		"fill"
	];
	
    this.colors = [// colors for plotting multiple datasets
        '#ff0000',
		'#f1c40f',
		'#9b59b6',
		'#34495e',
		'#e74c3c',
		'#3498db',
		'#2ecc71',
		'#9b59b6',
		'#34495e',
		'#e74c3c',
		'#3498db',
		'#2ecc71',
		'#9b59b6',
		'#34495e',
		'#e74c3c',
		'#3498db',
		'#2ecc71',
		'#9b59b6',
		'#34495e',
		'#e74c3c'
    ];

    this.seasons = [
        'Winter',
        'Winter',
        'Spring',
        'Spring',
        'Spring',
        'Summer',
        'Summer',
        'Summer',
        'Fall',
        'Fall',
        'Fall',
        'Winter'
    ];

    this.seasons_colors = {
        Winter: '#3498db',
        Spring: '#2ecc71',
        Summer: '#f1c40f',
        Fall: '#34495e'
    };

    this.intent_to = {
        pan: false, 			// t || f
        pan_scrubber: false, 	// t || f
        resize_scrubber: 0 		// -1 || 0 || 1
    };

    this.scrubber = {
        top: _this.size.height - 50,
        bottom: _this.size.height,
        width: _this.size.width,
        height: 50,
        left: 0,
        right: _this.size.width,
        range: 0
    };

    this.date_labels = {
        font_size: 10,
        font_color: 'rgba(0,0,0,1)',
        height: 14,
        bg_color: 'rgba(200,200,200,1)',
        label_width: null, // updated dynamically
        label_padding: 20
    };

    this.buttons = {
        left_handle: {
            x: 0,
            y: 0,
            r: 10,
            fill_color: 'rgba(75,75,75,.75)',
            onmouseover: function() {
            },
            onmouseout: function() {
            },
            ondrag: function() {
                _this.plot_start = _this.x_to_datapoint_index(_this.mouse.location[0]);
            }
        },
        right_handle: {
            x: 0,
            y: 0,
            r: 10,
            fill_color: 'rgba(75,75,75,.75)',
            onmouseover: function() {
            },
            onmouseout: function() {
            },
            ondrag: function() {
                _this.plot_end = _this.x_to_datapoint_index(_this.mouse.location[0]);
            }
        }
    };

    this.mouse = {
        dragging: false,
        isdown: false,
        hovering: [], //array of... things(? lol) under the mouse cursor
        location: [], // [x,y]
        down: [], // [x,y]
        up: [], // [x,y]
        delta: [] // [x,y]
    };

    this.touch = {
        start: [],
        stop: [],
        delta: []
    };
	

    this.plot_style = "fill"; // fill, wave
    this.stroke_width = (options.stroke_width) ? options.stroke_width : 2;
    this.stroke_opacity = (options.stroke_opacity) ? options.stroke_opacity : 1; // plot stroke alpha
    this.fill_opacity = (options.fill_opacity) ? options.fill_opacity : .85; // plot fill alpha

    // numbohrs fohr maths
    this.aggregate_visual_by = 512; // initial value is set high to spare memory during init. This number is updated almost instantly though.

    // arrays for processed data
    this.crunch_file = (options.crunch_file) ? options.crunch_file : "crunch.js";
    this.cruncher = new Worker(this.crunch_file);
    this.plot_start = 0;  // visual plpot index start
    this.plot_end = 0;  // visual plpot index stop
    this.raw_data = []; // 100% of the data in all it's massive glory
    this.shaved_data = []; // shaved raw data
    this.visual_data = []; // aggregated raw data for plotting
	this.trimmed_visual_data = [];
    this.dates = (options.dates) ? options.dates : []; // should be an array of integers with the exact length of the data provided
	
    var message_id = 0; // thread response is actually asynch so they may not return in order. This makes sure we're only updating the view with the most recent message
    this.cruncher.onmessage = function(e) {
        if (e.data.message_id === message_id) {
            for (var d in e.data) {
				
				if(d === 'trimmed_visual_data'){
					for(var i = 0; i < e.data[d].length; i++){
						_this.trimmed_visual_data[i] = new Float32Array(e.data[d][i]);
					}
				}else{
					_this[d] = e.data[d];
				}
				
				
				//_this[d] = e.data[d];
            }
            window.requestAnimationFrame(_this.draw);
        }
    };

    // Prepare data to be plotted
    this.prep_data = function() {

        message_id++;

        var message = {
            plot_start: _this.plot_start,
            plot_end: _this.plot_end,
            raw_data: _this.raw_data,
            width: _this.size.width,
            aggregate_visual_by: _this.aggregate_visual_by,
            message_id: message_id
        };

        //time stamps
        if (_this.dates.length === _this.raw_data[0].length) {
            message.dates = _this.dates;
        }

        // shave data
        if (_this.cap_y !== null && _this.shaving === true) {
            message.cap_y = _this.cap_y * 1;
        }

        // overlay update?
        if (_this.overlay_needs_update === true) {
            message.overlay_needs_update = true;
            _this.overlay_needs_update = false;
        }

        // post to worker
        _this.cruncher.postMessage(message);

    };

    // FETCH CSV FILE VIA AJAX... err it should be called JAX right?
    this.load_csv = function(data_location) {
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.open("GET", data_location, false);
        xmlhttp.send();
        _this.consume_csv(xmlhttp.responseText);
    };

    // PARSE CSV STRING
    this.consume_csv = function(string, resetZoom) {

        // replace line breaks with commas
        string = string.replace(/(?:\r\n|\r|\n)/g, ',');

        // convert aray of strings to an array of integers
        var temp = new Array();
        temp = string.split(",");

        for (a in temp) {
            var watts = parseFloat(temp[a]);
            
            if(typeof watts === 'number'){
				this.before_total += watts;
				temp[a] = watts;
			}
        }

        this.raw_data.push(temp);
		if(resetZoom !== false){
			this.plot_end = temp.length - 1;
		}
        

        if (this.raw_data.length === options.data.length) {
            this.cap_y = this.update_peak_y();
            this.prep_data();

            if (typeof options.ready === 'function') {
                options.ready.call(this);

            }

            this.date_labels_need_update = true;
            this.overlay_needs_update = true;
        }
    };

	// takes the average value of each context and adjusts the canvas's z index to put the lowest lines in front
	this.sort_contexts = function(){
		
		for(var i = 0; i < this.raw_data_average; i++){
		
		}
		
	}
	
    this.consume_array = function(arr, resetZoom) {

        this.raw_data.push(arr);
		
		if(resetZoom !== false){
			this.plot_end = arr.length - 1;
		}
        if (this.raw_data.length === options.data.length) {
            this.cap_y = this.update_peak_y();
            this.prep_data();

            if (typeof options.ready === 'function') {
                options.ready.call(this);

            }
            this.date_labels_need_update = true;
            this.overlay_needs_update = true;
        }
    }

    this.update_peak_y = function() {
        var peak = 0;
        var low = 0;
        for (var i = 0; i < this.raw_data.length; i++) {
            var data_set = this.raw_data[i];
            for (var d = 0; d < data_set.length; d++) {
                peak = (data_set[d] > peak) ? data_set[d] : peak;
                low = (data_set[d] < low) ? data_set[d] : low;
            }
        }
        peak = peak + (peak * .05); // 5% vertical padding at the top
        this.peak_y = peak;
        this.low_y = low;
        return peak;
    };

    // CLEAR CANVASES  -----------------------------
    this.clear_canvases = function() {
        var l = this.canvases.length;
        while (l--) {
            this.clear_canvas(l);
        }
    };
    this.clear_canvas = function(i) {
        _this.contexts[i].clearRect(0, 0, this.size.width, this.size.height);
    };

    // DRAW VISUAL DATA TO SCREEN ------------------
    this.plot_all = function() {
		if(_this.contexts.length !== _this.raw_data.length){
			_this.create_elements();
			_this.resize();
			return;
		}
        var l = _this.contexts.length;
        while (l--) {
            _this.plot_data(l);
        }
		
		if(options.autosort !== false){
			this.update_z_index();
		}
    };
	
    this.plot_data = function(i) {
		if(!_this.trimmed_visual_data[i]){
			return;
		}
        var visual_data = _this.trimmed_visual_data[i];
        var ctx = _this.contexts[i];
        var l = visual_data.length;
        var start = _this.convert_vector([0, visual_data[0]], l);

        // clear right before plotting
        _this.clear_canvas(i);

        ctx.beginPath();
        ctx.moveTo(start[0], (_this.size.height - _this.scrubber.height - _this.date_labels.height)); // Start at bottom left corner
        ctx.lineTo(start[0], start[1]); // Start at bottom left corner

        if (_this.plot_styles[i] === 'fill') {

            // lop through data
            for (var _i = 0; _i < l; _i++) {
                // PLOT LINE
                var line_to = _this.convert_vector([_i, visual_data[_i]], l);
                ctx.lineTo(line_to[0], line_to[1]);
            }

            var end = this.convert_vector([l, 0], l);
            ctx.lineTo(end[0], end[1]);
            ctx.closePath();

            ctx.fillStyle = this.colors[i];
            ctx.fill();
			return;
			
        }
		
		if(_this.plot_styles[i] === 'line'){
			
			// fattttttyyyyy
			ctx.beginPath();
			ctx.lineWidth = 3;
			var m = _this.convert_vector([0, visual_data[0]], l);
			ctx.moveTo(-10, m[1]);
			// lop through data
            for (var _i = 0; _i < l; _i++) {
                // PLOT LINE
                var line_to = _this.convert_vector([_i, visual_data[_i]], l);
                
                ctx.lineTo(line_to[0], line_to[1]);
            }
			
            var end = this.convert_vector([l, 0], l);
            ctx.lineTo(end[0], end[1]);

            ctx.strokeStyle = this.colors[i];
            ctx.stroke();
			return;
			
		}else{

            var y = this.size.height - this.scrubber.height - this.date_labels.height;

            // lop through data
            for (var _i = 0; _i < l; _i++) {
                // PLOT LINE
                var vec2 = _this.convert_vector([_i, visual_data[_i]], l);
                ctx.moveTo(vec2[0], y);
                ctx.lineTo(vec2[0], vec2[1]);
            }

            ctx.strokeStyle = this.colors[i];
            ctx.lineWidth = 1;
            ctx.stroke();

        }

    };

    // UI EVENTS 
    this.add_events = function() {

        options.container.addEventListener('mousedown', _this.mousedown);
        options.container.addEventListener('touchstart', _this.touchstart);

        options.container.addEventListener('mouseup', _this.mouseup);
        window.addEventListener('mouseup', _this.mouseup);

        options.container.addEventListener('touchend', _this.touchend);
        window.addEventListener('touchend', _this.touchend);

        options.container.addEventListener('mousemove', _this.mousemove);
        options.container.addEventListener('touchmove', _this.touchmove);

        if(typeof jQuery !== 'undefined'){
            $(options.container).mouseleave(function(){
                _this.fps = 4;
            })
            $(options.container).mouseenter(function(){
                _this.fps = 60;
            })
        }

        window.addEventListener('resize', _this.resize);
    };

    this.mousemove = function(e) {
        e.preventDefault();

        // UPDATE MOUSE POSITION
        var pos = getPosition(this);

        var event_x = (e.touches) ? e.touches[0].pageX : e.pageX;
        var event_y = (e.touches) ? e.touches[0].pageY : e.pageY;

        var scroll = getScrollTop();
        var scroll_x = scroll[0];
        var scroll_y = scroll[1];

        var relX = event_x - pos.x - scroll_x;
        var relY = event_y - pos.y - scroll_y;

        // UPDATE MOUSE MOVEMENT DELTA
        _this.mouse.delta = [
            _this.mouse.location[0] - relX,
            _this.mouse.location[1] - relY
        ];

        _this.mouse.location = [relX, relY];


        _this.mouse.data_index = _this.x_to_datapoint_index(_this.mouse.location[0]);


        // update hover array
        _this.update_mouse_over();

        // Drag cap for PV shaving
        if (_this.mouse.dragging !== false) {
            if (typeof _this.mouse.dragging.ondrag === 'function') {
                _this.mouse.dragging.ondrag();
            }
        } else {
            if (_this.mouse.isdown === true) {


                if (_this.mouse.down[1] < (_this.size.height - _this.scrubber.height)) {
                    // DRAGGING THE LINE CHART
                    var new_start = (_this.plot_start + _this.mouse.delta[0] < 0) ? 0 : (_this.plot_start + _this.mouse.delta[0]) >> 0;
                    var new_end = (_this.plot_end - _this.mouse.delta[0] > (_this.raw_data[0].length - 1)) ? (_this.raw_data[0].length - 1) : (_this.plot_end + _this.mouse.delta[0]) >> 0;

                    // lock plot start @ 0th index
                    if (new_start < 0) {
                        new_start = 0;
                    }
					
                    if (new_start > _this.raw_data[0].length - 8) {
                        new_start = _this.raw_data[0].length - 8;
                    }

                    // lock plot_end @ last index
                    if (new_end > _this.raw_data[0].length) {
                        new_end = _this.raw_data[0].length;
                    }
					
                    if (new_end < 8) {
                        new_end = 8;
                    }

                    _this.plot_start = new_start;
                    _this.plot_end = new_end;

                } else {
                    // DRAGGING THE SCRUBBER
                    var offset = _this.mouse.range * .5;
                    var center = _this.x_to_datapoint_index(_this.mouse.location[0]);
                    var new_start = (center - offset < 0) ? 0 : (center - offset) >> 0;
                    var new_end = (center + offset > (_this.raw_data[0].length - 1)) ? _this.raw_data[0].length - 1 : (center + offset) >> 0;
                    if ((new_end - new_start) < _this.mouse.range) {
                        return;
                    }
                    _this.plot_start = new_start;
                    _this.plot_end = new_end;

                }

            }
        }

    };
    this.touchmove = function(e) {
        e.preventDefault();

        // UPDATE MOUSE POSITION
        var pos = getPosition(this);

        var scroll = getScrollTop();
        var scroll_x = scroll[0];
        var scroll_y = scroll[1];

        var event_x = (e.touches) ? e.touches[0].pageX : e.pageX;
        var event_y = (e.touches) ? e.touches[0].pageY : e.pageY;

        var relX = event_x - pos.x - scroll_x;
        var relY = event_y - pos.y - scroll_y;

        // UPDATE MOUSE MOVEMENT DELTA
        _this.mouse.delta = [
            _this.mouse.location[0] - relX,
            _this.mouse.location[1] - relY
        ];

        _this.mouse.location = [relX, relY];

        // Drag cap for PV shaving
        if (_this.mouse.dragging !== false) {
            if (typeof _this.mouse.dragging.ondrag === 'function') {
                _this.mouse.dragging.ondrag();
            }
        }

    };
    this.mouseup = function(e) {
        _this.mouse.isdown = false;

        var scroll = getScrollTop();
        var scroll_x = scroll[0];
        var scroll_y = scroll[1];

        var pos = getPosition(this);
        var relX = e.pageX - pos.x - scroll_x;
        var relY = e.pageY - pos.y - scroll_y;

        _this.mouse.dragging = false;
        _this.mouse.up = [relX, relY];

    };
    this.touchend = function(e) {
        var pos = getPosition(this);

        var event_x = (e.touches) ? e.touches[0].pageX : e.pageX;
        var event_y = (e.touches) ? e.touches[0].pageY : e.pageY;

        var scroll = getScrollTop();
        var scroll_x = scroll[0];
        var scroll_y = scroll[1];

        var relX = event_x - pos.x - scroll_x;
        var relY = event_y - pos.y - scroll_y;

        _this.mouse.hovering = [];
        _this.mouse.dragging = false;
        _this.mouse.up = [relX, relY];

    };
    this.mousedown = function(e) {
        e.preventDefault();
        _this.mouse.isdown = true;
        var pos = getPosition(this);

        var event_x = (e.touches) ? e.touches[0].pageX : e.pageX || e.clientX;
        var event_y = (e.touches) ? e.touches[0].pageY : e.pageY || e.clientY;

        var scroll = getScrollTop();
        var scroll_x = scroll[0];
        var scroll_y = scroll[1];

        var relX = event_x - pos.x - scroll_x;
        var relY = event_y - pos.y - scroll_y;

        _this.mouse.down = [relX, relY];
        _this.mouse.range = _this.plot_end - _this.plot_start;

        if (_this.mouse.hovering.length > 0) {
            _this.mouse.dragging = _this.mouse.hovering[0];
        }


    };
    this.touchstart = function(e) {

		// e.preventDefault();

        // UPDATE MOUSE POSITION
        var pos = getPosition(this);

        var event_x = (e.touches) ? e.touches[0].pageX : e.pageX;
        var event_y = (e.touches) ? e.touches[0].pageY : e.pageY;

        var scroll = getScrollTop();
        var scroll_x = scroll[0];
        var scroll_y = scroll[1];

        var relX = event_x - pos.x - scroll_x;
        var relY = event_y - pos.y - scroll_y;

        _this.mouse.location = _this.touch.start = [relX, relY];

        _this.mouse.hovering = [];
        _this.mouse.dragging = false;

        _this.update_mouse_over();

        if (_this.mouse.hovering.length > 0) {
            _this.mouse.dragging = _this.mouse.hovering[0];
        }

    };

    this.resize = function(e) {

        _this.size = _this.container.getBoundingClientRect();

        var l = _this.canvases.length;
        var ratio = window.devicePixelRatio || 1;
		
        while (l--) {

            _this.canvases[l].width = _this.size.width * ratio;
            _this.canvases[l].height = _this.size.height * ratio;
            _this.canvases[l].style.width = _this.size.width + "px";
            _this.canvases[l].style.height = _this.size.height + "px";
            _this.contexts[l].scale(ratio, ratio);

        }

		_this.scrubber.top = _this.size.height - _this.scrubber.height,
		_this.scrubber.width = _this.size.width;
		_this.scrubber.left = 0;
		_this.scrubber.right = _this.size.width;

        // scale overlay canvas
        _this.overlay_canvas.width = _this.size.width * ratio;
        _this.overlay_canvas.height = _this.size.height * ratio;
        _this.overlay_canvas.style.width = _this.size.width + "px";
        _this.overlay_canvas.style.height = _this.size.height + "px";
        _this.overlay_ctx.scale(ratio, ratio);
		
		// scale bg canvas
        _this.bg_canvas.width = _this.size.width * ratio;
        _this.bg_canvas.height = _this.size.height * ratio;
        _this.bg_canvas.style.width = _this.size.width + "px";
        _this.bg_canvas.style.height = _this.size.height + "px";
        _this.bg_ctx.scale(ratio, ratio);
		
		// scale tariff canvas
		_this.tariff_canvas.width = _this.size.width * ratio;
        _this.tariff_canvas.height = _this.size.height * ratio;
        _this.tariff_canvas.style.width = _this.size.width + "px";
        _this.tariff_canvas.style.height = _this.size.height + "px";
        _this.tariff_ctx.scale(ratio, ratio);
		
        _this.overlay_needs_update = true;
    };

    this.update_mouse_over = function() {

        _this.mouse.hovering = [];

        for (var button_name in _this.buttons) {

            var button = _this.buttons[button_name];

            var button_start_x = button.x - (button.r);
            var button_end_x = button.x + (button.r);
            var button_start_y = button.y - (button.r);
            var button_end_y = button.y + (button.r);

            // BB collision
            if (
                    _this.mouse.location[0] > button_start_x &&
                    _this.mouse.location[0] < button_end_x &&
                    _this.mouse.location[1] > button_start_y &&
                    _this.mouse.location[1] < button_end_y
                    ) {

                _this.mouse.hovering.push(button);
                if (typeof button.onmouseover === 'function') {
                    button.onmouseover();
                }
            } else {
                if (typeof button.onmouseout === 'function') {
                    button.onmouseout();
                }
            }

        }

        _this.update_intent();
    };
    this.update_intent = function() {
        if (_this.mouse.hovering.length !== 0) {

            if (_this.mouse.isdown === true) {
                options.container.style.cursor = '-webkit-grabbing';
            } else {
                options.container.style.cursor = '-webkit-grab';
            }

        } else {

            if (_this.mouse.location[1] < (_this.size.height - _this.scrubber.height)) {

                if (_this.mouse.isdown === true) {
                    options.container.style.cursor = '-webkit-grabbing';
                } else {
                    options.container.style.cursor = '-webkit-grab';
                }
                _this.intent_to.pan = true;
                _this.intent_to.resize_scrubber = 0;
                _this.intent_to.pan_scrubber = false;

            } else {
                var index = _this.mouse.data_index;
                _this.intent_to.pan = false;

                if (index > _this.plot_start && index < _this.plot_end) {
                    options.container.style.cursor = '-webkit-grab';
                    _this.intent_to.pan_scrubber = true;
                    _this.intent_to.resize_scrubber = 0;
                }


            }
        }
    };

    this.draw_overlay = function() {
	
		this.overlay_ctx.clearRect(0, 0, _this.size.width, _this.size.height);
		
		
        if (_this.overlay_enabled !== true || !_this.overlay_data || _this.overlay_data.length === 0) {
            return;
        }

        

        this.plot_scrubber_line();
        this.draw_scrubber_handles();
        this.generate_date_labels();
		this.draw_y_axis();
    };
	
	this.calc_plot_height = function(){
		return _this.size.height - _this.scrubber.height - _this.date_labels.height;
	}
	
	this.y_axis = {
        points: 4,
        width: 0,
		label_padding: 4,
		font_size : 8,
		font_family: 'Verdana',
		align : (options.y_axis_align)? options.y_axis_align :  "right",
		label : (options.label_format)? options.label_format :function(n){return n;}
    };
	
	/*This is some wonky ish. Been changing it a lot so it's pretty sloppy.*/
	this.draw_y_axis = function(){
	
		var ctx = this.overlay_ctx;
		var bg_ctx = this.bg_ctx;
		
		// get value of the top of the chart
		var y_space = this.calc_plot_height();
		var gap = y_space / _this.y_axis.points;
		var bottom = 0;
		var watts_per_px = top / y_space;
		
		ctx.font= _this.y_axis.font_size+"px "+_this.y_axis.font_size;
		ctx.fillStyle = 'rgba(40,40,40, .45)';
		bg_ctx.strokeStyle = 'rgba(40,40,40, .25)';
		bg_ctx.beginPath();
		
		for(var i = 0; i <= _this.y_axis.points;i++){
		
			var y = gap * i;
			var line_y = y;
			var val = _this.y_to_kwatts(y);
			var text = _this.y_axis.label(val);
			var line_x;
			var label_x = _this.y_axis.label_padding;
			var line_width = _this.size.width;
			
			if(_this.y_axis.align === "right"){
				var text_width = ctx.measureText(text).width;
				line_x = 0;
				label_x += _this.size.width - (text_width + (_this.y_axis.label_padding * 2));
				line_width -= (text_width + (_this.y_axis.label_padding * 2));
			}else{
				line_x = _this.y_axis.label_padding + ctx.measureText(text).width + _this.y_axis.label_padding;
			}
			
			if(i === 0){
				y += _this.y_axis.font_size + _this.y_axis.label_padding;
			}else{
				y += _this.y_axis.font_size * .5;
			}
			
			if(i === _this.y_axis.points){
				y -= (_this.y_axis.font_size * .5) + + _this.y_axis.label_padding;
			}
			
			// fill text ---------------------------------------------------------------------------------------
			ctx.fillText(
				text,
				label_x,
				y);
			
			// stroke line ----------------------------------------------------------------------------------
			if(i !== 0){
				bg_ctx.moveTo(line_x,line_y);
				bg_ctx.lineTo(line_width,line_y);
			}
			
		}
		
		bg_ctx.stroke();
	}
	
    this.plot_scrubber_line = function() {
        var ctx = this.overlay_ctx;
		
		
		ctx.fillStyle = "rgba(0,0,0,.1)";
		ctx.fillRect(0, _this.scrubber.top, _this.size.width, (_this.size.height - _this.scrubber.top));
		
        // plot mini line -----------------------------------------------
        for (var i = 0; i < this.overlay_data.length; i++) {
            ctx.beginPath();
            ctx.strokeStyle = _this.colors[i];
            var visual_data_set = this.overlay_data[i];
            var l = visual_data_set.length;

            var start = this.convert_scrubber_vector([0, visual_data_set[0]], l, _this.scrubber);
            ctx.moveTo(start[0], start[1]);

            for (var d = 0; d < l; d++) {
                var data = visual_data_set[d];
                var pos = this.convert_scrubber_vector([d, data], l, _this.scrubber);
                ctx.lineTo(pos[0], pos[1]);
            }

            ctx.stroke();
        }
    };
    this.draw_scrubber_handles = function() {
		if(_this.plot_start < 0){_this.plot_start = 0;}
        // draw handles ----------------------------------------------
        var ctx = _this.overlay_ctx;
        var start_progress = this.plot_start / this.raw_data[0].length;
        var end_progress = this.plot_end / this.raw_data[0].length;
        var start_x = Math.round(this.overlay_data[0].length * start_progress);
        var end_x = Math.round(this.overlay_data[0].length * end_progress);
        var l = this.overlay_data[0].length;
        ctx.beginPath(start_progress);


        // scrubber line start
        var start = this.convert_scrubber_vector([start_x, 0], l, _this.scrubber);

        // scrubber line end
        var end = this.convert_scrubber_vector([end_x, 0], l, _this.scrubber);


        // grey out sections of the mini line that arent being plotted

        var left_block_width = start[0];
        var right_block_width = _this.size.width - end[0];
        var thickness = (_this.size.height - _this.scrubber.height);


        ctx.fillStyle = 'rgba(200,200,200,.75)';
        ctx.fillRect(_this.scrubber.left, thickness, left_block_width, _this.scrubber.height);
        ctx.fillRect(end[0], thickness, right_block_width, _this.scrubber.height);

        // update scrubber handle locations
        var base = thickness + (_this.scrubber.height * .5);
        var left_handle_y = base + (_this.buttons.left_handle.r * .5);
        var right_handle_y = base + (_this.buttons.right_handle.r * .5);

        this.buttons.left_handle.x = start[0];
        this.buttons.left_handle.y = left_handle_y;

        this.buttons.right_handle.x = end[0];
        this.buttons.right_handle.y = right_handle_y;

        for (var button_name in _this.buttons) {

            var button = this.buttons[button_name];


            ctx.beginPath();

            ctx.moveTo(button.x, button.y);

            // circular button for mobile
            ctx.arc(button.x, button.y, button.r, 0, 2 * Math.PI);


            ctx.fillStyle = button.fill_color;
            ctx.fill();

        }
    };

    this.hilight_seasons = function() {

        var ctx = this.bg_ctx;
        var l = _this.plot_end - _this.plot_start;
        var point_width = _this.size.width / l;
        var thickness = 2;

        var y = 0;
        var x = 0;
        var width = 0;


        var date = parseInt(this.dates[_this.plot_start]);
        var month = new Date(date).getMonth();
        var season = _this.seasons[month];
        ctx.fillStyle = _this.seasons_colors[season];

        for (var i = _this.plot_start; i < _this.plot_end; i++) {

            var _date = parseInt(this.dates[i]);
            var _month = new Date(_date).getMonth();
            var _season = _this.seasons[_month];

            // change seasons
            if (_season === season) {
                width += point_width;
                if (i === _this.plot_end) {
                    season = _season;
                }

            } else {

                // Season has changed

                // Written label
                ctx.fillStyle = '#000000';
                ctx.font = "10px Verdana";
                ctx.fillText(season + " ", x + 2, y + 12);

                // Draw colored line
                ctx.fillStyle = _this.seasons_colors[season];
                ctx.fillRect(x, y, width, thickness);

                x += width;
                width = 0;
                season = _season;

            }

        }

        ctx.fillStyle = '#000000';
        ctx.font = "10px Verdana";
        ctx.fillText(season + " ", x + 2, y + 12);
        ctx.fillStyle = _this.seasons_colors[season];

        ctx.fillStyle = _this.seasons_colors[season];
        ctx.fillRect(x, y, width, thickness);

    };

    this.hilight_months = function() {


        var ctx = this.bg_ctx;
        var l = _this.plot_end - _this.plot_start;
        var point_width = _this.size.width / l;
        var thickness = 2;

        var y = 0;
        var x = 0;
        var width = 0;


        var date = parseInt(this.dates[_this.plot_start]);
        var month = new Date(date).getMonth();
        ctx.fillStyle = _this.colors[month];

        for (var i = _this.plot_start; i < _this.plot_end; i++) {

            var _date = parseInt(this.dates[i]);
            var _month = new Date(_date).getMonth();

            // change seasons
            if (_month === month) {

                width += point_width;

                if (i === _this.plot_end) {
                    month = _month;
                    date = _date;
                }

            } else {

                // Season has changed

                // Written label
                ctx.fillStyle = '#000000';
                ctx.font = "10px Verdana";
                ctx.fillText(new Date(date).customFormat('#MM#/#YY#'), x + 2, y + 12);

                // Draw colored line
                ctx.fillStyle = _this.colors[month];
                ctx.fillRect(x, y, width, thickness);

                x += width;
                width = 0;
                month = _month;
                date = _date;

            }

        }

        ctx.fillStyle = '#000000';
        ctx.font = "10px Verdana";
        ctx.fillText(new Date(date).customFormat('#MM#/#YY#'), x + 2, y + 12);
        ctx.fillStyle = _this.colors[month];

        ctx.fillStyle = _this.colors[month];
        ctx.fillRect(x, y, width, thickness);


    };

    this.hilight_days = function() {


        var ctx = this.bg_ctx;
        var l = _this.plot_end - _this.plot_start;
        var point_width = _this.size.width / l;
        var thickness = 2;

        var y = 0;
        var x = 0;
        var width = 0;


        var date = parseInt(this.dates[_this.plot_start]);
        var day = new Date(date).getUTCDate();
        ctx.fillStyle = _this.colors[day];

        for (var i = _this.plot_start; i < _this.plot_end + 10; i++) {

            var _date = parseInt(this.dates[i]);
            var _day = new Date(_date).getUTCDate();

            // change seasons
            if (_day === day) {

                width += point_width;

                if (i === _this.plot_end) {
                    day = _day;
                    date = _date;
                }

            } else {

                // Written label
                ctx.fillStyle = '#000000';
                ctx.font = "10px Verdana";
                ctx.fillText(new Date(date).customFormat('#MM#/#DD#/#YY#  - #DDDD#'), x + 2, y + thickness + 10);

                // Draw colored line
                ctx.fillStyle = _this.colors[day];
                ctx.fillRect(x, y, width, thickness);

                x += width;
                width = 0;
                day = _day;
                date = _date;

            }

        }

        ctx.fillStyle = '#000000';
        ctx.font = "10px Verdana";
        ctx.fillText(new Date(date).customFormat('#MM#/#DD#/#YY#'), x + 2, y + 12);
        ctx.fillStyle = _this.colors[day];

        ctx.fillStyle = _this.colors[day];
        ctx.fillRect(x, y, width, thickness);
    };

    this.label_format = '#MM#/#DD#/#YYYY#';
    this.label_width = null;
    this.date_labels_need_update = true;

    this.generate_date_labels = function() {

        if (!options.dates || this.dates.length === 0) {
            this.bg_ctx.clearRect(0, 0, _this.size.width, _this.size.height);
            this.overlay_ctx.clearRect(0, 0, _this.size.width, _this.size.height);
            _this.date_labels.height = 0;
            return;
        }
		
        if (this.date_labels_need_update === false) {
            this.bg_ctx.clearRect(0, 0, _this.size.width, _this.size.height);
            this.overlay_ctx.clearRect(0, 0, _this.size.width, _this.size.height);
            return;
        }

        var ctx = this.overlay_ctx;
        ctx.font = "10px Verdana";

        // the number of datapoints in the view
        var start_date = new Date(this.dates[this.plot_start]);
        var end_date = new Date(this.dates[this.plot_end - 1]);

        var time_plotted = end_date - start_date;
        var time_per_px = time_plotted / this.size.width;
		
		if(!parseInt(time_per_px)){
			return;
		}
		
        // 1 day = 86400000
        var year_width = 31536000000 / time_per_px;
        var month_width = 2678400000 / time_per_px;
        var day_width = 86400000 / time_per_px;
        var hour_width = 3600000 / time_per_px;
        var minute_width = 60000 / time_per_px;

        // the hight of the labels themselves
        var thickness = _this.date_labels.height;

        // The location of the labels
        var y = _this.size.height - _this.scrubber.height - _this.date_labels.height;

        // The width of the label being plotted
        var ms = parseInt(this.dates[_this.plot_start]);
        var date = new Date(ms);

        // worst effing logic ever. Make not suck once you figure it out.

        var split = [
            function(date) {
                return date.getMinutes();
            },          
			function(date) {
                return date.getHours();
            },
            function(date) {
                return date.getDate();
            },
            function(date) {
                return date.getMonth();
            },
            function(date) {
                return date.getFullYear();
            }
        ];

        var _split = 0;
        var W = 0;
        var split_by = null;

		
		// minutes
        if (this.label_width < minute_width) {
            _split = 0;
            this.label_format = "#MM#/#DD# #hh#:#mm#";
            W = minute_width;
        }     
		
		// hours
		if (this.label_width < hour_width && this.label_width > minute_width) {
            
			_split = 1;
			
            this.label_format = "#MM#/#DD# #hh#:#mm#";
            W = hour_width;
			
            if ((this.label_width * 1.25) < hour_width) {
                this.label_format = "#MM#/#DD# #hh#:#mm# #ampm#";
            }
        }
		
		// days
        if (this.label_width > hour_width && this.label_width < day_width) {
            W = day_width;
            _split = 2;
            this.label_format = "#MMM# #DD##th#";
        }

        if (this.label_width > day_width && this.label_width < month_width) {
            W = month_width;
            _split = 3;
            this.label_format = "#MMM# #YYYY#";
        }

        if (this.label_width > month_width) {
            W = year_width;
            _split = 4;
            this.label_format = "#YYYY#";
        }


        var I = 0;
        var R = _this.plot_end - _this.plot_start + 1;
        var T = this.size.height - this.scrubber.height;


        this.bg_ctx.beginPath();
        this.bg_ctx.strokeStyle = 'rgba(0,0,0,.75)';
        this.bg_ctx.lineWidth = 1;
		
		

        for (var i = _this.plot_start; i < _this.plot_end; i++) {

            var _ms = this.dates[i];
            var _date = new Date(_ms);
            var _split_by = split[_split](_date);

            // change seasons
            if (_split_by === split_by) {

                if (i === _this.plot_end) {
                    split_by = _split_by;
                    date = _date;
                }

            } else {

                // Draw colored line
                var pos = this.convert_vector([I, 0], R);

                ctx.fillStyle = '#f5f5f5';
                ctx.fillRect(pos[0], y, W, thickness);

                // Written label
                ctx.fillStyle = '#555555';
				var label = _date.customFormat(this.label_format)
				if(label !== 0){
					ctx.fillText(label, pos[0] + 3, y + 10);
				}
                

                // vertical line
                if (I !== 0) {
                    this.bg_ctx.moveTo(pos[0], 0);
                    this.bg_ctx.lineTo(pos[0], y);
                }


                split_by = _split_by;
                date = _date;

            }

            I++;
        }

        // vertical line
        var pos = this.convert_vector([I, 0], R);
        /*this.bg_ctx.moveTo(pos[0], 0);
         this.bg_ctx.lineTo(pos[0], T);
         */
        this.bg_ctx.clearRect(0, 0, _this.size.width, _this.size.height);

        

        ctx.fillStyle = '#f5f5f5';
        ctx.fillRect(pos[0], y, W, thickness);

        ctx.fillStyle = '#555555';
		var label = date.customFormat(this.label_format);
		if(label !== 0){
			ctx.fillText(date.customFormat(this.label_format), pos[0] + 3, y + 10);
			this.bg_ctx.strokeStyle = 'rgba(40,40,40,.25)';
			this.bg_ctx.stroke();
		}
        



    };

    // because the aggregate_visual_by value can change mid draw we have to canche it during the draw call and pass it to the convert_vector method
    this.convert_vector = function(vec2, dataset_length) {
        var x = Math.round((this.size.width / (dataset_length - 1)) * vec2[0]); // round to avoid half pixels
        var y_space = _this.calc_plot_height();
        var y = y_space - ((y_space / _this.peak_y) * vec2[1]); // the chart does not move vertically, half pixels are totes cool

		// hacky to prevent negative numbers from covering the scrubber
        if (y > y_space) {
            y = y_space
        }


        return [x, y];
    };

    this.convert_scrubber_vector = function(vec2, dataset_length, size) {
        var x = ((size.width / dataset_length) * vec2[0]) | 0;
        var y = size.height - ((size.height / _this.peak_y) * vec2[1]) + (_this.size.height - size.height);
        return [x, y];
    };

    this.y_to_kwatts = function(y) {
		var height = _this.size.height - _this.scrubber.height - _this.date_labels.height;
        var r = _this.peak_y - ((y / height) * _this.peak_y);
        return r;
    };

    this.x_to_datapoint_index = function(x) {
        var px_val = _this.raw_data[0].length / _this.size.width;
        return (x * px_val) >> 0;
    };
	
	/*	TARIFF SHIT ---------------------------------------------------------------------------*/ 
	this.tariff_max = {kw: 0,kwh : 0}; // needed for dynamic scale
	
	this.draw_tariff_data = function(){

	
		if(_this.tariff_data.length === 0){return;};
		
		var span = _this.plot_end - _this.plot_start;
		var ratio = _this.size.width / span;
		this.tariff_ctx.clearRect(0,0, _this.size.width, _this.size.height);
		
		if(ratio >= 2){
			
			// clip window
			var windowSize = 100; // percent
			var percent = _this.size.width / 100;
			var space = (100 - windowSize) / 2;
		
			var plot_value = "kw";
		
			var baseline = _this.size.height - (_this.scrubber.height + _this.date_labels.height);
			var y_scale = baseline / this.tariff_max[plot_value];
			var step_width = ratio;
			var steps_taken = 0;
			
			var previous_val = _this.tariff_data[_this.plot_start][plot_value];
			
			var block = {
				location: [
					0, 
					0
				],
				size: [
					0, 
					baseline
				]
			};
			
			for(var i = _this.plot_start; i < _this.plot_end; i++){
				var val = _this.tariff_data[i][plot_value];
				
				if(val !== previous_val){
				
					_this.tariff_ctx.beginPath();
					
					var scale = previous_val / _this.tariff_max[plot_value];
					
					/*var fill = _this.tariff_ctx.createLinearGradient(0,0,0,(_this.size.height - _this.scrubber.height));
					fill.addColorStop(0,"rgba(255,121,39,"+scale+")");
					fill.addColorStop(.65,"rgba(255,155,40,"+scale * .8+")");
					fill.addColorStop(.75,"rgba(255,155,40,"+scale * .7+")");*/		
					
					var r =  225+ Math.round(30 * ( 1 - scale));
					var g = 225+ Math.round(30 * ( 1 - scale));
					var b = 225+ Math.round(30 * ( 1 - scale));
					
					var a = 1;
					var fill = "rgba("+r+","+g+","+b+","+a+")";
					
					var text = "$"+previous_val+"/kW";
					
					_this.tariff_ctx.strokeStyle = "rgba(0,0,0,1)";
					_this.tariff_ctx.fillStyle = fill;
					
					_this.tariff_ctx.fillRect(
						block.location[0], 
						block.location[1],
						block.size[0],
						block.size[1]
					);					
					
					_this.tariff_ctx.moveTo((block.location[0] + block.size[0]), 0);
					_this.tariff_ctx.lineTo((block.location[0] + block.size[0]), baseline);
					_this.tariff_ctx.stroke();
					var textWidth = _this.tariff_ctx.measureText(text).width;
					if(textWidth < block.size[0]){
					var text_x = (block.location[0] + (block.size[0] * .5) ) - (textWidth * .5);
						_this.tariff_ctx.fillStyle = "rgba(74,74,74,1)";
						_this.tariff_ctx.fillText(text, text_x, 20);
					}
					
					/*
					block = {
						location: [
							step_width * steps_taken, 
							baseline - (val * y_scale)
						],
						size: [
							step_width,
							val * y_scale
						]
					};	*/
					
					block = {
						location: [
							step_width * steps_taken, 
							0
						],
						size: [
							step_width,
							baseline
						]
					};
					
					previous_val = val;
					
				}else{
					
					block.size[0] += step_width;
				
				}
				
				steps_taken++;
				
			}
			
			_this.tariff_ctx.beginPath();
			
			if(_this.tariff_data[_this.plot_end]){
			
				var scale = _this.tariff_data[_this.plot_end][plot_value] / _this.tariff_max[plot_value];
				var r =  225+ Math.round(30 * ( 1 - scale));
				var g = 225+ Math.round(30 * ( 1 - scale));
				var b = 225+ Math.round(30 * ( 1 - scale));
				var a = 1;
				
				var fill = "rgba("+r+","+g+","+b+","+a+")";
					
				_this.tariff_ctx.fillStyle = fill;
				
				_this.tariff_ctx.fillRect(
					block.location[0], 
					block.location[1],
					block.size[0],
					block.size[1]
				);	
				
				var text = "$"+previous_val+"/"+plot_value;
				var textWidth = _this.tariff_ctx.measureText(text).width;
				
				if(textWidth < block.size[0]){
					var text_x = (block.location[0] + (block.size[0] * .5) ) - (textWidth * .5);
					_this.tariff_ctx.fillStyle = "rgba(74,74,74,1)";
					_this.tariff_ctx.fillText(text, text_x, 20);
				}
				
			}
			
			_this.tariff_ctx.clearRect(0,0, (percent * space), (_this.size.height - _this.scrubber.height));
			_this.tariff_ctx.clearRect((percent * (space + windowSize)),0,(percent * 100),(_this.size.height - _this.scrubber.height));
			
		}
	}
	/*	TARIFF SHIT ---------------------------------------------------------------------------*/ 
	
    this.draw = function() {
	
        _this.plot_all();
		
		_this.draw_tariff_data();
		
        _this.draw_overlay();

        if (typeof options.onupdate === 'function') {
            options.onupdate();
        }

        window.setTimeout(_this.prep_data, 1000 / _this.fps);
    };

    this.zoom_to_date_range = function(start, end) {

        var offset = new Date('1/1/2009'); // use this date because I know DST is not in effect
        offset = (offset.getTimezoneOffset()) * 60000; // millesecond difference between UTC and local time
		
		offset = 0;
		
        start = new Date(start);
        end = new Date(end);

        start = new Date(start.getTime() + offset);

        end = new Date(end.getTime() + offset);

        var start_index = 0;
        var end_index = 0;

        var l = _this.dates.length;

        for (var i = 0; i < l; i++) {

            var _date = new Date(_this.dates[i] + offset);

            if (_date <= start) {
                start_index = i;
            }

            if (_date >= end) {
                end_index = i;
                break; // end the loop
            }
        }

        if (typeof TweenLite === 'function') {

            var plot = {
                start: _this.plot_start,
                end: _this.plot_end
            };
            _this.fps = 60;
            TweenLite.to(plot, 1, {
                start: start_index,
                end: end_index,
                onUpdate: function() {
                    _this.plot_start = plot.start >> 0;
                    _this.plot_end = plot.end >> 0;
                },
                onComplete : function(){
                    window.setTimeout(function(){
                        _this.fps = 4;
                    }, 250)
                    
                }
            });
			
        } else {
            _this.plot_start = start_index;
            _this.plot_end = end_index;
        }
    };

	// looks at the average value of the visual data and adjusts the z index of each plot so the tallest plots are in back and the shortest are in front
	this.update_z_index = function(){
	
		var averages = [];
		var _averages = {};
		
		// populate averages array
		for(var set = 0; set < this.trimmed_visual_data.length; set++){
			var dataset = this.trimmed_visual_data[set];
			var visual_total = 0;
			for(var point = 0; point < dataset.length; point++){
				visual_total +=  dataset[point];
			}
			
			averages.push([set, (visual_total / dataset.length)]);
		}
		
		averages.sort(function(a, b) {return a[1] - b[1]})
		
		// apply sorted array to canvas z-index
		
		for(var i = 0; i < averages.length; i++){
			var sort = averages[i];
			var canvas = this.canvases[sort[0]];
			var z_index = 2 + (averages.length - i);
			canvas.style.zIndex = z_index;
		}
		
	}
	
    // CREATE AND STYLE THE DOM NODES USED IN THE INTERFACE
    this.create_elements = function() {
		
        // clear the container
        options.container.innerHTML = "";
		_this.canvases = [];
		_this.contexts = [];
		
		this.tariff_canvas = document.createElement('canvas');
		this.tariff_canvas.id = "tariff_canvas";
		this.tariff_canvas.style.position = "absolute";
		this.tariff_canvas.style.top = "0px";
		this.tariff_canvas.style.left = "0px";
		this.tariff_canvas.style.zIndex = "2";
		
		this.tariff_ctx = this.tariff_canvas.getContext('2d');
		this.container.appendChild(this.tariff_canvas);
		
        // create canvas elements
        for (var i = 0; i < this.raw_data.length; i++) {
            _this.canvases.push(document.createElement('canvas'));
            var canvas = this.canvases[i];
            canvas.id = "data_index_" + i;
            options.container.appendChild(canvas);
            canvas.style.position = "absolute";
            canvas.style.zIndex = i + 5;
            canvas.style.top = canvas.style.left = "0px";
            this.contexts.push(canvas.getContext("2d"));
            this.clear_canvas(i);
        } 

        this.overlay_canvas = document.createElement('canvas');
        this.overlay_canvas.style.position = "absolute";
        this.overlay_canvas.style.pointerEvents = "none";
        this.overlay_canvas.className = "overlay_canvas";
        this.overlay_canvas.style.top = this.overlay_canvas.style.left = "0px";
        this.overlay_canvas.style.zIndex = 100;
        options.container.appendChild(this.overlay_canvas);
        this.overlay_ctx = this.overlay_canvas.getContext("2d");

        this.bg_canvas = document.createElement('canvas');
        this.bg_canvas.style.position = "absolute";
        this.bg_canvas.style.pointerEvents = "none";
        this.bg_canvas.className = "bg_canvas";
        this.bg_canvas.style.top = this.bg_canvas.style.left = "0px";
        this.bg_canvas.style.zIndex = 1;
        options.container.appendChild(this.bg_canvas);
        this.bg_ctx = this.bg_canvas.getContext("2d");

        var l = this.canvases.length;
        while (l--) {
            var ratio = window.devicePixelRatio || 1;
            this.canvases[l].height = this.size.height * ratio;
            this.canvases[l].width = this.size.width * ratio;
        }
    };

    this.update_data = function(data, resetZoom) {
        // data can be a csv, the location of a csv resource, or a plain ol javascript array
        // data = [
        //			'1,2,3',
        //			http://link.to.some/file.csv,
        //			[7,8,9]
        //		]
        // data = [[1,2,3]] use an array in an array even if you're plotting a single dataset
		
		if(resetZoom !== false){
		    this.plot_start = 0;
			this.plot_end = 0;
		}

        this.date_labels_need_update = false;
		this.raw_data = [];
		
        for (var i = 0; i < data.length; i++) {

            if (typeof data[i] === 'string') {
                var comma_count = (data[i].split(",").length - 1);
                if (comma_count > 1) {
                    // is csv
                    this.consume_csv(data[i], resetZoom);
					return;
                } else {
                    // is resource location
                    //this.load_csv(data[i]);
                }
            } else {
                this.consume_array(data[i], resetZoom);
            }

        }
		_this.update_peak_y();
    }

    this.init = function() {

        // normalize request animation frame
        window.requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame || window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;

        // Custom date format
        if (!Date.customFormat) {
            Date.prototype.customFormat = function(formatString) {
                try {
                    var YYYY, YY, MMMM, MMM, MM, M, DDDD, DDD, DD, D, hhh, hh, h, mm, m, ss, s, ampm, AMPM, dMod, th;
                    var dateObject = this;
                    YY = ((YYYY = dateObject.getFullYear()) + "").slice(-2);
                    MM = (M = dateObject.getMonth() + 1) < 10 ? ('0' + M) : M;
                    MMM = (MMMM = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"][M - 1]).substring(0, 3);
                    DD = (D = dateObject.getDate()) < 10 ? ('0' + D) : D;
                    DDD = (DDDD = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"][dateObject.getDay()]).substring(0, 3);
                    th = (D >= 10 && D <= 20) ? 'th' : ((dMod = D % 10) == 1) ? 'st' : (dMod == 2) ? 'nd' : (dMod == 3) ? 'rd' : 'th';
                    formatString = formatString.replace("#YYYY#", YYYY).replace("#YY#", YY).replace("#MMMM#", MMMM).replace("#MMM#", MMM).replace("#MM#", MM).replace("#M#", M).replace("#DDDD#", DDDD).replace("#DDD#", DDD).replace("#DD#", DD).replace("#D#", D).replace("#th#", th);

                    h = (hhh = dateObject.getHours());
                    if (h == 0)
                        h = 24;
                    if (h > 12)
                        h -= 12;
                    hh = h < 10 ? ('0' + h) : h;
                    AMPM = (ampm = hhh < 12 ? 'am' : 'pm').toUpperCase();
                    mm = (m = dateObject.getMinutes()) < 10 ? ('0' + m) : m;
                    ss = (s = dateObject.getSeconds()) < 10 ? ('0' + s) : s;
                    return formatString.replace("#hhh#", hhh).replace("#hh#", hh).replace("#h#", h).replace("#mm#", mm).replace("#m#", m).replace("#ss#", ss).replace("#s#", s).replace("#ampm#", ampm).replace("#AMPM#", AMPM);
                } catch (err) {
                    return 0;
                }
            };
        }
		
		// load csv data
        _this.update_data(options.data);
		this.create_elements();
        this.label_width = this.overlay_ctx.measureText('xxxxxxxxxxxxx').width;
        this.add_events();

        // Canvases are positioned absolutely so the wrap needs to have a relative position.
        // TODO : Create inner wrap so the container position property isn't changed
        if (options.container.style.position == '') {
            options.container.style.position = 'relative';
        }

        // use colors
        if (options.colors) {
            for (var i = 0; i < options.colors.length; i++) {
                _this.colors[i] = options.colors[i];
            }
        }
		
        // Javascript media q
        if (window.matchMedia) {
            var is_phone = window.matchMedia("(min-device-width : 320px) and (max-device-width : 768px)");
            var is_tablet = window.matchMedia("(min-device-width : 769px) and (max-device-width : 1024px)");

            if (is_phone.matches === true) {
                _this.buttons.left_handle.r = _this.buttons.right_handle.r = 35;
            } else if (is_tablet.matches === true) {
                _this.buttons.left_handle.r = _this.buttons.right_handle.r = 20;
            }
        }
        // run resize once for good measure
        _this.resize();
    };

    this.init();

};


function hexToRgb(hex) {
    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return result ? {
        r: parseInt(result[1], 16),
        g: parseInt(result[2], 16),
        b: parseInt(result[3], 16)
    } : null;
}

function getPosition(element) {
    if(!element){
        return {x: 0, y: 0}
    }
    
    // I have no fucking idea why this is necessary but occasionally getBoundingClientRect is undefined
    if(!element.getBoundingClientRect){
        return {x: 0, y: 0}
    }

    var xPosition = 0;
    var yPosition = 0;

    // loop through parents and add that plop up
    // while (element) {
    //     xPosition += (element.offsetLeft - element.scrollLeft + element.clientLeft);
    //     yPosition += (element.offsetTop - element.scrollTop + element.clientTop);
    //     element = element.offsetParent;
    // }

    // client rect is more accurate. Firefox didn't like the above method.
    var rect = element.getBoundingClientRect();

    return {x: rect.left, y: rect.top};
}

function localize_date(date) {
    return new Date(date.getFullYear(), date.getMonth(), date.getDay(), date.getHours(), date.getMinutes(), date.getSeconds());
}

function prep_power_data(data) {

    if (typeof data === 'string') {
        try {
            data = JSON.parse(data);
        } catch (err) {
            console.warn(err);
        }
    }

    var ret = {
        power: [],
        dates: []
    };

    data = data.power;

    var i = data.length;
    while (i--) {

        var date = Math.round(data[i][0] * 1000);
        var power = data[i][1] * 1;

        ret.dates.push(date);
        ret.power.push(power);
    }

    return ret;
}

Date.prototype.customFormat = function(formatString) {
    try {
        var YYYY, YY, MMMM, MMM, MM, M, DDDD, DDD, DD, D, hhh, hh, h, mm, m, ss, s, ampm, AMPM, dMod, th;
        var dateObject = this;
        YY = ((YYYY = dateObject.getFullYear()) + "").slice(-2);
        MM = (M = dateObject.getMonth() + 1) < 10 ? ('0' + M) : M;
        MMM = (MMMM = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"][M - 1]).substring(0, 3);
        DD = (D = dateObject.getDate()) < 10 ? ('0' + D) : D;
        DDD = (DDDD = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"][dateObject.getDay()]).substring(0, 3);
        th = (D >= 10 && D <= 20) ? 'th' : ((dMod = D % 10) == 1) ? 'st' : (dMod == 2) ? 'nd' : (dMod == 3) ? 'rd' : 'th';
        formatString = formatString.replace("#YYYY#", YYYY).replace("#YY#", YY).replace("#MMMM#", MMMM).replace("#MMM#", MMM).replace("#MM#", MM).replace("#M#", M).replace("#DDDD#", DDDD).replace("#DDD#", DDD).replace("#DD#", DD).replace("#D#", D).replace("#th#", th);

        h = (hhh = dateObject.getHours());
        if (h == 0)
            h = 24;
        if (h > 12)
            h -= 12;
        hh = h < 10 ? ('0' + h) : h;
        AMPM = (ampm = hhh < 12 ? 'am' : 'pm').toUpperCase();
        mm = (m = dateObject.getMinutes()) < 10 ? ('0' + m) : m;
        ss = (s = dateObject.getSeconds()) < 10 ? ('0' + s) : s;
        return formatString.replace("#hhh#", hhh).replace("#hh#", hh).replace("#h#", h).replace("#mm#", mm).replace("#m#", m).replace("#ss#", ss).replace("#s#", s).replace("#ampm#", ampm).replace("#AMPM#", AMPM);
    } catch (err) {
        return err;
    }
};

/* MAY NOT WORK ON FF FOR LINUX AND MAC*/
function getScrollTop() {
    var r = [0, 0];

    if (typeof pageYOffset != 'undefined') {
        //most browsers except IE before #9
        r[1] = pageYOffset;
        r[0] = pageXOffset;
    } else {
        var B = document.body; //IE 'quirks'
        var D = document.documentElement; //IE with doctype
        D = (D.clientHeight) ? D : B;
        r[1] = D.scrollTop;
        r[0] = D.scrollLeft;
    }
    return r;
}

function isNumber(n) {
		return n === parseFloat(n);
	}
function isEven(n) {
		return isNumber(n) && (n % 2 == 0);
	}
