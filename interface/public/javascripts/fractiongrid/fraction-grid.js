/*Generates the fraction based grid*/


/*OPTIONS*/
var start = Date.now();
var columns = 48;
var lapMax = 1367; // laptop breakpoint
var handMax = 1367; // hand held breakpoint

var style = "div[class*=col-]{display:inline-block;vertical-align:top;float:left;box-sizing:border-box;-webkit-transition:width .25s ease-out;-moz-transition:width .25s ease-out;-ms-transition:width .25s ease-out;-o-transition:width .25s ease-out;transition:width .25s ease-out; min-height: 1px;}.row{clear:both;position:relative;display:block}.row.flex{display:flex;flex-wrap:wrap}.flex>div[class*=col-]{float:none}.row:after{visibility:hidden;display:block;font-size:0;content:'';clear:both;height:0}";
var desktop = "";
var lap = "@media(max-width:"+lapMax+"px){";
var hand = "@media (max-width:"+handMax+"px){";

/*Generate Columns*/
for(var a = 1; a < columns; a++){
	for(var b = 1; b < columns; b++){
		var percent = (a / b) * 100;
		desktop += ".col-"+a+"-"+b+"{width: "+percent+"%}";
		lap += ".col-"+a+"-"+b+"-lap{width: "+percent+"%}";
		hand += ".col-"+a+"-"+b+"-hand{width: "+percent+"%}";
	}
}
lap += "}";
hand += "}";

var styleElement = document.createElement("style");
styleElement.innerHTML = style+desktop+lap+hand;
document.head.appendChild(styleElement);
console.log("Fraction Grid generated in "+(Date.now() - start)+"ms")