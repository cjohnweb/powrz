// date stuff
$(function(){
    
    var offset = new Date().getTimezoneOffset() * 60000;

    var from = new Date(get_url_params()[2] * 1000 + offset);
    
    var to = new Date(get_url_params()[3] * 1000 + offset);


    $('.ig-date').datepicker({
        
        dateFormat: 'yy-mm-dd',

        onSelect: function(input, inst){

            var from = new Date($('.ig-date.from').val());

            var to = new Date($('.ig-date.to').val());

            if(inst.id == 'ig-date-to'){
            
                location.replace('/range/'+parseInt((from.getTime() / 1000))+'/'+parseInt((to.getTime() / 1000)));

            }
            
        }
        
    });
    
    $('#ig-date-from').datepicker("setDate", from);
    
    $('#ig-date-to').datepicker("setDate", to.format("yyyy-mm-dd"));
    
    
    
    
    
    $('.report-date').datepicker({

        dateFormat: 'yy-mm-dd',

        onSelect: function(input, inst){

            var from = new Date($('#from').val());

            var to = new Date($('#to').val());

            $('.download').html('<div class="download-report"><a href="/data/segment/'+parseInt((from.getTime() / 1000))+'/'+parseInt((to.getTime() / 1000))+'/dl/">Download Report</a></div>');
        
        }
        
    });
    
    
    
    
    
});



// return the unix timestamp of the current 15 minute segment we reside in at this moment
function current_hour_segment(){

    var now = new Date();

    var month = new Array();

    month[0] = "January";
    month[1] = "February";
    month[2] = "March";
    month[3] = "April";
    month[4] = "May";
    month[5] = "June";
    month[6] = "July";
    month[7] = "August";
    month[8] = "September";
    month[9] = "October";
    month[10] = "November";
    month[11] = "December";

    var month = month[now.getMonth()]; 

    var uts_base = Date.parse(month+' '+now.getDate()+', '+now.getFullYear()+' '+now.getHours()+':00:00');

    // our 15 minute segments for this our
    var points = [];
    points[0] = uts_base;
    points[1] = uts_base + 900000;
    points[2] = uts_base + 1800000;
    points[3] = uts_base + 2700000;

    for(var i=0; i < 3; i++){
        if(Date.parse(now) >= points[i] && Date.parse(now) < points[i+1]){
            return points[i];
        }else if(Date.parse(now) > points[3]){
            return points[3];
        }
    }

}

// break url into chunked array
function get_url_params(){
    return window.location.pathname.split('/');
}


// beautiful and cryptic
function days_in_month(y, m){
   return /8|3|5|10/.test(--m)?30:m==1?(!(y%4)&&y%100)||!(y%400)?29:28:31;
}





// realtime graph switching
$(document).ready(function(){

    $('.stat').on('click', 'a', function(){
        
        var graph = $(this).attr('data-graph');
        
        var hide = graph.substring(0, graph.length - 1);
        
        var prefix = graph.substr(0, graph.indexOf('-')); 
        
        var phase = graph.substring(graph.length -1);

        $('.'+prefix+'-label-a').hide();
        $('.'+prefix+'-label-b').hide();
        $('.'+prefix+'-label-c').hide();
        $('.'+prefix+'-label-d').hide();
        
        
        $('#'+hide+'a').hide();
        $('#'+hide+'b').hide();
        $('#'+hide+'c').hide();
        $('#'+hide+'d').hide();

        $('#'+graph).show();
        $('.'+prefix+'-label-'+phase).show();
        
        
        return false;

    });
    
    // show / hide settings page
    var url_params = get_url_params();


    if(url_params[4] == 'up'){
        $('.secret').show();
    }
    
    
    
    
    
    
    // demand stuff
    $(function(){
        $('.priority-box').change(function(){
            
            var val = $(this).val();
            var sel = $(this);
            
            if(val == ""){
                console.log('empty value');
                
                if(sel.data("selected")){

                    var oldval = sel.data("selected");              
                    sel
                    .removeData("selected")
                    .siblings('select')
                    .append($('<option/>').attr("value", oldval).text(oldval));
           
               }
               
            }else{   
                console.log('non empty value');
                
                if(sel.data("selected")){
                    
                    /*
                    var oldval = sel.data("selected");               
                    sel
                    .siblings('select')
                    .append($('<option/>').attr("value", oldval).text(oldval));
                    */
                   
                }else{
                    
                    console.log('not selected');
                    
                    // this is where other options are removed               
                    $('.priority-box').not($(this)).children('option[value=' + val + ']').remove();
                }

            }           
        });
    });


    // save
    $('.save-demand').on('click', function(){
        
        $.ajax({
            type: 'POST',
            url: "/demand",
            data: $("#demand_form").serialize(),
            
            success: function(data){
                alert('Demand Settings Saved');
            }
        });

        return false;
    });
    

});

