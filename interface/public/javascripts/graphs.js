/* 
 * JR Chew - jr.chew@jlmei.com
 * Graphing Interface Functionality
 * 
 * All graph related functionality for SGB-3P interface.
 * 
 */




function c2a(strData, strDelimiter){

    strDelimiter = (strDelimiter || ",");

    var objPattern = new RegExp(
        (
            // Delimiters.
            "(\\" + strDelimiter + "|\\r?\\n|\\r|^)" +

            // Quoted fields.
            "(?:\"([^\"]*(?:\"\"[^\"]*)*)\"|" +

            // Standard fields.
            "([^\"\\" + strDelimiter + "\\r\\n]*))"
        ),
        "gi"
        );

    var arrData = [[]];

    var arrMatches = null;

    while (arrMatches = objPattern.exec( strData )){

        var strMatchedDelimiter = arrMatches[ 1 ];

        if (
            strMatchedDelimiter.length &&
            strMatchedDelimiter !== strDelimiter
            ){

            arrData.push( [] );

        }

        var strMatchedValue;

        if (arrMatches[ 2 ]){

            strMatchedValue = arrMatches[ 2 ].replace(
                new RegExp( "\"\"", "g" ),
                "\""
                );

        } else {

            strMatchedValue = arrMatches[ 3 ];

        }

        arrData[ arrData.length - 1 ].push( strMatchedValue );
    }

    // Return the parsed data.
    return( arrData );
}


// find min and max range of data for sparkline charts below
function normalize(data){

    // usage highlights
    var range = {
        min: Math.floor(Math.min.apply(null, data)) -1,
        max: Math.ceil(Math.max.apply(null, data)) + 1
    }
    
    return range;
    
}




////////////////////////////////////////////////////////////////////////////////
// 1. DATA FORMATTING //////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

// extract data from real_time data to feed to bar_graph() in a format it understands;
function bar_graph_data(data, metric, phase){
    
    this.phase = phase;
    this.metric = metric;
    this.query = 'phase_'+phase+'_'+metric;
    
    // 60 segments of *metric_*phase
    var minute_data = [];
    
    for(i=0; i < 60; i++){
        if(typeof(data[i][query]) != 'undefined'){ // update - make sure we never recieve incongruent data and remove this if statement.
            minute_data.push(data[i][query]);
        }
    }
    
    return minute_data;
    
}


// extract data from real_time data to feed to bar_graph() in a format it understands;
function wind_graph_data(data){
    
    // 60 segments of *metric_*phase
    var minute_data = [];
    
    for(i=0; i < 60; i++){
        if(typeof(data[i]['anemometer']) !== 'undefined'){ // update - make sure we never recieve incongruent data and remove this if statement.
            minute_data.push(data[i]['anemometer']);
        }
    }
    
    return minute_data;
    
}


function all_phase_graph_data(data){
    
    //console.log(data);
    
    // 60 segments of *metric_*phase
    var minute_data = [];
    
    for(i=0; i < 60; i++){
        if(typeof(data[i]['power']) !== 'undefined'){ // update - make sure we never recieve incongruent data and remove this if statement.
            minute_data.push(data[i]['power']);
        }
    }
    
    return minute_data;
    
}


// extract data from real_time data to feed to segment_graph() in a format it understands;
function segment_graph_data(data){

    //var shedding_data = data[1].split(',').map(Number);
    
    var json = JSON.parse(data);
    
    var power = JSON.stringify(json[0]).substring(1, JSON.stringify(json[0]).length-1);
    var shedding = JSON.stringify(json[1]).substring(1, JSON.stringify(json[1]).length-1);

    var return_data = [
        json[0].map(Number),    // power
        parseInt(json[1].map(Number)) + parseInt(json[0].map(Number))	// shedding
    ];

    return return_data;   
   
}

// calculate dates based upon number of points provided and starting date
function segment_graph_dates(data){
    
    this.graph_data = data;
    
    // charts
    var dates = [];

    var url_params = get_url_params();

    //var now = new Date().getTime();
    var offset = new Date().getTimezoneOffset()*60000;

    // how many 15 minute segments between past and present?

    // handle ze dates
    var fromDate = new Date((url_params[2] * 1000) + offset);
    fromDate.setHours(0);
    fromDate.setMinutes(0);
    
    /*
    var toDate = new Date((url_params[3] * 1000) + offset);
    toDate.setHours(23);
    toDate.setMinutes(45); 
    */

    var from = fromDate.getTime();
    //var to = toDate.getTime();

    //var range = ((to - from) / 60000) / 15; // range in minutes
    



    for(var i=0; i < this.graph_data.length; i++){ 
        dates.push(from + (900000 * i));
    }

    return dates;

}

////////////////////////////////////////////////////////////////////////////////
// 2. RENDERING ////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

// display bar chart on interface
function bar_graph(data, metric, phase, color){
    
    var range = normalize(data);
    
    this.phase = phase;
    this.metric = metric;
    this.query = metric+'-phase-'+phase;
    
    Chart.defaults.global.responsive = true;
    
         
    // responsive stuff

    if($(window).width() <= 600){
        var barWidth = 3;
    }else if($(window).width() > 600 && $(window).width() <= 700){
        var barWidth = 5;
    }else if($(window).width() > 700 && $(window).width() <= 960){
        var barWidth = 7;
    }else if($(window).width() > 960 && $(window).width() <= 1350){
        var barWidth = 3;
    }else{
        var barWidth = 4;
    }
    
    
    
    $('#'+this.query).sparkline(data, {
        type: 'bar',
        height: '110',
        barWidth: barWidth,
        zeroAxis: false,
        barColor: color,
        chartRangeMin: range.min,
        chartRangeMax: range.max
    });
    
}

// display bar chart on interface
function wind_graph(data, color){


    Chart.defaults.global.responsive = true;
    
         
    // responsive stuff

    if($(window).width() <= 600){
        var barWidth = 3;
    }else if($(window).width() > 600 && $(window).width() <= 700){
        var barWidth = 5;
    }else if($(window).width() > 700 && $(window).width() <= 960){
        var barWidth = 7;
    }else if($(window).width() > 960 && $(window).width() <= 1350){
        var barWidth = 3;
    }else{
        var barWidth = 4;
    }
    
    
    $('#wind').sparkline(data, {
        type: 'bar',
        height: '110',
        barWidth: barWidth,
        zeroAxis: false,
        barColor: color,
        chartRangeMin: 0,
        chartRangeMax: 60
    });
    
}

// display bar chart on interface
function all_phase_graph(data, color){

    var range = normalize(data);

    Chart.defaults.global.responsive = true;

    // responsive stuff
    if($(window).width() <= 600){
        var barWidth = 3;
    }else if($(window).width() > 600 && $(window).width() <= 700){
        var barWidth = 5;
    }else if($(window).width() > 700 && $(window).width() <= 960){
        var barWidth = 7;
    }else if($(window).width() > 960 && $(window).width() <= 1350){
        var barWidth = 3;
    }else{
        var barWidth = 4;
    }
    
    
    $('#power-phase-d').sparkline(data, {
        type: 'bar',
        height: '110',
        barWidth: barWidth,
        zeroAxis: false,
        barColor: color,
        chartRangeMin: range.min,
        chartRangeMax: range.max
    });
    
}

// recieve 2 objects. data_points and dates 
function segment_graph(data, dates){
    
    var plot_data = [
        data[1],
        data[0]
    ];

    var dataGraph = new Triceraplots({
        crunch_file : '/javascripts/triceraplots/triceraplots-worker.js', // worker thread
        container : document.getElementById('segment-graph'), // html element 
        scrubber : true, // draw scrubber at bottom?
        data : plot_data, // Data to plot
        dates : dates, // UTC dates in MS integers
        autosort : false, // Stack charts based on avg value
        y_axis_align : 'right',
        label_format : function(val){
            if(val > 1000){
                    return (val / 1000).toFixed(3) + ' mW';
            }else{
                    return (val).toFixed(0) + ' kW';
            }
        },
    });

    $('.chart-wrap').css({'background-color':'rgba(235, 235, 235, 0.8)'});

    dataGraph.colors[0] = 'rgba(169,169,169,.8)';
    dataGraph.plot_styles[0] = 'fill';

    dataGraph.colors[1] = 'rgba(0, 150, 255, 0.9)';
    dataGraph.plot_styles[1] = 'fill';

    dataGraph.colors[2] = 'rgb(249, 161, 0)';
    dataGraph.plot_styles[2] = 'line';

    dataGraph.colors[3] = 'rgb(37, 194, 37)';
    dataGraph.plot_styles[3] = 'line';
    
}


function usage_highlights(data){

    var plot_data = c2a(JSON.parse(data)[0])[0];
    
    // usage highlights
    var low = Math.min.apply(null, plot_data);
    var peak = Math.max.apply(null, plot_data);
    
    var sum = 0;
    for(var i = 0; i < plot_data.length; i++){
        sum += parseInt(plot_data[i], 10);
    }

    var avg = sum/plot_data.length;

    $('.peak').html(peak.toFixed(2)+'kW');
    $('.avg').html(avg.toFixed(2)+'kW');
    $('.low').html(low.toFixed(2)+'kW');
    
}






////////////////////////////////////////////////////////////////////////////////
// MAIN ////////////////////////////////////////////////////////////////////////
// called from index.hjs(io) > data.js > graphs.js /////////////////////////////
////////////////////////////////////////////////////////////////////////////////

// turns data from data.js into graphs
function graphs(data, type){
    
    if(type == 'segment'){
        
        
        segment_graph(segment_graph_data(data), segment_graph_dates(data));
        usage_highlights(data);
        
    }else{
        
        // realtime data
        // update - options here should be set in config.js

        // power
        bar_graph(bar_graph_data(data, 'power', 'a'), 'power', 'a', 'rgba(0, 150, 255, 0.9)');
        bar_graph(bar_graph_data(data, 'power', 'b'), 'power', 'b', 'rgba(0, 150, 255, 0.9)');
        bar_graph(bar_graph_data(data, 'power', 'c'), 'power', 'c', 'rgba(0, 150, 255, 0.9)');

        // current
        bar_graph(bar_graph_data(data, 'current', 'a'), 'current', 'a', 'rgba(114, 114, 114, .9)');
        bar_graph(bar_graph_data(data, 'current', 'b'), 'current', 'b', 'rgba(114, 114, 114, .9)');
        bar_graph(bar_graph_data(data, 'current', 'c'), 'current', 'c', 'rgba(114, 114, 114, .9)');

        // voltage
        bar_graph(bar_graph_data(data, 'voltage', 'a'), 'voltage', 'a', '#25c225');
        bar_graph(bar_graph_data(data, 'voltage', 'b'), 'voltage', 'b', '#25c225');
        bar_graph(bar_graph_data(data, 'voltage', 'c'), 'voltage', 'c', '#25c225');

        // power factor
        bar_graph(bar_graph_data(data, 'power_factor', 'a'), 'power-factor', 'a', '#f9a100');
        bar_graph(bar_graph_data(data, 'power_factor', 'b'), 'power-factor', 'b', '#f9a100');
        bar_graph(bar_graph_data(data, 'power_factor', 'c'), 'power-factor', 'c', '#f9a100');
        
        // wind factor
        wind_graph(wind_graph_data(data), '#bcbcbc');
        all_phase_graph(all_phase_graph_data(data), 'rgba(0, 150, 255, 0.9)');
    }
    
}


