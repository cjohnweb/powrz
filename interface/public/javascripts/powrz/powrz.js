/* 
 * JR Chew - jr.chew@jlmei.com
 * 
 */

var powrz = powrz || {};

////////////////////////////////////////////////////////////////////////////////
//// COMMON ////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
powrz.config = {
	
	bar_graphs : {
		
		power : {
			phases: {
				a: true, 
				b: true, 
				c: true, 
				d: true
			},
			color: 'rgba(0, 150, 255, 0.9)',
			suffix: 'kW'
		},
		
		current : {
			phases: {
				a: true, 
				b: true, 
				c: true
			},
			color: 'rgba(114, 114, 114, .9)',
			suffix: 'A'
		},
		
		voltage : {
			phases: {
				a: true, 
				b: true, 
				c: true
			},
			color: '#25c225',
			suffix: 'V',
			data_key: ''
		},
		
		power_factor : {
			phases: {
				a: true, 
				b: true, 
				c: true
			},
			color: '#f9a100',
			suffix: '',
			data_key: ''
		},
		
		wind : {			
			phases: {
				a: true
			},
			color: '#bcbcbc',
			suffix: 'MPH',
			data_key: ''
		},
	}
	
}


powrz.common = {
// break url into chunked array
	
	// get url parameters
	get_url_params : function(){
		return window.location.pathname.split('/');
	},
	
	// csv to array
	c2a : function(strData, strDelimiter){

		strDelimiter = (strDelimiter || ",");

		var objPattern = new RegExp(
		    (
			// Delimiters.
			"(\\" + strDelimiter + "|\\r?\\n|\\r|^)" +

			// Quoted fields.
			"(?:\"([^\"]*(?:\"\"[^\"]*)*)\"|" +

			// Standard fields.
			"([^\"\\" + strDelimiter + "\\r\\n]*))"
		    ),
		    "gi"
		    );

		var arrData = [[]];

		var arrMatches = null;
	    
		if(objPattern.exec(strData)[0]){
	    
			while(arrMatches = objPattern.exec(strData)){

			    var strMatchedDelimiter = arrMatches[ 1 ];

			    if (
				strMatchedDelimiter.length &&
				strMatchedDelimiter !== strDelimiter
				){

				arrData.push( [] );

			    }

			    var strMatchedValue;

			    if (arrMatches[ 2 ]){

				strMatchedValue = arrMatches[ 2 ].replace(
				    new RegExp( "\"\"", "g" ),
				    "\""
				    );

			    } else {

				strMatchedValue = arrMatches[ 3 ];

			    }

			    arrData[ arrData.length - 1 ].push( strMatchedValue );
			}
			
		}
		
		// Return the parsed data.
		return(arrData);

	}, // c2a
	
	add_slashes : function(){
		return (str + '').replace(/[\\"']/g, '\\$&').replace(/\u0000/g, '\\0');	
	}
	
	
} // common



////////////////////////////////////////////////////////////////////////////////
//// DATA //////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
powrz.data = {
	
	bar_normalize : function(data){
		// do it
		var range = {
			min: Math.floor(Math.min.apply(null, data)) -1,
			max: Math.ceil(Math.max.apply(null, data)) + 1
		}
		
		return range;
		
	},
	
	bar_data : function(data, metric){
	
		var bar_data = [];
		
		$.each(data, function(point, point_data){
			bar_data.push(point_data[metric]);
		});
		
		return bar_data;
		
	},
	
	// dates for our segment graph
	segment_dates : function(data){

		var url_params = powrz.common.get_url_params();
		
		//var now = new Date().getTime();
		var offset = new Date().getTimezoneOffset()*60000;

		// how many 15 minute segments between past and present?

		// handle ze dates
		var fromDate = new Date((url_params[3] * 1000) + offset);
		
		fromDate.setHours(0);
		
		fromDate.setMinutes(0);

		var from = fromDate.getTime();

		var dates = [];
		
		for(var i=0; i < data.length; i++){
			
		    dates.push(from + (900000 * i));
		    
		}
		
		return dates;
	},
	
	// data for our segment graph
	segment_data : function(data){
		
		var power = new Array();
		
		var shedding = new Array();
		
		var out = new Array();
		
		for(i=0; i < data.length; i++){
		
			power.push(parseFloat(data[i].interval_data));
			
			shedding.push(parseFloat(data[i].interval_shed_data));
		
		}

		// shedding = power + shedding
		for(i=0; i < shedding.length; i++){
		
			if(shedding[i] > 0){
			
				shedding[i] = power[i] + shedding[i];
			
			}
		
		}
		
		out.push(shedding);
		
		out.push(power);
		
		return out;

	},
	
	highlight_data : function(data){
		
		var power = [];	
		
		for(i=0; i < data.length; i++){
			
			power[i] = parseFloat(data[i].interval_data).toString();
		
		}
		
		return power;
		
	},
	
	month_diff : function(d1, d2){

		var months;

		months = (d2.getFullYear() - d1.getFullYear()) * 12;

		months -= d1.getMonth() + 1;

		months += d2.getMonth();

		return months <= 0 ? 0 : months;

	}
	
}



////////////////////////////////////////////////////////////////////////////////
//// INTERFACE /////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
powrz.interface = {
	
	init : function(){
		//$('select').chosen({disable_search_threshold: 10});
	},
	
	stats : function(data){
		
		
		if(data[0] && data[0].phase_a_voltage){
			
			// power
			$('#power-section .phase-a').html('<span class="empty">&empty;A</span><a data-phase="a" href="#">'+Number(data[0].phase_a_power).toFixed(2)+' <sup>kW</sup><span class="phase-name">Phase A</span></a>');
			$('#power-section .phase-b').html('<span class="empty">&empty;B</span><a data-phase="b" href="#">'+Number(data[0].phase_b_power).toFixed(2)+' <sup>kW</sup><span class="phase-name">Phase B</span></a>');
			$('#power-section .phase-c').html('<span class="empty">&empty;C</span><a data-phase="c" href="#">'+Number(data[0].phase_c_power).toFixed(2)+' <sup>kW</sup><span class="phase-name">Phase C</span></a>');
			$('#power-section .phase-d').html('<span class="empty">POWER</span><a data-phase="d" href="#">'+Number(data[0].system_power).toFixed(2)+' <sup>kW</sup><span class="phase-name">All Phases</span></a>');


			// current
			$('#current-section .phase-a').html('<span class="empty">&empty;A</span><a data-phase="a" href="#">'+Number(data[0].phase_a_current).toFixed(2)+' <sup>A</sup><span class="phase-name">Phase A</span></a>');
			$('#current-section .phase-b').html('<span class="empty">&empty;B</span><a data-phase="b" href="#">'+Number(data[0].phase_b_current).toFixed(2)+' <sup>A</sup><span class="phase-name">Phase B</span></a>');
			$('#current-section .phase-c').html('<span class="empty">&empty;C</span><a data-phase="c" href="#">'+Number(data[0].phase_c_current).toFixed(2)+' <sup>A</sup><span class="phase-name">Phase C</span></a>');


			// voltage
			$('#voltage-section .phase-a').html('<span class="empty">&empty;A</span><a data-phase="a" href="#">'+Number(data[0].phase_a_voltage).toFixed(2)+' <sup>V</sup><span class="phase-name">Phase A</span></a>');
			$('#voltage-section .phase-b').html('<span class="empty">&empty;B</span><a data-phase="b" href="#">'+Number(data[0].phase_b_voltage).toFixed(2)+' <sup>V</sup><span class="phase-name">Phase B</span></a>');
			$('#voltage-section .phase-c').html('<span class="empty">&empty;C</span><a data-phase="c" href="#">'+Number(data[0].phase_c_voltage).toFixed(2)+' <sup>V</sup><span class="phase-name">Phase C</span></a>');


			// power factor
			$('#power-factor-section .phase-a').html('<span class="empty">&empty;A</span><a data-phase="a" href="#">'+Number(data[0].phase_a_power_factor).toFixed(2)+'<span class="phase-name">Phase A Leading</span></a>');
			$('#power-factor-section .phase-b').html('<span class="empty">&empty;B</span><a data-phase="b" href="#">'+Number(data[0].phase_b_power_factor).toFixed(2)+'<span class="phase-name">Phase B Leading</span></a>');
			$('#power-factor-section .phase-c').html('<span class="empty">&empty;C</span><a data-phase="c" href="#">'+Number(data[0].phase_c_power_factor).toFixed(2)+'<span class="phase-name">Phase C Leading</span></a>');

			if(data[0].wind_speed){
				// windspeed
				$('#wind-section .phase-a').html('<a data-phase="c" href="#">'+data[0].wind_speed.toFixed(2)+' <sup>MPH</sup></a>');
			}else{
				// hide windspeed if module doesn't return data
				$('#wind-section').hide();
			}
		}		

	}, // stats
	
	demand : function(data){
		
		if(data[0] && data[0]['relay_1']){
			
			// enforce an object
			if(typeof(data[0]['relay_1']) == 'object'){
				
				var relay = [
				    data[0]['relay_1'],
				    data[0]['relay_2'],
				    data[0]['relay_3'],
				    data[0]['relay_4']
				];
				
			}else{
				
				var relay = [
				    JSON.parse(data[0]['relay_1']),
				    JSON.parse(data[0]['relay_2']),
				    JSON.parse(data[0]['relay_3']),
				    JSON.parse(data[0]['relay_4'])
				];
				
			}
			


			if(relay[0]){

				for(i=0; i < 4; i++){

					$('.demand-peak').val(data[i].system_peak_power);

					$('.demand-'+i+'-name').val(relay[i].name);
					$('.demand-'+i+'-desc').val(relay[i].desc);
					$('.demand-'+i+'-power').val(relay[i].power);
					$('.demand-'+i+'-priority').val(relay[i].priority);
					$('.demand-'+i+'-delay').val(relay[i].delay);

					if(relay[i].enabled == true){
						$('.demand-'+i+'-enabled').val('true');
					}else{
						$('.demand-'+i+'-enabled').val('false');
					}

				}	

			}
			
		}

		// ajax form submit on button click
		$('.demand-tab .btn').on('click', function(){

			var form = $('.demand-tab form');

			$.ajax({
				type: 'POST',
				url: '/save',
				data: form.serialize(),
				success: function(data){
					alert('Save Complete');
				}
			});

			return false;

		});

	}, // demand
	
	demand_led : function(data){
		
		if(data[0] && data[0]['relay_1']){
			
			// enforce an object
			if(typeof(data[0]['relay_1']) == 'object'){
				
				var relay = [
				    data[0]['relay_1'],
				    data[0]['relay_2'],
				    data[0]['relay_3'],
				    data[0]['relay_4']
				];
				
			}else{
				
				var relay = [
				    JSON.parse(data[0]['relay_1']),
				    JSON.parse(data[0]['relay_2']),
				    JSON.parse(data[0]['relay_3']),
				    JSON.parse(data[0]['relay_4'])
				];
				
			}
			


			for(i=0; i < 4; i++){

				if(relay[i].currentState !== 'off' || relay[i].enabled == false){
					$('.demand-'+i+'-state p.circle').removeClass('circle-red');
					$('.demand-'+i+'-state p.circle').addClass('circle-green');
				}else{
					$('.demand-'+i+'-state p.circle').removeClass('circle-green');
					$('.demand-'+i+'-state p.circle').addClass('circle-red');	
				}

			}			
			
		}

	},
	
	settings : function(data){

		$('.rs485 input').val(data.rs485);
		
		$('.ct1 select').val('CT'+data.ct1);
		
		$('.voltage_config select').val(data.voltage_config);
		
		$('.current_config select').val(data.current_config);
		
		$('.cd1 select').val('CD1'+data.cd1);
		
		$('.cd2 select').val('CD2'+data.cd2);
		
		$('.cd3 select').val('CD3'+data.cd3);
	       
		$('.op2 select').val(data.op2);
		
		// ajax form submit on select change
		$('.settings-tab select').on('change', function(){
			
			var form = $(".settings-tab form");
			
			$.ajax({
				type: 'POST',
				url: '/save',
				data: form.serialize(),
				success: function(data){
					
				}
			});

		});
		
		// ajax form submit on button click
		$('.settings-tab .btn').on('click', function(){
			
			$.ajax({
				type: 'POST',
				url: '/save',
				data: {
					type: $(this).attr('data-call'), 
					device: powrz.common.get_url_params()[3]
				},
				success: function(data){
					
				}
			});
			
			return false;
			
		});
		
		// hide phase tabs that aren't enabled (2 phase vs 3 phase system)		
	},
	
	highlights : function(data){
		
		// usage highlights
		var eval = powrz.common.c2a(powrz.data.highlight_data(data))[0];
		
		var low = Math.min.apply(null, eval);
		var peak = Math.max.apply(null, eval);
		
		var sum = 0;
		for(var i = 0; i < eval.length; i++){
		    sum += parseInt(eval[i], 10);
		}
		
		var avg = sum/eval.length;
		
		$('.stats-container .peak span').html(peak.toFixed(2)+' kW');
		$('.stats-container .avg span').html(avg.toFixed(2)+' kW');
		$('.stats-container .low span').html(low.toFixed(2)+' kW');
		
	},
	
	// calendar interface functions
	calendars : function(){
		
		var offset = new Date().getTimezoneOffset() * 60000;

		var from = new Date(powrz.common.get_url_params()[3] * 1000 + offset);

		var to = new Date(powrz.common.get_url_params()[4] * 1000 + offset);
		
		$('#datepicker1').datepicker({
			
			dateFormat: 'yy-mm-dd'
			
		});
			
		$('#datepicker2').datepicker({

			dateFormat: 'yy-mm-dd',

			onSelect: function(input, inst){
				
				var from = new Date($('#datepicker1').val());

				var to = new Date($('#datepicker2').val());

				if(inst.id == 'datepicker2'){

					location.replace('/device/'+powrz.common.get_url_params()[2]+'/'+parseInt((from.getTime() / 1000))+'/'+parseInt((to.getTime() / 1000)));

				}

			}

		});

		$('#datepicker1').datepicker("setDate", from.format('yyyy-mm-dd'));

		$('#datepicker2').datepicker("setDate", to.format("yyyy-mm-dd"));

		$('#datepicker3').datepicker("setDate", from.format("yyyy-mm-dd"));
    
		$('#datepicker4').datepicker("setDate", to.format("yyyy-mm-dd"));
    
		$('#datepicker3').datepicker({
			
			dateFormat: 'yy-mm-dd',

			onSelect: function(input, inst){
				
				var from = new Date($('#datepicker3').val());

				var to = new Date($('#datepicker4').val());
				
				to.setHours(24);
				
				var from_ts = parseInt(from.getTime()) + parseInt(offset);
				
				var to_ts = parseInt(to.getTime()) + parseInt(offset);
				
				$('.reports-tab .download').html('<a href="/segment/' + powrz.common.get_url_params()[2] + '/' + from_ts / 1000 + '/' + to_ts / 1000 + '/dl" class="btn">Download Report</a>');
				
			}
			
		});
		
		
		
		$('#datepicker4').datepicker({
			
			dateFormat: 'yy-mm-dd',

			onSelect: function(input, inst){
				
				var from = new Date($('#datepicker3').val());

				var to = new Date($('#datepicker4').val());
				
				to.setHours(24);
				
				var from_ts = parseInt(from.getTime()) + parseInt(offset);
				
				var to_ts = parseInt(to.getTime()) + parseInt(offset);
				
				$('.reports-tab .download').html('<a href="/segment/' + powrz.common.get_url_params()[2] + '/' + from_ts / 1000 + '/' + to_ts / 1000 + '/dl" class="btn">Download Report</a>');
				
			}
			
		});
		
		
    
	},
	
	phase_tabs : function(){
		
		// hide upon init
		$('.section-value-box.phase-b').hide();
		
		$('.section-value-box.phase-c').hide();
		
		$('.section-value-box.phase-d').hide();
		
		$('.section-description .phase').on('click', function(){
			
			var section = $(this).closest('.section');
			
			var phase = $(this).find('a').attr('data-phase');
		
			$(section).find('.section-value-box').hide();
			
			$(section).find('.section-value-box.phase-'+phase).show();
			
			$(section).find('.section-graph-area').hide();
			
			$(section).find('.section-graph-area.phase_'+phase).show();
			
			return false;
			
		});
		
	},
	
	
	reports : function(data){ // improve this logic by moving it into the route

		// NOTE: report generation calendars / button functionality is in powrz.interface.calendars
		
		if(data[0]){

			// use this to reference the number value that javascript returns with getMonth
			var month = new Array();
			month[0] = "January";
			month[1] = "February";
			month[2] = "March";
			month[3] = "April";
			month[4] = "May";
			month[5] = "June";
			month[6] = "July";
			month[7] = "August";
			month[8] = "September";
			month[9] = "October";
			month[10] = "November";
			month[11] = "December";

			var commission_year = data[0].created.substr(0, 4);

			var commission_month = data[0].created.substr(5, 2)-1;

			//var start = new Date('July 01, 2011 00:00:00'); // feed commission date

			var start = new Date(month[commission_month]+' 01, ' + commission_year + ' 00:00:00');

			var end = new Date();

			end.setDate(01);

			var months = powrz.data.month_diff(start, end);

			for(i=0; i <= months+1; i++){

				var current_date = new Date(new Date(start).setMonth(start.getMonth()+i)); 

				var end_of_month = new Date(current_date.getFullYear(), current_date.getMonth() + 1, 0);
				
				console.log(current_date);
				
				var structure = '<div class="table-row" style="display: flex;">';

				structure += '<div style="width: 80%">';

				structure += '<a href="/segment/' + powrz.common.get_url_params()[2] + '/' + current_date.getTime() / 1000 + '/' + end_of_month.getTime() / 1000 + '/dl">' + month[current_date.getMonth()] + ' '+ current_date.getFullYear() +'</a>';

				structure += '</div>';

				structure += '<div style="width: 20%"><a href="/segment/'+powrz.common.get_url_params()[2]+'/' + current_date.getTime() / 1000 + '/' + end_of_month.getTime() / 1000 + '/dl">CSV</a></div></div>';

				$('.reports-tab .data-table').append(structure);

			}
		}
	}
	
} // interface



////////////////////////////////////////////////////////////////////////////////
//// GRAPHS ////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
powrz.graph = {
	
	line : function(data){
		
		//data.reverse();

		$("#sparkline").sparkline([5,6,7,9,9,5,3,2,2,4,6,7], {
			type: 'line',
			fillColor: '#ffffff'
		});
		
		
		
	},
	
	bar : function(data){
		
		//data.reverse();
		
		// which graphs shall we load
		$.each(powrz.config.bar_graphs, function(index, graph){
		
			$.each(graph, function(key, value){
				
				$.each(graph['phases'], function(k, v){
					
					var metric = 'phase_'+k+'_'+index;
					
					
					// friday hack: make all graphs show up NOW!
					if(metric == 'phase_d_power'){
						metric = 'system_power';
					}
					
					if(metric == 'phase_a_wind'){
						metric = 'wind_speed';
					}
					
					
					var range = powrz.data.bar_normalize(powrz.data.bar_data(data, metric));
					
					$('.'+index+'_graph_'+k).sparkline(powrz.data.bar_data(data, metric), {
						type: 'bar',
						height: '110',
						barWidth: 4,
						zeroAxis: false,
						barColor: graph['color'],
						chartRangeMin: range.min,
						chartRangeMax: range.max
					});

				});

			});
		});
	},
		
	segment : function(data){
		
		var dataGraph = new Triceraplots({
			crunch_file : '/javascripts/triceraplots/triceraplots-worker.js',
			container : document.getElementById('segment-graph'), // html element 
			scrubber : true,
			data : powrz.data.segment_data(data),
			dates : powrz.data.segment_dates(data), // UTC dates in MS integers
			autosort : false,
			y_axis_align : 'right',
			label_format : function(val){
				if(val > 1000){
					return (val / 1000).toFixed(3) + ' mW';
				}else{
					return (val).toFixed(0) + ' kW';
				}
			},
		});

		$('.chart-wrap').css({'background-color':'rgba(235, 235, 235, 0.8)'});

		dataGraph.colors[0] = 'rgba(169,169,169,.8)';
		dataGraph.plot_styles[0] = 'fill';

		dataGraph.colors[1] = 'rgba(0, 150, 255, 0.9)';
		dataGraph.plot_styles[1] = 'fill';

		dataGraph.colors[2] = 'rgb(249, 161, 0)';
		dataGraph.plot_styles[2] = 'line';

		dataGraph.colors[3] = 'rgb(37, 194, 37)';
		dataGraph.plot_styles[3] = 'line';
		
	} // segment graph

} // graphs



$(document).ready(function(){

	powrz.interface.init();	// call all interface elements
	powrz.interface.calendars(); // calendar functionality
	powrz.interface.phase_tabs(); // calendar functionality
	
	// move this stuff to powrz.elements.init
	var url_params = powrz.common.get_url_params();
	
	// realtime data request
	setInterval(function(){
		
		$.ajax({
			type: 'GET',
			url: '/realtime/'+url_params[2],
			success: function(data){
				powrz.interface.stats(data);
				powrz.interface.demand_led(data);
				
				if(window.initial_load != 1){
					powrz.interface.demand(data);	
					window.initial_load = 1;
				}
				
				powrz.graph.bar(data);
			}
		});
		
	}, 1500);
	

	// segment data request
	$.ajax({
		type: 'GET',
		url: '/segment/'+url_params[2]+'/'+url_params[3]+'/'+url_params[4]+'',
		success: function(data){			
			powrz.graph.segment(data);
			powrz.interface.highlights(data);
			
		}
	});
	
	
	// settings request
	$.ajax({
		type: 'GET',
		url: '/settings/'+url_params[3],
		success: function(data){
			powrz.interface.settings(data);
		}
	});
	

	$.ajax({
		type: 'GET',
		url: '/commission_date/'+url_params[3],
		success: function(data){
			powrz.interface.reports(data);
		}
	});
	
});