/* 
 * JR Chew - jr.chew@jlmei.com
 * Data Interface Functionality
 * 
 * Data is piped in here from socket or ajax. Depending on the source (local or 
 * Measurz) it is converted to a common format and piped to stats.js and graphs.js to be displayed.
 */


// convert data to SGB-3P Common Format
// @data = data
// @type = segment / real_time
// @source = local / measurz
function common_format(data, type, source){
    
    if(type == 'segment'){
        // segment data
   
        if(source == 'measurz'){
            // take measurz data and convert it to SGB-3P Common Format
            
            return data;

        }else{
            
            //source == 'local'
            // the locally pulled data is already in our native format so we're good

            return data.response;

        }
    
    }else{ // real time data

        if(source == 'measurz'){

            // take measurz data and convert it to SGB-3P Common Format
            var points = {};
            
            var i = 0;
            
            // loop thru until we get to the finer things in life
            $.each(data, function(key, value){
                
                // actual point data
                $.each(data[key], function(point, point_data){  
                    
                    //console.log(point_data.phase_a_power);
                    
                    points[i] = {
                        'unixtime'              : '',
                        'frequency'             : '',
                        'power'                 : '',
                        'phase_a_power'         : point_data.phase_a_power,
                        'phase_b_power'         : point_data.phase_b_power,
                        'phase_c_power'         : point_data.phase_c_power,
                        'average_current'       : '',
                        'phase_a_current'       : point_data.phase_a_current,
                        'phase_b_current'       : point_data.phase_b_current,
                        'phase_c_current'       : point_data.phase_c_current,
                        'average_voltage'       : '',
                        'phase_a_voltage'       : point_data.phase_a_voltage,
                        'phase_b_voltage'       : point_data.phase_b_voltage,
                        'phase_c_voltage'       : point_data.phase_c_voltage,
                        'power_factor'          : '',
                        'phase_a_power_factor'  : point_data.phase_a_power_factor,
                        'phase_b_power_factor'  : point_data.phase_b_power_factor,
                        'phase_c_power_factor'  : point_data.phase_c_power_factor,
                        'phase_a_cd'            : '',
                        'phase_b_cd'            : '',
                        'phase_c_cd'            : '',
                        'unknown'               : ''
                    }
                    
                    i++;
                    
                });
                
            });
            
            //console.log(points);
            
            
            return points;

        }else{
            
            //source == 'local'
            // the locally pulled data is already in our native format so we're good
            return JSON.parse(data.response);

        }
    }
}


function segment(data){
    
    graphs(common_format(data, 'segment', 'local'), 'segment');
    
}

function real_time(data, type){
    
    if(type == 'measurz'){
        stats(common_format(data, 'real_time', 'measurz'), 'real_time');
        graphs(common_format(data, 'real_time', 'measurz'), 'real_time');
    }else{
        stats(common_format(data, 'real_time', 'local'), 'real_time');
        graphs(common_format(data, 'real_time', 'local'), 'real_time');
    }
    
}

// populate demand tab with current settings
function demand(data){
    
    var demand_data = JSON.parse(data.response);

    // sort thru priority object and build a list an ordered array of relays
    // SHOULD BE SORTED BY RELAY # on J's end.
    
    var priorities = [
        demand_data['priority_1'],
        demand_data['priority_2'],
        demand_data['priority_3'],
        demand_data['priority_4'],
    ];
    
    var relay = [];
    
    //console.log(priorities);
    
    for(i=0; i < priorities.length; i++){
  
        if(priorities[i].relay == 1){
            relay[0] = priorities[i];
            relay[0]['priority'] = i+1;
        }

        if(priorities[i].relay == 2){
            relay[1] = priorities[i];
            relay[1]['priority'] = i+1;
        }

        if(priorities[i].relay == 3){
            relay[2] = priorities[i];
            relay[2]['priority'] = i+1;
        }

        if(priorities[i].relay == 4){
            relay[3] = priorities[i];
            relay[3]['priority'] = i+1;
        }

    }
    
    $('.demand-system-peak-power').val(demand_data.system_peak_power);
    
    $('.demand-0-name').val(relay[0].name);
    $('.demand-0-desc').val(relay[0].desc);
    $('.demand-0-power').val(relay[0].power);
    $('.demand-0-priority').val(relay[0].priority);
    $('.demand-0-enabled').val(relay[0].enabled);

    $('.demand-1-name').val(relay[1].name);
    $('.demand-1-desc').val(relay[1].desc);
    $('.demand-1-power').val(relay[1].power);
    $('.demand-1-priority').val(relay[1].priority);
    $('.demand-1-enabled').val(relay[1].enabled);
    
    $('.demand-2-name').val(relay[2].name);
    $('.demand-2-desc').val(relay[2].desc);
    $('.demand-2-power').val(relay[2].power);
    $('.demand-2-priority').val(relay[2].priority);
    $('.demand-2-enabled').val(relay[2].enabled);
    
    $('.demand-3-name').val(relay[3].name);
    $('.demand-3-desc').val(relay[3].desc);
    $('.demand-3-power').val(relay[3].power);
    $('.demand-3-priority').val(relay[3].priority);
    $('.demand-3-enabled').val(relay[3].enabled);
    
    
    
}