// saving ze settings
$(document).ready(function(){

    function send_command(cmd){
        $.ajax({
            type: 'POST',
            url: '/settings',
            data: { 
                'cmd': cmd, 
            },
            success: function(data){

            }
        });

    }
    
    function cd(phase, direction){
        return 'CD'+phase+direction;    
    }
    
    // ct-scale 1
    $('.ct-scale').on('click', function(){
        var scale = $(this).attr('data-scale');
        send_command('CT'+scale);
        return false;
    });

    // cd +
    $('.cd-positive').on('click', function(){
        var phase = $(this).attr('data-phase');
        switch(phase){
            case 'a':
                send_command(cd(1, '+'));
            break;
            case 'b':
                send_command(cd(2, '+'));
            break;
            case 'c':
                send_command(cd(3, '+'));
            break;
            default:
                exit();
        } 
       return false;
    });
    
    // cd -
    $('.cd-negative').on('click', function(){
       var phase = $(this).attr('data-phase');
        switch(phase){
            case 'a':
                send_command(cd(1, '-'));
            break;
            case 'b':
                send_command(cd(2, '-'));
            break;
            case 'c':
                send_command(cd(3, '-'));
            break;
            default:
                exit();
        } 
        return false;
    });
    
    // GPRE
    $('.gpre').on('click', function(){
        send_command('GPRE');
        return false;
    });
    
    // PRE
    $('.pre').on('click', function(){
        send_command('PRE');
        return false;
    });
    
    // SEC
    $('.sec').on('click', function(){
        send_command('SEC');
        return false;
    });
    
    
    
    // energy-clear
    $('.e-clear').on('click', function(){
        send_command('energy_clear');
        return false;
    });
    
    // demand-clear
    $('.d-clear').on('click', function(){
        send_command('demand_clear');
        return false;
    });
    
    // runtime-clear
    $('.r-clear').on('click', function(){
        send_command('runtime_clear');
        return false;
    });

    // imp-clear
    $('.i-clear').on('click', function(){
        send_command('imp_clear');
        return false;
    });
    
    
    
    

});
