
var Dashboard = function(options){
	var _this = this;
} 


/*
	Going the jquery DnD route again.
	I don't want to write another DnD lib
*/
$(document).ready(function() {

	/*
		The list items represent installations and installation nodes
	*/
	$("#list li").draggable({
		helper : function(event,ui){
			var helper = $(this).clone();
			console.log($(this))
			helper[0].style.width = $(this).context.getBoundingClientRect().width+"px";
			helper[0].style.height = $(this).context.getBoundingClientRect().height+"px";
			return helper;
		}
	});

	/*
		the dashboard's immediate children are portlets.
		These will give an immediate view into an installation or an installation node
		Their size and sort order can be changed with the mouse
	*/
	$( "#dashboard" ).sortable({
		delay: 150,
		helper: "clone"
	});
	$( "#dashboard" ).droppable({
		accept: "li",
		drop : function(event,ui){

			// Container column
			var portlet = ele('div.col-1-4.portlet');

			// portlet guts
			var panel =	ele({
				'div.marg.panel' : [
					'div.visualization',
					{
						'div.marg.row.flex':
						[
							{'div.col-auto': ['h3.marg-s:Status','p:Charging']},
							{'div.col-auto': ['h3.marg-s:Charge','p:90%']},
							{'div.col-auto': ['h3.marg-s:Savings','p:$100.00']},
						]
					},
					{'div.window_options.marg.pad-n':['div.button.green.marg-e.resize','div.button.red.marg-e.close']}
				]
			});

			portlet.appendChild(panel)

			$(portlet).on('click', '.resize', function(){
				if($(portlet).hasClass('col-1-4')){
					$(portlet).removeClass('col-1-4').addClass('col-2-4')
					return
				}
				if($(portlet).hasClass('col-2-4')){
					$(portlet).removeClass('col-2-4').addClass('col-3-4')
					return
				}
				if($(portlet).hasClass('col-3-4')){
					$(portlet).removeClass('col-3-4').addClass('col-4-4')
					return
				}
				if($(portlet).hasClass('col-4-4')){
					$(portlet).removeClass('col-4-4').addClass('col-1-4')
				}
			})

			$(portlet).on('click', '.close', function(){
				$(portlet).remove();
			})
			
			console.dir(portlet)
			$(this).append(portlet)
		}
	});
})