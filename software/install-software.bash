#!/bin/bash

#== - - -                                                              - - - ==#
#== - - -                      Manual install                          - - - ==#
#== - - -                                                              - - - ==#

# Install Adafruit Beagle Bone IO Python from GIT
cd /root/powrz/software
echo -en 'Install.bash: Installing Adafruit Beaglebone IO Python from GIT\n'
#git clone git://github.com/adafruit/adafruit-beaglebone-io-python.git
cd adafruit-beaglebone-io-python
python setup.py install

# Install Python-Redis Client
echo -en 'Install.bash: Installing Python-Redis Client from GIT\n'
cd /root/powrz/software
#git clone git://github.com/andymccurdy/redis-py.git
cd redis-py
python setup.py install

# Install Anemometer
echo -en "Install.bash: Installing Anemometer"
cd /root/powrz/software
#git clone https://jlm_hardware@bitbucket.org/jlm_hardware/anemometer.git
# pwd herlongCA
cd anemometer
bash install.bash

# Install Forever
echo -en "Install.bash: Installing Forever"
cd /root/powrz/software
npm -g install forever

