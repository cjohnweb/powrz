#!/bin/bash
# This one
# Last Modified: 5/11/2015 4:00:00 PM

echo -en 'Please enter a hostname within 30 seconds: '
read -r -t 30 hostname
if [ "$hostname" == "" ]
then
	hostname='sgb3p00000000'
fi

hostname $hostname
echo "$hostname" > /etc/hostname

cd /root
echo -en "Do you want to pull from git at this time? [y/n] If you have already pulled from git then answer no this question.\n"
read -r quest
if [ "$quest" == "y" ]
then
	git clone https://porwz.update:Topsy23Kretts@heartbeat.measurz.net:5555/powrz/powrz-0.0.0.git
	mv powrz-0.0.0/ powrz/
fi


echo -en 'Install.bash: SSH Banner\n'
echo -en "\nSGB3P ${version}\n\nJLM Energy, Inc.\n(916) 304-1603\n4401 Granite Drive\nRocklin, Ca 95677\n\nFor support, please call us at (916) 304-1603 or visit us online at http://www.jlmenergyinc.com/\n\n" > /etc/motd
echo -en "\nSGB3P ${version}\n\nJLM Energy, Inc.\n(916) 304-1603\n4401 Granite Drive\nRocklin, Ca 95677\n\nFor support, please call us at (916) 304-1603 or visit us online at http://www.jlmenergyinc.com/\n\n" > /etc/issue.net

# Fix led_aging.sh
echo -en 'Install.bash: Fixing LED AGING\n'
echo -en "#\!/bin/sh\n### BEGIN INIT INFO\n# Provides:          led_aging.sh\n# Required-Start:    \$local_fs\n# Required-Stop:     \$local_fs\n# Default-Start:     2 3 4 5\n# Default-Stop:      0 1 6\n# Short-Description: Start LED aging\n# Description:       Starts LED aging (whatever that is)\n### END INIT INFO\n\nx=\$(/bin/ps -ef | /bin/grep \"[l]ed_acc\")\nif [ \! -n \"\$x\" -a -x /usr/bin/led_acc ]; then\n    /usr/bin/led_acc &\nfi" > /etc/init.d/led_aging.sh

# Networking configuration / DCHP
echo -en 'Install.bash: Add DHCP setting for eth0 and restarting Networking\n'
echo -en '\niface eth0 inet dhcp\n' >> /etc/network/interfaces
/etc/init.d/networking restart

# Enable I2C and UART
echo -en 'Install.bash: Enable I2C1 & UART1, Disable HDMI, SystemD, and other non-essential services\n'
echo -en "\n\n# Enable I2C & UART on the BBB (You can enable UART1-5, I2C1-3, tetc).\ncapemgr.enable_partno=BB-UART1,BB-I2C1\n\n" >> /boot/uboot/uEnv.txt

# Cronjobs
crontab /root/powrz/scripts/cronjobs

cd /root
git clone https://heartbeat.measurz.net:5555/john.minton/Heartbeat.git
mv Heartbeat/ heartbeat
bash /root/heartbeat/install-heartbeat.bash
python /root/heartbeat/heartbeat.py "Heartbeat Installed!"
echo "#!/bin/sh\n\nFLAGFILE=/var/run/report-boot-via-heartbeat\n\ncase \"$IFACE\" in\n    lo)\n        # The loopback interface does not count.\n        # only run when some other interface comes up\n        exit 0\n        ;;\n\n    usb0)\n        exit 0\n        ;;\n    *)\n        ;;\nesac\n\nif [ -e $FLAGFILE ]; then\n    exit 0\nelse\n    touch $FLAGFILE\nfi\npython /root/heartbeat/heartbeat.py \"Powered On\"" > /etc/network/if-up.d/report-boot.bash
chmod +x /etc/network/if-up.d/report-boot.bash

echo -en 'Install.bash: Removing Apache\n'
apt-get purge apache2 apache2-utils apache2.2-bin apache2-common lightdm xrdp -y
rm -rf /etc/apache2 -y 

# Tweaks, after install
# Remove Redis
service redis_6379 stop
rm /usr/local/bin/redis-*
rm -r /etc/redis/
rm /var/log/redis_*
rm -r /var/lib/redis/
rm /etc/init.d/redis_*
rm /var/run/redis_*

echo -en 'Install.bash: Disable everything else we can think of that we do not need\n'
systemctl disable cloud9.service
systemctl disable gateone.service
systemctl disable bonescript-autorun.service
systemctl disable bonescript.service
systemctl disable bonescript.socket
systemctl disable gdm.service
systemctl disable mpd.service
systemctl disable avahi-daemon.service
systemctl disable avahi-daemon.service
apt-get remove lightdm -y

# Base Upgrades and Systems
echo -en 'Install.bash: Update\n'
apt-get update -y

echo -en 'Install.bash: Upgrade\n'
apt-get upgrade -y

echo -en 'Install.bash: Dist-Upgrade\n'
apt-get dist-upgrade -y 

echo -en "Install.bash: Install Everything else"
apt-get install expect dnsutils ntp mlocate build-essential python-pip python-smbus python-dev i2c-tools git tcl8.5 redis-server -y


#== - - -                                                              - - - ==#
#== - - -                        NTP & RTC                             - - - ==#
#== - - -                                                              - - - ==#

# Set US / Pacific Timezone
# remove current timezone file
rm /etc/localtime

# Make symlink so /etc/localtime points to $timezone file
ln -s /usr/share/zoneinfo/US/Pacific /etc/localtime

# Sync system to time servers
echo -en "Retrieving time from internet source..."
ntpdate -b -s -u pool.ntp.org

# Register ds1307 device at address 0x68 onto I2C1  
echo ds1307 0x68 > /sys/class/i2c-adapter/i2c-1/new_device

# Read the time from the RTC
hwclock -r -f /dev/rtc1

# Make this directory
mkdir /usr/share/rtc_ds1307

# Make this bash file, used to initialize the RTC
echo -en "#!/bin/bash
sleep 5
echo ds1307 0x68 > /sys/class/i2c-adapter/i2c-1/new_device
hwclock -s -f /dev/rtc1
hwclock -w" > /usr/share/rtc_ds1307/clock_init.bash

# Make this file, so RTC can be a service
echo -en "[Unit]
Description=DS1307 RTC Service

[Service]
Type=simple
WorkingDirectory=/usr/share/rtc_ds1307
ExecStart=/bin/bash clock_init.bash
SyslogIdentifier=rtc_ds1307

[Install]
WantedBy=multi-user.target" > /lib/systemd/system/rtc-ds1307.service

# Enable the service!
systemctl enable rtc-ds1307.service

apt-get autoremove -y

# Install everything else!
bash /root/powrz/init.bash

reboot
