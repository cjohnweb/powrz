#!/bin/bash

if [ ! -f /root/heartbeat ]; then                     
        cd /root
        git clone https://powrz.update:Topsy23Kretts@heartbeat.measurz.net:5555/john.minton/Heartbeat.git
        mv Heartbeat/ heartbeat/
        cd heartbeat
        bash /root/heartbeat/install-heartbeat.bash
fi

# Report Powered On when eth0 is up
echo "#!/bin/sh\n\nFLAGFILE=/var/run/report-boot-via-heartbeat\n\ncase \"$IFACE\" in\n    lo)\n        # The loopback interface does not count.\n        # only run when some other interface comes up\n        exit 0\n        ;;\n\n    usb0)\n        exit 0\n        ;;\n    *)\n        ;;\nesac\n\nif [ -e $FLAGFILE ]; then\n    exit 0\nelse\n    touch $FLAGFILE\nfi\npython /root/heartbeat/heartbeat.py \"Powered On\"" > /etc/network/if-up.d/report-boot.bash
chmod +x /etc/network/if-up.d/report-boot.bash

echo "Stopping Python"
sleep 1
cd /root/powrz/backend/
kill `pidof python`

echo "Stopping Node"
/usr/local/bin/forever stopall

echo "...sleeping for 5..."
sleep 5

# Install New Windspeed
echo -en "Install.bash: Installing Windspeed"

# Stop the service so the new update can overwrite the files
systemctl stop wind.service

cd /root/powrz/backend/
FILE="windspeed_v1.0.1.bin"
chmod 775 $FILE
chmod +x $FILE 
./$FILE 

# Start the new wind.service
systemctl start wind.service

echo "ctrl+c here for windspeed testing...."
sleep 5

# Install Adafruit Beagle Bone IO Python from GIT
cd /root/powrz/software
echo -en 'Install.bash: Installing Adafruit Beaglebone IO Python from GIT\n'
#git clone git://github.com/adafruit/adafruit-beaglebone-io-python.git
cd adafruit-beaglebone-io-python
python setup.py install		# If successfully installed, we add the key to the log otherwise we move to the next item to install

# Install Python-Redis Client
echo -en 'Install.bash: Installing Python-Redis Client from GIT\n'
cd /root/powrz/software
#git clone git://github.com/andymccurdy/redis-py.git
cd redis-py
python setup.py install

# Install Forever
echo -en "Install.bash: Installing Forever"
cd /root/powrz/software
npm -g install forever

# Set Cronjobs - Friday June 5th 2015
echo /root/powrz/scripts/
crontab /root/powrz/scripts/cronjobs

# So we can use dig to get the WAN IP as part of our get_network_ip script
apt-get install dnsutils -y

cd /root

echo "Starting Node"
sleep 1
nohup /usr/local/bin/forever start -c /usr/bin/node /root/powrz/interface/app.js >/dev/null 2>&1 &

echo "Starting Python"
sleep 10
cd /root/powrz/backend/
nohup python /root/powrz/backend/main.py >/dev/null 2>&1 & 

# Root
cd /root


DATE=$(date)
python /root/heartbeat/heartbeat.py "Run: /root/powrz/init.bash"

echo "Done...Reboot the system for all changes to take effect"
#shutdown -r now
